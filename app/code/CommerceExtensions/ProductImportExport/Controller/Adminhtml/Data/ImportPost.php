<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data;

use Magento\Framework\Controller\ResultFactory;

class ImportPost extends \CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data
{

    /**
     * import action from import/export data
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
		$messagetoreturn=array();
		$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
		if ($this->getRequest()->isPost()){
			try {
				$params = $this->getRequest()->getParams();
				$importHandler = $this->_objectManager->create('CommerceExtensions\ProductImportExport\Model\Data\CsvImportHandler');
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
				$data = $resource->getTableName('productimportexport_uploadedfiledata') ;
				$rec = $resource->getConnection()->query("SELECT * FROM ".$data." WHERE file_name='".$params['selectfilename']."'");
				$request_list = $rec->fetchAll(); 
				$data_one=array();
				$data_one=array_filter($request_list);
				if(isset($data_one[0])) {
					$filepath = $data_one[0]['file_uploaded_path'].'/'.$data_one[0]['file_name'];
					#$filepath = 'ProductImportExport/'.$params['selectfilename'];
					#$success = $this->messageManager->addSuccess(__('The Products have been imported Successfully.'));
					$memoryused = filesize($filepath);	
					session_write_close(); // so ajax keeps running
					flush();// so ajax keeps running
					$dataReturned = $importHandler->readCsvFile($filepath, $params);	
					
					if($dataReturned['import_status'] == "finished"){
						$this->reindexdata();
						$messagetoreturn['success'] = "The Products have been imported Successfully.";
						$messagetoreturn['total_rows'] = $dataReturned['total_rows'];
						$messagetoreturn['totalwarnings'] = $dataReturned['warnings'];
						$messagetoreturn['memory_used'] = $memoryused;
					} else {
						$messagetoreturn['error'] = "The Products were NOT imported.";
						$messagetoreturn['total_rows'] = $dataReturned['total_rows'];
						$messagetoreturn['totalwarnings'] = $dataReturned['warnings'];
						$messagetoreturn['memory_used'] = $memoryused;
					}
				} else {
					$messagetoreturn['error'] = "Invalid POST attempt file not found";
				}
				
			}
			catch (\Magento\Framework\Exception\LocalizedException $e) {
				$messagetoreturn['error'] = $e->getMessage();
			} 
			catch (\Exception $e) {
				$messagetoreturn['error'] = "Invalid file upload attempt: ". $e->getMessage();
			}
		} else {
			$messagetoreturn['error'] = "Invalid POST attempt file not found";
		}
		
		$resultJson->setData($messagetoreturn);
		return $resultJson;
				
    }
	public function reindexdata(){
		$Indexer = $this->_objectManager->create('Magento\Indexer\Model\Processor');
		$Indexer->reindexAll();
	}
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_ProductImportExport::import_export'
        );

    }
}
