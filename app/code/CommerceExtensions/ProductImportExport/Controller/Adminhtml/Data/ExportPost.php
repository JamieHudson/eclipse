<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
#use Magento\Catalog\Model\Product;
#use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class ExportPost extends \CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data
{
    /**
     * Export action from import/export data
     *
     * @return ResponseInterface
     */
	const MULTI_DELIMITER = ' , ';
	
	/* REMOVE ATTRIBUTES WE DO NOT WANT TO EXPORT */ 
    protected $_systemFields = ['price','weight','special_price','cost','msrp','sku','attribute_set_id','entity_id','type_id','has_options','required_options','name','swatch_image','image','small_image','thumbnail','url_key','meta_title','meta_description','meta_keyword','image_label','small_image_label','thumbnail_label','short_description','description','created_at','updated_at','special_from_date','special_to_date','custom_design_from','custom_design_to','news_from_date','news_to_date','custom_layout_update'];
	
    protected $_disabledAttributes = ['attribute_set_id','tier_price','entity_id','old_id','media_gallery','sku_type','weight_type','shipment_type','price_type','groupyprice'];
	
    protected $_attributes = array();
	
	 /**
     * Prepare products media gallery
     *
     * @param  int[] $productIds
     * @return array 
     */
	protected $resourceConnection;
	protected $fileFactory;
	protected $frameworkUrl;
	protected $storeManager;
	protected $categoryModel;
	protected $stockRegistry;
	protected $taxClassModel;
	protected $productModel;
	protected $configurableProduct;
	protected $downloadableLink;
	protected $downloadableSample;
	protected $bundleOption;
	protected $bundleSelection;
	protected $attributeSet;
    protected $productAttributeCollection;
	

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
		\Magento\Framework\Url $frameworkUrl,
		\Magento\Store\Model\StoreManager $storeManager,
		\Magento\Catalog\Model\Category $categoryModel,
		\Magento\CatalogInventory\Model\StockRegistry $stockRegistry,
		\Magento\Tax\Model\ClassModel $taxClassModel,
        \Magento\Catalog\Model\Product $productModel,
		\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct,
		\Magento\Downloadable\Model\Link $downloadableLink,
		\Magento\Downloadable\Model\Sample $downloadableSample,
		\Magento\Bundle\Model\Option $bundleOption,
		\Magento\Bundle\Model\Selection $bundleSelection,
		\Magento\Eav\Model\Entity\Attribute\Set $attributeSet,
		\Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $productAttributeCollection
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->fileFactory = $fileFactory;
        $this->_frameworkUrl = $frameworkUrl;
        $this->_storeManager = $storeManager;
        $this->_categoryModel = $categoryModel;
        $this->_stockRegistry = $stockRegistry;
        $this->_taxClassModel = $taxClassModel;
        $this->_productModel = $productModel;
        $this->_configurableProduct = $configurableProduct;
        $this->_downloadableLink = $downloadableLink;
        $this->_downloadableSample = $downloadableSample;
        $this->_bundleOption = $bundleOption;
		$this->_bundleSelection = $bundleSelection;
		$this->_attributeSet = $attributeSet;
        $this->_productAttributeCollection = $productAttributeCollection;
		parent::__construct($context,$fileFactory);
    }
	
    protected function getMediaGallery(array $productIds)
    {
        if (empty($productIds)) {
            return [];
        }
		$_resource = $this->resourceConnection;
		$connection = $_resource->getConnection();
        $select = $connection->select()->from(
            ['mg' => $_resource->getTableName('catalog_product_entity_media_gallery')],
            [
                'mg.value_id',
                'mg.attribute_id',
                'filename' => 'mg.value',
                'mgv.label',
                'mgv.position',
                'mgv.disabled'
            ]
        )->joinLeft(
            ['mgv' => $_resource->getTableName('catalog_product_entity_media_gallery_value')],
            '(mg.value_id = mgv.value_id AND mgv.store_id = 0)',
            []
        )->where(
            'mgv.entity_id IN(?)', //was 'mg.value_id IN(?)'
            $productIds
        );

        $rowMediaGallery = [];
        $stmt = $connection->query($select);
        while ($mediaRow = $stmt->fetch()) {
            $rowMediaGallery[] = [
                '_media_attribute_id' => $mediaRow['attribute_id'],
                '_media_image' => $mediaRow['filename'],
                '_media_label' => $mediaRow['label'],
                '_media_position' => $mediaRow['position'],
                '_media_is_disabled' => $mediaRow['disabled'],
            ];
        }

        return $rowMediaGallery;
    }

    public function execute()
    {				
		// Export functionality from Export  Products button and through cron job 
		$params = $this->getRequest()->getParams();
		
		if(count($params) == '23'){
		
			$this->export_functionality_from_export_form($params,$cronjob_export_path="",$cronjob_export_name="");	
		
		} else {
		
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$prefix = $resource->getTableName('productimportexport_cronjobdata');
			$core_config_data = $resource->getTableName('core_config_data');
			$rs = $resource->getConnection()->query("SELECT * FROM ".$prefix);	
			$rows = $rs->fetchAll();
			$export = array(
				'export_delimiter'=> $rows['0']['export_delimiter'],
				'export_enclose'=> $rows['0']['export_enclose'],
				'export_fields'=> $rows['0']['export_fields'],
				'apply_additional_filters'=> $rows['0']['apply_additional_filters'],
				'filter_qty_from'=> $rows['0']['filter_qty_from'],
				'filter_qty_to'=> $rows['0']['filter_qty_to'],
				'filter_status'=> $rows['0']['filter_status'],
				'product_id_from'=> $rows['0']['product_id_from'],
				'product_id_to'=> $rows['0']['product_id_to'],
				'export_grouped_position'=> $rows['0']['export_grouped_position'],
				'export_related_position'=> $rows['0']['export_related_position'],
				'export_crossell_position'=> $rows['0']['export_crossell_position'],
				'export_upsell_position'=> $rows['0']['export_upsell_position'],
				'export_category_paths'=> $rows['0']['export_category_paths'],
				'export_full_image_paths'=> $rows['0']['export_full_image_paths'],
				'export_multi_store'=> $rows['0']['export_multi_store']
			);
			$cronjob_export_path = $rows['0']['export_file_path'];
			$cronjob_export_name = $rows['0']['export_file_name'];
			$request = $resource->getConnection()->query("SELECT * FROM ".$core_config_data);
			$request_list = $request->fetchAll();
				
			foreach($request_list as $key => $value){
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr', $request_list[$key]) || in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr', $request_list[$key])){
					$import_result[] = array(
											'config_id' => $value['config_id'],	
											'path' => $value['path']
											);
				}							
			}
				 
			foreach($import_result as $key => $value){
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr', $import_result[$key])){	
					$this->export_functionality_from_export_form($export,$cronjob_export_path,$cronjob_export_name);
				}	
			}	 	
		} 
	}
	
	public function	export_functionality_from_export_form($params,$cronjob_export_path,$cronjob_export_name){
			
		$_resource = $this->resourceConnection;
		$catalog_product_bundle_option_value = $_resource->getTableName('catalog_product_bundle_option_value');
		$_productData = $this->_productModel;
		$_productAttributes = $this->_productAttributeCollection->load();
		$_stockData = $this->_stockRegistry;
		$connection = $_resource->getConnection();
		
		if($params['export_delimiter'] != "") {
			$delimiter = $params['export_delimiter'];
		} else {
			$delimiter = ",";
		}
		if($params['export_enclose'] != "") {
			$enclose = $params['export_enclose'];
		} else {
			$enclose = "\"";
		}
		
		if($params['export_fields'] == "true") {
			$template = "";
			$attributesArray = array();
			foreach ($params['gui_data']['map']['product']['db'] as $mappedField) {
				if($mappedField != "0") {
					$template .= ''.$enclose.'{{'.$mappedField.'}}'.$enclose.''.$delimiter.'';
					$attributesArray[$mappedField] = $mappedField;
				}
			}
		
		} else {
			/* BUILD OUT COLUMNS NOT IN DEFAULT ATTRIBUTES */
			$template = ''.$enclose.'{{store}}'.$enclose.''.$delimiter.''.$enclose.'{{websites}}'.$enclose.''.$delimiter.''.$enclose.'{{attribute_set}}'.$enclose.''.$delimiter.''.$enclose.'{{prodtype}}'.$enclose.''.$delimiter.''.$enclose.'{{related}}'.$enclose.''.$delimiter.''.$enclose.'{{upsell}}'.$enclose.''.$delimiter.''.$enclose.'{{crosssell}}'.$enclose.''.$delimiter.''.$enclose.'{{tier_prices}}'.$enclose.''.$delimiter.''.$enclose.'{{associated}}'.$enclose.''.$delimiter.''.$enclose.'{{config_attributes}}'.$enclose.''.$delimiter.''.$enclose.'{{bundle_options}}'.$enclose.''.$delimiter.''.$enclose.'{{bundle_selections}}'.$enclose.''.$delimiter.''.$enclose.'{{grouped}}'.$enclose.''.$delimiter.''.$enclose.'{{group_price_price}}'.$enclose.''.$delimiter.''.$enclose.'{{downloadable_options}}'.$enclose.''.$delimiter.''.$enclose.'{{downloadable_sample_options}}'.$enclose.''.$delimiter.''.$enclose.'{{gallery_label}}'.$enclose.''.$delimiter.''.$enclose.'{{qty}}'.$enclose.''.$delimiter.''.$enclose.'{{min_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_min_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{is_qty_decimal}}'.$enclose.''.$delimiter.''.$enclose.'{{backorders}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_backorders}}'.$enclose.''.$delimiter.''.$enclose.'{{min_sale_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_min_sale_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{max_sale_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_max_sale_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{is_in_stock}}'.$enclose.''.$delimiter.''.$enclose.'{{low_stock_date}}'.$enclose.''.$delimiter.''.$enclose.'{{notify_stock_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_notify_stock_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{manage_stock}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_manage_stock}}'.$enclose.''.$delimiter.''.$enclose.'{{stock_status_changed_auto}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_qty_increments}}'.$enclose.''.$delimiter.''.$enclose.'{{qty_increments}}'.$enclose.''.$delimiter.''.$enclose.'{{enable_qty_increments}}'.$enclose.''.$delimiter.''.$enclose.'{{is_decimal_divided}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_enable_qty_increments}}'.$enclose.''.$delimiter.''.$enclose.'{{use_config_enable_qty_inc}}'.$enclose.''.$delimiter.''.$enclose.'{{stock_status_changed_automatically}}'.$enclose.''.$delimiter.''.$enclose.'{{product_id}}'.$enclose.''.$delimiter.''.$enclose.'{{store_id}}'.$enclose.''.$delimiter.''.$enclose.'{{additional_attributes}}'.$enclose.''.$delimiter.'';
			
			$attributesArray = array('store' => 'store', 'websites' => 'websites', 'attribute_set' => 'attribute_set', 'prodtype' => 'prodtype', 'related' => 'related', 'upsell' => 'upsell', 'crosssell' => 'crosssell', 'tier_prices' => 'tier_prices', 'associated' => 'associated', 'config_attributes' => 'config_attributes', 'bundle_options' => 'bundle_options', 'bundle_selections' => 'bundle_selections', 'grouped' => 'grouped', 'group_price_price' => 'group_price_price', 'downloadable_options' => 'downloadable_options', 'downloadable_sample_options' => 'downloadable_sample_options', 'gallery_label' => 'gallery_label', 'qty' => 'qty', 'min_qty' => 'min_qty', 'use_config_min_qty' => 'use_config_min_qty', 'is_qty_decimal' => 'is_qty_decimal', 'backorders' => 'backorders', 'use_config_backorders' => 'use_config_backorders', 'min_sale_qty' => 'min_sale_qty', 'use_config_min_sale_qty' => 'use_config_min_sale_qty', 'max_sale_qty' => 'max_sale_qty', 'use_config_max_sale_qty' => 'use_config_max_sale_qty', 'is_in_stock' => 'is_in_stock', 'low_stock_date' => 'low_stock_date', 'notify_stock_qty' => 'notify_stock_qty', 'use_config_notify_stock_qty' => 'use_config_notify_stock_qty', 'manage_stock' => 'manage_stock', 'use_config_manage_stock' => 'use_config_manage_stock', 'stock_status_changed_auto' => 'stock_status_changed_auto', 'use_config_qty_increments' => 'use_config_qty_increments', 'qty_increments' => 'qty_increments', 'enable_qty_increments' => 'enable_qty_increments', 'is_decimal_divided' => 'is_decimal_divided', 'use_config_enable_qty_increments' => 'use_config_enable_qty_increments', 'use_config_enable_qty_inc' => 'use_config_enable_qty_inc', 'stock_status_changed_automatically' => 'stock_status_changed_automatically', 'product_id' => 'product_id', 'store_id' => 'store_id', 'additional_attributes' => 'additional_attributes');
		
		
			if($params['export_category_paths'] == "true") {
				$template .= ''.$enclose.'{{categories}}'.$enclose.''.$delimiter.'';
				$attributesArray = array_merge($attributesArray, array('categories' => 'categories'));
			}
			#$product_stock_attributes = $_stockData->getStockItem(2);
			foreach ($_productAttributes as $productAttr) {
				$col = $productAttr->getAttributeCode();
				if (!in_array($col, $this->_disabledAttributes)) {
					$attributesArray[$col] = $col;
					$template .= ''.$enclose.'{{'.$col.'}}'.$enclose.''.$delimiter.'';
				}
			}
		
		}
		
		if($params['product_id_from'] != "" && $params['product_id_to'] != "") {
			$productCollection = $_productData->getCollection()
									->addAttributeToSelect('*')
									->addAttributeToFilter ( 'entity_id' , array( "from" => $params['product_id_from'], "to" => $params['product_id_to'] ))
									->load();	
		} else if($params['apply_additional_filters'] == "yes_additional_filters") {	
			
			$productCollection = $_productData->getCollection();
			
			if($params['export_filter_by_attribute_code'] != "") {	
				$attribute_code = $params['export_filter_by_attribute_code'];
				$attribute_value = $params['export_filter_by_attribute_value'];
				 
				$productCollection->addAttributeToSelect($attribute_code);
				$productCollection->addFieldToFilter(array(array('attribute'=>$attribute_code,'eq'=> $this->_objectManager->create('Magento\Catalog\Model\ResourceModel\Product')
								->getAttribute($attribute_code)
								->getSource()
								->getOptionId(trim($attribute_value)))));
			}
			
			if($params['filter_qty_from'] !="" && $params['filter_qty_to'] !="") {
				#$entityIds_cat_filters->addAttributeToFilter('attribute_set_id',  $this->getVar('filter/attribute_set'));
				$cataloginventory_stock_item = $_resource->getTableName('cataloginventory_stock_item');
				$productCollection->addAttributeToSelect('*')
								  ->joinField('qty',
									 $cataloginventory_stock_item,
									 'qty',
									 'product_id=entity_id',
									 '{{table}}.stock_id=1',
									 'left'
								  );
				$productCollection->addAttributeToFilter('qty' , array( "from" => $params['filter_qty_from'], "to" => $params['filter_qty_to']));
			}
			if($params['filter_status'] != "") {	
				$productCollection->addAttributeToFilter('status' , array('eq' => $params['filter_status']));
			}
			/*				
			if($this->getVar('filter/type')!="") {
				$entityIds->addAttributeToFilter('type_id', array('eq' => $this->getVar('filter/type')));
			}
			if($this->getVar('filter/visibility')!="") {
				$entityIds->addAttributeToFilter('visibility', array('eq' => $this->getVar('filter/visibility')));
			}
			if($this->getVar('filter/attribute_set')!="") {
				$entityIds->addAttributeToFilter('attribute_set_id',  $this->getVar('filter/attribute_set'));
			}
			*/
		  
		} else {
			$productCollection = $_productData->getCollection()
									->addAttributeToSelect('*')
									->load();
		}
		
		$Custdata = array();
		if($params['export_fields'] != "true") {
			foreach($productCollection as $product){
				$_prodModel = $this->_objectManager->create('Magento\Catalog\Model\Product')->setStoreId(0)->load($product->getId());
				if(is_array($_prodModel->getOptions())) {
					foreach ($_prodModel->getOptions() as $o) {
						if(!empty($o->getData())){
							$customoptionstitle = str_replace(" ", "_", $o->getData('title')) . "__" . $o->getData('type') . "__" . $o->getData('is_require') . "__". $o->getData('sort_order') . "," ;
							$CustInattrArrayAndTemp = substr_replace($customoptionstitle,"",-1);
							$attributesArray = array_merge($attributesArray, array($CustInattrArrayAndTemp => $CustInattrArrayAndTemp));
							
							if (!strpos($template, $CustInattrArrayAndTemp) !== false) {
								$template .= ''.$enclose.'{{'.$CustInattrArrayAndTemp.'}}'.$enclose.''.$delimiter.'';	
							}
							$Custdata[] = $CustInattrArrayAndTemp;
						}
					}
				}
			}
		}
       
		$headers = new \Magento\Framework\DataObject($attributesArray);
        $ExtendToString = $headers->toString($template);
		$content = str_replace('__' , ':' , $ExtendToString);
		$content .= "\n";
		$storeTemplate = [];
		
		$productCollection->addAttributeToSelect(
                			'name'
           				  )->addAttributeToSelect(
							'price'
           				  )->addAttributeToSelect(
							'special_price'
           				  )->addAttributeToSelect(
							'special_from_date'
           				  )->addAttributeToSelect(
							'special_to_date'
           				  )->addAttributeToSelect(
							'cost'
           				  )->addAttributeToSelect(
							'msrp'
           				  )->addAttributeToSelect(
							'weight'
           				  )->addAttributeToSelect(
							'url_key'
           				  )->addAttributeToSelect(
							'meta_title'
           				  )->addAttributeToSelect(
							'meta_keyword'
           				  )->addAttributeToSelect(
							'meta_description'
           				  )->addAttributeToSelect(
							'short_description'
           				  )->addAttributeToSelect(
							'description'
           				  )->addAttributeToSelect(
							'visibility'
           				  )->addAttributeToSelect(
							'image'
           				  )->addAttributeToSelect(
							'small_image'
           				  )->addAttributeToSelect(
							'thumbnail'
           				  )->addAttributeToSelect(
							'swatch_image'
           				  )->addAttributeToSelect(
							'image_label'
           				  )->addAttributeToSelect(
							'small_image_label'
           				  )->addAttributeToSelect(
							'thumbnail_label'
           				  )->addAttributeToSelect(
							'news_from_date'
           				  )->addAttributeToSelect(
							'news_to_date'
           				  )->addAttributeToSelect(
							'options_container'
           				  )->addAttributeToSelect(
							'country_of_manufacture'
           				  )->addAttributeToSelect(
							'page_layout'
           				  )->addAttributeToSelect(
							'custom_design'
           				  )->addAttributeToSelect('*');
							
		foreach($productCollection as $_productEntity){
			
			if($params['export_multi_store'] == "true") {
				#foreach ($this->_storeManager->getCollection()->setLoadDefault(false) as $store) {
				foreach ($this->_objectManager->create('Magento\Store\Model\Store')->getCollection()->setLoadDefault(false) as $store) {
					$_prodModel = $this->_objectManager->create('Magento\Catalog\Model\Product')->setStoreId($store->getId())->load($_productEntity->getId());
					$_product = $this->exportProductData($_prodModel, $params, $Custdata, $_stockData);
					$content .= $_product->toString($template) . "\n";	
				}
			} else {
				//$_product = $_productEntity;
				$_prodModel = $this->_objectManager->create('Magento\Catalog\Model\Product')->setStoreId(0)->load($_productEntity->getId());
				$_product = $this->exportProductData($_prodModel, $params, $Custdata, $_stockData);
				$content .= $_product->toString($template) . "\n";	
			}
			//$cron[] = $_product;
		}
			
		if(isset($cronjob_export_path) && !empty($cronjob_export_path) && !empty($cronjob_export_name)){
			#$this->Properarray($_product,$cronjob_export_path) ;
			$cronjobarray[] = $_product->toArray();
			$this->putcsvdata($cronjobarray,$cronjob_export_path,$cronjob_export_name);
		}		
		
		if(empty($cronjob_export_path)){
			if($params['export_manual_file_name'] != "") {
				$export_file_name = $params['export_manual_file_name'];
			} else {
				$export_file_name = "export_products.csv";
			}
			return $this->fileFactory->create($export_file_name, $content, DirectoryList::VAR_DIR);
		}
	}
	public function exportProductData($_product, $params, $Custdata, $_stockData)
    {
			// THIS CLEANS HTML and other " qoute data
			foreach ($_product->getData() as $field => $_productElementData){
				#echo "FIELD: " . $field . "<br/>";	
				#echo "VALUE: " . $_productElementData . "<br/>";
				if(!is_object($_productElementData) && !is_array($_productElementData) && strpos($field, '__') !== '__'){
					$storeTemplate[$field] = str_replace( '"', '""', $_productElementData);
				}
				
				$option="";
				
				if (in_array($field, $this->_systemFields) || is_object($_productElementData)) {
					continue;
				}
				
				$attribute = $this->getAttribute($field);
				if (!$attribute) {
					continue;
				}
				#print_r($attribute);
				if ($attribute->usesSource()) {
					#print_r($_productElementData);
					#echo "FIELD: " . $field . "<br/>";	
					if (is_array($_productElementData)) {	
						if($field == "quantity_and_stock_status") { 
							$option = $attribute->getSource()->getOptionText($_productElementData['is_in_stock']);
							$_productElementData = $option;
						}
					} else {	
						$option = $attribute->getSource()->getOptionText($_productElementData);
						$_productElementData = $option;
					}
					/*
					if (is_object($attribute->usesSource())) {		
						$option = $attribute->getSource()->getOptionText($_productElementData);
					} else {				
						$option = $attribute->getSource()->getOptionText($_productElementData);					
					}
					*/
				}
				if (is_array($option)) {
					$_productElementData = join(self::MULTI_DELIMITER, $option);
				}
				#echo "FIELD: " . $field . "";	
				#echo "VALUE: " . $_productElementData . "<br/><br/>";
				unset($option);
				$storeTemplate[$field] = $_productElementData;
				
			}
			#exit;
			$storeTemplate['store'] = $this->storeCodeByID($_product->getStoreId());
			$storeTemplate['websites'] = $this->websiteCodeById($_product->getWebsiteIds());
			$storeTemplate['attribute_set'] = $this->attributebyid($_product->getData('attribute_set_id'));
			$storeTemplate['prodtype'] = $_product->getData('type_id');
			$storeTemplate['msrp_enabled'] = $this->msrpriceActual($_product->getData('msrp_display_actual_price_type'));
			$storeTemplate['product_id'] = $_product->getData('entity_id');
			$storeTemplate['url_path'] =  $_product->getUrlKey(); //$_product->getProductUrl();
			//Retrieve accessible external product attributes
			#$storeTemplate['msrp_display_actual_price_type'] = $this->msrpriceActual($_product->getData('msrp_display_actual_price_type'));
			$storeTemplate['store_id'] = $_product->getStoreId();
			$storeTemplate['additional_attributes'] = $this->GetExternalFields($_product->getData('entity_id'),$_product->getData('type_id'));
			
			/* PRODUCT CATEGORIES EXPORT START */
			if($params['export_category_paths'] == "true") {
				$storeTemplate['category_ids'] = $this->sptidwithcoma($_product->getCategoryIds());
				$finalimportofcategories = "";
				$okforallcategoriesnow = "";
				$finalvcategoriesproductoptions1 = "";
				$finalvcategoriesproductoptions2 = "";
				$finalvcategoriesproductoptions2before = "";
				foreach(explode(',',$storeTemplate['category_ids']) as $productcategoryId)
				{
						$cat = $this->_categoryModel->load($productcategoryId);
						$finalvcategoriesproductoptions1 = $cat->getName();
						$subcatsforreverse = $cat->getParentIds();
						$subcats = array_shift($subcatsforreverse);
						$subcats1 = array_shift($subcatsforreverse);
						$finalvcategoriesproductoptions2before = "";
						foreach($subcatsforreverse as $subcatsproductcategoryId)
						{
								$subcat = $this->_categoryModel->load($subcatsproductcategoryId);
								$finalvcategoriesproductoptions2before .= $subcat->getName() . "/";
								$subsubcats = $subcat->getChildren();
						}
						$finalimportofcategories .= $finalvcategoriesproductoptions2before . 
						$finalvcategoriesproductoptions1 . " , ";
				}
				$okforallcategoriesnow = substr_replace($finalimportofcategories,"",-3);
				$storeTemplate['categories'] = $okforallcategoriesnow;
			} else {
				$storeTemplate['category_ids'] = $this->sptidwithcoma($_product->getCategoryIds());
			}
			/* PRODUCT CATEGORIES EXPORT END */
			
			/* RELATED */
			$finalrelatedproducts = "";
			$incoming_RelatedProducts = $_product->getRelatedProducts();
			foreach($incoming_RelatedProducts as $relatedproducts_str){
				if($params['export_related_position'] == "true") {
					$finalrelatedproducts .= $relatedproducts_str['position'] .":". $relatedproducts_str->getSku() . ",";
				} else {
					$finalrelatedproducts .= $relatedproducts_str->getSku() . ",";
				}
			}
			$storeTemplate['related'] = substr_replace($finalrelatedproducts,"",-1);
			/* UP SELL */
			$finalupsellproducts = "";
			$incoming_UpSellProducts = $_product->getUpSellProducts();
			foreach($incoming_UpSellProducts as $UpSellproducts_str){
				if($params['export_crossell_position'] == "true") {
					$finalupsellproducts .= $UpSellproducts_str['position'] .":". $UpSellproducts_str->getSku() . ",";
				} else {
					$finalupsellproducts .= $UpSellproducts_str->getSku() . ",";
				}
			}
			$storeTemplate['upsell'] = substr_replace($finalupsellproducts,"",-1);
			/* CROSS SELL  */
			$finalcrosssellproducts = "";
			$incoming_CrossSellProducts = $_product->getCrossSellProducts();
			foreach($incoming_CrossSellProducts as $CrossSellproducts_str){
				if($params['export_upsell_position'] == "true") {
					$finalcrosssellproducts .= $CrossSellproducts_str['position'] .":". $CrossSellproducts_str->getSku() . ",";
				} else {
					$finalcrosssellproducts .= $CrossSellproducts_str->getSku() . ",";
				}
			}
			$storeTemplate['crosssell'] = substr_replace($finalcrosssellproducts,"",-1);
			
			/* EXPORTS TIER PRICING */
			
			if(!empty($_product->getTierPrice())) {
				$tier_pricing = $this->TierPrice($_product->getTierPrice());
			} else {
				if ($attribute = $_product->getResource()->getAttribute('tier_price')) {
					$attribute->getBackend()->afterLoad($_product);
					$tier_pricing = $this->TierPrice($_product->getData('tier_price'));
				}
			}
			$storeTemplate['tier_prices'] = (string)$tier_pricing;

			/* EXPORTS ASSOICATED BUNDLE SKUS */
			if($_product->getTypeId() == "bundle") {
				$finalbundleoptions = "";
				$finalbundleselectionoptions = "";
				$finalbundleselectionoptionssorting = "";
				$optionModel = $this->_bundleOption->getResourceCollection()->setProductIdFilter($_product->getId());
				$_resource = $this->resourceConnection;
				$connection = $_resource->getConnection();
				$catalog_product_bundle_option_value = $_resource->getTableName('catalog_product_bundle_option_value');
					
				foreach($optionModel as $eachOption) {
						
						$selectOptionID = "SELECT title FROM ".$catalog_product_bundle_option_value." WHERE option_id = ".$eachOption->getData('option_id')."";
						$Optiondatarows = $connection->query($selectOptionID);
						while ($Option_row = $Optiondatarows->fetch()) {
							$finaltitle = str_replace(' ','_',$Option_row['title']);
						}
						$finalbundleoptions .=  $finaltitle . "," . $eachOption->getData('type') . "," . $eachOption->getData('required') . "," . $eachOption->getData('position') . "|";
						$selectionModel =$this->_bundleSelection->setOptionId($eachOption->getData('option_id'))->getResourceCollection();
						
						foreach($selectionModel as $eachselectionOption) {
							if($eachselectionOption->getData('option_id') == $eachOption->getData('option_id')) {
							$finalbundleselectionoptionssorting .=  $eachselectionOption->getData('sku') . ":" . $eachselectionOption->getData('selection_price_type') . ":" . $eachselectionOption->getData('selection_price_value') . ":" . $eachselectionOption->getData('is_default') . ":" . $eachselectionOption->getData('selection_qty') . ":" . $eachselectionOption->getData('selection_can_change_qty'). ":" . $eachselectionOption->getData('position') . ",";
							}
						}
						$finalbundleselectionoptionssorting = substr_replace($finalbundleselectionoptionssorting,"",-1);
						$finalbundleselectionoptionssorting .=  "|";
						$finalbundleselectionoptions = substr_replace($finalbundleselectionoptionssorting,"",-1);
				}
				$storeTemplate['bundle_options'] = substr_replace($finalbundleoptions,"",-1);
				$storeTemplate['bundle_selections'] = substr_replace($finalbundleselectionoptions,"",-1);
			}
			
			/* EXPORTS DOWNLOADABLE OPTIONS */
			$finaldownloabledproductoptions = "";
			$finaldownloabledsampleproductoptions = "";
			
			if($_product->getTypeId() == "downloadable") {
			$_linkCollection = $this->_downloadableLink->getCollection()
								->addProductToFilter($_product->getId())
								->addTitleToResult($_product->getStoreId())
								->addPriceToResult($_product->getStore()->getWebsiteId());

			 foreach ($_linkCollection as $link) {
			 
				if($link->getLinkType() =="url" && $link->getSampleType() =="url") {
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkUrl() . "," . $link->getSampleUrl() . "|";
				} else if($link->getLinkType() =="url" && $link->getSampleType() =="file") {
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkUrl() . "," . $link->getSampleFile() . "," . $link->getSampleType() . "|";
				} else if($link->getLinkType() =="file" && $link->getSampleType() =="url") {
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkFile() . "," . $link->getSampleUrl() . "," . $link->getSampleType() . "|";
				} else if($link->getLinkType() =="file" && $link->getSampleType() =="file") {
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkFile() . "," . $link->getSampleFile() . "|";
				}else if($link->getLinkType() =="file" && $link->getSampleType() ==""){
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkFile() . "," . $link->getSampleFile() . "|";
				}else if($link->getLinkType() =="url" && $link->getSampleType() ==""){
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkUrl() . "," . $link->getSampleUrl() . "|";
				}else if($link->getLinkType() =="" && $link->getSampleType() =="file"){
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkFile() . "," . $link->getSampleFile() . "," . $link->getSampleType() . "|";
				}else if($link->getLinkType() =="" && $link->getSampleType() =="url"){
				$finaldownloabledproductoptions .= $link->getTitle() . "," . $link->getPrice() . "," . $link->getNumberOfDownloads() . "," . $link->getLinkType() . "," . $link->getLinkUrl() . "," . $link->getSampleUrl() . "," . $link->getSampleType() . "|";
				}else{
					$finaldownloabledproductoptions .= "";
				}
				
				
			 }
			 	$storeTemplate['downloadable_options'] = substr_replace($finaldownloabledproductoptions,"",-1);
				$_linkSampleCollection = $this->_downloadableSample->getCollection()
									->addProductToFilter($_product->getId())
									->addTitleToResult($_product->getStoreId());
			
				foreach ($_linkSampleCollection as $sample_link) {
					/* @var Mage_Downloadable_Model_Sample $sample_link */
					#Main file,file,/test.mp3,/sample.mp3
					if($sample_link->getSampleType() == "url") {
						$finaldownloabledsampleproductoptions .= $sample_link->getTitle() . "," . $sample_link->getSampleType() . "," . $sample_link->getSampleUrl() . "|";
					} else if($sample_link->getSampleType() == "file") {
						$finaldownloabledsampleproductoptions .= $sample_link->getTitle() . "," . $sample_link->getSampleType() . "," . $sample_link->getSampleFile() . "|";
					}
				}
			 $storeTemplate['downloadable_sample_options'] = substr_replace($finaldownloabledsampleproductoptions,"",-1);
			 
			} else {
					$storeTemplate['downloadable_sample_options'] = "";
					$storeTemplate['downloadable_options'] = "";
			}// end for check of downloadable type
			
						
			/* EXPORTS ASSOICATED CONFIGURABLE SKUS */
			$storeTemplate['associated'] = '';
			if($_product->getTypeId() == "configurable") {
				$associatedProducts = $_product->getTypeInstance()->getUsedProducts($_product, null);
				foreach($associatedProducts as $associatedProduct) {
						$storeTemplate['associated'] .= $associatedProduct->getSku() . ",";
				}
			}
			/* EXPORTS ASSOICATED GROUPED SKUS */
			$storeTemplate['grouped'] = '';
			if($_product->getTypeId() == "grouped") {
				$associatedProducts = $_product->getTypeInstance()->getAssociatedProducts($_product, null);
				foreach($associatedProducts as $associatedProduct) {
						if($params['export_grouped_position'] == "true") {
							$storeTemplate['grouped'] .= $associatedProduct->getPosition() . ":" . $associatedProduct->getSku() . ":" . $associatedProduct->getQty() . ",";
						} else {
							$storeTemplate['grouped'] .= $associatedProduct->getSku() . ",";
						}
				}
			}
			/* IMAGE EXPORT [START] */
			
			if($params['export_full_image_paths'] == "true") {
				$getBaseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
				if($_product->getData('image')!="") { $storeTemplate['image'] = $getBaseUrl . "catalog/product" . $_product->getData('image'); }
				if($_product->getData('small_image')!="") { $storeTemplate['small_image'] = $getBaseUrl . "catalog/product" . $_product->getData('small_image'); }
				if($_product->getData('thumbnail')!="") { $storeTemplate['thumbnail'] = $getBaseUrl . "catalog/product" . $_product->getData('thumbnail'); }
				if($_product->getData('swatch_image')!="") { $storeTemplate['swatch_image'] = $getBaseUrl . "catalog/product" . $_product->getData('swatch_image'); }
			}
			
			/* IMAGE EXPORT [END] */
			
			/* GALLERY IMAGE EXPORT [START] */
			$finalgalleryimages = "";
			$galleryImagesModel = $this->getMediaGallery(array($_product->getId()));
		
			if (count($galleryImagesModel) > 0) {
				
				#$getBaseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
				foreach ($galleryImagesModel as $_image) {
					#if($params['export_full_image_paths'] == "true") {
					if(!in_array($_image['_media_image'], array($_product->getData('image'),$_product->getData('small_image'),$_product->getData('thumbnail')), true )) {
						if ($params['export_full_image_paths'] == "true") {
							$finalgalleryimages .= $getBaseUrl . "catalog/product" .  $_image['_media_image'] . ",";
						} else {
							$finalgalleryimages .= $_image['_media_image'] . ",";
						}
					}
				}
			}
			
			$storeTemplate['gallery'] = substr_replace($finalgalleryimages,"",-1);
			/* GALLERY IMAGE EXPORT [END] */
			
			/* EXPORTS CONFIGURABLE ATTRIBUTES [START] */
			$storeTemplate['config_attributes'] = '';
			$finalproductattributes = "";
			//check if product is a configurable type or not
			if($_product->getTypeId() == "configurable") {
				 //get the configurable data from the product
				 $config = $_product->getTypeInstance(true);
				 //loop through the attributes                                  
				 foreach($config->getConfigurableAttributesAsArray($_product) as $attributes)
				 {
					$finalproductattributes .= $attributes['attribute_code'] . ",";
				 }
			}
			$storeTemplate['config_attributes'] = substr_replace($finalproductattributes,"",-1);
			/* EXPORTS CONFIGURABLE ATTRIBUTES [END] */
			
			/*  EXPORT PRODUCT OPTIONS [START]  */
			/*  EMPTY CUSTOM OPTION TITLE  */
			foreach ($Custdata as $OptTitl){
				$storeTemplate[$OptTitl] = "";
			}
			if(is_array($_product->getOptions())) {
				foreach ($_product->getOptions() as $CustmOptData) {
					
					$customoptionvalues = "";
					$CustOptnTitle = str_replace(" ", "_", $CustmOptData->getData('title')) . "__" . $CustmOptData->getData('type') . "__" . $CustmOptData->getData('is_require') . "__". $CustmOptData->getData('sort_order');	
					if($CustmOptData->getData('type')=="checkbox" || $CustmOptData->getData('type')=="drop_down" || $CustmOptData->getData('type')=="radio" || $CustmOptData->getData('type')=="multiple") {
						foreach ( $CustmOptData->getValues() as $oValues ) {
							if($oValues->getData('price_type')=="") { $price_type = "fixed"; } else { $price_type = $oValues->getData('price_type'); }
							if($oValues->getData('price')=="") { $price = "0.0000"; } else { $price = $oValues->getData('price'); }
							if($oValues->getData('sku')=="") { $sku = " "; } else { $sku = $oValues->getData('sku'); }
							if($oValues->getData('sort_order')=="") { $sort_order = "0"; } else { $sort_order = $oValues->getData('sort_order'); }
							if($oValues->getData('max_characters')=="") { $max_characters = "0"; } else { $max_characters = $oValues->getData('max_characters'); }
							$customoptionvalues .= $oValues->getData('title') . ":" . $price_type . ":" . $price . ":" . $sku . ":" . $sort_order . ":" . $max_characters . "|";
						}
						
					  }else{
						if($CustmOptData->getData('price_type')=="") { $price_type = "fixed"; } else { $price_type = $CustmOptData->getData('price_type'); }
						if($CustmOptData->getData('price')=="") { $price = "0.0000"; } else { $price = $CustmOptData->getData('price'); }
						if($CustmOptData->getData('sku')=="") { $sku = " "; } else { $sku = $CustmOptData->getData('sku'); }
						if($CustmOptData->getData('sort_order')=="") { $sort_order = "0"; } else { $sort_order = $CustmOptData->getData('sort_order'); }
						if($CustmOptData->getData('max_characters')=="") { $max_characters = "0"; } else { $max_characters = $CustmOptData->getData('max_characters'); }
						$customoptionvalues .= $CustmOptData->getData('title') . ":" . $price_type . ":" . $price . ":" . $sku . ":" . $sort_order . ":" . $max_characters . "|";
						
					  }
					  $storeTemplate[$CustOptnTitle] = substr_replace($customoptionvalues,"",-1);
				}
			}
			
			
			/*  EXPORT PRODUCT OPTIONS [END]  */
			$storeTemplate['qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getQty();
			$storeTemplate['min_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getMinQty();
			$storeTemplate['use_config_min_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getUseConfigMinQty();
			$storeTemplate['is_qty_decimal'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('is_qty_decimal');
			$storeTemplate['backorders'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('backorders');
			$storeTemplate['use_config_backorders'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('use_config_backorders');
			$storeTemplate['min_sale_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getMinSaleQty();
			$storeTemplate['use_config_min_sale_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getUseConfigMinSaleQty();
			$storeTemplate['max_sale_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getMaxSaleQty();
			$storeTemplate['use_config_max_sale_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getUseConfigMaxSaleQty();
			$storeTemplate['is_in_stock'] = $_stockData->getStockItem($_product->getData('entity_id'))->getIsInStock();
			$storeTemplate['low_stock_date'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('low_stock_date');
			$storeTemplate['notify_stock_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getNotifyStockQty();
			$storeTemplate['use_config_notify_stock_qty'] = $_stockData->getStockItem($_product->getData('entity_id'))->getUseConfigNotifyStockQty();
			$storeTemplate['manage_stock'] = $_stockData->getStockItem($_product->getData('entity_id'))->getManageStock();
			$storeTemplate['use_config_manage_stock'] = $_stockData->getStockItem($_product->getData('entity_id'))->getUseConfigManageStock();
			$storeTemplate['stock_status_changed_auto'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('stock_status_changed_auto');
			$storeTemplate['use_config_qty_increments'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('use_config_qty_increments');
			$storeTemplate['qty_increments'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('qty_increments');
			$storeTemplate['enable_qty_increments'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('enable_qty_increments');
			$storeTemplate['is_decimal_divided'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('is_decimal_divided');
			$storeTemplate['use_config_enable_qty_increments'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('use_config_enable_qty_inc');
			$storeTemplate['use_config_enable_qty_inc'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('use_config_enable_qty_inc');
			$storeTemplate['stock_status_changed_automatically'] = $_stockData->getStockItem($_product->getData('entity_id'))->getData('stock_status_changed_auto');
			
			$_product->addData($storeTemplate);
			unset($storeTemplate);
			
			return $_product;
	
	}
	/*
	public function Properarray($cron,$cronjob_export_path){
				
				 $cronjobarray = array() ;
				foreach($cron as $key => $value){
				
					$cronjobarray[] = array(
					'store' => (!empty($value['store'])) ? $value['store'] : '',
					'websites' => (!empty($value['websites'])) ? $value['websites'] : '',
					'attribute_set' => (!empty($value['attribute_set'])) ? $value['attribute_set'] : '',
					'prodtype' => (!empty($value['prodtype'])) ? $value['prodtype'] : '',
					'related' => (!empty($value['related'])) ? $value['related'] : '',
					'upsell' => (!empty($value['upsell'])) ? $value['upsell'] : '',
					'crosssell' => (!empty($value['crosssell'])) ? $value['crosssell'] : '',
					'tier_prices' => (!empty($value['tier_prices'])) ? $value['tier_prices'] : '',
					'associated' => (!empty($value['associated'])) ? $value['associated'] : '',
					'config_attributes' => (!empty($value['config_attributes'])) ? $value['config_attributes'] : '',
					'grouped' => (!empty($value['grouped'])) ? $value['grouped'] : '',
					'group_price_price' => (!empty($value['group_price_price'])) ? $value['group_price_price'] : '',
					'downloadable_options' => (!empty($value['downloadable_options'])) ? $value['downloadable_options'] : '',
					'downloadable_sample_options' => (!empty($value['downloadable_sample_options'])) ? $value['downloadable_sample_options'] : '',
					'gallery_label' => (!empty($value['gallery_label'])) ? $value['gallery_label'] : '',
					'qty' => (!empty($value['qty'])) ? $value['qty'] : '',
					'min_qty' => (!empty($value['min_qty'])) ? $value['min_qty'] : '',
					'use_config_min_qty' => (!empty($value['use_config_min_qty'])) ? $value['use_config_min_qty'] : '',
					'is_qty_decimal' => (!empty($value['is_qty_decimal'])) ? $value['is_qty_decimal'] : '',
					'backorders' => (!empty($value['backorders'])) ? $value['backorders'] : '',
					'use_config_backorders' => (!empty($value['use_config_backorders'])) ? $value['use_config_backorders'] : '',
					'min_sale_qty' => (!empty($value['min_sale_qty'])) ? $value['min_sale_qty'] : '',
					'use_config_min_sale_qty' => (!empty($value['use_config_min_sale_qty'])) ? $value['use_config_min_sale_qty'] : '',
					'max_sale_qty' => (!empty($value['max_sale_qty'])) ? $value['max_sale_qty'] : '',
					'use_config_max_sale_qty' => (!empty($value['use_config_max_sale_qty'])) ? $value['use_config_max_sale_qty'] : '',
					'is_in_stock' => (!empty($value['is_in_stock'])) ? $value['is_in_stock'] : '',
					'low_stock_date' => (!empty($value['low_stock_date'])) ? $value['low_stock_date'] : '',
					'notify_stock_qty' => (!empty($value['notify_stock_qty'])) ? $value['notify_stock_qty'] : '',
					'use_config_notify_stock_qty' => (!empty($value['use_config_notify_stock_qty'])) ? $value['use_config_notify_stock_qty'] : '',
					'manage_stock' => (!empty($value['manage_stock'])) ? $value['manage_stock'] : '',
					'use_config_manage_stock' => (!empty($value['use_config_manage_stock'])) ? $value['use_config_manage_stock'] : '',
					'stock_status_changed_auto' => (!empty($value['stock_status_changed_auto'])) ? $value['stock_status_changed_auto'] : '',
					'use_config_qty_increments' => (!empty($value['use_config_qty_increments'])) ? $value['use_config_qty_increments'] : '',
					'qty_increments' => (!empty($value['qty_increments'])) ? $value['qty_increments'] : '',
					'enable_qty_increments' => (!empty($value['enable_qty_increments'])) ? $value['enable_qty_increments'] : '',
					'is_decimal_divided' => (!empty($value['is_decimal_divided'])) ? $value['is_decimal_divided'] : '',
					'use_config_enable_qty_increments' => (!empty($value['use_config_enable_qty_increments'])) ? $value['use_config_enable_qty_increments'] : '',
					'use_config_enable_qty_inc' => (!empty($value['use_config_enable_qty_inc'])) ? $value['use_config_enable_qty_inc'] : '',
					'stock_status_changed_automatically' => (!empty($value['stock_status_changed_automatically'])) ? $value['stock_status_changed_automatically'] : '',
					'product_id' => (!empty($value['product_id'])) ? $value['product_id'] : '',
					'store_id' => (!empty($value['store_id'])) ? $value['store_id'] : '',
					'additional_attributes' => (!empty($value['additional_attributes'])) ? $value['additional_attributes'] : '',
					'category_ids' => (!empty($value['category_ids'])) ? $value['category_ids'] : '',
					'color' => (!empty($value['color'])) ? $value['color'] : '',
					'cost' => (!empty($value['cost'])) ? $value['cost'] : '',
					'country_of_manufacture' => (!empty($value['country_of_manufacture'])) ? $value['country_of_manufacture'] : '',
					'created_at' => (!empty($value['created_at'])) ? $value['created_at'] : '',
					'custom_design' => (!empty($value['custom_design'])) ? $value['custom_design'] : '',
					'custom_design_from' => (!empty($value['custom_design_from'])) ? $value['custom_design_from'] : '',
					'custom_design_to' => (!empty($value['custom_design_to'])) ? $value['custom_design_to'] : '',
					'custom_layout_update' => (!empty($value['custom_layout_update'])) ? $value['custom_layout_update'] : '',
					'description' => (!empty($value['description'])) ? $value['description'] : '',
					'gallery' => (!empty($value['gallery'])) ? $value['gallery'] : '',
					'gift_message_available' => (!empty($value['gift_message_available'])) ? $value['gift_message_available'] : '',
					'has_options' => (!empty($value['has_options'])) ? $value['has_options'] : '',
					'image' => (!empty($value['image'])) ? $value['image'] : '',
					'image_label' => (!empty($value['image_label'])) ? $value['image_label'] : '',
					'links_exist' => (!empty($value['links_exist'])) ? $value['links_exist'] : '',
					'links_purchased_separately' => (!empty($value['links_purchased_separately'])) ? $value['links_purchased_separately'] : '',
					'links_title' => (!empty($value['links_title'])) ? $value['links_title'] : '',
					'manufacturer' => (!empty($value['manufacturer'])) ? $value['manufacturer'] : '',
					'meta_description' => (!empty($value['meta_description'])) ? $value['meta_description'] : '',
					'meta_keyword' => (!empty($value['meta_keyword'])) ? $value['meta_keyword'] : '',
					'meta_title' => (!empty($value['meta_title'])) ? $value['meta_title'] : '',
					'minimal_price' => (!empty($value['minimal_price'])) ? $value['minimal_price'] : '',
					'msrp' => (!empty($value['msrp'])) ? $value['msrp'] : '',
					'msrp_display_actual_price_type' => (!empty($value['msrp_display_actual_price_type'])) ? $value['msrp_display_actual_price_type'] : '',
					'name' => (!empty($value['name'])) ? $value['name'] : '',
					'news_from_date' => (!empty($value['news_from_date'])) ? $value['news_from_date'] : '',
					'news_to_date' => (!empty($value['news_to_date'])) ? $value['news_to_date'] : '',
					'options_container' => (!empty($value['options_container'])) ? $value['options_container'] : '',
					'page_layout' => (!empty($value['page_layout'])) ? $value['page_layout'] : '',
					'price' => (!empty($value['price'])) ? $value['price'] : '',
					'price_view' => (!empty($value['price_view'])) ? $value['price_view'] : '',
					'quantity_and_stock_status' => (!empty($value['quantity_and_stock_status'])) ? $value['quantity_and_stock_status'] : '',
					'required_options' => (!empty($value['required_options'])) ? $value['required_options'] : '',
					'samples_title' => (!empty($value['samples_title'])) ? $value['samples_title'] : '',
					'short_description' => (!empty($value['short_description'])) ? $value['short_description'] : '',
					'sku' => (!empty($value['sku'])) ? $value['sku'] : '',
					'small_image' => (!empty($value['small_image'])) ? $value['small_image'] : '',
					'small_image_label' => (!empty($value['small_image_label'])) ? $value['small_image_label'] : '',
					'special_from_date' => (!empty($value['special_from_date'])) ? $value['special_from_date'] : '',
					'special_price' => (!empty($value['special_price'])) ? $value['special_price'] : '',
					'special_to_date' => (!empty($value['special_to_date'])) ? $value['special_to_date'] : '',
					'swatch_image' => (!empty($value['swatch_image'])) ? $value['swatch_image'] : '',
					'tax_class_id' => (!empty($value['tax_class_id'])) ? $value['tax_class_id'] : '',
					'thumbnail' => (!empty($value['thumbnail'])) ? $value['thumbnail'] : '',
					'thumbnail_label' => (!empty($value['thumbnail_label'])) ? $value['thumbnail_label'] : '',
					'updated_at' => (!empty($value['updated_at'])) ? $value['updated_at'] : '',
					'url_key' => (!empty($value['url_key'])) ? $value['url_key'] : '',
					'url_path' => (!empty($value['url_path'])) ? $value['url_path'] : '',
					'weight' => (!empty($value['weight'])) ? $value['weight'] : ''
					);
				
				} 
			
				$this->putcsvdata($cronjobarray,$cronjob_export_path);
	}
	*/
	public function putcsvdata($data,$cronjob_export_path, $cronjob_export_name){
		//$name = "CronJob_exported_products.csv";
		//$this->fileFactory->create('export_products.csv', $content, DirectoryList::VAR_DIR);
		//Warning: fopen(var/ProductImportExport/cron_export_products.csv): failed to open stream: No such file or directory in /home/scottb61/public_html/app/code/CommerceExtensions/ProductImportExport/Controller/Adminhtml/Data/ExportPost.php on line 877
		//$importDir = $this->getMediaDirImportDir();
		//$url = $importDir . '/' . $cronjob_export_path.'/'.$cronjob_export_name;
		$fullPath = getcwd();
		$url = $fullPath.'/'.$cronjob_export_path.'/'.$cronjob_export_name;
		$outstream = fopen($url, 'w');	 
			$first = true;
			$temp = $data['0'];    
			foreach($data as $result){
				if($first){
					$titles = array();
					foreach($temp as $key=>$val){
						$titles[] = $key;
					}
				//print_r ($titles);exit;
					fputcsv($outstream, $titles);
				}
				$first = false;
				fputcsv($outstream, $result);
			} 
			fclose($outstream); 
	}
	
	public function GetExternalFields($ProductId,$ProductType){
		if($ProductType == 'configurable' && $ProductId !=""){
			$product = $this->_productModel->load($ProductId); 
			$config = $product->getTypeInstance(true);
			$ProductData = array();
			foreach($config->getConfigurableAttributesAsArray($product) as $attributes)
			{
				$associated_products = $this->_configurableProduct->getUsedProductCollection($product)->addAttributeToSelect('*')->addFilterByRequiredOptions();
				foreach($associated_products as $Associatedproduct){
					$ProductData[]= $Associatedproduct->getSku() .'='.$attributes['attribute_code'] .'='. $Associatedproduct->getAttributeText($attributes['attribute_code']);	
				}
			}
			return implode(',',$ProductData);
		}
	}
	
    public function getAttribute($code)
    {
        if (!isset($this->_attributes[$code])) {
            $this->_attributes[$code] = $this->_productModel->getResource()->getAttribute($code);
        }
        return $this->_attributes[$code];
    }
	
	public function sptidwithcoma($sdata){
		$data =	implode(",",$sdata);
		return $data;
	}
	
	public function storeCodeByID($storeid){
		return $this->_storeManager->getStore($storeid)->getCode();
	}
	
	public function msrpriceActual($msrp_displayActualPrice){
		$data ="";
		if($msrp_displayActualPrice == 0){ $data .= "Use config"; }
		if($msrp_displayActualPrice == 1){ $data .= "On Gesture"; }
		if($msrp_displayActualPrice == 2){ $data .= "In Cart"; }
		if($msrp_displayActualPrice == 3){ $data .= "Before Order Confirmation"; }
		return $data;
	}
	
	public function TierPrice($incoming_tierps){
		$data="";
		if(is_array($incoming_tierps)) {
			$export_data="";
			foreach($incoming_tierps as $tier_str){
				#print_r($tier_str);
				$export_data .= $tier_str['cust_group'] . "=" . round($tier_str['price_qty']) . "=" . $tier_str['price'] . "|";
			}
			$data = substr_replace($export_data,"",-1);
		}
		
		return $data;
	}
	
	public function websiteCodeById($webid){
		$withname = array();
		foreach($webid as $webids){
			$withname[] = $this->_storeManager->getWebsite($webids)->getCode();
		}
		$data =	implode(",",$withname);
		return $data;
	}
	
	public function attributebyid($attributeid){
		$data = $this->_attributeSet->load($attributeid)->getAttributeSetName();
		return $data;
	}
	
	
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_ProductImportExport::import_export'
        );

    }
	
}
