<?php
/**
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Filesystem;
use Magento\Config\Model\Config\Backend\Image as SourceImage;

class CronJob extends \CommerceExtensions\ProductImportExport\Controller\Adminhtml\Data
{	
	
	public function execute()
	{
	 	if ($this->getRequest()->isPost()) {
			$params = $this->getRequest()->getParams();
			$Profile_type = $this->getRequest()->getPost('Profile_type');
			$Schedule = $this->getRequest()->getPost('Schedule');
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$core_config_data = $resource->getTableName('core_config_data');
					
			$request = $resource->getConnection()->query("SELECT * FROM ".$core_config_data);
			$request_list = $request->fetchAll();
	
			$import_result = array(); 
					
			foreach($request_list as $key => $value){
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr', $request_list[$key]) || in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr', $request_list[$key])){
					$import_result[] = array(
										'config_id' => $value['config_id'],	
										'path' => $value['path']
											);
				}							
			}
			
			if(!empty(array_key_exists('0', $import_result)) && empty(array_key_exists('1', $import_result))){
				$core_config_data = $resource->getTableName('core_config_data');	
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr', $import_result['0']) && empty(array_key_exists('1', $import_result))){
						
						if($Profile_type == 'Import_Products'){
								if($Schedule == 'Hourly'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array('value'=> '0 * * * *'), 'config_id=' . $import_result['0']['config_id'] .'');
								}
								elseif($Schedule == 'Every_24hrs'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 23 * * *'), 'config_id=' . $import_result['0']['config_id'] .'');
								}
								else{
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 0 * * 0'), 'config_id=' . $import_result['0']['config_id'] .''); 	
								}
						}
						if($Profile_type == 'Export_Products'){
							if($Schedule == 'Hourly'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
												'value' => '0 * * * *'
												);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);					
							}  
							elseif($Schedule == 'Every_24hrs'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
												'value' => '0 23 * * *'
												);
									$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
							}
							else{
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
												'value' => '0 0 * * 0'
												);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
							}
						}	
				}		
				
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr', $import_result['0']) && empty(array_key_exists('1', $import_result))){
						if($Profile_type == 'Export_Products'){
							if($Schedule == 'Hourly'){	
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
									'value'=> '0 * * * *'), 'config_id=' . $import_result['0']['config_id'] .'');
								}
								elseif($Schedule == 'Every_24hrs'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 23 * * *'), 'config_id=' . $import_result['0']['config_id'] .''); 
								}
								else{
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 0 * * 0'), 'config_id=' . $import_result['0']['config_id'] .''); 	
								}
				
						}
						
						if($Profile_type == 'Import_Products'){  
							if($Schedule == 'Hourly'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
												'value' => '0 * * * *'
													);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);					
							}  
							elseif($Schedule == 'Every_24hrs'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
												'value' => '0 23 * * *'
													);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
							}
							else{
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
												'value' => '0 0 * * 0'
													);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
							}		
						}
				}
						
			}
			if(count($import_result) == '0'){
			
				if($Profile_type == 'Export_Products'){
						if($Schedule == 'Hourly'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
												'value' => '0 * * * *'
													);
									$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);					
						}  
						elseif($Schedule == 'Every_24hrs'){
							$hourly_array = array(
											'scope' => 'default',
											'scope_id' => '0',
											'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
											'value' => '0 23 * * *'
												);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
						}
						else{
							$hourly_array = array(
											'scope' => 'default',
											'scope_id' => '0',
											'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr',
											'value' => '0 0 * * 0'
												);
							$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
						}
				}
				if($Profile_type == 'Import_Products'){  
					if($Schedule == 'Hourly'){
						$hourly_array = array(
								'scope' => 'default',
								'scope_id' => '0',
								'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
								'value' => '0 * * * *'
									);
							$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);					
							}  
					elseif($Schedule == 'Every_24hrs'){
								$hourly_array = array(
												'scope' => 'default',
												'scope_id' => '0',
												'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
												'value' => '0 23 * * *'
													);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
					}
					else{
						$hourly_array = array(
										'scope' => 'default',
										'scope_id' => '0',
										'path' => 'crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr',
										'value' => '0 0 * * 0'
											);
								$connection = $resource->getConnection()->insert(''.$core_config_data.'', $hourly_array);
					}		
				}
			}
			
			if(!empty(array_key_exists('0', $import_result)) && !empty(array_key_exists('1', $import_result))){
					 if($Profile_type == 'Import_Products'){
								if($Schedule == 'Hourly'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array('value'=> '0 * * * *'), 'config_id=' . $import_result['1']['config_id'] .'');
								}
								elseif($Schedule == 'Every_24hrs'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 23 * * *'), 'config_id=' . $import_result['1']['config_id'] .'');
								}
								else{
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 0 * * 0'), 'config_id=' . $import_result['1']['config_id'] .''); 	
								}
						}
					if($Profile_type == 'Export_Products'){
							if($Schedule == 'Hourly'){	
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
									'value'=> '0 * * * *'), 'config_id=' . $import_result['0']['config_id'] .'');
								}
								elseif($Schedule == 'Every_24hrs'){
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 23 * * *'), 'config_id=' . $import_result['0']['config_id'] .''); 
								}
								else{
									$connection = $resource->getConnection()->update(''.$core_config_data.'', array(
										'value'=> '0 0 * * 0'), 'config_id=' . $import_result['0']['config_id'] .''); 	
								}
					} 
					
			}
					
					$File_name = $this->getRequest()->getPost('import_file_name');
					$File_path = $this->getRequest()->getPost('import_file_path'); 
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$prefix = $resource->getTableName('productimportexport_cronjobdata');
					$insData = array();
					$insData = array(
					'root_catalog_id'=> $params['root_catalog_id'],
					'update_products_only'=> $params['update_products_only'],
					'ref_by_product_id'=> $params['ref_by_product_id'],
					'import_attribute_value'=> $params['import_attribute_value'],
					'attribute_for_import_value'=> $params['attribute_for_import_value'],
					'import_images_by_url'=> $params['import_images_by_url'],
					'reimport_images'=> $params['reimport_images'],
					'deleteall_andreimport_images'=> $params['deleteall_andreimport_images'],
					'append_tier_prices'=> $params['append_tier_prices'],
					'append_categories'=> $params['append_categories'],
					'append_websites'=> $params['append_websites'],
					'apply_additional_filters'=> $params['apply_additional_filters'],
					'filter_qty_from'=> $params['filter_qty_from'],
					'filter_qty_to'=> $params['filter_qty_to'],
					'product_id_from'=> $params['product_id_from'],
					'product_id_to'=> $params['product_id_to'],
					'export_grouped_position'=> $params['export_grouped_position'],
					'export_related_position'=> $params['export_related_position'],
					'export_crossell_position'=> $params['export_crossell_position'],
					'export_upsell_position'=> $params['export_upsell_position'],
					'export_category_paths'=> $params['export_category_paths'],
					'export_full_image_paths'=> $params['export_full_image_paths'],
					'export_multi_store'=> $params['export_multi_store'],
					'import_file_name'=> $params['import_file_name'],
					'import_file_path'=> $params['import_file_path'],
					'export_file_name'=> $params['export_file_name'],
					'export_file_path'=> $params['export_file_path'],
					'import_enclose'=> $params['import_enclose'],
					'import_delimiter'=> $params['import_delimiter'],
					'export_enclose'=> $params['export_enclose'],
					'export_delimiter'=> $params['export_delimiter'],
					'Profile_type'=> $params['Profile_type'],
					'Schedule'=> $params['Schedule'],
					);
					
					$rs = $resource->getConnection()->query("SELECT * FROM ".$prefix." WHERE Profile_Type = '".$params['Profile_type']."'");
					$rows = $rs->fetchAll();
						
					if(count($rows)){
							$connection = $resource->getConnection()->update(''.$prefix.'', array(
							'root_catalog_id'=> $params['root_catalog_id'],
							'update_products_only'=> $params['update_products_only'],
							'ref_by_product_id'=> $params['ref_by_product_id'],
							'import_attribute_value'=> $params['import_attribute_value'],
							'attribute_for_import_value'=> $params['attribute_for_import_value'],
							'import_images_by_url'=> $params['import_images_by_url'],
							'reimport_images'=> $params['reimport_images'],
							'deleteall_andreimport_images'=> $params['deleteall_andreimport_images'],
							'append_tier_prices'=> $params['append_tier_prices'],
							'append_categories'=> $params['append_categories'],
							'append_websites'=> $params['append_websites'],
							'apply_additional_filters'=> $params['apply_additional_filters'],
							'filter_qty_from'=> $params['filter_qty_from'],
							'filter_qty_to'=> $params['filter_qty_to'],
							'filter_status'=> $params['filter_status'],
							'product_id_from'=> $params['product_id_from'],
							'product_id_to'=> $params['product_id_to'],
							'export_grouped_position'=> $params['export_grouped_position'],
							'export_related_position'=> $params['export_related_position'],
							'export_crossell_position'=> $params['export_crossell_position'],
							'export_upsell_position'=> $params['export_upsell_position'],
							'export_category_paths'=> $params['export_category_paths'],
							'export_full_image_paths'=> $params['export_full_image_paths'],
							'export_multi_store'=> $params['export_multi_store'],
							'import_file_name'=> $params['import_file_name'],
							'import_file_path'=> $params['import_file_path'],
							'export_file_name'=> $params['export_file_name'],
							'export_file_path'=> $params['export_file_path'],
							'import_enclose'=> $params['import_enclose'],
							'import_delimiter'=> $params['import_delimiter'],
							'export_enclose'=> $params['export_enclose'],
							'export_delimiter'=> $params['export_delimiter'],
							'Profile_type'=> $params['Profile_type'],
							'Schedule'=> $params['Schedule']
							),array('Profile_Type = ?'=> $params['Profile_type']));
							$this->_redirect($this->_redirect->getRefererUrl());
							$this->messageManager->addSuccess(__('Your "'.$params['Profile_type'].'" Cron Job Has Been Updated'));
					} else {
						$connection = $resource->getConnection()->insert(''.$prefix.'', $insData);
						//$rs = $resource->getConnection()->query("SELECT * FROM ".$prefix);	
						$this->_redirect($this->_redirect->getRefererUrl());
						$this->messageManager->addSuccess(__('Your "'.$params['Profile_type'].'" Cron Job Has Been Successfully Saved'));
					} 
		} else {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$prefix = $resource->getTableName('productimportexport_cronjobdata');
			$core_config_data = $resource->getTableName('core_config_data');
						
			$rs = $resource->getConnection()->query("SELECT * FROM ".$prefix);	
			$rows = $rs->fetchAll();
							
			$request = $resource->getConnection()->query("SELECT * FROM ".$core_config_data);
			$request_list = $request->fetchAll();
				
			foreach($request_list as $key => $value){
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr', $request_list[$key]) || in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/export_products/schedule/cron_expr', $request_list[$key])){
					$import_result[] = array('config_id' => $value['config_id'], 'path' => $value['path']);
				}							
			}
			// Send the fetched array for import functionality 
			foreach($import_result as $key => $value){
				if(in_array('crontab/default/jobs/CommerceExtensions/ProductImportExport/import_products/schedule/cron_expr', $import_result[$key])){	
					$this->Cronjob_import($rows);
				}	
			}	 
		}				
	}
		
	// For Import 
	public function Cronjob_import($params){
	
		try {
		
			$importHandler = $this->_objectManager->create('CommerceExtensions\ProductImportExport\Model\Data\CsvImportHandler');  
			$File_path = $params['0']['content'] ;
			$File_name = $params['0']['import_file_name'] ;
			$url = $File_path.'/'.$File_name;
			$name = $File_name;
			
			$fetched_file_name = array();
			$tempName = tempnam('/tmp', 'php_files');
			$imgRawData = file_get_contents($url);
			file_put_contents($tempName, $imgRawData);
			
			$fetched_file_name = array(
				'name' => $name,
				'type' => 'application/vnd.ms-excel',
				'tmp_name' => $tempName,
				'error' => 0,
				'size' => strlen($imgRawData),
			); 
			
			$readData = $importHandler->UploadCsvOfproduct($fetched_file_name);
			$filepath = $readData['path'].'/'.$readData['file'];
			
			$param1 = array(
			'import_delimiter' => $params['0']['import_delimiter'],
			'import_enclose' => $params['0']['import_enclose'],
			'root_catalog_id'=> $params['0']['root_catalog_id'],
			'ref_by_product_id'=> $params['0']['ref_by_product_id'],
			'import_attribute_value'=> $params['0']['import_attribute_value'],
			'attribute_for_import_value'=> $params['0']['attribute_for_import_value'],
			'update_products_only'=> $params['0']['update_products_only'],
			'import_images_by_url'=> $params['0']['import_images_by_url'],
			'reimport_images'=> $params['0']['reimport_images'],
			'deleteall_andreimport_images'=> $params['0']['deleteall_andreimport_images'],
			'append_tier_prices'=> $params['0']['append_tier_prices'],
			'append_categories'=> $params['0']['append_categories'],
			'append_websites'=> $params['0']['append_websites']
			);
						
			$importHandler->readCsvFile($filepath, $param1);
			$success = $this->messageManager->addSuccess(__('The Products have been imported Successfully.'));
			if($success){
				$this->reindexdata();
			}
			$this->_redirect($this->_redirect->getRefererUrl()); 
		
		}
		catch (\Magento\Framework\Exception\LocalizedException $e) {
			$this->messageManager->addError($e->getMessage());
		} catch (\Exception $e) {
			$this->messageManager->addError(__('Invalid file upload attempt' . $e->getMessage()));
		} 
					
	}
	
	public function reindexdata(){
		$Indexer = $this->_objectManager->create('Magento\Indexer\Model\Processor');
		$Indexer->reindexAll();
	}

}	