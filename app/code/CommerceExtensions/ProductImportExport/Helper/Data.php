<?php
/**
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $customerSession;
    protected $product;
    protected $productLinkInterfaceFactory;
    protected $urlRewriteCollection;
    protected $directoryList;
    public $msgtoreturn = array();
    public $rowCount = 1;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
	 
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Catalog\Model\Product $product,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkInterfaceFactory,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection,
        DirectoryList $directoryList
    )
    {
		$this->customerSession             = $customerSession;
		$this->product 					   = $product;
		$this->productLinkInterfaceFactory = $productLinkInterfaceFactory;
        $this->urlRewriteCollection 		= $urlRewriteCollection;
        $this->directoryList = $directoryList;
				
        parent::__construct($context);
    }
	
	protected function getMediaDirImportDir()
    {
        return $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'import';
    }
	
   	public function checkUrlKey($storeId, $urlKey) {
	
			$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			//->addFieldToFilter('store_id', $storeId)
			if($storeId !=0) {
				$urlrewritesCollection->addFieldToFilter('store_id', $storeId);
			} 
			$urlrewritesCollection->addFieldToFilter('entity_type', 'product')
								  ->addFieldToFilter('request_path', $urlKey)//.".html"
								  ->setPageSize(1);
			return $urlrewritesCollection->getFirstItem();
	}
	
	public function addImage($imageName, $columnName, $imageArray = array()) {
	
		if($imageName == "no_selection") {
			if (array_key_exists($imageName, $imageArray)) {
				array_push($imageArray[$imageName],$columnName);
			} else {
				$imageArray[$imageName] = array($columnName);
			}
			return $imageArray; 
		}
		if($imageName=="") { return $imageArray; }
		$importDir = $this->getMediaDirImportDir();
		
		if($columnName == "media_gallery") {
			$galleryData = explode(',', $imageName);
			foreach( $galleryData as $gallery_img ) {
				if(file_exists($importDir.$gallery_img)) {  
					if (array_key_exists($gallery_img, $imageArray)) {
						array_push($imageArray[$gallery_img],$columnName);
					} else {
						$imageArray[$gallery_img] = array($columnName);
					}
				} else {
					return $imageArray;
				}
			}
		} else {
			if(file_exists($importDir.$imageName)) {  
				if (array_key_exists($imageName, $imageArray)) {
					array_push($imageArray[$imageName],$columnName);
				} else {
					$imageArray[$imageName] = array($columnName);
				}
			} else {
				return $imageArray; 
			}
		}
		return $imageArray;
	}
    
	public function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", " ", $data ) );
	} 
	
	public function sendLog($rowCount , $column, $error) {
		$this->msgtoreturn[] = array('line' => $rowCount , 'column' => $column, 'error' => $error);
	}
	
	public function AppendRelatedProduct($ReProduct , $sku){
	
		$URCProducts = explode(',',$ReProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkedSku){
			if($linkedSku!="") {
				$id = $this->product->getIdBySku($linkedSku);
				if($id > 0) {
					$linkData = $this->productLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkedSku)
						->setLinkType("related");
					$linkDataAll[] = $linkData;
				} else {
					$this->sendLog($this->rowCount,'related','The column contains sku that does NOT exist "'.$linkedSku.'"');
				}
			}
		}
		return $linkDataAll;
		
		
	}
	public function AppendUpsellProduct($UpProduct , $sku){
		
		$URCProducts = explode(',',$UpProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkedSku){
			if($linkedSku!="") {
				$id = $this->product->getIdBySku($linkedSku);
				if($id > 0) {
					$linkData = $this->productLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkedSku)
						->setLinkType("upsell");
					$linkDataAll[] = $linkData;
				} else {
					$this->sendLog($this->rowCount,'upsell','The column contains sku that does NOT exist "'.$linkedSku.'"');
				}
			}
		}
		return $linkDataAll;
		
	}
	public function AppendCrossSellProduct($CsProduct , $sku){
	
		$URCProducts = explode(',',$CsProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkedSku){
			if($linkedSku!="") {
				$id = $this->product->getIdBySku($linkedSku);
				if($id > 0) {
					$linkData = $this->productLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkedSku)
						->setLinkType("crosssell");
					$linkDataAll[] = $linkData;
				} else {
					$this->sendLog($this->rowCount,'crosssell','The column contains sku that does NOT exist "'.$linkedSku.'"');
				}
			}
		}
		return $linkDataAll;
	}
}