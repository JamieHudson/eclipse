<?php
/**
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace CommerceExtensions\ProductImportExport\Model\Data;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Store\Model\Website;

class CsvImportHandler
{
    const MULTI_DELIMITER = ' , ';

    protected $_resource;

    protected $_filesystem;

    protected $date;

    protected $csvProcessor;

    protected $_eavConfig;

    protected $objectManager;

    protected $Product;

    protected $_imageFields     = ['image', 'swatch_image', 'small_image', 'thumbnail', 'media_gallery', 'gallery', 'gallery_label'];

    protected $_requiredFields  = ['sku', 'store', 'prodtype'];

    protected $_stockDataFields = ['manage_stock',
                                   'use_config_manage_stock',
                                   'qty',
                                   'min_qty',
                                   'use_config_min_qty',
                                   'min_sale_qty',
                                   'use_config_min_sale_qty',
                                   'max_sale_qty',
                                   'use_config_max_sale_qty',
                                   'is_qty_decimal',
                                   'backorders',
                                   'use_config_backorders',
                                   'notify_stock_qty',
                                   'use_config_notify_stock_qty',
                                   'enable_qty_increments',
                                   'use_config_enable_qty_inc',
                                   'qty_increments',
                                   'use_config_qty_increments',
                                   'is_in_stock',
                                   'low_stock_date',
                                   'stock_status_changed_auto'];

    protected $_categoryCache   = [];

    protected $directoryList;

    protected $file;

    /**
     * @var array
     */
    private $optionValues = [];

    public function __construct(
        ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        Filesystem $filesystem,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\SimpleProduct $SimpleProduct,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\BundleProduct $BundleProduct,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\VirtualProduct $VirtualProduct,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\ConfigurableProduct $ConfigurableProduct,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\GroupedProduct $GroupedProduct,
        \CommerceExtensions\ProductImportExport\Model\Data\Import\DownloadableProduct $DownloadableProduct,
        \Magento\Catalog\Api\ProductTierPriceManagementInterface $ProductTierPriceManagementInterface,
        \Magento\Catalog\Model\Product $Product,
        \Magento\Store\Model\Website $Website,
        \CommerceExtensions\ProductImportExport\Helper\Data $helper,
        \Magento\Eav\Model\Config $eavConfig,
        DirectoryList $directoryList,
        File $file,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        \Magento\Catalog\Api\ProductAttributeOptionManagementInterface $optionManager
    ) {
        // prevent admin store from loading
        $this->_resource            = $resource;
        $this->scopeConfig          = $scopeConfig;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem          = $filesystem;
        $this->csvProcessor         = $csvProcessor;
        $this->localeFormat         = $localeFormat;
        $this->_objectManager       = $objectManager;
        $this->SimpleProduct        = $SimpleProduct;
        $this->BundleProduct        = $BundleProduct;
        $this->VirtualProduct       = $VirtualProduct;
        $this->ConfigurableProduct  = $ConfigurableProduct;
        $this->GroupedProduct       = $GroupedProduct;
        $this->DownloadableProduct  = $DownloadableProduct;
        $this->tierPrice            = $ProductTierPriceManagementInterface;
        $this->_ProductModel        = $Product;
        $this->website              = $Website;
        $this->helper               = $helper;
        $this->_eavConfig           = $eavConfig;
        $this->directoryList        = $directoryList;
        $this->file                 = $file;
        $this->optionFactory        = $optionFactory;
        $this->optionManager        = $optionManager;
    }

    public function requiredDataForSaveProduct($rowCount, $product, $params)
    {

        $ProductStockdata     = [];
        $ProductAttributeData = [];
        $custom_options       = [];
        foreach ($product as $field => $value) {
            if (in_array($field, $this->_imageFields)) {
                continue;
            }
            if (in_array($field, $this->_stockDataFields)) {
                $ProductStockdata[$field] = $value;
                continue;
            }
            #if (!in_array( $field, $this -> _imageFields ) ) {
            #$ProductAttributeData[$field] = $setValue;
            #}
            #$attribute = $objectManager->create('Magento\Eav\Model\Config')->getAttribute(ModelProduct::ENTITY, $field);
            $attribute = $this->_eavConfig->getAttribute(ModelProduct::ENTITY, $field);

            if (!$attribute->getId()) {
                /* CUSTOM OPTION CODE END */
                if (strpos($field, ':') !== false && strlen($value)) {
                    $values = explode('|', $value);
                    if (count($values) > 0) {
                        $iscustomoptions = "true";

                        foreach ($values as $v) {
                            $parts = explode(':', $v);
                            $title = $parts[0];
                        }
                        @list($title, $type, $is_required, $sort_order) = explode(':', $field);
                        $title2           = $title;
                        $custom_options[] = [
                            'is_delete'      => 0,
                            'title'          => $title2,
                            'previous_group' => '',
                            'previous_type'  => '',
                            'type'           => $type,
                            'is_require'     => $is_required,
                            'sort_order'     => $sort_order,
                            'values'         => []
                        ];
                        if ($is_required == 1) {
                            $iscustomoptionsrequired = "true";
                        }
                        foreach ($values as $v) {
                            $parts = explode(':', $v);
                            $title = $parts[0];
                            if (count($parts) > 1) {
                                $price_type = $parts[1];
                            } else {
                                $price_type = 'fixed';
                            }
                            if (count($parts) > 2) {
                                $price = $parts[2];
                            } else {
                                $price = 0;
                            }
                            if (count($parts) > 3) {
                                $sku = $parts[3];
                            } else {
                                $sku = '';
                            }
                            if (count($parts) > 4) {
                                $sort_order = $parts[4];
                            } else {
                                $sort_order = 0;
                            }
                            if (count($parts) > 5) {
                                $max_characters = $parts[5];
                            } else {
                                $max_characters = '';
                            }
                            if (count($parts) > 6) {
                                $file_extension = $parts[6];
                            } else {
                                $file_extension = '';
                            }
                            if (count($parts) > 7) {
                                $image_size_x = $parts[7];
                            } else {
                                $image_size_x = '';
                            }
                            if (count($parts) > 8) {
                                $image_size_y = $parts[8];
                            } else {
                                $image_size_y = '';
                            }
                            switch ($type) {
                                case 'file':
                                    $custom_options[count($custom_options) - 1]['price_type']     = $price_type;
                                    $custom_options[count($custom_options) - 1]['price']          = $price;
                                    $custom_options[count($custom_options) - 1]['sku']            = $sku;
                                    $custom_options[count($custom_options) - 1]['file_extension'] = $file_extension;
                                    $custom_options[count($custom_options) - 1]['image_size_x']   = $image_size_x;
                                    $custom_options[count($custom_options) - 1]['image_size_y']   = $image_size_y;
                                    break;

                                case 'field':
                                    $custom_options[count($custom_options) - 1]['max_characters'] = $max_characters;
                                case 'area':
                                    $custom_options[count($custom_options) - 1]['max_characters'] = $max_characters;

                                case 'date':
                                case 'date_time':
                                case 'time':
                                    $custom_options[count($custom_options) - 1]['price_type'] = $price_type;
                                    $custom_options[count($custom_options) - 1]['price']      = $price;
                                    $custom_options[count($custom_options) - 1]['sku']        = $sku;
                                    break;

                                case 'drop_down':
                                case 'radio':
                                case 'checkbox':
                                case 'multiple':
                                default:
                                    $custom_options[count($custom_options) - 1]['values'][] = [
                                        'is_delete'      => 0,
                                        'title'          => $title,
                                        'option_type_id' => -1,
                                        'price_type'     => $price_type,
                                        'price'          => $price,
                                        'sku'            => $sku,
                                        'sort_order'     => $sort_order,
                                        'max_characters' => $max_characters,
                                    ];
                                    break;
                            }
                        }
                    }
                }
                /* CUSTOM OPTION CODE END */
                continue;
            }

            if (strtolower($params['import_attribute_value']) === "true") {
                if ($params['attribute_for_import_value'] != "") {
                    $attributestocheck = explode(',', trim($params['attribute_for_import_value']));
                    foreach ($attributestocheck as $single_attribute) {
                        if ($field === $single_attribute) {
							if (!$this->checkAttributeOptionValue($single_attribute, $product[$field]) && $product[$field] !== '') {
                                $option = $this->optionFactory->create();
                                $option->setLabel($product[$field]);
                                $this->optionManager->add($field, $option);

                                /** add the newly created option to the values array so we don't create it again */
                                $this->addAttributeOptionValue($single_attribute, $product[$field]);
                            }
                        }
                    }
                }
            }
            $isArray  = false;
            $setValue = $value;

            if ($attribute->getFrontendInput() == 'multiselect') {
                $value    = explode(self :: MULTI_DELIMITER, $value);
                $isArray  = true;
                $setValue = [];
            }

            if ($attribute->getData('is_global') == '1') {
                $arrayOfFieldstoSkip[] = $field;
            }
            if ($value && $attribute->getBackendType() == 'decimal') {
                $setValue = $this->localeFormat->getNumber($value);
            }

            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);

                if ($isArray) {
                    foreach ($options as $item) {
                        if (in_array($item['label'], $value)) {
                            $setValue[] = $item['value'];
                        }
                    }
                } else {
                    $setValue = false;
                    foreach ($options as $item) {
                        if (is_array($item['value'])) {
                            foreach ($item['value'] as $subValue) {
                                if (isset($subValue['value']) && $subValue['value'] == $value) {
                                    $setValue = $value;
                                }
                            }
                        } else {
                            if ($item['label'] == $value) {
                                $setValue = $item['value'];
                            }
                        }
                    }
                }
            }

            $product[$field] = $setValue;
        }

        //Checks if it is new Product
        $newProduct        = true;
        $productRepository = "";
        if ($params['ref_by_product_id'] == "true" && isset($product['product_id'])) {
            if ($productRepository = $this->_ProductModel->load($product['product_id'])) {
                $newProduct = false;
            }
        } else {
            if ($productRepository = $this->_ProductModel->loadByAttribute('sku', $product['sku'])) {
                $newProduct = false;
            }
        }

        #keeps existing category_ids and adds new ones to them
        if (isset($product['category_ids'])) {
            if ($params['append_categories'] == "true") {
                #$product1 = $this->_ProductModel->loadByAttribute('sku', $product['sku']);
                if (!empty($productRepository)) {
                    $productModel            = $this->_ProductModel->load($productRepository->getId());
                    $cats                    = $productModel->getCategoryIds();
                    $catsarray               = explode(",", $product['category_ids']);
                    $finalcatsimport         = array_merge($cats, $catsarray);
                    $product['category_ids'] = $finalcatsimport;
                } else {
                    $catsarray               = explode(",", $product['category_ids']);
                    $product['category_ids'] = $catsarray;
                }
            } else {
                $catsarray               = explode(",", $product['category_ids']);
                $product['category_ids'] = $catsarray;
            }
        }

        $ProductData = $this->ProductData($product, $params);

        if (isset($product['categories'])) {
            if ($params['append_categories'] == "true") {
                #$product1 = $this->_ProductModel->loadByAttribute('sku', $product['sku']);
                if (!empty($productRepository)) {
                    $productModel            = $this->_ProductModel->load($productRepository->getId());
                    $cats                    = $productModel->getCategoryIds();
                    $catsarray               = explode(",", $ProductData['categories']);
                    $finalcatsimport         = array_merge($cats, $catsarray);
                    $product['category_ids'] = $finalcatsimport;
                } else {
                    $catsarray               = explode(",", $ProductData['categories']);
                    $product['category_ids'] = $catsarray;
                }
            } else {
                $catsarray               = explode(",", $ProductData['categories']);
                $product['category_ids'] = $catsarray;
            }
        }

        $ProductAttributeData = $this->ProductAttributeData($product);

        $ProductImageGallery = [];
        if ($newProduct || $params['reimport_images'] == "true") {
            $ProductImageGallery = $this->ProductImageGallery($newProduct, $product, $params);
        }
        #print_r($ProductStockdata);
        $ProductSupperAttribute = $this->ProductSupperAttribute($newProduct, $product, $params);

        return $this->CreateProductWithrequiredField($rowCount,
                                                     $newProduct,
                                                     $productRepository,
                                                     $product['prodtype'],
                                                     $params,
                                                     $ProductData,
                                                     $ProductAttributeData,
                                                     $ProductImageGallery,
                                                     $ProductStockdata,
                                                     $ProductSupperAttribute,
                                                     $custom_options);
    }

    protected function _filterData(array $RawDataHeader, array $RawData)
    {
        $rowCount    = 0;
        $RawDataRows = [];
        foreach ($RawData as $rowIndex => $dataRow) {
            // skip headers
            if ($rowIndex == 0) {
                if (!in_array("sku", $dataRow)) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "sku" NOT FOUND'));
                }
                if (!in_array("prodtype", $dataRow)) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "prodtype" NOT FOUND'));
                }
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
            /* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
                foreach ($dataRow as $rowIndex => $dataRowNew) {
                    try {
                        $RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
                    } catch (\Exception $e) {
                        throw new \Magento\Framework\Exception\LocalizedException(__("CHECK CSV DELIMITER SETTINGS AND/OR CSV FORMAT. CSV CANNOT BE PARSED"), $e);
                    }
                }
            }
            $rowCount++;
        }

        return $RawDataRows;
    }

    public function UploadCsvOfproduct($file)
    {
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $file]);
        $uploader->setAllowedExtensions(['csv']);
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);

        $path = $this->getVarDirProductImportExportDir();
        $this->file->checkAndCreateFolder($path);
        #$path = $this->_filesystem->getDirectoryRead(DirectoryList::VAR_DIR)->getAbsolutePath('ProductImportExport');
        //$result = $uploader->save($path);
        $result = $uploader->save(realpath($path));

        return $result;
    }

    protected function _getStoreConfig($path, $storeId)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    public function _getNotCachedRow($path, $storeId)
    {
        $type = 'product';
        $cfg  = $this->_getStoreConfig($path . '/' . $type, $storeId);

        $scope   = 'default';
        $scopeId = 0;

        //'core/config_data_collection'
        $collection = $this->_objectManager->create("Magento\Config\Model\ResourceModel\Config\Data\Collection");
        $collection->addFieldToFilter('scope', $scope);
        $collection->addFieldToFilter('scope_id', $scopeId);
        $collection->addFieldToFilter('path', $path . '/' . $type . '/' . $path);
        $collection->setPageSize(1);

        $v = $this->_objectManager->create('Magento\Framework\App\Config\Value');
        if (count($collection)) {
            $v = $collection->getFirstItem();
        } else {
            $v->setScope($scope);
            $v->setScopeId($scopeId);
            $v->setPath($path . '/' . $type . '/' . $path);
        }

        return $v;
    }

    public function readCsvFile($PfilePath, $params)
    {

        $warnings        = [];
        $messagetoreturn = [];
        if ($params['import_delimiter'] != "") {
            $this->csvProcessor->setDelimiter($params['import_delimiter']);
        }
        if ($params['import_enclose'] != "") {
            $this->csvProcessor->setEnclosure($params['import_enclose']);
        }

        $RawProductData = $this->csvProcessor->getData($PfilePath); //$RawProductData[0] represents headers
        $productData    = $this->_filterData($RawProductData[0], $RawProductData);

        $importStatus = $this->_getNotCachedRow('importstatus', 0);
        $importStatus->setValue("started")->save();

        $totalnumberofrows = count($productData);
        $totalRow          = $this->_getNotCachedRow('totalrow', 0);
        $totalRow->setValue($totalnumberofrows)->save();
        $rowCount = 0;
        foreach ($productData as $product) {
            $rowCount++;
            $warnings   = $this->requiredDataForSaveProduct($rowCount, $product, $params);
            $currentRow = $this->_getNotCachedRow('currentrow', 0);
            if ($currentRow->getValue() != $rowCount) {
                $currentRow->setValue($rowCount)->save();
            }
        }

        $importStatus = $this->_getNotCachedRow('importstatus', 0);
        $importStatus->setValue("finished")->save();

        $messagetoreturn['total_rows']    = $totalnumberofrows;
        $messagetoreturn['import_status'] = "finished";
        $messagetoreturn['warnings']      = $warnings;

        return $messagetoreturn;
    }

    protected function getMediaDirImportDir()
    {
        return $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'import';
    }

    protected function getVarDirProductImportExportDir()
    {
        return $this->directoryList->getPath(DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR . 'ProductImportExport';
    }

    public function CreateProductWithrequiredField(
        $rowCount,
        $newProduct,
        $SetProductData,
        $prodtype,
        $params,
        $ProductData,
        $ProductAttributeData,
        $ProductImageGallery,
        $ProductStockdata,
        $ProductSupperAttribute,
        $ProductCustomOption
    ) {


        if ($prodtype == "simple") {
            $messagetoreturn['simple'] = $this->SimpleProduct->SimpleProductData($rowCount,
                                                                                 $newProduct,
                                                                                 $SetProductData,
                                                                                 $params,
                                                                                 $ProductData,
                                                                                 $ProductAttributeData,
                                                                                 $ProductImageGallery,
                                                                                 $ProductStockdata,
                                                                                 $ProductSupperAttribute,
                                                                                 $ProductCustomOption);
        } elseif ($prodtype == "bundle") {
            $messagetoreturn['bundle'] = $this->BundleProduct->BundleProductData($rowCount,
                                                                                 $newProduct,
                                                                                 $SetProductData,
                                                                                 $params,
                                                                                 $ProductData,
                                                                                 $ProductAttributeData,
                                                                                 $ProductImageGallery,
                                                                                 $ProductStockdata,
                                                                                 $ProductSupperAttribute,
                                                                                 $ProductCustomOption);
        } elseif ($prodtype == "virtual") {
            $messagetoreturn['virtual'] = $this->VirtualProduct->VirtualProductData($rowCount,
                                                                                    $newProduct,
                                                                                    $SetProductData,
                                                                                    $params,
                                                                                    $ProductData,
                                                                                    $ProductAttributeData,
                                                                                    $ProductImageGallery,
                                                                                    $ProductStockdata,
                                                                                    $ProductSupperAttribute);
        } elseif ($prodtype == "configurable") {
            $messagetoreturn['configurable'] = $this->ConfigurableProduct->ConfigurableProductData($rowCount,
                                                                                                   $newProduct,
                                                                                                   $SetProductData,
                                                                                                   $params,
                                                                                                   $ProductData,
                                                                                                   $ProductAttributeData,
                                                                                                   $ProductImageGallery,
                                                                                                   $ProductStockdata,
                                                                                                   $ProductSupperAttribute);
        } elseif ($prodtype == "grouped") {
            $messagetoreturn['grouped'] = $this->GroupedProduct->GroupedProductData($rowCount,
                                                                                    $newProduct,
                                                                                    $SetProductData,
                                                                                    $params,
                                                                                    $ProductData,
                                                                                    $ProductAttributeData,
                                                                                    $ProductImageGallery,
                                                                                    $ProductStockdata,
                                                                                    $ProductSupperAttribute);
        } elseif ($prodtype == "downloadable") {
            $messagetoreturn['downloadable'] = $this->DownloadableProduct->DownloadableProductData($rowCount,
                                                                                                   $newProduct,
                                                                                                   $SetProductData,
                                                                                                   $params,
                                                                                                   $ProductData,
                                                                                                   $ProductAttributeData,
                                                                                                   $ProductImageGallery,
                                                                                                   $ProductStockdata,
                                                                                                   $ProductSupperAttribute);
        } else {
            $messagetoreturn['noprodtype'] = ['line' => $rowCount, 'column' => 'prodtype', 'error' => 'The "prodtype" column is required'];
        }

        return $messagetoreturn;
    }

    public function ProductData($product, $params)
    {

        $defaultProductData['sku'] = $product['sku'];
        if (isset($product['url_key'])) {
            $defaultProductData['url_key'] = $product['url_key'];
        }
        if (isset($product['store'])) {
            $defaultProductData['store'] = $product['store'];
        } else {
            $defaultProductData['store'] = "admin";
        }
        if (isset($product['store_id'])) {
            $defaultProductData['store_id'] = $product['store_id'];
        } else {
            $defaultProductData['store_id'] = "0";
        }
        if (isset($product['websites'])) {
            $defaultProductData['websites'] = $this->websitenamebyid($params, $product['websites'], $product['sku']);
        }
        if (isset($product['attribute_set'])) {
            $defaultProductData['attribute_set'] = $this->attributeSetNamebyid($product['attribute_set']);
        }
        if (isset($product['prodtype'])) {
            $defaultProductData['prodtype'] = $product['prodtype'];
        }
        if (isset($product['categories'])) {
            $defaultProductData['categories'] = $this->addCategories($product['categories'], $product['store_id'], $params);
        }
        if (isset($product['category_ids'])) {
            $defaultProductData['category_ids'] = $product['category_ids'];
        }

        return $defaultProductData;
    }

    public function ProductAttributeData($product)
    {

        $defaultAttributeData = [];

        foreach ($product as $field => $value) {
            if (!in_array($field, $this->_imageFields)) {
                $defaultAttributeData[$field] = $value;
            }
        }

        return $defaultAttributeData;
    }

    public function ProductSupperAttribute($newProduct, $product, $params)
    {

        $defaultSupperAttributeData = [
            'related'                     => (isset($product['related'])) ? $product['related'] : '',
            'upsell'                      => (isset($product['upsell'])) ? $product['upsell'] : '',
            'crosssell'                   => (isset($product['crosssell'])) ? $product['crosssell'] : '',
            'tier_prices'                 => (isset($product['tier_prices'])) ? $this->TierPricedata($newProduct, $product['tier_prices'], $product['sku'], $params) : '',
            'associated'                  => (isset($product['associated'])) ? $product['associated'] : '',
            'bundle_options'              => (isset($product['bundle_options'])) ? $product['bundle_options'] : '',
            'bundle_selections'           => (isset($product['bundle_selections'])) ? $product['bundle_selections'] : '',
            'grouped'                     => (isset($product['grouped'])) ? $product['grouped'] : '',
            'group_price_price'           => (isset($product['group_price_price'])) ? $product['group_price_price'] : '',
            'downloadable_options'        => (isset($product['downloadable_options'])) ? $product['downloadable_options'] : '',
            'downloadable_sample_options' => (isset($product['downloadable_sample_options'])) ? $product['downloadable_sample_options'] : '',
        ];

        return $defaultSupperAttributeData;
    }

    public function ProductImageGallery($newProduct, $product, $params)
    {

        if ($params['deleteall_andreimport_images'] == "true" && !$newProduct) {
            // bug here.. something about this delete image and resave wipes out all store view default check box settings
            $productRepository = $this->_objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface');
            $productModel      = $productRepository->get($product['sku']);
            $productModel->setMediaGalleryEntries([]);
            $productRepository->save($productModel);

            //DELETES EXTRA BLANK STORE IMAGES AND OTHER STORE VIEW IMAGES
            $product_id = $productModel->getId();
            $connection = $this->_resource->getConnection();

            $eav_attribute                              = $this->_resource->getTableName('eav_attribute');
            $catalog_product_entity_varchar             = $this->_resource->getTableName('catalog_product_entity_varchar');
            $catalog_product_entity_media_gallery       = $this->_resource->getTableName('catalog_product_entity_media_gallery');
            $catalog_product_entity_media_gallery_value = $this->_resource->getTableName('catalog_product_entity_media_gallery_value');

            $allImages = $connection->fetchAll("SELECT value_id FROM " . $catalog_product_entity_media_gallery_value . " WHERE entity_id = '" . $product_id . "'");
            foreach ($allImages as $eachImageRemove) {
                if ($eachImageRemove['value_id'] != "") {
                    $connection->query("DELETE FROM " . $catalog_product_entity_media_gallery . " WHERE value_id = '" . $eachImageRemove['value_id'] . "'");
                }
            }
            $connection->query("DELETE FROM " . $catalog_product_entity_media_gallery_value . " WHERE entity_id = '" . $product_id . "'");
            $connection->query("DELETE FROM " . $catalog_product_entity_varchar . " WHERE entity_id = '" . $product_id . "' AND ( attribute_id = ( SELECT attribute_id FROM " . $eav_attribute . " AS eav WHERE eav.attribute_code = 'image' AND eav.entity_type_id ='4') OR attribute_id = ( SELECT attribute_id FROM " . $eav_attribute . " AS eav WHERE eav.attribute_code = 'small_image' AND eav.entity_type_id ='4') OR attribute_id = ( SELECT attribute_id FROM " . $eav_attribute . " AS eav WHERE eav.attribute_code = 'thumbnail' AND eav.entity_type_id ='4') OR attribute_id = ( SELECT attribute_id FROM " . $eav_attribute . " AS eav WHERE eav.attribute_code = 'media_gallery' AND eav.entity_type_id ='4'))");
        }

        if ($params['import_images_by_url'] == "true") {
            $arr = ["image", "small_image", "thumbnail", "gallery", "swatch_image"];
            foreach ($arr as $mediaAttributeCode) {
                if (isset($product[$mediaAttributeCode])) {
                    if ($product[$mediaAttributeCode] != "") {
                        if ($mediaAttributeCode == "gallery") {
                            $finalgalleryfiles = "";
                            $eachImageUrls     = explode(',', $product[$mediaAttributeCode]);
                            foreach ($eachImageUrls as $imageUrl) {
                                if ($imageUrl != "") {
                                    /** @var string $tmpDir */
                                    $importDir = $this->getMediaDirImportDir();
                                    /** create folder if it is not exists */
                                    $this->file->checkAndCreateFolder($importDir);
                                    /** @var string $newFileName */
                                    $newFileName = $importDir . '/' . baseName($imageUrl);
                                    /** read file from URL and copy it to the new destination */
                                    $result = $this->file->read($imageUrl, $newFileName);
                                    if ($result) {
                                        $finalgalleryfiles .= '/' . baseName($newFileName) . ",";
                                    } else {
                                        $this->helper->sendLog($this->helper->rowCount, 'gallery', 'Image URL: ' . $imageUrl . ' cannot be Found 404');
                                        $ProductImageGallery[$mediaAttributeCode] = 'no_selection';
                                    }
                                }
                            }
                            $ProductImageGallery[$mediaAttributeCode] = substr_replace($finalgalleryfiles, "", -1);
                        } else {
                            $imageUrl = $product[$mediaAttributeCode];
                            try {
                                /** @var string $tmpDir */
                                $importDir = $this->getMediaDirImportDir();
                                /** create folder if it is not exists */
                                $this->file->checkAndCreateFolder($importDir);
                                /** @var string $newFileName */
                                $newFileName = $importDir . '/' . baseName($imageUrl);
                                /** read file from URL and copy it to the new destination */
                                $result = $this->file->read($imageUrl, $newFileName);
                                if ($result) {
                                    $ProductImageGallery[$mediaAttributeCode] = '/' . baseName($newFileName);
                                } else {
                                    $this->helper->sendLog($this->helper->rowCount, $mediaAttributeCode, 'Image URL: ' . $imageUrl . ' cannot be Found 404');
                                    $ProductImageGallery[$mediaAttributeCode] = 'no_selection';
                                }
                            } catch (\Exception $e) {
                                throw new \Magento\Framework\Exception\LocalizedException(__("IMAGE IMPORT ERROR: " . $e->getMessage()), $e);
                            }
                        }
                    } else {
                        $ProductImageGallery[$mediaAttributeCode] = '';
                    }
                } else {
                    $ProductImageGallery[$mediaAttributeCode] = 'no_selection';
                }
            }
        } else {
            $ProductImageGallery = [
                'gallery'       => (isset($product['gallery'])) ? $product['gallery'] : 'no_selection',
                'image'         => (isset($product['image'])) ? $product['image'] : 'no_selection',
                'small_image'   => (isset($product['small_image'])) ? $product['small_image'] : 'no_selection',
                'thumbnail'     => (isset($product['thumbnail'])) ? $product['thumbnail'] : 'no_selection',
                'swatch_image'  => (isset($product['swatch_image'])) ? $product['swatch_image'] : 'no_selection',
                'gallery_label' => (isset($product['gallery_label'])) ? $product['gallery_label'] : 'no_selection'
            ];
        }

        return $ProductImageGallery;
    }

    public function TierPricedata($newProduct, $TPData, $product_sku, $params)
    {

        if ($newProduct) {
            //parse incoming tier prices string
            $incoming_tierps = explode('|', $TPData);
            $tps_toAdd       = [];
            $tierpricecount  = 0;

            foreach ($incoming_tierps as $tier_str) {

                if (empty($tier_str)) {
                    continue;
                }
                $tmp = [];
                $tmp = explode('=', $tier_str);
                if (!isset($tmp[1])) {
                    //throw new \Magento\Framework\Exception\LocalizedException(__('Invalid data in "tier_prices" column. No value for QTY found'));
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. No value for QTY found');
                    continue;
                }
                if (!isset($tmp[2])) {
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. No value for PRICE found');
                    continue;
                }
                if ($tmp[1] <= 0) {
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. QTY cannot be 0 using 1');
                    $tmp[1] = 1;
                }
                if ($tmp[1] == 0 && $tmp[2] == 0) {
                    continue;
                }

                $tps_toAdd[$tierpricecount] = [
                    'website_id' => 0, // !!!! this is hard-coded for now
                    #'website_id' => $tmp[0], // !!!! this is hard-coded for now
                    #'website_id' => $store->getWebsiteId(),
                    'cust_group' => $tmp[0],
                    'price_qty'  => $tmp[1],
                    'price'      => $tmp[2],
                    'delete'     => ''
                ];

                $tierpricecount++;
            }
        } else {

            if ($params['append_tier_prices'] != "true") {
                //get current product tier prices
                $existing_tps = [];
                $productModel = $this->_ProductModel->loadByAttribute('sku', $product_sku);
                if ($productModel) {
                    $productModelTier = $this->_ProductModel->load($productModel->getId());
                    if (!empty($productModelTier->getTierPrice())) {
                        $existing_tps = $productModelTier->getTierPrice();
                    } else {
                        if ($attribute = $productModelTier->getResource()->getAttribute('tier_price')) {
                            $attribute->getBackend()->afterLoad($productModelTier);
                            $existing_tps = $productModelTier->getData('tier_price');
                        }
                    }
                }
                $etp_lookup = [];
                //make a lookup array to prevent dup tiers by qty
                foreach ($existing_tps as $key => $etp) {
                    $etp_lookup[intval($etp['cust_group']) . "_" . intval($etp['price_qty'])] = $key;
                }
            }

            //parse incoming tier prices string
            $incoming_tierps = explode('|', $TPData);
            $tps_toAdd       = [];
            foreach ($incoming_tierps as $tier_str) {

                if (empty($tier_str)) {
                    continue;
                }

                $tmp = [];
                $tmp = explode('=', $tier_str);
                if (!isset($tmp[1])) {
                    //throw new \Magento\Framework\Exception\LocalizedException(__('Invalid data in "tier_prices" column. No value for QTY found'));
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. No value for QTY found');
                    continue;
                }
                if (!isset($tmp[2])) {
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. No value for PRICE found');
                    continue;
                }
                if ($tmp[1] <= 0) {
                    $this->helper->sendLog($this->helper->rowCount, 'tier_prices', 'Invalid data in column. QTY cannot be 0 using 1');
                    $tmp[1] = 1;
                }
                if ($tmp[1] == 0 && $tmp[2] == 0) {
                    continue;
                }

                if ($tmp[2] != "0.00") {
                    $this->tierPrice->add($product_sku, $tmp[0], $tmp[2], $tmp[1]);
                }

                if ($params['append_tier_prices'] != "true") {
                    //drop any existing tier values by qty
                    if (isset($etp_lookup[intval($tmp[0]) . "_" . intval($tmp[1])])) {
                        unset($existing_tps[$etp_lookup[intval($tmp[0]) . "_" . intval($tmp[1])]]);
                    }
                }
            }

            //remove the non matches them if we are not appending
            if ($params['append_tier_prices'] != "true") {
                foreach ($existing_tps as $key => $etp) {
                    $this->tierPrice->remove($product_sku, $etp['cust_group'], $etp['price_qty']);
                }
            }
        }

        return $tps_toAdd;
    }

    protected function addCategories($categories, $storeId, $params)
    {
        //$rootId = $store->getRootCategoryId();
        //$rootId = Mage::app()->getStore()->getRootCategoryId();
        //$rootId = 2; // our store's root category id
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $delimitertouse = "/";
        if ($params['root_catalog_id'] != "") {
            $rootId = $params['root_catalog_id'];
        } else {
            $rootId = 2;
        }
        if (!$rootId) {
            return [];
        }
        $rootPath = '1/' . $rootId;
        if (empty($this->_categoryCache[$storeId])) {

            $collection = $objectManager->create('Magento\Catalog\Model\Category')->getCollection()
                                        ->setStoreId($storeId)
                                        ->addAttributeToSelect('name');
            /*
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->setStore($store)
                ->addAttributeToSelect('name');
            */
            $collection->getSelect()->where("path like '" . $rootPath . "/%'");

            foreach ($collection as $cat) {
                $pathArr  = explode('/', $cat->getPath());
                $namePath = '';
                for ($i = 2, $l = sizeof($pathArr); $i < $l; $i++) {
                    //if(!is_null($collection->getItemById($pathArr[$i]))) { }
                    $name     = $collection->getItemById($pathArr[$i])->getName();
                    $namePath .= (empty($namePath) ? '' : '/') . trim($name);
                }
                $cat->setNamePath($namePath);
            }

            $cache = [];
            foreach ($collection as $cat) {
                $cache[$cat->getNamePath()] = $cat;
                $cat->unsNamePath();
            }
            $this->_categoryCache[$storeId] = $cache;
        }
        $cache =& $this->_categoryCache[$storeId];

        $catIds = [];
        //->setIsAnchor(1)
        //Delimiter is ' , ' so people can use ', ' in multiple categorynames
        foreach (explode(' , ', $categories) as $categoryPathStr) {
            //Remove this line if your using ^ vs / as delimiter for categories.. fix for cat names with / in them
            $categoryPathStr = preg_replace('#\s*/\s*#', '/', trim($categoryPathStr));
            if (!empty($cache[$categoryPathStr])) {
                $catIds[] = $cache[$categoryPathStr]->getId();
                continue;
            }
            $path     = $rootPath;
            $namePath = '';
            #foreach (explode($delimitertouse, $categoryPathStr) as $catName) {
            foreach (explode('/', $categoryPathStr) as $catName) {
                $namePath .= (empty($namePath) ? '' : '/') . $catName;
                if (empty($cache[$namePath])) {
                    $cat              = $objectManager->create('Magento\Catalog\Model\Category')
                                                      ->setStoreId($storeId)
                                                      ->setPath($path)
                                                      ->setName($catName)
                                                      ->setIsActive(1)
                                                      ->save();
                    $cache[$namePath] = $cat;
                }
                $catId = $cache[$namePath]->getId();
                $path  .= '/' . $catId;
                if ($catId) {
                    $catIds[] = $catId;
                }
            }
        }

        return join(',', $catIds);
    }

    public function attributeSetNamebyid($attributeSetName)
    {

        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSetCollection */
        $objectManager          = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $entityType             = $objectManager->create('\Magento\Eav\Model\Entity\Type')->loadByCode('catalog_product');
        $attributeSetCollection = $objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection');
        $attributeSetCollection->addFilter('attribute_set_name', $attributeSetName);
        $attributeSetCollection->addFilter('entity_type_id', $entityType->getId());
        $attributeSetCollection->setOrder('attribute_set_id'); // descending is default value
        $attributeSetCollection->setPageSize(1);
        $attributeSetCollection->load();
        /** @var \Magento\Eav\Model\Entity\Attribute\Set $attributeSet */
        $attributeSet = $attributeSetCollection->fetchItem();
        if ($attributeSet) {
            return $attributeSet->getId();
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('Attribute Set "' . $attributeSetName . '" does NOT exist.'));
        }
    }

    public function websitenamebyid($params, $webid, $product_sku)
    {

        if ($params['append_websites'] == "true") {
            $productWebsites = $this->_ProductModel->loadByAttribute('sku', $product_sku);
            try {
                $websiteIds = $productWebsites->getWebsiteIds();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__("SKU: " . $product_sku . " is not assoicated to any website. Please check product or set append websites false"), $e);
            }
            if (!is_array($websiteIds)) {
                $websiteIds = [];
            }
        } else {
            $websiteIds = [];
        }
        if ($webid != "") {
            $webidX = explode(',', $webid);
            foreach ($webidX as $webids) {
                $website = $this->website->load($webids);
                if (!in_array($website->getId(), $websiteIds)) {
                    $websiteIds[] = $website->getId();
                }
            }
        } else {
            $websiteIds = [];
            $this->helper->sendLog($this->helper->rowCount, 'websites', 'The column is empty. If the intended result is to remove the product from all websites then disregard');
        }

        return $websiteIds;
    }

    /**
     * Checks to see if we have already stored option values for the attribute
     *
     * @param string $attributeCode
     *
     * @return bool
     */
    private function hasStoredOptionValuesForAttribute($attributeCode)
    {
        return array_key_exists($attributeCode, $this->optionValues);
    }

    /**
     * Creates a placeholder array for the attribute that we will store values for
     *
     * @param $attributeCode
     *
     * @return $this
     */
    private function initAttributeOptionValues($attributeCode)
    {
        if (!$this->hasStoredOptionValuesForAttribute($attributeCode)) {
            $this->optionValues[$attributeCode] = [];
        }

        return $this;
    }

    /**
     * Stores the attribute option value so we don't have to pull from db again
     *
     * @param string $attributeCode
     * @param string $optionValue
     *
     * @return $this
     */
    private function addAttributeOptionValue($attributeCode, $optionValue)
    {
        $this->initAttributeOptionValues($attributeCode);

        $optionValue = strtolower($optionValue);
        if (!in_array($optionValue, $this->optionValues[$attributeCode])) {
            $this->optionValues[$attributeCode][] = $optionValue;
        }

        return $this;
    }

    /**
     * Pulls all options for a specific attribute and stores them
     *
     * @param string $attributeCode
     *
     * @return $this
     */
    private function addAttributeOptionValues($attributeCode)
    {
        if (!$this->hasStoredOptionValuesForAttribute($attributeCode)) {
			$this->initAttributeOptionValues($attributeCode);
            $values = $this->optionManager->getItems($attributeCode);
            foreach ($values as $value) {
                if (trim($value['label']) !== '') {
                    $this->addAttributeOptionValue($attributeCode, $value['label']);
                }
            }
        }

        return $this;
    }

    /**
     * Checks to see if the attribute option value already exists
     *
     * @param string $attributeCode
     * @param string $optionValue
     *
     * @return bool
     */
    private function checkAttributeOptionValue($attributeCode, $optionValue)
    {
        $this->addAttributeOptionValues($attributeCode);

        return in_array(strtolower($optionValue), $this->optionValues[$attributeCode]);
    }
}