<?php

/**
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Model\Data\Import;

#use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
#use Magento\Framework\Filesystem;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;

/**
 *  CSV Import Handler Bundle Product
 */
 
class BundleProduct{

	protected $ProductFactory;
	
    public function __construct(
        ResourceConnection $resource,
		\Magento\Catalog\Model\ProductFactory $ProductFactory,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $ProductLinkInterfaceFactory,
		\Magento\Catalog\Model\Product $Product,
		\CommerceExtensions\ProductImportExport\Helper\Data $helper
    ) {
         // prevent admin store from loading
         $this->_resource = $resource;
		 $this->ProductFactory = $ProductFactory;
		 $this->ProductLinkInterfaceFactory = $ProductLinkInterfaceFactory;
		 $this->Product = $Product;
         $this->helper = $helper;
    }
	
	public function BundleProductData($rowCount,$newProduct,$SetProductData,$params,$ProcuctData,$ProductAttributeData,$ProductImageGallery,$ProductStockdata,$ProductSupperAttribute,$ProductCustomOption){
	
	$this->helper->rowCount = $rowCount;
	
	//UPDATE PRODUCT ONLY [START]
	$allowUpdateOnly = false;
	if(!$SetProductData || empty($SetProductData)) {
		$SetProductData = $this->ProductFactory->create();
	}
	if($newProduct && $params['update_products_only'] == "true") {
		$allowUpdateOnly = true;
	} 
	
	//UPDATE PRODUCT ONLY [END]
	if ($allowUpdateOnly == false) {
		
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('import');
		$imagePath = "/import";
		
		if(empty($ProductAttributeData['url_key'])) {
			unset($ProductAttributeData['url_key']);
		} else {
			//this solve the error:  URL key for specified store already exists. 
			$urlrewrite = $this->helper->checkUrlKey($ProcuctData['store_id'], $ProductAttributeData['url_key']);
			if ($urlrewrite->getId()) {
				for ($addNumberUrlKey = 0; $addNumberUrlKey <= 10; $addNumberUrlKey++) {
					$addToKey = $addNumberUrlKey + 1;
					$newUrlKey = $ProductAttributeData['url_key'] . '-' . $addToKey;
					$urlrewriteCheck = $this->helper->checkUrlKey($ProcuctData['store_id'], $newUrlKey);
					if (!$urlrewriteCheck->getId()) {
						break;
					}
				}
				$ProductAttributeData['url_key'] = $newUrlKey;
				$ProductAttributeData['url_path'] = $newUrlKey;
			}
		}
		
		if(empty($ProductAttributeData['url_path'])) {
			unset($ProductAttributeData['url_path']);
		}
		$SetProductData->setSku($ProcuctData['sku']);
		$SetProductData->setStoreId($ProcuctData['store_id']);
		#if(isset($ProcuctData['name'])) { $SetProductData->setName($ProcuctData['name']); }
		if(isset($ProcuctData['websites'])) { $SetProductData->setWebsiteIds($ProcuctData['websites']); }
		if(isset($ProcuctData['attribute_set'])) { $SetProductData->setAttributeSetId($ProcuctData['attribute_set']); }
		if(isset($ProcuctData['prodtype'])) { $SetProductData->setTypeId($ProcuctData['prodtype']); }
		if(isset($ProcuctData['category_ids'])) { 
			if($ProcuctData['category_ids'] == "remove") { 
				$SetProductData->setCategoryIds(array()); 
			} else if($ProcuctData['category_ids'] != "") { 
				$SetProductData->setCategoryIds($ProcuctData['category_ids']);
			}
		}
		
		$SetProductData->addData($ProductAttributeData);
		
		if($newProduct || $params['reimport_images'] == "true") { 
			//media images
			$_productImages = array(
				'media_gallery'       => ($ProductImageGallery['gallery']!="") ? $ProductImageGallery['gallery'] : 'no_selection',
				'image'       => ($ProductImageGallery['image']!="") ? $ProductImageGallery['image'] : 'no_selection',
				'small_image'       => ($ProductImageGallery['small_image']!="") ? $ProductImageGallery['small_image'] : 'no_selection',
				'thumbnail'       => ($ProductImageGallery['thumbnail']!="") ? $ProductImageGallery['thumbnail'] : 'no_selection',
				'swatch_image'       => ($ProductImageGallery['swatch_image']!="") ? $ProductImageGallery['swatch_image'] : 'no_selection'
		
			);
			//create array of images with duplicates combind
			$imageArray = array();
			foreach ($_productImages as $columnName => $imageName) {
				$imageArray = $this->helper->addImage($imageName, $columnName, $imageArray);
			}
			
			foreach ($imageArray as $ImageFile => $imageColumns) {
				if($ImageFile != "no_selection") {
					$SetProductData->addImageToMediaGallery($imagePath . $ImageFile, $imageColumns, false, false);
				} else {
					foreach( $imageColumns as $mediaAttribute ) {
						$SetProductData->setData($mediaAttribute, 'no_selection');
					}
				}
			}
		}
		
		if($ProductStockdata!=""){ $SetProductData->setStockData($ProductStockdata); }
		
		if(!empty($ProductCustomOption)) {
			#$SetProductData->setProductOptions($ProductCustomOption);
			$SetProductData->setCanSaveCustomOptions(true);
			#$SetProductData->save(); 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			foreach ($ProductCustomOption as $arrayOption) {
					$option = $objectManager->create('\Magento\Catalog\Model\Product\Option')
					->setProductId($SetProductData->getId())
					->setStoreId($ProcuctData['store_id'])
					->addData($arrayOption);
					$option->save();
					$SetProductData->addOption($option);
			}
			$SetProductData->save();
		} 
			 	
		$relatedProductData = array();
		$upSellProductData = array();
		$crossSellProductData = array();
		
		if($ProductSupperAttribute['related']!=""){ $relatedProductData = $this->helper->AppendRelatedProduct($ProductSupperAttribute['related'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['upsell']!=""){ $upSellProductData = $this->helper->AppendUpsellProduct($ProductSupperAttribute['upsell'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['crosssell']!=""){ $crossSellProductData = $this->helper->AppendCrossSellProduct($ProductSupperAttribute['crosssell'] , $ProcuctData['sku']); }
		
		if(!empty($relatedProductData) || !empty($upSellProductData) || !empty($crossSellProductData)) {
			$allProductLinks = array_merge($relatedProductData, $upSellProductData, $crossSellProductData);
			$SetProductData->setProductLinks($allProductLinks);
			$SetProductData->save();
		}
		
		//THIS IS FOR BUNDLE PRODUCTS
		if($ProductSupperAttribute['bundle_options'] == "") {
			//throw new \Magento\Framework\Exception\LocalizedException(__('SKU: '.$ProcuctData['sku'].' ERROR column "bundle_options" is empty. If you are just updating other data simply remove the column'));
			$this->helper->sendLog($this->helper->rowCount,'bundle_options','The column is empty. If you are just updating other data simply remove the column');
		} else {
			if ($newProduct) {
				$optionscount=0;
				$items = array();
				//THIS IS FOR BUNDLE OPTIONS
				$commadelimiteddata = explode('|',$ProductSupperAttribute['bundle_options']);
				foreach ($commadelimiteddata as $data) {
					$configBundleOptionsCodes = $this->helper->userCSVDataAsArray($data);
					$titlebundleselection = ucfirst(str_replace('_',' ',$configBundleOptionsCodes[0]));
					$items[$optionscount]['title'] = $titlebundleselection;
					$items[$optionscount]['default_title'] = $titlebundleselection;
					if(isset($configBundleOptionsCodes[1])) {
						$items[$optionscount]['type'] = $configBundleOptionsCodes[1];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "type"');
					}
					if(isset($configBundleOptionsCodes[2])) {
						$items[$optionscount]['required'] = $configBundleOptionsCodes[2];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "required"');
					}
					if(isset($configBundleOptionsCodes[3])) {
						$items[$optionscount]['position'] = $configBundleOptionsCodes[3];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "position"');
					}
					$items[$optionscount]['delete'] = 0;
					
					if ($items) {
						$SetProductData->setBundleOptionsData($items);
						#$SetProductData->save();
					}
					$optionscount+=1;
					
					$selections = array();
					$bundleConfigData = array();
					$optionscountselection=0;
					//THIS IS FOR BUNDLE SELECTIONS
					if($ProductSupperAttribute['bundle_selections'] !="") {
						$commadelimiteddataselections = explode('|',$ProductSupperAttribute['bundle_selections']);
						foreach ($commadelimiteddataselections as $selection) {
							$configBundleSelectionCodes = $this->helper->userCSVDataAsArray($selection);
							$selectionscount=0;
							foreach ($configBundleSelectionCodes as $selectionItem) {
								$bundleConfigData = explode(':',$selectionItem);
								if(isset($bundleConfigData[0])) {
									if($bundleSelection_product_id = $SetProductData->getIdBySku($bundleConfigData[0])) {
										$selections[$optionscountselection][$selectionscount]['product_id'] = $bundleSelection_product_id;
									} else {
										$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The sku "'.$bundleConfigData[0].'" is not found and cannot be assoicated with the bundle');
									}
								} else {
								    $this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "sku"');
								}
								if(isset($bundleConfigData[1])) {
									$selections[$optionscountselection][$selectionscount]['selection_price_type'] = $bundleConfigData[1];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_price_type"');
	   							}
								if(isset($bundleConfigData[2])) {
				  					$selections[$optionscountselection][$selectionscount]['selection_price_value'] = $bundleConfigData[2];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_price_value"');
	   							}
								if(isset($bundleConfigData[3])) {
									$selections[$optionscountselection][$selectionscount]['is_default'] = $bundleConfigData[3];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "is_default"');
	   							}
								if(isset($bundleConfigData[4]) && $bundleConfigData[4] != '') {
									$selections[$optionscountselection][$selectionscount]['selection_qty'] = $bundleConfigData[4];
									$selections[$optionscountselection][$selectionscount]['selection_can_change_qty'] = $bundleConfigData[5];
								} else {
									$selections[$optionscountselection][$selectionscount]['selection_qty'] = '';
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_qty"');
								}
								if(isset($bundleConfigData[6]) && $bundleConfigData[6] != '') {
									$selections[$optionscountselection][$selectionscount]['position'] = $bundleConfigData[6];
								}
								$selections[$optionscountselection][$selectionscount]['delete'] = 0;
								$selectionscount+=1;
							}
							$optionscountselection+=1;
						}
						if ($selections) {
							$SetProductData->setBundleSelectionsData($selections);
						}
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The column is empty. If you are just updating other data simply disregard');
	   				}
				}
				

				if ($SetProductData->getPriceType() == '0') {
					$SetProductData->setCanSaveCustomOptions(true);
					if ($customOptions = $SetProductData->getProductOptions()) {
						foreach ($customOptions as $key => $customOption) {
							$customOptions[$key]['is_delete'] = 1;
						}
						$SetProductData->setProductOptions($customOptions);
					}
				}
			
				if ($SetProductData->getBundleOptionsData()) {
					$options = [];
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$productRepository = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
					foreach ($SetProductData->getBundleOptionsData() as $key => $optionData) {
						if (!(bool)$optionData['delete']) {
							$option = $objectManager->create('\Magento\Bundle\Api\Data\OptionInterfaceFactory')
								->create(['data' => $optionData]);
							$option->setSku($SetProductData->getSku());
							$option->setOptionId(null);
							$links = [];
							$bundleLinks = $SetProductData->getBundleSelectionsData();
							if (!empty($bundleLinks[$key])) {
								foreach ($bundleLinks[$key] as $linkData) {
									if (!(bool)$linkData['delete']) {
										/** @var \Magento\Bundle\Api\Data\LinkInterface$link */
										$link = $objectManager->create('\Magento\Bundle\Api\Data\LinkInterfaceFactory')
											->create(['data' => $linkData]);
										$linkProduct = $productRepository->getById($linkData['product_id']);
										$link->setSku($linkProduct->getSku());
										$link->setQty($linkData['selection_qty']);
										if (isset($linkData['selection_can_change_qty'])) {
											$link->setCanChangeQuantity($linkData['selection_can_change_qty']);
										}
										$links[] = $link;
									}
								}
								$option->setProductLinks($links);
								$options[] = $option;
							}
						}
					}
					$extension = $SetProductData->getExtensionAttributes();
					$extension->setBundleProductOptions($options);
					$SetProductData->setExtensionAttributes($extension);
				}
				#$SetProductData->setCanSaveBundleSelections();
			} else {
			
				$optionscount=0;
				$items = array();
				//THIS IS FOR BUNDLE OPTIONS
				$commadelimiteddata = explode('|',$ProductSupperAttribute['bundle_options']);
				foreach ($commadelimiteddata as $data) {
					$configBundleOptionsCodes = $this->helper->userCSVDataAsArray($data);
					$titlebundleselection = ucfirst(str_replace('_',' ',$configBundleOptionsCodes[0]));
					$items[$optionscount]['title'] = $titlebundleselection;
					$items[$optionscount]['default_title'] = $titlebundleselection;
					if(isset($configBundleOptionsCodes[1])) {
						$items[$optionscount]['type'] = $configBundleOptionsCodes[1];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "type"');
					}
					if(isset($configBundleOptionsCodes[2])) {
						$items[$optionscount]['required'] = $configBundleOptionsCodes[2];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "required"');
					}
					if(isset($configBundleOptionsCodes[3])) {
						$items[$optionscount]['position'] = $configBundleOptionsCodes[3];
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_options','The format is incorrect no value found for "position"');
					}
					$items[$optionscount]['delete'] = 0;
					
					$options_id = "";
					$product2 = $this->Product->loadByAttribute('sku', $ProcuctData['sku']);
					#$options_id = $product2->getOptionId();
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
				    #$catalog_product_bundle_option_value = $_resource->getTableName('catalog_product_bundle_option_value');
					$_bundleOption = $objectManager->create('\Magento\Bundle\Model\Option');
				    $optionModel = $_bundleOption->getResourceCollection()->setProductIdFilter($product2->getId());
					$_resource = $this->_resource;
					$connection = $_resource->getConnection();
					$catalog_product_bundle_option_value = $_resource->getTableName('catalog_product_bundle_option_value');
					
					foreach($optionModel as $eachOption) {
						
						$selectOptionID = "SELECT title FROM ".$catalog_product_bundle_option_value." WHERE option_id = ".$eachOption->getData('option_id')."";
						$Optiondatarows = $connection->query($selectOptionID);
						while ($Option_row = $Optiondatarows->fetch()) {
							$finaltitle = $Option_row['title'];
						}
						if($titlebundleselection == $finaltitle) {
			 				#throw new \Magento\Framework\Exception\LocalizedException(__('MATCH OPTION ID: ' .$eachOption->getData('option_id')));
							$options_id = $eachOption->getData('option_id');
							$items[$optionscount]['option_id'] = $eachOption->getData('option_id');
						}
					}
					
					if ($items) {
						$SetProductData->setBundleOptionsData($items);
						#$SetProductData->save();
					} 
					
					$optionscount+=1;
					$selections = array();
					$bundleConfigData = array();
					$optionscountselection=0;
					//THIS IS FOR BUNDLE SELECTIONS
					if($ProductSupperAttribute['bundle_selections'] !="") {
						$commadelimiteddataselections = explode('|',$ProductSupperAttribute['bundle_selections']);
						foreach ($commadelimiteddataselections as $selection) {
							$configBundleSelectionCodes = $this->helper->userCSVDataAsArray($selection);
							$selectionscount=0;
							foreach ($configBundleSelectionCodes as $selectionItem) {
								$bundleConfigData = explode(':',$selectionItem);
								if($options_id !="") { $selections[$optionscountselection][$selectionscount]['option_id'] = $options_id; }
								
								if(isset($bundleConfigData[0])) {
									if($bundleSelection_product_id = $SetProductData->getIdBySku($bundleConfigData[0])) {
										$selections[$optionscountselection][$selectionscount]['product_id'] = $bundleSelection_product_id;
									} else {
										$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The sku "'.$bundleConfigData[0].'" is not found and cannot be assoicated with the bundle');
									}
								} else {
								    $this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "sku"');
								}
								if(isset($bundleConfigData[1])) {
									$selections[$optionscountselection][$selectionscount]['selection_price_type'] = $bundleConfigData[1];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_price_type"');
	   							}
								if(isset($bundleConfigData[2])) {
				  					$selections[$optionscountselection][$selectionscount]['selection_price_value'] = $bundleConfigData[2];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_price_value"');
	   							}
								if(isset($bundleConfigData[3])) {
									$selections[$optionscountselection][$selectionscount]['is_default'] = $bundleConfigData[3];
								} else {
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "is_default"');
	   							}
								if(isset($bundleConfigData[4]) && $bundleConfigData[4] != '') {
									$selections[$optionscountselection][$selectionscount]['selection_qty'] = $bundleConfigData[4];
									$selections[$optionscountselection][$selectionscount]['selection_can_change_qty'] = $bundleConfigData[5];
								} else {
									$selections[$optionscountselection][$selectionscount]['selection_qty'] = '';
									$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The format is incorrect no value found for "selection_qty"');
								}
								if(isset($bundleConfigData[6]) && $bundleConfigData[6] != '') {
									$selections[$optionscountselection][$selectionscount]['position'] = $bundleConfigData[6];
								}
								$selections[$optionscountselection][$selectionscount]['delete'] = 0;
								$selectionscount+=1;
							}
							$optionscountselection+=1;
						}
						if ($selections) {
							$SetProductData->setBundleSelectionsData($selections);
						}
					} else {
						$this->helper->sendLog($this->helper->rowCount,'bundle_selection','The column is empty. If you are just updating other data simply disregard');
	   				}
				}
				

				if ($SetProductData->getPriceType() == '0') {
					$SetProductData->setCanSaveCustomOptions(true);
					if ($customOptions = $SetProductData->getProductOptions()) {
						foreach ($customOptions as $key => $customOption) {
							$customOptions[$key]['is_delete'] = 1;
						}
						$SetProductData->setProductOptions($customOptions);
					}
				}
				if ($SetProductData->getBundleOptionsData()) {
					
					
					$options = [];
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$productRepository = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
					foreach ($SetProductData->getBundleOptionsData() as $key => $optionData) {
						if (!(bool)$optionData['delete']) {
							$option = $objectManager->create('\Magento\Bundle\Api\Data\OptionInterfaceFactory')
								->create(['data' => $optionData]);
							$option->setSku($SetProductData->getSku());
							$option->setOptionId(null);
							$links = [];
							$bundleLinks = $SetProductData->getBundleSelectionsData();
							
							if (!empty($bundleLinks[$key])) {
								foreach ($bundleLinks[$key] as $linkData) {
									if (!(bool)$linkData['delete']) {
										/** @var \Magento\Bundle\Api\Data\LinkInterface$link */
										$link = $objectManager->create('\Magento\Bundle\Api\Data\LinkInterfaceFactory')
											->create(['data' => $linkData]);
										$linkProduct = $productRepository->getById($linkData['product_id']);
										$link->setSku($linkProduct->getSku());
										$link->setQty($linkData['selection_qty']);
										if (isset($linkData['selection_can_change_qty'])) {
											$link->setCanChangeQuantity($linkData['selection_can_change_qty']);
										}
										$links[] = $link;
									}
								}
								$option->setProductLinks($links);
								$options[] = $option;
							}
						}
					}
					$extension = $SetProductData->getExtensionAttributes();
					$extension->setBundleProductOptions($options);
					$SetProductData->setExtensionAttributes($extension);
				}
			}
		}
		
		try {
			$SetProductData->save(); 
		}
		catch (\Exception $e) { 
			throw new \Magento\Framework\Exception\LocalizedException(__('SKU: '.$ProcuctData['sku'].' ERROR : '. $e));
		}		
			
		if(isset($ProductSupperAttribute['tier_prices'])) { 
			#print_r($ProductSupperAttribute['tier_prices']);
			if($ProductSupperAttribute['tier_prices']!=""){ $SetProductData->setTierPrice($ProductSupperAttribute['tier_prices'])->save(); }
		}
		
	  }//END UPDATE ONLY CHECK
	  return $this->helper->msgtoreturn;
	}
	
}