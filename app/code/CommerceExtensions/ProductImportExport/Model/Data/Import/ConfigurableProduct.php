<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Model\Data\Import;

//use Magento\Framework\App\Filesystem\DirectoryList;
//use Magento\Framework\Filesystem;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 *  CSV Import Handler Configurable Product
 */
 
class ConfigurableProduct{

	protected $ProductFactory;
	
    public function __construct(
		\Magento\Catalog\Model\ProductFactory $ProductFactory,
		\Magento\Catalog\Model\Product $Product,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $ProductLinkInterfaceFactory,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute $Attribute,
		\Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $ConfigurableProduct,
		\Magento\Catalog\Model\ResourceModel\Product\Collection $ProductCollection,
		\CommerceExtensions\ProductImportExport\Helper\Data $helper
		
    ) {
         // prevent admin store from loading
		 $this->ProductFactory = $ProductFactory;
		 //$this->_filesystem = $filesystem;
		 $this->Product = $Product;
		 $this->ProductLinkInterfaceFactory = $ProductLinkInterfaceFactory;
		 $this->Attribute = $Attribute;
		 $this->ConfigurableProduct = $ConfigurableProduct;
		 $this->ProductCollection = $ProductCollection;
         $this->helper = $helper;
    }
	
	public function ConfigurableProductData($rowCount,$newProduct,$SetProductData,$params,$ProcuctData,$ProductAttributeData,$ProductImageGallery,$ProductStockdata,$ProductSupperAttribute){
	
	$this->helper->rowCount = $rowCount;
	//UPDATE PRODUCT ONLY [START]
	$allowUpdateOnly = false;
	if(!$SetProductData || empty($SetProductData)) {
		$SetProductData = $this->ProductFactory->create();
	}
	if($newProduct && $params['update_products_only'] == "true") {
		$allowUpdateOnly = true;
	}
	//UPDATE PRODUCT ONLY [END]
	
	if ($allowUpdateOnly == false) {
		
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('catalog').'/product';
		$imagePath = "/import";
		
		if(empty($ProductAttributeData['url_key'])) {
			unset($ProductAttributeData['url_key']);
		} else {
			//this solve the error:  URL key for specified store already exists. 
			$urlrewrite = $this->helper->checkUrlKey($ProcuctData['store_id'], $ProductAttributeData['url_key']);
			if ($urlrewrite->getId()) {
				for ($addNumberUrlKey = 0; $addNumberUrlKey <= 10; $addNumberUrlKey++) {
					$addToKey = $addNumberUrlKey + 1;
					$newUrlKey = $ProductAttributeData['url_key'] . '-' . $addToKey;
					$urlrewriteCheck = $this->helper->checkUrlKey($ProcuctData['store_id'], $newUrlKey);
					if (!$urlrewriteCheck->getId()) {
						break;
					}
				}
				$ProductAttributeData['url_key'] = $newUrlKey;
				$ProductAttributeData['url_path'] = $newUrlKey;
			}
		}
		if(empty($ProductAttributeData['url_path'])) {
			unset($ProductAttributeData['url_path']);
		}
		
		
		$SetProductData->setSku($ProcuctData['sku']);
		$SetProductData->setStoreId($ProcuctData['store_id']);
		#if(isset($ProcuctData['name'])) { $SetProductData->setName($ProcuctData['name']); }
		if(isset($ProcuctData['websites'])) { $SetProductData->setWebsiteIds($ProcuctData['websites']); }
		if(isset($ProcuctData['attribute_set'])) { $SetProductData->setAttributeSetId($ProcuctData['attribute_set']); }
		if(isset($ProcuctData['prodtype'])) { $SetProductData->setTypeId($ProcuctData['prodtype']); }
		if(isset($ProcuctData['category_ids'])) { 
			if($ProcuctData['category_ids'] == "remove") { 
				$SetProductData->setCategoryIds(array()); 
			} else if($ProcuctData['category_ids'] != "") { 
				$SetProductData->setCategoryIds($ProcuctData['category_ids']);
			}
		}
		
		$SetProductData->addData($ProductAttributeData);
		
		if($newProduct || $params['reimport_images'] == "true") { 
			//media images
			$_productImages = array(
				'media_gallery'       => ($ProductImageGallery['gallery']!="") ? $ProductImageGallery['gallery'] : 'no_selection',
				'image'       => ($ProductImageGallery['image']!="") ? $ProductImageGallery['image'] : 'no_selection',
				'small_image'       => ($ProductImageGallery['small_image']!="") ? $ProductImageGallery['small_image'] : 'no_selection',
				'thumbnail'       => ($ProductImageGallery['thumbnail']!="") ? $ProductImageGallery['thumbnail'] : 'no_selection',
				'swatch_image'       => ($ProductImageGallery['swatch_image']!="") ? $ProductImageGallery['swatch_image'] : 'no_selection'
		
			);
			//create array of images with duplicates combind
			$imageArray = array();
			foreach ($_productImages as $columnName => $imageName) {
				$imageArray = $this->helper->addImage($imageName, $columnName, $imageArray);
			}
			
			foreach ($imageArray as $ImageFile => $imageColumns) {
				if($ImageFile != "no_selection") {
					$SetProductData->addImageToMediaGallery($imagePath . $ImageFile, $imageColumns, false, false);
				} else {
					foreach( $imageColumns as $mediaAttribute ) {
						$SetProductData->setData($mediaAttribute, 'no_selection');
					}
				}
			}
		}
		
		$SetProductData->setCanSaveConfigurableAttributes(true);
		$SetProductData->setCanSaveCustomOptions(true);
	 
		$cProductTypeInstance = $SetProductData->getTypeInstance();
		if(isset($ProductAttributeData['config_attributes'])) {
			$attribute_ids = $this->getConfigAttributesId($ProductAttributeData['config_attributes']);
			if(is_array($attribute_ids)) {
				$cProductTypeInstance->setUsedProductAttributeIds($attribute_ids,$SetProductData);
				$attributes_array = $cProductTypeInstance->getConfigurableAttributesAsArray($SetProductData);
				
				foreach($attributes_array as $key => $attribute_array) 
				{
					$attributes_array[$key]['use_default'] = 1;
					$attributes_array[$key]['position'] = 0;
			
					if (isset($attribute_array['frontend_label']))
					{
						$attributes_array[$key]['label'] = $attribute_array['frontend_label'];
					}
					else {
						$attributes_array[$key]['label'] = $attribute_array['attribute_code'];
					}
				}
				// Add it back to the configurable product..
				$SetProductData->setConfigurableAttributesData($attributes_array);	
			}
		}
		
		if($ProductStockdata!=""){ $SetProductData->setStockData($ProductStockdata); }
		
		try {
			$SetProductData->save();
		}
		catch (\Exception $e) { 
			throw new \Magento\Framework\Exception\LocalizedException(__('SKU: '.$ProcuctData['sku'].' ERROR : '. $e));
		}
		
		$relatedProductData = array();
		$upSellProductData = array();
		$crossSellProductData = array();
		
		if($ProductSupperAttribute['related']!=""){ $relatedProductData = $this->helper->AppendRelatedProduct($ProductSupperAttribute['related'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['upsell']!=""){ $upSellProductData = $this->helper->AppendUpsellProduct($ProductSupperAttribute['upsell'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['crosssell']!=""){ $crossSellProductData = $this->helper->AppendCrossSellProduct($ProductSupperAttribute['crosssell'] , $ProcuctData['sku']); }
		
		if(!empty($relatedProductData) || !empty($upSellProductData) || !empty($crossSellProductData)) {
			$allProductLinks = array_merge($relatedProductData, $upSellProductData, $crossSellProductData);
			$SetProductData->setProductLinks($allProductLinks);
			$SetProductData->save();
		}
		
		if(isset($ProductAttributeData['additional_attributes'])){
			$this->SetDataTosimpleProducts($ProductAttributeData['additional_attributes']);
		}
		$ConfigurableId = $ProcuctData['sku'];
		$this->SimpleAssociatedWithConfigureable($ProductSupperAttribute['associated'],$ConfigurableId);
		
	
	  }//END UPDATE ONLY CHECK
	  return $this->helper->msgtoreturn;
	}
	
	public function SetDataTosimpleProducts($ProductsFieldArray){
	    $Atdata = explode(',', $ProductsFieldArray);
		foreach($Atdata as $data){
		if(!empty($data) && $data !="")
			$pdata = explode('=', $data);
			if(isset($pdata[1])) {
				$AttributeCol = $this->Product->getResource()->getAttribute($pdata[1]);
				$OptionId = $AttributeCol->getSource()->getOptionId($pdata[2]);
				$ProductId = $this->Product->getResource()->getIdBySku($pdata[0]);
				$product = $this->Product->load($ProductId);
				$product->setData($pdata[1] , $OptionId);
				$product->getResource()->saveAttribute($product, $pdata[1]);
			}
		}
	
	}
	
	public function SimpleAssociatedWithConfigureable($childProduct, $configurableProduct){
		if($childProduct!="") {
			$cpId = $this->Product->getResource()->getIdBySku($configurableProduct);
			$Products_sku = explode(',',$childProduct);
			$ProductId = array();
			foreach($Products_sku as $sku){
				if($sku){
					if(!$this->Product->getResource()->getIdBySku($sku)) {
						//throw new \Magento\Framework\Exception\LocalizedException(__('The Following sku: "' . $sku. '" does NOT exist and cannot be used to create a configurable product. However you do have it listed as a sku in your "associated" column'));
						$this->helper->sendLog($this->helper->rowCount,'associated','The Following sku: "' . $sku. '" does NOT exist and cannot be used to create a configurable product. However you do have it listed as a sku in your "associated" column');
					} else {
						$ProductId[] = $this->Product->getResource()->getIdBySku($sku);
						$ProductId = array_unique($ProductId); //remove any duplicates if customer puts same sku twice in assoicated field
					}
				}
			}
			$productModel = $this->Product->load($cpId);
			$this->ConfigurableProduct->saveProducts($productModel,$ProductId);
		} else {
			$this->helper->sendLog($this->helper->rowCount,'associated',"The column is empty and must contain the sku's in a comma delimited from that you want to associate to a configurable product");		
		}
	}
	
	public function getConfigAttributesId($AttributesCode){
		if($AttributesCode!="") {
			$Codes = explode(',', $AttributesCode);
			$AttributeId = array();
			foreach($Codes as $Code){ 
				if($this->Attribute->getIdByCode('catalog_product',trim($Code))) {
					$AttributeId[] = $this->Attribute->getIdByCode('catalog_product',trim($Code)); //getIdByCode($entityType, $code)
				} else {
					$this->helper->sendLog($this->helper->rowCount,'config_attributes','The column contains the following attribute "'.$Code.'" and it does NOT exist in the install');
				}
			}	
			return $AttributeId;
		} else {
			//throw new \Magento\Framework\Exception\LocalizedException(__('The column "config_attributes" is empty and should contain the attribute names you want to use to create a configurable product e.g "color,size"'));
			$this->helper->sendLog($this->helper->rowCount,'config_attributes','The column is empty and should contain the attribute names you want to use to create a configurable product e.g "color,size"');
		}
	}
}