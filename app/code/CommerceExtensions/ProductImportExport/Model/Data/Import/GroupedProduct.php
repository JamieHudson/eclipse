<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Model\Data\Import;

//use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;

/**
 *  CSV Import Handler Grouped Product
 */
 
class GroupedProduct{

	protected $_filesystem;
		
	protected $ProductFactory;
	
    public function __construct(
		\Magento\Catalog\Model\ProductFactory $ProductFactory,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $ProductLinkInterfaceFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $ProductRepositoryInterface,
		\Magento\Catalog\Model\Product $Product,
		\CommerceExtensions\ProductImportExport\Helper\Data $helper
    ) {
         // prevent admin store from loading
		 $this->ProductFactory = $ProductFactory;
		 $this->ProductLinkInterfaceFactory = $ProductLinkInterfaceFactory;
		 $this->ProductRepositoryInterface = $ProductRepositoryInterface;
		 $this->Product = $Product;
         $this->helper = $helper;
    }
	
	public function GroupedProductData($rowCount,$newProduct,$SetProductData,$params,$ProcuctData,$ProductAttributeData,$ProductImageGallery,$ProductStockdata,$ProductSupperAttribute){
	
	$this->helper->rowCount = $rowCount;
	//UPDATE PRODUCT ONLY [START]
	$allowUpdateOnly = false;
	if(!$SetProductData || empty($SetProductData)) {
		$SetProductData = $this->ProductFactory->create();
	}
	if($newProduct && $params['update_products_only'] == "true") {
		$allowUpdateOnly = true;
	}
	//UPDATE PRODUCT ONLY [END]
	
	if ($allowUpdateOnly == false) {
			
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('catalog').'/product';
		$imagePath = "/import";
		
		if(empty($ProductAttributeData['url_key'])) {
			unset($ProductAttributeData['url_key']);
		} else {
			//this solve the error:  URL key for specified store already exists. 
			$urlrewrite = $this->helper->checkUrlKey($ProcuctData['store_id'], $ProductAttributeData['url_key']);
			if ($urlrewrite->getId()) {
				for ($addNumberUrlKey = 0; $addNumberUrlKey <= 10; $addNumberUrlKey++) {
					$addToKey = $addNumberUrlKey + 1;
					$newUrlKey = $ProductAttributeData['url_key'] . '-' . $addToKey;
					$urlrewriteCheck = $this->helper->checkUrlKey($ProcuctData['store_id'], $newUrlKey);
					if (!$urlrewriteCheck->getId()) {
						break;
					}
				}
				$ProductAttributeData['url_key'] = $newUrlKey;
				$ProductAttributeData['url_path'] = $newUrlKey;
			}
		}
		if(empty($ProductAttributeData['url_path'])) {
			unset($ProductAttributeData['url_path']);
		}
		
		$SetProductData->setSku($ProcuctData['sku']);
		$SetProductData->setStoreId($ProcuctData['store_id']);
		#if(isset($ProcuctData['name'])) { $SetProductData->setName($ProcuctData['name']); }
		if(isset($ProcuctData['websites'])) { $SetProductData->setWebsiteIds($ProcuctData['websites']); }
		if(isset($ProcuctData['attribute_set'])) { $SetProductData->setAttributeSetId($ProcuctData['attribute_set']); }
		if(isset($ProcuctData['prodtype'])) { $SetProductData->setTypeId($ProcuctData['prodtype']); }
		if(isset($ProcuctData['category_ids'])) { 
			if($ProcuctData['category_ids'] == "remove") { 
				$SetProductData->setCategoryIds(array()); 
			} else if($ProcuctData['category_ids'] != "") { 
				$SetProductData->setCategoryIds($ProcuctData['category_ids']);
			}
		}
		
		$SetProductData->addData($ProductAttributeData);
		
		if($newProduct || $params['reimport_images'] == "true") { 
			//media images
			$_productImages = array(
				'media_gallery'       => ($ProductImageGallery['gallery']!="") ? $ProductImageGallery['gallery'] : 'no_selection',
				'image'       => ($ProductImageGallery['image']!="") ? $ProductImageGallery['image'] : 'no_selection',
				'small_image'       => ($ProductImageGallery['small_image']!="") ? $ProductImageGallery['small_image'] : 'no_selection',
				'thumbnail'       => ($ProductImageGallery['thumbnail']!="") ? $ProductImageGallery['thumbnail'] : 'no_selection',
				'swatch_image'       => ($ProductImageGallery['swatch_image']!="") ? $ProductImageGallery['swatch_image'] : 'no_selection'
		
			);
			//create array of images with duplicates combind
			$imageArray = array();
			foreach ($_productImages as $columnName => $imageName) {
				$imageArray = $this->helper->addImage($imageName, $columnName, $imageArray);
			}
			
			foreach ($imageArray as $ImageFile => $imageColumns) {
				if($ImageFile != "no_selection") {
					$SetProductData->addImageToMediaGallery($imagePath . $ImageFile, $imageColumns, false, false);
				} else {
					foreach( $imageColumns as $mediaAttribute ) {
						$SetProductData->setData($mediaAttribute, 'no_selection');
					}
				}
			}
		}
		
		if($ProductStockdata!=""){ $SetProductData->setStockData($ProductStockdata); }
		
		/* MODDED TO ALLOW FOR GROUP POSITION AS WELL AND SHOULD WORK IF NO POSITION IS SET AS WELL CAN COMBO */
		$groupedpositionproducts = false;
		$finalIDssthatneedtobeconvertedto=array();
		
		if ($ProductSupperAttribute['grouped'] != "") {
			
			$newLinks = [];
			$finalskusforarraytoexplode = explode(",",$ProductSupperAttribute['grouped']);
			foreach($finalskusforarraytoexplode as $productskuexploded)
			{
				$pos = strpos($productskuexploded, ":");
				if ($pos !== false) {
					$finalidsforarraytoexplode = explode(":",$productskuexploded);
					$id = $this->Product->getIdBySku($finalidsforarraytoexplode[1]);
					if($id > 0) {
						/** @var \Magento\Catalog\Api\Data\ProductLinkInterface $productLink */
						$productLink = $this->ProductLinkInterfaceFactory->create();
						$linkedProduct = $this->ProductRepositoryInterface->getById($id);
						$productLink->setSku($SetProductData->getSku())
							->setLinkType('associated')
							->setLinkedProductSku($linkedProduct->getSku())
							->setLinkedProductType($linkedProduct->getTypeId())
							->setPosition($finalidsforarraytoexplode[0])
							->getExtensionAttributes()
							->setQty($finalidsforarraytoexplode[2]);
							
						$newLinks[] = $productLink;
					}
				} else {
					$id = $this->Product->getIdBySku($productskuexploded);
					if($id > 0) {
						/** @var \Magento\Catalog\Api\Data\ProductLinkInterface $productLink */
						$productLink = $this->ProductLinkInterfaceFactory->create();
						$linkedProduct = $this->ProductRepositoryInterface->getById($id);
						$productLink->setSku($SetProductData->getSku())
							->setLinkType('associated')
							->setLinkedProductSku($linkedProduct->getSku())
							->setLinkedProductType($linkedProduct->getTypeId())
							->setPosition(1)
							->getExtensionAttributes()
							->setQty(1);
							
						$newLinks[] = $productLink;
					}
				}
			}
			
			$SetProductData->setProductLinks($newLinks);
			#$SetProductData->save();

			/*
			$finalskusthatneedtobeconvertedtoID="";
			$groupedpositioncounter=0;
			$finalskusforarraytoexplode = explode(",",$ProductSupperAttribute['grouped']);
			foreach($finalskusforarraytoexplode as $productskuexploded)
			{
					$pos = strpos($productskuexploded, ":");
					if ($pos !== false) {
					//if( isset($finalidsforarraytoexplode[1]) ) {	
						$groupedpositionproducts = true;
						$finalidsforarraytoexplode = explode(":",$productskuexploded);
						$finalIDssthatneedtobeconvertedto[$groupedpositioncounter]['position'] = $finalidsforarraytoexplode[0];
						$finalIDssthatneedtobeconvertedto[$groupedpositioncounter]['sku'] = $finalidsforarraytoexplode[1];
						if (isset($finalidsforarraytoexplode[2])) {
						$finalIDssthatneedtobeconvertedto[$groupedpositioncounter]['qty'] = $finalidsforarraytoexplode[2];
						}
						$finalskusthatneedtobeconvertedtoID .= $finalidsforarraytoexplode[1] . ",";
					} else {
						$groupedpositionproducts = false;
						$finalskusthatneedtobeconvertedtoID .= $productskuexploded . ",";
					}	
					$groupedpositioncounter++;
			}		
			$linkIds = $this -> skusToIds( $finalskusthatneedtobeconvertedtoID);
			if ( !empty( $linkIds ) ) {
				$SetProductData -> setGroupedLinkData( $linkIds );
			} 
			*/
		} else {
			$this->helper->sendLog($this->helper->rowCount,'grouped','The column is empty. If you are just updating other data just disregard');
		}
		
		$relatedProductData = array();
		$upSellProductData = array();
		$crossSellProductData = array();
		
		if($ProductSupperAttribute['related']!=""){ $relatedProductData = $this->helper->AppendRelatedProduct($ProductSupperAttribute['related'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['upsell']!=""){ $upSellProductData = $this->helper->AppendUpsellProduct($ProductSupperAttribute['upsell'] ,$ProcuctData['sku']); }
		if($ProductSupperAttribute['crosssell']!=""){ $crossSellProductData = $this->helper->AppendCrossSellProduct($ProductSupperAttribute['crosssell'] , $ProcuctData['sku']); }
		
		if(!empty($relatedProductData) || !empty($upSellProductData) || !empty($crossSellProductData)) {
			$allProductLinks = array_merge($relatedProductData, $upSellProductData, $crossSellProductData);
			$SetProductData->setProductLinks($allProductLinks);
		}
		
		try {
			$SetProductData->save(); 
		}
		catch (\Exception $e) { 
			throw new \Magento\Framework\Exception\LocalizedException(__('SKU: '.$ProcuctData['sku'].' ERROR : '. $e));
		}
			
		if(isset($ProductSupperAttribute['tier_prices'])) { 
			#print_r($ProductSupperAttribute['tier_prices']);
			if($ProductSupperAttribute['tier_prices']!=""){ $SetProductData->setTierPrice($ProductSupperAttribute['tier_prices'])->save(); }
		}
		
	  }//END UPDATE ONLY CHECK
	  return $this->helper->msgtoreturn;
	}
}