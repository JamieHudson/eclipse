<?php 

namespace CommerceExtensions\ProductImportExport\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('productimportexport_cronjobdata');
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();

                  // Declare data
                $columns = [
                    'apply_additional_filters' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Apply Additional Filters',
                        'after' => 'Profile_type',
                    ],
                    'filter_qty_from' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Filter Qty From',
                        'after' => 'Profile_type',
                    ],
                    'filter_qty_to' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Filter Qty To',
                        'after' => 'Profile_type',
                    ],
                    'append_websites' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Append Websites',
                        'after' => 'append_categories',
                    ],
                    'ref_by_product_id' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Lookup By ProductID',
                        'after' => 'update_products_only',
                    ],
                    'import_attribute_value' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Import Attribute Value',
                        'after' => 'update_products_only',
                    ],
                    'attribute_for_import_value' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Attribute To Import Value',
                        'after' => 'update_products_only',
                    ],
                    'export_file_name' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Cronjob Export File Name',
                        'after' => 'export_full_image_paths',
                    ],
                    'export_multi_store' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                  		'length' =>100,
                        'comment' => 'Export Multi Store Data',
                        'after' => 'export_full_image_paths',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
				/*
                $connection->changeColumn(
                    $tableName,
                    'File_name',
                    'import_file_name',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
                    'Import File Name'
                );
                $connection->changeColumn(
                    $tableName,
                    'File_path',
                    'import_file_path',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
                    'Import File Path'
                );
                $connection->changeColumn(
                    $tableName,
                    'Export_File_Location',
                    'export_file_path',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
                    'Export File Path'
                );
				*/
            }
        }
        $setup->endSetup();

    }

}