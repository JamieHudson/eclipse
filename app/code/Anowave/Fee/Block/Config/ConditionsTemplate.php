<?php
/**
 * Anowave Magento 2 Extra Fee
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Fee
 * @copyright 	Copyright (c) 2018 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Fee\Block\Config;

class ConditionsTemplate extends \Magento\Backend\Block\Template
{
	/**
	 * @var \Magento\Framework\Registry
	 */
	protected $registry;
	
	/**
	 * Renderer fieldset
	 *
	 * @var \Magento\Backend\Block\Widget\Form\Renderer\Fieldset
	 */
	protected $rendererFieldset;
	
	/**
	 * @var \Magento\Rule\Block\Conditions
	 */
	protected $conditions;
	
	/**
	 * @var \Anowave\Fee\Model\ResourceModel\Conditions\CollectionFactory
	 */
	protected $conditionsCollectionFactory;
	
	/**
	 * @var \Anowave\Fee\Model\ConditionsFactory
	 */
	protected $conditionsFactory;
	
	/**
	 * @var \Magento\Framework\Data\FormFactory
	 */
	protected $formFactory = null;
	
	/**
	 * @var Magento\Framework\App\Request\Http
	 */
	protected $request;
	
	/**
	 * Constructor 
	 * 
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Framework\Data\FormFactory $formFactory
	 * @param \Magento\Rule\Block\Conditions $conditions
	 * @param \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset
	 * @param \Anowave\Fee\Model\ResourceModel\Conditions\CollectionFactory $conditionsCollectionFactory
	 * @param \Anowave\Fee\Model\ConditionsFactory $conditionsFactory
	 * @param array $data
	 */
	public function __construct
	(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Rule\Block\Conditions $conditions,
		\Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset,
		\Anowave\Fee\Model\ResourceModel\Conditions\CollectionFactory $conditionsCollectionFactory,
		\Anowave\Fee\Model\ConditionsFactory $conditionsFactory,
		array $data = []
	)
	{
		parent::__construct($context);
		
		$this->registry 					= $registry;
		$this->rendererFieldset 			= $rendererFieldset;
		$this->conditions 					= $conditions;
		$this->conditionsCollectionFactory 	= $conditionsCollectionFactory;
		$this->conditionsFactory 			= $conditionsFactory;
		
		/**
		 * Set form factgory 
		 * 
		 * @var \Magento\Framework\Data\FormFactory
		 */
		$this->formFactory = $formFactory;
		
		/**
		 * Set request 
		 * 
		 * @var \Magento\Framework\App\Request\Http $request
		 */
		$this->request = $context->getRequest();
	}
	
	public function _construct()
	{
		parent::_construct();
		
		$this->setTemplate('conditions.phtml');
	}
	
	public function getForm()
	{
		$collection = $this->conditionsCollectionFactory->create()->addFieldToFilter('rule_default', \Anowave\Fee\Observer\Config::DEFAULT_CONDITION_VALUE);
		
		if ($collection->getSize())
		{
			$model = $collection->getFirstItem();
		}
		else 
		{
			$model = $this->conditionsFactory->create();
		}

		$formName = 'sales_rule_form';
		
		/**
		 * Get conditions fieldset
		 * 
		 * @var Ambiguous $conditionsFieldSetId
		 */
		$conditionsFieldSetId = $model->getConditionsFieldSetId($formName);
		
		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->formFactory->create();

		$form->setHtmlIdPrefix('rule_');
		
		/**
		 * Get renderer
		 * 
		 * @var Magento\Backend\Block\Widget\Form\Renderer\Fieldset $renderer
		 */
		$renderer = $this->rendererFieldset->setTemplate('Magento_CatalogRule::promo/fieldset.phtml')->setNewChildUrl($this->getUrl('sales_rule/promo_quote/newConditionHtml/form/'))->setFieldSetId($conditionsFieldSetId);
		
		$fieldset = $form->addFieldset('conditions_fieldset',['legend' => __('Apply the rule only if the following conditions are met (leave blank for all products)')])->setRenderer($renderer);
		
		$fieldset->addField
		(
			'conditions',
			'text',
			[
				'name' 	=> 'conditions', 
				'label' => __('Conditions'), 
				'title' => __('Conditions')	
			]
		)->setRule($model)->setRenderer($this->conditions);
	
		$form->setValues($model->getData());
		
		return $form;
	}
}