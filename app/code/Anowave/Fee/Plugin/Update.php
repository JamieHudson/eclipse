<?php
/**
 * Anowave Magento 2 Extra Fee
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Fee
 * @copyright 	Copyright (c) 2018 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

namespace Anowave\Fee\Plugin;

class Update
{
	/**
	 * Subtotal key
	 *
	 * @var string
	 */
	const AMOUNT_SUBTOTAL = 'subtotal';
	
	/**
	 * @var \Magento\Quote\Model\Quote
	 */
	protected $quote;
	
	/**
	 * @var \Magento\Checkout\Model\Session
	 */
	protected $checkoutSession;
	
	/**
	 * @var \Magento\Framework\Registry
	 */
	protected $registry;
	
	/**
	 * @var \Anowave\Fee\Helper\Data
	 */
	protected $helper;
	
	/**
	 * Cosntructor
	 *
	 * @param \Magento\Quote\Model\Quote $quote
	 * @param \Magento\Checkout\Model\Session $checkoutSession
	 * @param \Magento\Framework\Registry $registry
	 * @param \Anowave\Fee\Helper\Data $helper
	 */
	public function __construct
	(
		\Magento\Quote\Model\Quote $quote,
		\Magento\Checkout\Model\Session $checkoutSession,
		\Magento\Framework\Registry $registry,
		\Anowave\Fee\Helper\Data $helper
		)
	{
		/**
		 * Set quote
		 *
		 * @var \Magento\Quote\Model\Quote $quote
		 */
		$this->quote = $quote;
		
		/**
		 * Set checkout session
		 *
		 * @var \Magento\Checkout\Model\Session $checkoutSession
		 */
		$this->checkoutSession = $checkoutSession;
		
		/**
		 * Set registry
		 *
		 * @var \Magento\Framework\Registry $registry
		 */
		$this->registry = $registry;
		
		/**
		 * Set helper
		 *
		 * @var \Anowave\Fee\Helper\Data $helper
		 */
		$this->helper = $helper;
	}
	
	/**
	 * Get shipping, tax, subtotal and discount amounts all together
	 *
	 * @return array
	 */
	public function afterGetAmounts(\Magento\Paypal\Model\Cart $cart, $total)
	{
		$loop = $this->registry->registry('loop') ? true : false;
		
		if (!$loop)
		{
			$this->registry->register('loop', true);
			
			/**
			 * Get quote
			 *
			 * @var \Magento\Quote\Model\Quote $quote
			 */
			$quote = $this->checkoutSession->getQuote();
			
			if ($quote && $quote->getId())
			{
				/**
				 * Add fee from quote
				 */
				if(in_array($quote->getPayment()->getMethod(),
					[
						'payflowpro',
						'payflow_link',
						'payflow_advanced',
						'braintree_paypal',
						'paypal_express_bml',
						'payflow_express_bml',
						'payflow_express',
						'paypal_express'
					]))
				{
					$total[self::AMOUNT_SUBTOTAL] = $total[self::AMOUNT_SUBTOTAL] + $quote->getFee();
				}
			}
		}
		
		return $total;
	}
}