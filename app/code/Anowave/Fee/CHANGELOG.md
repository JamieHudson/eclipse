# Changelog

All notable changes to this project will be documented in this file.


## [3.0.5]

### Added

- Taxable fee

## [3.0.4]

### Fixed

- Compilation issues

## [3.0.3]

### Added

- Ability to calculate category based fee for associated products (not assigned to category)

## [3.0.2]

### Fixed

- Conditions not working properly

## [3.0.1]

### Fixed

- PayPal issue with totals

## [3.0.0]

### Added

- Added fee row in Invoice pdf

## [2.0.9]

- Fixed wrong fee calculation on grand total instead of sub-total

## [2.0.8]

- Fixed fatal error on order view page

## [2.0.7]

- Fixed Magento 2.2 doubled fee

## [2.0.6]

- Magento 2.2 compatibility updates

## [2.0.5]

- Allowed for setting fee for simple products (in configurable)

## [2.0.4]

### Fixed

- Magento 2.2.x compatibility updates

## [2.0.3]

### Fixed

- Restored "Payment method" condition

## [1.0.0]

- Initial version