<?php

namespace BoostMyShop\AdvancedStock\Plugin\Grouped\Model\ResourceModel\Indexer\Stock;



class Grouped extends \Magento\GroupedProduct\Model\ResourceModel\Indexer\Stock\Grouped
{
    protected function _getStockStatusSelect($entityIds = null, $usePrimaryTable = false)
    {

        $select = parent::_getStockStatusSelect($entityIds, $usePrimaryTable);

        $whereParts = $select->getPart("where");
        foreach($whereParts as $key => $whereOption)
        {
            if ($whereOption == "(cis.website_id = 0)")
                $whereParts[$key] = "(1=1)";
        }

        $select->reset("where");
        foreach($whereParts as $part)
        {
            $part = str_replace("AND ", "", $part);
            $select->where($part);
        }

        return $select;
    }

}