<?php

namespace BoostMyShop\Supplier\Block\Order\Edit\Tab\Products\Renderer;

use Magento\Framework\DataObject;

class Qty extends AbstractRenderer
{

    public function render(DataObject $row)
    {
    	$moq  = $row->getMoq();
        $html = '<input size="6" type="textbox" name="products['.$row->getId().'][qty]" id="products['.$row->getId().'][qty]" value="'.$row->getpop_qty().'">';
        if($moq>0){
        	$color = '';
        	if($moq>$row->getpop_qty()){
        		$color = "RED";
        	}
        	$html.= "<br><span style='font-style: italic; color: ".$color.";'>MOQ: ".$moq."<span>";
        }
        return $html;
    }
}