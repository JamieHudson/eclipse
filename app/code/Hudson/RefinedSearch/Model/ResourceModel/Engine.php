<?php
/**
 * Copyright Â© Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Hudson\RefinedSearch\Model\ResourceModel;

/**
 * CatalogSearch Fulltext Index Engine resource model
 */
class Engine extends \Magento\CatalogSearch\Model\ResourceModel\Engine
{

    /**
     * Prepare index array as a string glued by separator
     * Support 2 level array gluing
     *
     * @param array $index
     * @param string $separator
     * @return array
     */
    public function prepareEntityIndex($index, $separator = ' ')
    {
        $indexData = [];
        foreach ($index as $attributeId => $value) {
            // $indexData[$attributeId] = is_array($value) ? implode($separator, $value) : $value;

            if(is_array($value)){
                $value = array_unique($value);
                $processedIndex = strtolower(implode($separator, $value));
                $processedIndexTokens = explode(" ",$processedIndex);
                $value = array_unique($processedIndexTokens);
                $indexData[$attributeId] = implode(" ", $value);
            } else{
                $indexData[$attributeId] = $value;
            }


        }

        return $indexData;
    }
}
