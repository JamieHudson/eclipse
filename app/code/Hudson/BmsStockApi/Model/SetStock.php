<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 08/01/2018
 * Time: 10:23
 */

namespace Hudson\BmsStockApi\Model;


use Hudson\BmsStockApi\Api\SetStockInterface;
use Hudson\BmsStockApi\Model\MassApiHelper;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use Magento\Framework\Webapi\Exception;
use BoostMyShop\AdvancedStock\Model\Warehouse\ItemFactory as WarehouseItemFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;



class SetStock extends MassApiHelper implements SetStockInterface
{

    //Model Specific
    const REQUIRED_FIELDS = ['productCode','qty', 'restockDate'];
    const NUMERIC_FIELDS = ['qty'];
    const DATE_FIELDS = ['restockDate'];


    const WAREHOUSE_ID = 1; //Default warehouse

    protected $productCollectionFactory;

    private $warehouseItemFactory;

    private $backendAuthSession;
    private $stockMovementFactory;
    protected $stockState;
    protected $stockRegistry;

    public function __construct(DateFormatTool $dateFormatTool,
                                \Hudson\BmsStockApi\Helper\Logger\Logger $hudsonLogger,
                                CollectionFactory $productCollectionFactory,
                                WarehouseItemFactory $warehouseItemFactory,
                                \Magento\Backend\Model\Auth\Session $backendAuthSession,
                                \BoostMyShop\AdvancedStock\Model\StockMovementFactory $stockMovementFactory,\Magento\CatalogInventory\Api\StockStateInterface $stockState, \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry)
    {
        parent::__construct($hudsonLogger,$dateFormatTool);
        $this->productCollectionFactory = $productCollectionFactory;
        $this->backendAuthSession = $backendAuthSession;
        $this->warehouseItemFactory = $warehouseItemFactory;
        $this->stockMovementFactory = $stockMovementFactory;
        $this->stockState = $stockState;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @inheritdoc
     */
    public function setStock($records)
    {

        $errors = $this->validateRecords($records,self::REQUIRED_FIELDS,self::NUMERIC_FIELDS,self::DATE_FIELDS);

        if(true !== $errors) {
            //Logging errors for records
            $this->logValidationErrors($errors,get_class($this),__FUNCTION__);

            throw new Exception(__("Errors detected on validation. No records are processed."),400,Exception::HTTP_BAD_REQUEST,$errors);
        }

        //ACtually set the stocks
        $result = $this->setStocks($records);

        //Return the success
        if($result === true) {
            return $result;
        }

        //Logging errors for records
        $this->logProcessingErrors($result,get_class($this),__FUNCTION__);

        throw new Exception(__("Errors detected when processing. Previous records are processed."),400,Exception::HTTP_BAD_REQUEST,[$result]);
    }

    protected function setStocks($records){

        foreach ($records as $index => $record){

            try {
                $productCode = $record['productCode'];
                $quantity = $record['qty'];

                $productCollection = $this->productCollectionFactory->create();
                $productCollection->addAttributeToSelect('*')->addAttributeToFilter('sku', ["eq" => $productCode]);
                $product = $productCollection->getFirstItem();

                $productId = $product->getData("entity_id");

                if(!$productId){
                    $this->hudsonLogger->log("API: Product with code $productCode cannot be found!");
                    continue;
                }

		// SMCN
		if ($record['restockDate'] != null) {
			$product->setData('restock_date', $record['restockDate']);
			$product->getResource()->saveAttribute($product, 'restock_date');
		}
		// SMCN

                $userId = null;
                if ($this->backendAuthSession->getUser())
                    $userId = $this->backendAuthSession->getUser()->getId();

                $stockItem = $this->warehouseItemFactory->create()
                    ->loadByProductWarehouse($productId, self::WAREHOUSE_ID);

                $reserved = $stockItem->getwi_reserved_quantity();
                $this->stockMovementFactory->create()->updateProductQuantity($stockItem->getwi_product_id(),
                    $stockItem->getwi_warehouse_id(),
                    $stockItem->getwi_physical_quantity(),
                    $quantity + $reserved,
                    'From API',
                    $userId);


                    //added by Richie Lennox
                    //This will check incoming updates and match new quantities against pack size, if exists
                    //If the quantity is below the pack size, the product will be marked as out of stock

                    $packsize = $product->getCustomAttribute('packsize');
                    
                    if($packsize){
                        if($packsize->getValue()){
                            //packsize value found, now we are checking if packsize is more than quantity
                            $productStock = $this->stockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
                            if(intVal($packsize->getValue())){
                                if(intVal($packsize->getValue()) > 1){
                                    if($productStock < $packsize->getValue()){
                                        $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                                        if($stockItem->getIsInStock())
                                            $stockItem->setIsInStock(false);

                                        $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                                        $this->hudsonLogger->log("API: Stock for product $productId ($productCode) in warehouse " . self::WAREHOUSE_ID . " set to $quantity plus reserved qty of $reserved & changed Is In Stock to (Out of Stock)");
                                        
                                    }elseif($productStock >= $packsize->getValue()){
                                        $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                                        $stockItem->setIsInStock(true);
                                            $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                                            $this->hudsonLogger->log("API: Stock for product $productId ($productCode) in warehouse " . self::WAREHOUSE_ID . " set to $quantity plus reserved qty of $reserved & changed Is In Stock to (In Stock)");
                                    }
                                }
                            }
                        }
                    }else{
                        $this->hudsonLogger->log("API: Stock for product $productId ($productCode) in warehouse " . self::WAREHOUSE_ID . " set to $quantity plus reserved qty of $reserved");
                    }



                
            }catch (\Exception $e){
                return "Execution stopped: ". $e->getMessage(). " - At record $index (index) OBJECT: ".implode(" , ",$record);
            }
        }

        return true;
    }
}
