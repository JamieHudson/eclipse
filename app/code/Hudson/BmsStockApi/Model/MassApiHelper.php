<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 03/01/2018
 * Time: 10:03
 */

namespace Hudson\BmsStockApi\Model;

use \Magento\Framework\Stdlib\DateTime as DateFormatTool;


class MassApiHelper
{

    const NUMBER_LOG_ERRORS = 10;
    const RECORD_LIMIT = 1000; //TODO: Test and discuss (seems reasonable for now)



    //Date formatting for validation
    protected $dateFormatTool;


    protected $hudsonLogger;


    //This class is non-instantiable
    protected function __construct(
                                   \Hudson\BmsStockApi\Helper\Logger\Logger $hudsonLogger,
                                   DateFormatTool $dateFormatTool)
    {
        $this->hudsonLogger = $hudsonLogger;
        $this->dateFormatTool = $dateFormatTool;
    }


    /**
     *
     * @param $array
     * @return array|bool
     */
    protected function validateRecords($array, $requiredFields,$numericFields,$dateFields)
    {

        $errors = [];
        //Validate size
        if (sizeof($array) > self::RECORD_LIMIT) return ["Too many records in request. Limit is " . self::RECORD_LIMIT];
        
        foreach ($array as $index => $record) {

            $valid = $this->validateRecord($record, $requiredFields,$numericFields,$dateFields, $index);
            if ($valid !== true) {
                $errors = array_merge($errors,$valid);
            }

        }


        if(0 == sizeof($errors))         return true;


        return $errors;

    }

    /**
     * @param $object
     * @return bool|array
     */
    protected function validateRecord($object, $requiredFields,$numericFields,$dateFields,$index)
    {

        $recordErrors = [];

        //Required Fields check
        foreach ($requiredFields as $REQUIRED_FIELD) {
            if (!isset($object[$REQUIRED_FIELD])) {
                $recordErrors[] = "Field $REQUIRED_FIELD is REQUIRED. At record: $index (index)";
            }
        }

        //Numeric Fields Check
        foreach ($numericFields as $NUMERIC_FIELD) {

            if (isset($object[$NUMERIC_FIELD])) {

                $postedValue = $object[$NUMERIC_FIELD];

                if(!is_numeric($postedValue)){
                    $recordErrors[] = "Field $NUMERIC_FIELD failed NUMERIC format check. At record: $index (index) : \"$postedValue\"";
                }
            }
        }

        //Date fields check
        foreach ($dateFields as $DATE_FIELD) {



            if (isset($object[$DATE_FIELD])) {

                $postedDate = $object[$DATE_FIELD];


                try {
                    $this->dateFormatTool->formatDate($postedDate, false);
                } catch (\Exception $e) {
                    $recordErrors[] = "Field $DATE_FIELD failed DATE format check. At record: $index (index) : \"$postedDate\"";
                }
            }
        }

        if(sizeof($recordErrors) == 0)    return true;

        return $recordErrors;
    }

    /**
     * Logging validation errors
     * @param $errorsArray
     * @param string $className
     * @param string $methodName
     */
    protected function logValidationErrors($errorsArray,$className = "Unknown Class",$methodName = "Unknown Method"){
        //Show which location and which method is logging (gives info about request attempted)
        $location = $className . " - " . $methodName . "\n";

        $logMessage = $location . "API call resulted in errors on validation. First " . self::NUMBER_LOG_ERRORS . "Errors: \n";
        //stringify first N errors
        $errorsString = implode("\n",array_slice($errorsArray,0,self::NUMBER_LOG_ERRORS,true));
        $logMessage .= $errorsString;

        //Validation Errors are logged in info file as they are expected to be client errors
        $this->hudsonLogger->log($logMessage,\Hudson\BmsStockApi\Helper\Logger\Logger::HUDSON_API_INFO);
    }

    /**
     * Logging processing errors
     * @param $errorMessage
     * @param string $className
     * @param string $methodName
     */
    protected function logProcessingErrors($errorMessage,$className = "Unknown Class",$methodName = "Unknown Method"){
        $location = $className . " - " . $methodName . "\n";
        $logMessage = $location . "API call resulted in errors when processing. Errors: \n";
        $errorsString = $errorMessage;
        $logMessage .= $errorsString;

        //Logged as exception as they are expected to succeed after passing validation
        $this->hudsonLogger->log($logMessage,\Hudson\BmsStockApi\Helper\Logger\Logger::HUDSON_API_EXCEPTION);
    }
}