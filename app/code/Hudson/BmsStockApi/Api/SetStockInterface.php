<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:03
 */

namespace Hudson\BmsStockApi\Api;


interface SetStockInterface
{


    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function setStock($records);

}