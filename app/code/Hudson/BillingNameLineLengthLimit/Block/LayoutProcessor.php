<?php

namespace Hudson\BillingNameLineLengthLimit\Block;

class LayoutProcessor
{
        /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ) {
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children']['realexpayments_hpp-form']['children']['form-fields']['children']['firstname']['validation']['max_text_length'] = 15 ;
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children']['realexpayments_hpp-form']['children']['form-fields']['children']['lastname']['validation']['max_text_length'] = 15 ;

        return $jsLayout;
    }
}
