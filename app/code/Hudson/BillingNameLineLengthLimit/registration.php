<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Hudson_BillingNameLineLengthLimit',
    __DIR__
);
