<?php

namespace Hudson\SampleRoom\Controller\Cart;

use Hudson\SampleRoom\Model\Cart;
use Hudson\SampleRoom\Model\ResourceModel\Cart\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:16
 */
class Remove extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $sampleCartItemFactory;

    protected $logger;

    protected $sampleCartCollectionFactory;



    public function __construct(Context $context,
                                \Hudson\SampleRoom\Helper\Logger $logger,
                                \Hudson\SampleRoom\Model\Cart\ItemFactory $sampleCartItemFactory,
                                \Hudson\SampleRoom\Model\ResourceModel\Cart\CollectionFactory $sampleCartCollectionFactory,
                                \Magento\Customer\Model\Session $customerSession
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
        $this->customerSession = $customerSession;
        $this->sampleCartItemFactory = $sampleCartItemFactory;
        $this->sampleCartCollectionFactory = $sampleCartCollectionFactory;
    }

    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if(!$this->getRequest()->isPost()){
            throw new NotFoundException(__("Not Found."));
        }


        try {

            if(!$this->customerSession->isLoggedIn() || !($customerId = $this->customerSession->getCustomerId())){
                throw new LocalizedException(__("User not logged in."));
            }

            $post = $this->getRequest()->getPostValue();

            $cartItemId = (int)$post['sample_item_id'];

            $cartItem = $this->sampleCartItemFactory->create();

            /** @var $cartItem \Hudson\SampleRoom\Model\Cart\Item */
            $cartItem->loadById($cartItemId);

            $currentCart = $this->getCurrentCart($customerId);

            if(!$currentCart->getId()){
                throw new LocalizedException(__("Samples cart not found."));
            }

            if($cartItem->getCartId() !== $currentCart->getId()){
                throw new LocalizedException(__("Not allowed."));
            }


            $cartItem->delete();

            $this->messageManager->addSuccessMessage(__('Sample removed from cart'));


        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Could not remove sample from cart. '.$e->getMessage()));
            $this->logger->log($e->getMessage(), \Hudson\SampleRoom\Helper\Logger::HUDSON_EXCEPTION);
        }



        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }

    protected function getCurrentCart($customerId){

        $collection = $this->sampleCartCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Collection */
        $collection->addFieldToFilter("customer_id",["eq" => $customerId]);
        $customerCart = $collection->getFirstItem();

        return $customerCart;
    }



}