<?php

namespace Hudson\SampleRoom\Controller\Cart;

use Hudson\SampleRoom\Model\Cart;
use Hudson\SampleRoom\Model\ResourceModel\Cart\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:16
 */
class Submit extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $generalHelper;

    protected $sampleCartCollectionFactory;

    protected $sampleOrderFactory;

    protected $logger;

    protected $productRepository;

    protected $emailHelper;

    protected  $requiredAddressFields = [
        "name",
        "company",
        "street",
        "city",
        "postcode"
    ];

    protected  $allAddressfields = [
        "name",
        "company",
        "telephone",
        "street",
        "city",
        "region",
        "postcode"
    ];

    public function __construct(Context $context,
                                \Hudson\SampleRoom\Helper\Logger $logger,
                                \Hudson\SampleRoom\Helper\GeneralHelper $generalHelper,
                                \Hudson\SampleRoom\Helper\EmailHelper $emailHelper,
                                \Hudson\SampleRoom\Model\OrderFactory $sampleOrderFactory,
                                \Hudson\SampleRoom\Model\ResourceModel\Cart\CollectionFactory $sampleCartCollectionFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                ProductRepositoryInterface $productRepository
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
        $this->sampleOrderFactory = $sampleOrderFactory;
        $this->productRepository = $productRepository;
        $this->customerSession = $customerSession;
        $this->generalHelper = $generalHelper;
        $this->emailHelper = $emailHelper;
        $this->sampleCartCollectionFactory = $sampleCartCollectionFactory;
    }

    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if(!$this->getRequest()->isPost()){
            throw new NotFoundException(__("Not Found."));
        }


        try {

            if(!$this->customerSession->isLoggedIn() || !($customerId = $this->customerSession->getCustomerId())){
                throw new LocalizedException(__("User not logged in."));
            }


            $addressValue = $this->validateAndStringifyAddress($this->getRequest()->getPost());

            $cart = $this->getCurrentCart($customerId);

            /** @var $cart \Hudson\SampleRoom\Model\Cart */


            if(!$cart->getId()){
                throw new LocalizedException(__("Cart not found."));
            }

            $interceptionFlag = false;
            foreach ($cart->getAllItems() as $item){
                if(!$this->generalHelper->checkIfSampleIsAvailable($item->getProductId())){
                    $interceptionFlag = true;
                    $item->delete();
                }
            }

            if($interceptionFlag){
                throw new LocalizedException(__("Some samples are no longer available. They were removed from your cart."));
            }


            $order = $this->createOrderFromCart($cart);
            $orderId = $order->getId();

            $order->setHtmlAddress($addressValue);
            $order->save();

            //Delete cart after order is submitted
            $cart->delete();

            $emailSent = $this->emailHelper->sendEmail($order);

            $order->setEmailSent($emailSent);
            $order->save();

            $this->messageManager->addSuccessMessage(__("Your order was created (ID: $orderId). You can see your SampleRoom order history in your account."));


        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Could not create order. '.$e->getMessage()));
            $this->logger->log($e->getMessage(), \Hudson\SampleRoom\Helper\Logger::HUDSON_EXCEPTION);
        }


        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }

    protected function getCurrentCart($customerId){

        $collection = $this->sampleCartCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Collection */
        $collection->addFieldToFilter("customer_id",["eq" => $customerId]);
        $customerCart = $collection->getFirstItem();

        return $customerCart;
    }

    protected function createOrderFromCart($cart)
    {
        $order = $this->sampleOrderFactory->create();

        /** @var $order \Hudson\SampleRoom\Model\Order */

        $order->createFromCart($cart);

        return $order;

    }

    private function validateAndStringifyAddress($post)
    {

        if(empty($post)){
            throw new LocalizedException(__("No address data found."));
        }

        $errors = [];

        $addressData = array_fill_keys($this->allAddressfields,"");

        //check if all fields are filled
        foreach ($this->allAddressfields as $addressfield){


            if(!isset($post[$addressfield]) || $post[$addressfield] == "" || !$this->checkIfArrayFieldIsSetOrStringIsWhitespace($post[$addressfield])){

                if(in_array($addressfield,$this->requiredAddressFields)) {
                    $errors[] = $addressfield;
                }

            }else{
                $postedValue = $post[$addressfield];

                if(is_array($postedValue)){
                    for($i=0;$i<sizeof($postedValue);$i++){
                        $postedValue[$i] = htmlspecialchars($postedValue[$i]);
                    }

                    $addressData[$addressfield] = $postedValue;
                }else{
                    $addressData[$addressfield] = htmlspecialchars($postedValue);

                }
            }

        }

        if(sizeof($errors) > 0){
            throw new LocalizedException(__("The following fields are required: ". implode(", ",$errors) . " . Please fill them in before you continue."));
        }

        $addressValue = '<p>';


        foreach ($addressData as $addrField => $addressDatum){

            $rowValue = "";

            if(is_array($addressDatum)){
                $addressDatum = $this->removeEmptyRows($addressDatum);
                $rowValue = implode("<br/>",$addressDatum);
            }else{
                $rowValue = $addressDatum;
            }

            if($addrField == "telephone"){
                $rowValue = '<span id="telephone">'.$rowValue.'</span>';
            }

            if($rowValue != ""){
                $addressValue .= $rowValue.'<br/>';
            }

        }

        $addressValue .= '</p>';

        return $addressValue;

    }

    private function checkIfArrayFieldIsSetOrStringIsWhitespace($value)
    {
        if(!is_array($value)){
            if(trim($value) != ""){
                return true;
            }
        }

        foreach ($value as $valuePart){
            if(trim($valuePart) != ""){
                return true;
            }
        }

        return false;
    }

    private function removeEmptyRows($rows)
    {
        $newRows = [];

        foreach ($rows as $addressRow){
            if(trim($addressRow) != ""){
                $newRows[] = $addressRow;
            }
        }

        return $newRows;
    }

}