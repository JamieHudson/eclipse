<?php

namespace Hudson\SampleRoom\Controller\Cart;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:16
 */
class View extends \Magento\Framework\App\Action\Action
{

    protected $pageFactory;
    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $page_object = $this->pageFactory->create();
        return $page_object;
    }

}