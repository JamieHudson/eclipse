<?php

namespace Hudson\SampleRoom\Controller\Referrer;

use Hudson\SampleRoom\Model\Cart;
use Hudson\SampleRoom\Model\ResourceModel\Cart\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:16
 */
class AddToCart extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;


    protected $sampleCartCollectionFactory;

    protected $sampleCartFactory;

    protected $logger;

    protected $productRepository;

    protected $configHelper;


    public function __construct(Context $context,
                                \Hudson\SampleRoom\Helper\Logger $logger,
                                \Hudson\SampleRoom\Model\CartFactory $sampleCartFactory,
                                \Hudson\SampleRoom\Model\ResourceModel\Cart\CollectionFactory $sampleCartCollectionFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                \Hudson\SampleRoom\Helper\ConfigHelper $configHelper,
                                ProductRepositoryInterface $productRepository
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
        $this->sampleCartFactory = $sampleCartFactory;
        $this->productRepository = $productRepository;
        $this->customerSession = $customerSession;
        $this->sampleCartCollectionFactory = $sampleCartCollectionFactory;
        $this->configHelper = $configHelper;

    }

    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($this->getRequest()->isPost()) {
            throw new NotFoundException(__("Not Found."));
        }


        try {

            if (!$this->customerSession->isLoggedIn() || !($customerId = $this->customerSession->getCustomerId())) {
                throw new LocalizedException(__("User not logged in."));
            }

            $sampleString = $this->getRequest()->getParam('samples');

            if(!$sampleString || trim($sampleString) == ""){
                $resultRedirect->setUrl($this->_url->getUrl('sampleroom/cart/view'));
                return $resultRedirect;
            }

            $samplesArray = explode(";", $sampleString);


            foreach ($samplesArray as $sampleCode) {

                try {

                    try {
                        $product = $this->productRepository->get($sampleCode);
                    } catch (NoSuchEntityException $e) {
                        $product = $this->productRepository->get("$sampleCode-GROUPED");
                    }

                    $sampleAttributeCode = $this->configHelper->getSampleAttribute();


                    if (!$sampleAttributeCode || !$product->getCustomAttribute($sampleAttributeCode)->getValue()) {
                        throw new LocalizedException(__("Sample not available for product " . $product->getSku() . "."));
                    }

                    $cart = $this->getCurrentCartOrCreate($customerId);

                    /** @var $cart \Hudson\SampleRoom\Model\Cart */
                    $cart->addCartItem($product->getId());

                    $this->messageManager->addSuccessMessage(__("Sample for code $sampleCode added to cart"));


                } catch (NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('Could not find sample for code: ' . $sampleCode));
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__("Sample for code $sampleCode not added. " . $e->getMessage()));
                }


            }


        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Could not add sample to cart. ' . $e->getMessage()));
            $this->logger->log($e->getMessage(), \Hudson\SampleRoom\Helper\Logger::HUDSON_EXCEPTION);
        }


        $resultRedirect->setUrl($this->_url->getUrl('sampleroom/cart/view'));
        return $resultRedirect;

    }

    protected function getCurrentCartOrCreate($customerId)
    {

        $collection = $this->sampleCartCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Collection */
        $collection->addFieldToFilter("customer_id", ["eq" => $customerId]);
        $customerCart = $collection->getFirstItem();

        if ($customerCart->getId()) {
            return $customerCart;
        }

        $customerCart = $this->sampleCartFactory->create();
        $customerCart->setCustomerId($customerId)->save();

        return $customerCart;

    }

}