<?php

namespace Hudson\SampleRoom\Controller\Adminhtml\Orders;

use Hudson\SampleRoom\Model\Cart;
use Hudson\SampleRoom\Model\ResourceModel\Cart\Collection;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:16
 */
class SendEmail extends \Magento\Framework\App\Action\Action
{

    protected $emailHelper;

    protected $sampleOrderFactory;

    protected $logger;


    public function __construct(Context $context,
                                \Hudson\SampleRoom\Model\OrderFactory $sampleOrderFactory,
                                \Hudson\SampleRoom\Helper\Logger $logger,
                                \Hudson\SampleRoom\Helper\EmailHelper $emailHelper)
    {
        parent::__construct($context);
        $this->emailHelper = $emailHelper;
        $this->logger = $logger;
        $this->sampleOrderFactory = $sampleOrderFactory;

    }

    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        try {


            $order = $this->getCurrentSampleOrder();

            $emailSent = $this->emailHelper->sendEmail($order);

            $order->setEmailSent($emailSent);
            $order->save();

            if($emailSent) {
                $this->messageManager->addSuccessMessage(__("Success. Sample Order Email sent."));
            }else{
                $this->messageManager->addErrorMessage(__('Could not send email. Error message sent to the specified email in "Configuration -> Hudson -> SampleRoom Settings -> Email Settings".'));
            }


        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Could not send email. ' . $e->getMessage()));
            $this->logger->log($e->getMessage(), \Hudson\SampleRoom\Helper\Logger::HUDSON_EXCEPTION);
        }


        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }

    protected function getCurrentSampleOrder()
    {

        if ($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)) {

            return $this->sampleOrderFactory->create()->loadById($id);

        }

        return null;

    }

}