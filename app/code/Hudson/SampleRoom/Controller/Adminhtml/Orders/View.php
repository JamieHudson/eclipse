<?php

namespace Hudson\SampleRoom\Controller\Adminhtml\Orders;


class View extends \Hudson\SampleRoom\Controller\Adminhtml\Orders\AbstractAction{

    public function execute(){

        try {

            $this->_registerCurrentSampleOrder();

            $this->_view->loadLayout();

            $sampleOrder = $this->_coreRegistry->registry('current_sample_order');
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Sample Order #%1', $sampleOrder->getId()));

            $this->_view->renderLayout();

        }catch(\Exception $e){

            $this->messageManager->addErrorMessage(__('An error occurred : '.$e->getMessage()));
            return $this->resultRedirectFactory->create()->setPath('sampleroom/orders/index');

        }

    }

}