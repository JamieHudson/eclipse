<?php

namespace Hudson\SampleRoom\Controller\Adminhtml\Orders;


class ExportCSV extends \Magento\Backend\App\AbstractAction
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $_resultLayoutFactory;

    protected $_sampleOrderCollectionFactory;
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $_resultRawFactory;
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_backendAuthSession;

    protected $_httpFactory;

    protected $_fileFactory;

    protected $_csvWriter;

    protected $_directoryList;

    protected $_sampleItemCollectionFactory;

    protected $_date;

    // protected $_request;

    /**
     * Constructor
     *
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Hudson\SampleRoom\Model\ResourceModel\Order\Item\CollectionFactory $sampleItemCollectionFactory,
        \Hudson\SampleRoom\Model\ResourceModel\Order\CollectionFactory $sampleOrderCollectionFactory,
        \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvWriter,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
        // \Magento\Framework\App\RequestInterface $request

    )
    {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_sampleItemCollectionFactory = $sampleItemCollectionFactory;
        $this->_sampleOrderCollectionFactory = $sampleOrderCollectionFactory;
        $this->_resultRawFactory = $resultRawFactory;
        $this->_backendAuthSession = $backendAuthSession;
        $this->_httpFactory = $httpFactory;
        $this->_fileFactory = $fileFactory;
        $this->_csvWriter = $csvWriter;
        $this->_directoryList = $directoryList;
        $this->_date = $date;
        // $this->_request = $request;

    }


    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }


    public function execute()
    {


        $start_date = $this->getRequest()->getPostValue('start-date');
        $end_date = $this->getRequest()->getPostValue('end-date');

        $data = [];

        $collection = $this->_sampleItemCollectionFactory->create();


        $joincon = 'main_table.order_id = hud_samples_order.order_id';
        $collection->getSelect()->joinLeft(
            ['hud_samples_order'],
            $joincon,
            [
                'customer_id' => 'hud_samples_order.customer_id',
                'customer_email' => 'hud_samples_order.customer_email',
                'customer_account' => 'hud_samples_order.customer_account_number',
                'total_items' => 'hud_samples_order.total_items',
                'html_address' => 'hud_samples_order.html_address',
                'email_sent' => 'hud_samples_order.email_sent',
                'order_created_at' => 'hud_samples_order.created_at',
                'order_updated_at' => 'hud_samples_order.updated_at'
            ]
        );


        if ($start_date != NULL && $start_date != "" && $this->validateDate($start_date)) {
            $collection->addFieldToFilter('main_table.created_at', ['gteq' => $start_date]);
        }
        if ($end_date != NULL && $end_date != "" && $this->validateDate($end_date)) {
            $collection->addFieldToFilter('main_table.created_at', ['lteq' => $end_date]);
        }

        $headers = array(
            'order_id',
            'customer_id',
            'customer_email',
            'customer_account_number',
            'total_items',
            'html_address',
            'email_sent',
            'order_created_at',
            'order_updated_at',
            'order_item_id',
            'sku',
            'name',
            'created_at',
            'updated_at'
        );


        $data[] = $headers;
        foreach ($collection as $Oproduct) {

            $valuesArray = [];

            $valuesArray[] = $Oproduct->getData('order_id');
            $valuesArray[] = $Oproduct->getData('customer_id');
            $valuesArray[] = $Oproduct->getData('customer_email');
            $valuesArray[] = $Oproduct->getData('customer_account_number');
            $valuesArray[] = $Oproduct->getData('total_items');
            $valuesArray[] = $Oproduct->getData('html_address');
            $valuesArray[] = $Oproduct->getData('email_sent');
            $valuesArray[] = $Oproduct->getData('order_created_at');
            $valuesArray[] = $Oproduct->getData('order_updated_at');
            $valuesArray[] = $Oproduct->getData('order_item_id');
            $valuesArray[] = $Oproduct->getData('sku');
            $valuesArray[] = $Oproduct->getData('name');
            $valuesArray[] = $Oproduct->getData('created_at');
            $valuesArray[] = $Oproduct->getData('updated_at');

            $data[] = $valuesArray;

        }


        $month = date("F");
        $fileDirectory = \Magento\Framework\App\Filesystem\DirectoryList::MEDIA;
        $fileName = 'exportSampleOrders_' . $month . '.csv';
        $filePath = $this->_directoryList->getPath($fileDirectory) . "/" . $fileName;

        $this->_csvWriter
            ->setEnclosure('"')
            ->setDelimiter(',')
            ->saveData($filePath, $data);

        $this->_fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA,
            'text/csv',
            null
        );

        $resultRaw = $this->_resultRawFactory->create();

        return $resultRaw;

    }

}


?>
