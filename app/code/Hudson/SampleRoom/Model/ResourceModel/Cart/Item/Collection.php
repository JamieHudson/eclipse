<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 21/12/2017
 * Time: 09:29
 */

namespace Hudson\SampleRoom\Model\ResourceModel\Cart\Item;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init('Hudson\SampleRoom\Model\Cart\Item', 'Hudson\SampleRoom\Model\ResourceModel\Cart\Item');
    }


}