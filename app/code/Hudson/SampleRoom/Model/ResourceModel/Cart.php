<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:00
 */

namespace Hudson\SampleRoom\Model\ResourceModel;


class Cart extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = "hud_samples_cart";

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME,'cart_id');
    }

}