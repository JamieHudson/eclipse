<?php

namespace Hudson\SampleRoom\Model;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 14:41
 */
class Order extends \Magento\Framework\Model\AbstractModel implements  \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_samples_order';

    protected $samplesOrderItemFactory;

    protected $customerRepository;

    protected $sampleOrderItemCollectionFactory;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Hudson\SampleRoom\Model\Order\ItemFactory $samplesOrderItemFactory,
        CustomerRepository $customerRepository,
        \Hudson\SampleRoom\Model\ResourceModel\Order\Item\CollectionFactory $sampleOrderItemCollectionFactory,
        array $data = []
    ){
        parent::__construct($context, $registry, null, null, $data);
        $this->samplesOrderItemFactory = $samplesOrderItemFactory;
        $this->customerRepository = $customerRepository;
        $this->sampleOrderItemCollectionFactory = $sampleOrderItemCollectionFactory;

    }

    protected function _construct()
    {
        $this->_init('Hudson\SampleRoom\Model\ResourceModel\Order');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function loadById($id)
    {
        $this->getResource()->load($this, $id);
        if (! $this->getId()) {
            throw new NoSuchEntityException(__('Unable to find sample order with ID "%1"', $id));
        }
        return $this;
    }

    /**
     * @param $samplesCart Cart
     * @throws LocalizedException
     */
    public function createFromCart($samplesCart){

        if ($this->getId()) {
            throw new LocalizedException(__("Cannot overwrite order."));
        }


        $customer = $this->customerRepository->getById($samplesCart->getCustomerId());

        $this->setCustomerId($customer->getId());
        $this->setCustomerEmail($customer->getEmail());


        $customerNumberAttribute = $customer->getCustomAttribute("customer_number");
        if($customerNumberAttribute){
            $this->setCustomerAccountNumber($customerNumberAttribute->getValue());
        }

        $cartItems = $samplesCart->getAllItems();

        if(!($cartItems && $cartItems->getSize())){
            throw new LocalizedException(__("No items in cart."));
        }

        $this->setTotalItems($cartItems->getSize());


        $this->save();

        $orderId = $this->getId();


        try {
            //Add items
            foreach ($cartItems as $cartItem) {
                $orderItem = $this->samplesOrderItemFactory->create();
                /** @var $orderItem \Hudson\SampleRoom\Model\Order\Item */
                $orderItem->createFromCartItem($cartItem,$orderId);



            }
        }catch (\Exception $e){
            //Deleting order if items were unsuccessful
            $this->delete();
            throw new LocalizedException(__($e->getMessage()));
        }


        return $this;
    }


    public function getAllItems(){

        $collection = $this->sampleOrderItemCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Item\Collection */
        $collection->addFieldToFilter("order_id",["eq" => $this->getId()]);
        $collection->load();

        return $collection;
    }


}