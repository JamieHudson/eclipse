<?php

namespace Hudson\SampleRoom\Model\Order;

use Hudson\SampleRoom\Helper\FormatSkuHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 14:41
 */
class Item extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_samples_order_item';


    protected $productRepository;

    protected $formatSkuHelper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ProductRepositoryInterface $productRepository,
        FormatSkuHelper $formatSkuHelper,
        array $data = []
    )
    {
        parent::__construct($context, $registry, null, null, $data);
        $this->productRepository = $productRepository;
        $this->formatSkuHelper = $formatSkuHelper;

    }

    protected function _construct()
    {
        $this->_init('Hudson\SampleRoom\Model\ResourceModel\Order\Item');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function loadById($id)
    {
        $this->getResource()->load($this, $id);
        if (!$this->getId()) {
            throw new NoSuchEntityException(__('Unable to find sample cart item with ID "%1"', $id));
        }
        return $this;
    }

    public function createFromCartItem($cartItem, $orderId)
    {

        /** @var $cartItem \Hudson\SampleRoom\Model\Cart\Item */

        if ($this->getId()) {
            throw new LocalizedException(__("Cannot overwrite order."));
        }

        $this->setOrderId($orderId);

        $product = $this->productRepository->getById($cartItem->getProductId());

        $sampleSku = $this->formatSkuHelper->getFormattedSku($product);

        $this->setSku($sampleSku);
        $this->setName($product->getName());

        $this->save();

        return $this;


    }


}