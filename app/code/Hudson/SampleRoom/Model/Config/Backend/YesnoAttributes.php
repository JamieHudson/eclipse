<?php

namespace Hudson\SampleRoom\Model\Config\Backend;

class YesnoAttributes implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var \Magento\Catalog\Model\Resource\Product\Attribute\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param \Magento\Directory\Model\Resource\Country\Collection $countryCollection
     */
    public function __construct(\Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory)
    {
        $this->_collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $options = array();
        $collection = $this->_collectionFactory->create()->addVisibleFilter();

        foreach($collection as $item)
        {
            //Only Yes/No attributes allowed
            if($item->getFrontendInput() == "boolean") {
                $options[] = array('value' => $item->getAttributeCode(), 'label' => $item->getAttributeCode());
            }
        }

        return $options;
    }

}
