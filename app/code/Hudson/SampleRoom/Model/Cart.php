<?php

namespace Hudson\SampleRoom\Model;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;


/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 14:41
 */
class Cart extends \Magento\Framework\Model\AbstractModel implements  \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_samples_cart';

    protected $sampleCartItemFactory;

    protected $sampleCartItemCollectionFactory;
    /**
     * @var \Hudson\SampleRoom\Helper\ConfigHelper
     */
    protected $configHelper;

    protected function _construct()
    {
        $this->_init('Hudson\SampleRoom\Model\ResourceModel\Cart');
    }

    public function __construct( \Magento\Framework\Model\Context $context,
                                 \Magento\Framework\Registry $registry,
                                 \Hudson\SampleRoom\Model\Cart\ItemFactory $sampleCartItemFactory,
                                 \Hudson\SampleRoom\Model\ResourceModel\Cart\Item\CollectionFactory $sampleCartItemCollectionFactory,
                                 \Hudson\SampleRoom\Helper\ConfigHelper $configHelper,
                                 array $data = [])
    {
        parent::__construct($context, $registry, null, null, $data);
        $this->sampleCartItemFactory = $sampleCartItemFactory;
        $this->sampleCartItemCollectionFactory = $sampleCartItemCollectionFactory;
        $this->configHelper = $configHelper;
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


    public function addCartItem($productId){

        if($this->getAllItems()->getSize() == $this->getCartItemsLimit()){
            throw new LocalizedException(__("Samples cart is full."));
        }

        $cartItem = $this->sampleCartItemFactory->create();
        $cartItem->setCartId($this->getId());
        $cartItem->setProductId($productId);
        $cartItem->save();

        $this->save();
    }

    private function getCartItemsLimit(){
        return $this->configHelper->getCartItemsLimit();
    }

    public function getAllItems(){

        $collection = $this->sampleCartItemCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Item\Collection */
        $collection->addFieldToFilter("cart_id",["eq" => $this->getId()]);
        $collection->load();

        return $collection;
    }

    public function loadById($id)
    {
        $this->getResource()->load($this, $id);
        if (! $this->getId()) {
            throw new NoSuchEntityException(__('Unable to find sample cart with ID "%1"', $id));
        }
        return $this;
    }

}