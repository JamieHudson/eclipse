<?php

namespace Hudson\SampleRoom\Model\Cart;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 14:41
 */
class Item extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_samples_cart_item';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ){
        parent::__construct($context, $registry, null, null, $data);
    }

    protected function _construct()
    {
        $this->_init('Hudson\SampleRoom\Model\ResourceModel\Cart\Item');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


    public function loadById($id)
    {
        $this->getResource()->load($this, $id);
        if (!$this->getId()) {
            throw new NoSuchEntityException(__('Unable to find sample cart item with ID "%1"', $id));
        }
        return $this;
    }

}