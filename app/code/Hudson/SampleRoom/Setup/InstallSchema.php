<?php

namespace Hudson\SampleRoom\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;


        //TODO: Add email,company and account number to orders
        $installer->startSetup();

        /**
         * Table for sample room cart
         * Create table 'hud_samples_cart'
         */
        $samlplesCartTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_samples_cart'))
            ->addColumn(
                'cart_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Samples Cart Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer Id'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_samples_cart',
                    ['customer_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_id'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Samples Cart');
        $installer->getConnection()->createTable($samlplesCartTable);


        /**
         * Table for sample room cart item
         * Create table 'hud_samples_cart_item'
         */
        $samlplesCartItemTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_samples_cart_item'))
            ->addColumn(
                'cart_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Samples Cart Item Id'
            )
            ->addColumn(
                'cart_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true,'nullable' => false],
                'Samples Cart Id'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true,'nullable' => false],
                'Product Id'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addForeignKey(
                $installer->getFkName('hud_samples_cart_item', 'product_id', 'catalog_product_entity', 'entity_id'),
                'product_id',
                'catalog_product_entity',
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName('hud_samples_cart_item', 'cart_id', 'hud_samples_cart', 'cart_id'),
                'cart_id',
                'hud_samples_cart',
                'cart_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_samples_cart_item',
                    ['cart_id']
                ),
                ['cart_id']
            )
            ->setComment('Hudson Samples Cart Item');
        $installer->getConnection()->createTable($samlplesCartItemTable);


        /**
         * Table for sample room order
         * Create table 'hud_samples_order'
         */
        $samplesOrderTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_samples_order'))
            ->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Samples Order Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer Id'
            )
            ->addColumn(
                'customer_email',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Email'
            )
            ->addColumn(
                'customer_account_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,'default'=> null],
                'Customer Account Number'
            )
            ->addColumn(
                'total_items',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false,'default' => 0],
                'Total Cart Items'
            )
            ->addColumn(
                'html_address',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                2048,
                ['nullable' => true,'default'=> null],
                'Html Address'
            )
            ->addColumn(
                'email_sent',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false,'default' => false],
                'Is Email Sent'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_samples_order',
                    ['customer_id']
                ),
                ['customer_id']
            )
            ->setComment('Hudson Samples Order');
        $installer->getConnection()->createTable($samplesOrderTable);


        /**
         * Table for sample room order item
         * Create table 'hud_samples_order_item'
         */
        $samlplesOrderItemTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_samples_order_item'))
            ->addColumn(
                'order_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Samples Order Item Id'
            )
            ->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true,'nullable' => false],
                'Samples Order Id'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Product SKU'
            )->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Product Name'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addForeignKey(
                $installer->getFkName('hud_samples_order_item', 'order_id', 'hud_samples_order', 'order_id'),
                'order_id',
                'hud_samples_order',
                'order_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_samples_order_item',
                    ['order_id']
                ),
                ['order_id']
            )
            ->setComment('Hudson Samples Order Item');
        $installer->getConnection()->createTable($samlplesOrderItemTable);


        $installer->endSetup();

    }
}
