<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 13/04/2018
 * Time: 12:08
 */

namespace Hudson\SampleRoom\Helper;


class ConfigHelper extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_scopeConfig;


    const FIELD_ITEMLIMIT = 'sampleroom/cart/itemlimit';
    const FIELD_SAMPLEATTRIBUTE = 'sampleroom/product/sampleattribute';


    public function __construct(\Magento\Framework\App\Helper\Context $context)
    {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }


    /**
     * Get custom option string
     * @return mixed|int
     */
    public function getCartItemsLimit()
    {

        $limit = $this->_scopeConfig->getValue(self::FIELD_ITEMLIMIT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $limit;
    }


    public function getSampleAttribute()
    {
        $attributeCode = $this->_scopeConfig->getValue('sampleroom/product/sampleattribute');
        return $attributeCode;
    }


    public function getSampleOrdersEmail()
    {
        $email = $this->_scopeConfig->getValue('sampleroom/email/destination');
        return $email;
    }

    public function getSampleOrdersErrorEmail()
    {
        $email = $this->_scopeConfig->getValue('sampleroom/email/error');
        return $email;
    }

}