<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 13/04/2018
 * Time: 12:08
 */

namespace Hudson\SampleRoom\Helper;


use DOMDocument;
use Hudson\SampleRoom\Model\Order;
use Magento\Framework\Exception\LocalizedException;

class EmailHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    protected $configHelper;

    protected $localeDate;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Hudson\SampleRoom\Helper\ConfigHelper $configHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
    )
    {
        parent::__construct($context);
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->configHelper = $configHelper;
        $this->localeDate = $localeDate;
    }

    /**
     * @param $sampleOrder Order
     * @return boolean
     */
    public function sendEmail($sampleOrder)
    {

        try {

            $destinationEmail = $this->configHelper->getSampleOrdersEmail();

            if($destinationEmail == "") return false;

            $templateVariables = $this->prepareVariables($sampleOrder);

            $customerEmail = $sampleOrder->getCustomerEmail();

            $store = $this->_storeManager->getStore()->getId();
            $transport = $this->_transportBuilder->setTemplateIdentifier('sampleroom_order')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
                ->setTemplateVars($templateVariables)
                ->setFrom('general')
                ->addTo($customerEmail)
                ->addBcc($destinationEmail)
                ->getTransport();

            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->sendOrderErrorEmail($e->getMessage(),$e->getTraceAsString(),$sampleOrder);
            return false;
        }

        return true;

    }

    private function sendOrderErrorEmail($errorMessage,$stackTrace,$sampleOrder)
    {
        $errorEmail = $this->configHelper->getSampleOrdersEmail();

        if($errorEmail == "") return;

        $store = $this->_storeManager->getStore()->getId();
        $transport = $this->_transportBuilder->setTemplateIdentifier('sampleroom_order_error')
            ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
            ->setTemplateVars([
                "errorMessage" => $errorMessage,
                "stackTrace"=>$stackTrace,
                'order_id'=> $sampleOrder->getId()
            ])
            ->setFrom('general')
            // you can config general email address in Store -> Configuration -> General -> Store Email Addresses
            ->addTo($errorEmail, 'SampleRoom Error Receiver')
            ->getTransport();

        $transport->sendMessage();
    }


    /**
     * @param $sampleOrder Order
     * @return array
     */
    private function prepareVariables($sampleOrder)
    {
        $variables =
            [
                'store' => $this->_storeManager->getStore(),
                'order_id'=> $sampleOrder->getId(),
                'customer_email' => $sampleOrder->getCustomerEmail(),
                'customer_account_number' => $sampleOrder->getCustomerAccountNumber(),
                'total_items' => $sampleOrder->getTotalItems(),
                'created_at' => $this->formatDate($sampleOrder->getCreatedAt()),
                'address_html' => $this->removeTelephoneFromAddress($sampleOrder->getHtmlAddress()),
                'order' => $sampleOrder,
            ];



        return $variables;

    }

    /**
     * Retrieve formatting date
     *
     * @param null|string|\DateTime $date
     * @param int $format
     * @param bool $showTime
     * @param null|string $timezone
     * @return string
     */
    public function formatDate(
        $date = null,
        $format = \IntlDateFormatter::SHORT,
        $showTime = false,
        $timezone = null
    ) {
        $date = $date instanceof \DateTimeInterface ? $date : new \DateTime($date);
        return $this->localeDate->formatDateTime(
            $date,
            $format,
            $showTime ? $format : \IntlDateFormatter::NONE,
            null,
            $timezone
        );
    }

    private function removeTelephoneFromAddress($htmlAddress)
    {

        $doc = new DOMDocument();
        $doc->loadHTML($htmlAddress);

        $teleponeElement = $doc->getElementById('telephone');

        if($teleponeElement)      $teleponeElement->parentNode->removeChild($teleponeElement);

        $html = $doc->saveHTML();

        return str_replace("<br><br>","<br>",$html);

    }


}