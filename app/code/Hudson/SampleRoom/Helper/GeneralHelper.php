<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 13/04/2018
 * Time: 12:08
 */

namespace Hudson\SampleRoom\Helper;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;

class GeneralHelper extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $productRepository;

    protected $configHelper;


    public function __construct(\Magento\Framework\App\Helper\Context $context,
                                \Hudson\SampleRoom\Helper\ConfigHelper $configHelper,
                                ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->configHelper = $configHelper;
        parent::__construct($context);
    }


    /**
     * Get custom option string
     * @return mixed|int
     */
    public function checkIfSampleIsAvailable($productID)
    {
        $product = $this->productRepository->getById($productID);


        $sampleAttributeCode = $this->configHelper->getSampleAttribute();

        if ($sampleAttributeCode && $product->getCustomAttribute($sampleAttributeCode)->getValue()) {
            return true;
        }

        return false;
    }


}