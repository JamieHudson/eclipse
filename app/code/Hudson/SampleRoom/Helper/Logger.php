<?php

namespace Hudson\SampleRoom\Helper;

class Logger
{

    const HUDSON_EXCEPTION = 'exception';
    const HUDSON_INFO = 'info';

    public function log($msg, $type = self::HUDSON_INFO)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hudson_sampleroom_'.$type.'.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }

}