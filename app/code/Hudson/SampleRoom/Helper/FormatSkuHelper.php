<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 13/04/2018
 * Time: 12:08
 */

namespace Hudson\SampleRoom\Helper;


use Magento\Catalog\Model\Product;

class FormatSkuHelper extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @param $product Product|\Magento\Catalog\Api\Data\ProductInterface
     * @return mixed|int
     */
    public function getFormattedSku($product)
    {
        $type = $product->getTypeId();
        $sku = $product->getSku();

        if($type == "grouped"){
            $sku = str_replace("-GROUPED","",$sku);
        }

        return $sku;
    }


}