<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Hudson\SampleRoom\Block\Orders;

use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\Registry;
/**
 * Invoice view  comments form
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Info extends \Magento\Framework\View\Element\Template
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param TemplateContext $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Sample Order # %1', $this->getOrder()->getId()));
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Hudson\SampleRoom\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_sample_order');
    }
}
