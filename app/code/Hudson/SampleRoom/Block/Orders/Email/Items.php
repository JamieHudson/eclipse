<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Sales Order Email order items
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Hudson\SampleRoom\Block\Orders\Email;

class Items extends \Magento\Framework\View\Element\Template
{
}
