<?php

namespace Hudson\SampleRoom\Block\Adminhtml\Renderer;

use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

class CustomerRenderer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $customerRepository;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Context $context,
                                CustomerRepository $customerRepository,
                                array $data = [])
    {

        parent::__construct($context, $data);
        $this->_authorization = $context->getAuthorization();
        $this->customerRepository = $customerRepository;
    }

    public function render(DataObject $row)
    {

        $html = $row->getData('customer_email');

        try{
            $this->customerRepository->getById($row->getData('customer_id'));
            $url = $this->getUrl("customer/index/edit",['id'=>$row->getData('customer_id')]);
            $html = '<a href="'.$url.'">'.$row->getData('customer_email').'</a>';

        }catch (NoSuchEntityException $e){

        }

        return $html;
    }
}