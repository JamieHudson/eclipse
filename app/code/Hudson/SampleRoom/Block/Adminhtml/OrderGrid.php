<?php

namespace Hudson\SampleRoom\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Column;

class OrderGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{


    protected $modelCollectionFactory;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Hudson\SampleRoom\Model\ResourceModel\Order\CollectionFactory $modelCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {

        parent::__construct($context, $backendHelper, $data);

        $this->modelCollectionFactory = $modelCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sampleOrders');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Sample Orders'));
        $this->setSaveParametersInSession(true);
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->modelCollectionFactory->create();




        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('order_id', ['header' => __('Order ID'), 'index' => 'order_id','align' => 'center', 'type' => 'number']);
        $this->addColumn('customer_email',
            ['header' => __('Customer'),
                'index' => 'customer_email',
                'align' => 'center',
                'type' => 'renderer',
                'renderer' => '\Hudson\SampleRoom\Block\Adminhtml\Renderer\CustomerRenderer']);
        $this->addColumn('customer_account_number', ['header' => __('Customer Number (Acc No)'), 'index' => 'customer_account_number','align' => 'center', 'type' => 'text']);
        $this->addColumn('total_items', ['header' => __('Total Items'), 'index' => 'total_items','align' => 'center', 'type' => 'number']);
        $this->addColumn('html_address',
            ['header' => __('Delivery Address'),
                'index' => 'html_address',
                'align' => 'left',
                'type' => 'renderer',
                'renderer' => '\Hudson\SampleRoom\Block\Adminhtml\Renderer\AddressRenderer']);
        $this->addColumn('email_sent',
            ['header' => __('Email Sent'),
                'index' => 'email_sent',
                'align' => 'center',
                'type' => 'options',
                'options' => $this->_getBooleanOptions()]
        );
        $this->addColumn('created_at', ['header' => __('Created At'), 'index' => 'created_at','align' => 'center', 'type' => 'datetime']);

        return parent::_prepareColumns();
    }


    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item){

        return $this->getUrl('sampleroom/orders/view', ['id' => $item->getId()]);

    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/index');
    }

    private function _getBooleanOptions()
    {
        return [
            0 => "No",
            1 => "Yes"
        ];
    }


}
