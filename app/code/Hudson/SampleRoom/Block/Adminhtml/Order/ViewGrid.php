<?php

namespace Hudson\SampleRoom\Block\Adminhtml\Order;

use Magento\Backend\Block\Widget\Grid\Column;

class ViewGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{


    protected $modelCollectionFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Hudson\SampleRoom\Model\ResourceModel\Order\Item\CollectionFactory $modelCollectionFactory,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;

        parent::__construct($context, $backendHelper, $data);

        $this->modelCollectionFactory = $modelCollectionFactory;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('sampleOrderItems');
        $this->setDefaultSort('sku');
        $this->setDefaultDir('ASC');
        $this->setTitle(__('Sample Order Items'));
        $this->setSaveParametersInSession(true);
    }

    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();//get the parent class buttons
        $addButton = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
            ->setData(array(
                'label' => __('Resend Sample Order Email'),
                'onclick' => 'window.setLocation("'.$this->getUrl('*/*/sendEmail', ['_current' => true, 'id' => $this->getSampleOrder()->getId()]).'")',
                'class'   => 'task'
            ))->toHtml();
        return $html.$addButton;
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->modelCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Order\Item\Collection */
        $collection->addFieldToFilter("order_id",['eq'=>$this->getSampleOrder()->getId()]);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('sku', ['header' => __('SKU'), 'index' => 'sku','filter' => false, 'sortable' => false,'align' => 'center', 'type' => 'text']);
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name','filter' => false, 'sortable' => false,'align' => 'center', 'type' => 'text']);
        $this->addColumn('quantity', ['header' => __('Quantity'),'filter' => false, 'sortable' => false,'align' => 'center', 'type' => 'renderer','renderer' => '\Hudson\SampleRoom\Block\Adminhtml\Order\Renderer\QuantityRenderer']);

        return parent::_prepareColumns();
    }

    protected function getSampleOrder(){
        return $this->_coreRegistry->registry('current_sample_order');
    }



}
