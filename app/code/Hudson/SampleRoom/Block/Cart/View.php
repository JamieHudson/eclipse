<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/04/2018
 * Time: 16:30
 */

namespace Hudson\SampleRoom\Block\Cart;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

class View extends Template
{
    protected $customerSession;

    protected $sampleCartCollectionFactory;
    protected $productRepository;
    protected $logger;
    protected $formatSkuHelper;
    protected $imageBuilder;




    public function __construct(Template\Context $context,
                                \Hudson\SampleRoom\Helper\Logger $logger,
                                \Magento\Customer\Model\Session $customerSession,
                                \Hudson\SampleRoom\Model\ResourceModel\Cart\CollectionFactory $sampleCartCollectionFactory,
                                ProductRepositoryInterface $productRepository,
                                \Hudson\SampleRoom\Helper\FormatSkuHelper $formatSkuHelper,
                                \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
                                array $data = [])
    {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->sampleCartCollectionFactory = $sampleCartCollectionFactory;
        $this->logger = $logger;
        $this->formatSkuHelper = $formatSkuHelper;
        $this->imageBuilder = $imageBuilder;

    }


    public function getCartItems()
    {
        if (!$this->customerSession->isLoggedIn() || !($customerId = $this->customerSession->getCustomerId())) {
            return null;
        }



        if($cart = $this->getCurrentCart($customerId)) {
            return $cart->getAllItems();
        }

        return null;
    }

    public function getProductById($productId){

        $product = null;

        try {
            $product = $this->productRepository->getById($productId);
        }catch (NoSuchEntityException $e){
            $this->logger->log($e->getMessage(), \Hudson\SampleRoom\Helper\Logger::HUDSON_EXCEPTION);
        }

        return $product;
    }


    /**
     * @param $customerId
     * @return \Hudson\SampleRoom\Model\Cart|\Magento\Framework\DataObject
     */
    protected function getCurrentCart($customerId){

        $collection = $this->sampleCartCollectionFactory->create();

        /** @var $collection \Hudson\SampleRoom\Model\ResourceModel\Cart\Collection */
        $collection->addFieldToFilter("customer_id",["eq" => $customerId]);
        $customerCart = $collection->getFirstItem();

        return $customerCart;

    }

    public function getRemoveUrl(){
            return $this->getUrl('sampleroom/cart/remove');
    }

    public function formatSku($product){
        return $this->formatSkuHelper->getFormattedSku($product);
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    public function getPostUrl(){
        return $this->getUrl('sampleroom/cart/submit');
    }

}