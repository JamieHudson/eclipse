<?php

namespace Hudson\SampleRoom\Block\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 13/04/2018
 * Time: 10:20
 */
class AddSample extends \Magento\Catalog\Block\Product\View
{

    protected $configHelper;


    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Hudson\SampleRoom\Helper\ConfigHelper $configHelper,
        array $data = []
    ){
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);
        $this->configHelper = $configHelper;
    }

    public function getAddSampleUrl(){
        return $this->getUrl('sampleroom/cart/add');
    }



    public function isSampleAvailable(\Magento\Catalog\Model\Product $product){

        $attributeCode = $this->configHelper->getSampleAttribute();

        return $product->getData($attributeCode);
    }


}