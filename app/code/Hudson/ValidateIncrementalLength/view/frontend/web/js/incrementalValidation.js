requirejs([
	'jquery',
	'jquery/ui',
	'jquery/validate',
	'mage/translate'
], function ($) {
	'use strict';
	var inc_len = '';
	
	$.validator.addMethod(
		"incrementValidation",
		function(value, element) {

			value *= 100;

			if (typeof $(element).attr('data-validate-bss') !== 'undefined') {
				inc_len = parseFloat(JSON.parse($(element).attr('data-validate-bss')).incrementValidation) * 100;
			} else {
				inc_len = parseFloat(JSON.parse($(element).attr('data-validate')).incrementValidation) * 100;
			}

			return value % inc_len == 0;
		},
		function () {

		inc_len /= 100;
		return $.mage.__("Length must be in increments of " + inc_len.toString() + "m. i.e. " + 
			inc_len.toString() + ", " + (inc_len * 2).toString() + ", " + 
			(inc_len * 3).toString() + ", " + (inc_len * 4).toString() + ", " + 
			(inc_len * 5).toString())}
		);
});
