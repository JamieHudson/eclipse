<?php


namespace Hudson\FeeTaxDiscount\Helper;

use Anowave\Package\Helper\Package;

use \Magento\Tax\Model\Calculation\Rate;

class Data extends \Anowave\Fee\Helper\Data
{

    protected $pricingHelper;

	public function __construct
	(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Anowave\Fee\Model\ConditionsFactory $conditionsFactory,
		\Anowave\Fee\Model\ResourceModel\Conditions\CollectionFactory $conditionsCollectionFactory,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Quote\Model\QuoteFactory $quoteFactory,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
		\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType,
		\Anowave\Fee\Model\ResourceModel\Fee\CollectionFactory $feeCollectionFactory,
		\Magento\Tax\Model\TaxCalculation $taxCalculation,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Customer\Model\Session $customerSession,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper
	)
	{
		parent::__construct($context,$productFactory,$conditionsFactory,$conditionsCollectionFactory,$categoryFactory,$quoteFactory,$orderRepository,
            $configurableType,$feeCollectionFactory,$taxCalculation,$storeManager,$customerSession);

		$this->pricingHelper = $pricingHelper;

	}
	/**
	 * Get fee tax
	 * 
	 * @param \Magento\Quote\Model\Quote $quote
	 * @return number
	 */
	public function getFeeTax(\Magento\Quote\Model\Quote $quote = null)
    {
        $feetax = parent::getFeeTax($quote);

        if ($this->customerSession->isLoggedIn()) {

            $customer = $this->customerSession->getCustomer();
            //Check if valid user is retrieved
            if ($customer) {
                $discountPercentage = $this->pricingHelper->getOrderDiscount($customer);

                $discountMultiplier = $discountPercentage / 100;

                return round($feetax*(1 - $discountMultiplier),2);

            }
        }

        return $feetax;
    }
}