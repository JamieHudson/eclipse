<?php

namespace Hudson\UpdateProformaTax\Observer;

class ChangeTaxTotal implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $fee = $quote->getFeeAmount();
        if ($fee) {
            $feeTax = $fee / 5;
        }
        /** @var Magento\Quote\Model\Quote\Address\Total */
        $total = $observer->getData('total');

        //make sure tax value exist
        if (count($total->getAppliedTaxes()) > 0 && $fee) {
            $total->addTotalAmount('tax', $feeTax);
            $total->addBaseTotalAmount('tax', $feeTax);
            $total->setGrandTotal($total->getGrandTotal() + $feeTax);
            $total->setBaseGrandTotal($total->getBaseGrandTotal() + $feeTax);
        }

        return $this;
    }
}
