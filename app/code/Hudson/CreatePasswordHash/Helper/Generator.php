<?php

namespace Hudson\CreatePasswordHash\Helper;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Encryption\Encryptor;

/**
 *  CSV Import Handler
 */
class Generator
{

    private $encryptor;

    public function __construct(
        Encryptor $encryptor
    ) {
        $this->encryptor=$encryptor;
    }

    /**
     *
     */
    public function checkData($path, $filename)
    {


        function validateHeaders($items){

            $required = ['plain_password'];

            $errors = [];

            foreach ($required as $req){
                if(!in_array($req,$items)){
                    $errors[] = "Header '".$req."' is a required!";
                }
            }

            return $errors;
        }


        function validateRow($row,$headers,$line){

            $errors = [];
            $errorsStart = "On line ".$line." : ";

            $required = [
//                'plain_password'  //Comment out to leave blanks
            ];

            $values = array_combine($headers,$row);

            foreach ($required as $req){
                if(!isset($values[$req]) || trim($values[$req]) == ""){
                    $errors[] = $errorsStart . "Value for field '$req' is required.";
                }
            }

            return $errors;

        }

        $infilename = $path.$filename;

        $headers = [];

        if (($incoming = fopen($infilename, "r")) !== FALSE) {
            $headers = fgetcsv($incoming, ",");
        }


        for($i = 0;$i<sizeof($headers);$i++){
            $headers[$i] = trim(strtolower($headers[$i]));
        }

        $allErrors = [];

        $headersErrors = validateHeaders($headers);

        if(sizeof($headersErrors) != 0){
            return $headersErrors;
        }

        $allErrors = array_merge($allErrors,$headersErrors);
//$outcoming = fopen("product_imports_magento/".$infilename."-MAGENTO.csv", "w");

//        $child_skus = array();

        $rowNumber = 1;
        while(($row = fgetcsv($incoming)) !== FALSE ){

            $rowErrors = validateRow($row,$headers,$rowNumber);
            $allErrors = array_merge($allErrors,$rowErrors);

            $rowNumber++;
        }

        fclose($incoming);


        if(sizeof($allErrors) == 0){
            return false;
        }

        return $allErrors;
    }


    public function generateFile($path, $filename)
    {

        $errors = array();

        $infilename = $path . $filename;

        $headers = [];

        if (($incoming = fopen($infilename, "r")) !== FALSE) {
            $headers = fgetcsv($incoming, ",");
        }else{
            $errors[] = "Cannot open file!";
            return $errors;
        }


        for ($i = 0; $i < sizeof($headers); $i++) {
            $headers[$i] = trim(strtolower($headers[$i]));
        }

        $newheaders = $headers;

        $newheaders[] = 'hash';

        $newfilename = explode(".", $filename)[0] . "-MAGENTO.csv";

        $outcoming = fopen($path . $newfilename, "w");

        foreach ($newheaders as $header) {
            $header = strtolower($header);
        }

        fputcsv($outcoming, $newheaders);


        while (($row = fgetcsv($incoming)) !== FALSE) {

            $values = array_combine($headers, $row);

            $password = $values['plain_password'];
            $hash = $this->encryptor->getHash($password, true);

            fputcsv($outcoming, [$password,$hash]);


        }

        fclose($outcoming);
        fclose($incoming);


        if(sizeof($errors) != 0){
            return $errors;
        }

        return $newfilename;

    }

}