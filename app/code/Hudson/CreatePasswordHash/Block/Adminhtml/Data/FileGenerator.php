<?php

namespace Hudson\CreatePasswordHash\Block\Adminhtml\Data;

class FileGenerator extends \Magento\Backend\Block\Widget
{
    /**
     * @var string
     */
    protected $_template = 'createpasswordhash.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->setUseContainer(true);
    }
}
