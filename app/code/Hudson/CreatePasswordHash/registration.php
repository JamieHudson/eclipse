<?php
/*
use \Magento\Framework\Component\ComponentRegistrar;
*/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Hudson_CreatePasswordHash',
    __DIR__
);