<?php

namespace Hudson\CreatePasswordHash\Controller\Adminhtml\Data;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Hudson\CreatePasswordHash\Helper\Generator;

class GenerateFile extends Action
{
    protected $fileSystem;

    protected $uploaderFactory;

    protected $generator;

    protected $fileFactory;

    protected $allowedExtensions = ['csv'];

    protected $fileId = 'file_import';

    public function __construct(
        Action\Context $context,
        Filesystem $fileSystem,
        UploaderFactory $uploaderFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        Generator $generator
    )
    {
        $this->fileSystem = $fileSystem;
        $this->uploaderFactory = $uploaderFactory;
        $this->generator = $generator;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        if (!$this->getRequest()->isPost()) {
            throw new NotFoundException(__('Not found.'));
        }
        $params = $this->getRequest()->getParams();

        $destinationPath = $this->getDestinationPath();

        try {
            $uploader = $this->uploaderFactory->create(['fileId' => $this->fileId])
                ->setAllowCreateFolders(true)
                ->setAllowedExtensions($this->allowedExtensions)
                ->addValidateCallback('validate', $this, 'validateFile');
            if (!$uploader->save($destinationPath)) {
                throw new LocalizedException(
                    __('File cannot be saved to path: $1', $destinationPath)
                );
            }
            
            $filename = $uploader->getUploadedFileName();

            $generatedFileName = "";

            $errors = $this->generator->checkData($destinationPath,$filename);

            if ($errors === false) {
                $generatedFileName = $this->generator->generateFile($destinationPath,$filename);

                if(is_array($generatedFileName)){
                    $errors = $generatedFileName;
                    $generatedFileName = "";
                }
            }

            if($errors != false){

                foreach ($errors as $error){
                    $this->messageManager->addErrorMessage($error);
                }

                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

                $resultPage->setActiveMenu('Hudson_CreatePasswordHash::createpasswordhash');

                $resultPage->addContent(
                    $resultPage->getLayout()->createBlock('Hudson\CreatePasswordHash\Block\Adminhtml\Data\FileGenerator')
                );
                $resultPage->getConfig()->getTitle()->prepend(__('Password Hash File Generator'));

                return $resultPage;


            }

            $relativeToVarPath = explode("/var/",$destinationPath);
            $relativeToVarPath = $relativeToVarPath[sizeof($relativeToVarPath)-1];


            $content = array(
                'type' => 'filename',
                'value' => $relativeToVarPath.$generatedFileName,
                'rm' => true
            );

            return $this->fileFactory->create($generatedFileName, $content, DirectoryList::VAR_DIR,'text/csv');


            // @todo
            // process the uploaded file
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __($e->getMessage())
            );
        }


        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('Hudson_CreatePasswordHash::createpasswordhash');

        $resultPage->addContent(
            $resultPage->getLayout()->createBlock('Hudson\CreatePasswordHash\Block\Adminhtml\Data\FileGenerator')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Password Hash File Generator'));

        return $resultPage;
    }

    public function validateFile($filePath)
    {
        // @todo
        // custom validation code here
    }

    public function getDestinationPath()
    {
        return $this->fileSystem
            ->getDirectoryWrite(DirectoryList::TMP)
            ->getAbsolutePath('/passwordfiles/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Hudson_CreatePasswordHash::createpasswordhash'
        );

    }
}