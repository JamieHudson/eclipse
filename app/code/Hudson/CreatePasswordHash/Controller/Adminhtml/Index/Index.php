<?php
namespace Hudson\CreatePasswordHash\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Hudson\CreatePasswordHash\Controller\Adminhtml\Data
{
    /**
     * Import and export Page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('Hudson_CreatePasswordHash::createpasswordhash');

        $resultPage->addContent(
            $resultPage->getLayout()->createBlock('Hudson\CreatePasswordHash\Block\Adminhtml\Data\FileGenerator')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Password Hash File Generator'));

        return $resultPage;
    }

}
