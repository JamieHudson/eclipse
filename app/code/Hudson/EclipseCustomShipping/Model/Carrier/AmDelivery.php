<?php
namespace Hudson\EclipseCustomShipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class AmDelivery extends \Hudson\EclipseCustomShipping\Model\Carrier\AbstractCustomCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'amdelivery';

}