<?php
namespace Hudson\EclipseCustomShipping\Plugin\Checkout\Model\Checkout;


class ShippingInformation
{
    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $ext = $shippingAddress->getExtensionAttributes();
	$cust = $shippingAddress->getCustomAttributes();
	$por = null;

	if($ext){
        $por = $ext->getPurchaseOrderReference();
	} elseif($cust){
        $por = $cust->getPurchaseOrderReference();
	}
	

        if($por) {

            $quote = $this->quoteRepository->getActive($cartId);
            $quote->setPurchaseOrderReference($por);
        }
    }
}