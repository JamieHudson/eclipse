/**
 * Created by Vassil on 16/05/2018.
 */
var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/checkout-data-resolver': 'Hudson_EclipseCustomShipping/js/model/checkout-data-resolver'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Hudson_EclipseCustomShipping/js/action/set-shipping-information-mixin': true
            }
        }
    }
};