<?php

namespace Hudson\EclipseCustomShipping\Setup;

use Magento\Framework\DB\Ddl\Table as DdlTable;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();


        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'purchase_order_reference',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Purchase Order Reference',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'purchase_order_reference',
            [
                'type' => 'text',
                'nullable' => true,
                'comment' => 'Purchase Order Reference',
            ]
        );

        $installer->endSetup();
    }
}
