<?php

namespace Hudson\BasketExport\Controller\Cart;

class UpdatePost extends \Magento\Checkout\Controller\Cart\UpdatePost
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart
        );

        $this->fileFactory = $fileFactory;
        $this->cart = $cart;
    }

    protected function _printShoppingCart()
    {
        $name = date('Ymd') . '_eclipse_basket_export.csv';

        $cartItems = $this->cart->getQuote()->getAllVisibleItems();

        $csv = ['product_name,quantity,price'];

        foreach ($cartItems as $product) {
            $row = [];
            $row[] = $product->getName();
            $row[] = $product->getQty();
            $row[] = number_format($product->getPrice(), 2);

            $csv[] = implode(',', $row);
        }

        return $this->fileFactory->create($name, implode(PHP_EOL, $csv));
    }

    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            case 'print_cart':
                $this->_printShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }

        if ($updateAction != 'print_cart') {
            return $this->_goBack();
        }
    }
}
