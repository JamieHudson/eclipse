<?php

namespace hudson\CustomerAttribute\model\entity\attribute\source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ShippingDays extends AbstractSource
{
    public function getAllOptions()
    {
        return [
            'option1' => [
                'label' => 'Sunday',
                'value' => 'sunday'
            ],
            'option2' => [
                'label' => 'Monday',
                'value' => 'monday'
            ],
            'option3' => [
                'label' => 'Tuesday',
                'value' => 'tuesday'
            ],
            'option4' => [
                'label' => 'Wednesday',
                'value' => 'wednesday'
            ],
            'option5' => [
                'label' => 'Thursday',
                'value' => 'thursday'
            ],
            'option6' => [
                'label' => 'Friday',
                'value' => 'friday'
            ],
            'option7' => [
                'label' => 'Saturday',
                'value' => 'saturday'
            ]
        ];
    }
}
