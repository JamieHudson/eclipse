<?php
namespace Hudson\CustomerAttribute\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        //insert cataloginventory_stock for websites
        if (version_compare($context->getVersion(), '1.0.1') < 0)
        {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $customerSetup->addAttribute(Customer::ENTITY, 'rep_name', [
                'type' => 'varchar',
                'label' => 'Rep Name',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' =>999,
                'system' => 0,
            ]);

            $customerSetup->addAttribute(Customer::ENTITY, 'rep_email', [
                'type' => 'varchar',
                'label' => 'Rep Email',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' =>999,
                'system' => 0,
            ]);

            $customerSetup->addAttribute(Customer::ENTITY, 'rep_contact', [
                'type' => 'varchar',
                'label' => 'Rep Contact',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' =>999,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'rep_name')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer'],
                ]);

            $attribute->save();

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'rep_email')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer'],
                ]);

            $attribute->save();

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'rep_contact')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer'],
                ]);

            $attribute->save();



        }
        $setup->endSetup();
    }

}