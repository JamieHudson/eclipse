<?php

namespace Hudson\LoginCustomerFromMtm\Model;

use Magento\Store\Model\ScopeInterface;

class LoginCustomer implements \Hudson\LoginCustomerFromMtm\Api\LoginCustomerInterface
{
    const FIELD_TARGETENDPOINT = 'getdataapi/general/targetendpoint';
    const FIELD_SECRETKEY = 'getdataapi/general/secret';
    const FIELD_LISTENERCLASS = 'getdataapi/general/listenerclass';

    public function __construct(
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->customer = $customer;
        $this->customerSession = $customerSession;
        $this->response = $response;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->customerRegistry = $customerRegistry;
        $this->_secretKey = $this->getSecretKey();
        $this->customerRepository = $customerRepository;
    }

    /**
     * @api
     * @param mixed $data
     * @return mixed
     */
    public function loginCustomer($data)
    {
        $expectedKey = md5($data['email'] . $this->_secretKey);
        if ($data['hash'] === $expectedKey) {
            try {
                $customer = $this->customerRepository->get($data['email']);
            } catch (NoSuchEntityException $e) {
                throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
            }

            // $customer = $this->customer->setWebsiteId(1)->loadByEmail($data['email']);
            // $this->customerSession->setCustomerAsLoggedIn($customer);
            $this->customerSession->setCustomerDataAsLoggedIn($customer);
            $this->customerSession->setUsername($data['email']);

            // return true;
        }

        return $this->response->setRedirect('/trade-home')->sendResponse();
    }

    public function getSecretKey()
    {
        $secret = $this->_scopeConfig->getValue(self::FIELD_SECRETKEY, ScopeInterface::SCOPE_STORE);
        $secret = trim($secret);

        return $secret;
    }
}
