<?php

namespace Hudson\LoginCustomerFromMtm\Api;

interface LoginCustomerInterface
{
    /**
     * @api
     * @param mixed $data
     * @return mixed
     */
    public function loginCustomer($data);
}
