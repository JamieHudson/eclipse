<?php
/**
* Copyright Â© 2013-2017 Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Hudson\PostcodeFix\Model\Data;

/**
*
* Speical Start Date attribute backend
*
* @author      Magento Core Team <core@magentocommerce.com>
*/
  class Address extends \Magento\Customer\Model\Data\Address
  {

    public function getPostcode()
    {
      $country = $this->getCountryId();
      $pc = $this->_get(self::POSTCODE);

      if ($country == "GB")
      {
        $pc = strtoupper(str_replace(' ', '', $pc));
        $pc = substr_replace($pc, ' ', -3, 0);
      }

      return $pc;
    }
  }
