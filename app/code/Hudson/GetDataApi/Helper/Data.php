<?php

namespace Hudson\GetDataApi\Helper;

use Hudson\GetDataApi\Api\DataInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper implements DataInterface
{

    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;

    protected $_scopeConfig;

    protected $_targetEndpoint;

    protected $_secretKey;

    const FIELD_TARGETENDPOINT = 'getdataapi/general/targetendpoint';
    const FIELD_SECRETKEY = 'getdataapi/general/secret';
    const FIELD_LISTENERCLASS = 'getdataapi/general/listenerclass';


    public function __construct(\Magento\Framework\App\Helper\Context $context,
                                \Magento\Customer\Model\CustomerRegistry $customerRegistry)
    {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
        $this->customerRegistry = $customerRegistry;
        $this->_targetEndpoint = $this->getTargetEndpoint();
        $this->_secretKey = $this->getSecretKey();
    }

    /**
     * Get custom option string
     * @return mixed|string
     */
    protected function getTargetEndpoint()
    {

        $endpoint = $this->_scopeConfig->getValue(self::FIELD_TARGETENDPOINT, ScopeInterface::SCOPE_STORE);
        $endpoint = trim($endpoint);

        return $endpoint;
    }

    /**
     * Returns the set price label
     * @return mixed|string
     */
    public function getSecretKey(){
        $secret = $this->_scopeConfig->getValue(self::FIELD_SECRETKEY, ScopeInterface::SCOPE_STORE);
        $secret = trim($secret);

        return $secret;
    }


    /**
     * {@inheritdoc}
     */
    public function getData($customerId)
    {

        $customerModel = $this->customerRegistry->retrieve($customerId);

        $email = $customerModel->getEmail();
        
        $hash = md5($email.$this->_secretKey);

        $target = $this->_targetEndpoint;

        $dataArray = [];

        $dataArray['email'] = $email;
        $dataArray['hash'] = $hash;
        $dataArray['target'] = $target;

        return $dataArray;
    }

    public function getListenerClass(){
        return $this->_scopeConfig->getValue(self::FIELD_LISTENERCLASS, ScopeInterface::SCOPE_STORE);
    }


}
