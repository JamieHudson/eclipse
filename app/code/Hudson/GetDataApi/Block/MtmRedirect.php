<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 16/01/2018
 * Time: 09:29
 */

namespace Hudson\GetDataApi\Block;


class MtmRedirect extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Hudson\GetDataApi\Helper\Data
     */
    protected $dataHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Hudson\GetDataApi\Helper\Data $dataHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return string additional charge
     */
    public function getListenerClass()
    {
        return $this->dataHelper->getListenerClass();
    }
}