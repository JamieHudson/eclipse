<?php


class Product extends \Clerk\Clerk\Model\Adapter\Product{
	
	
	/**
     * Add field handlers for products
     */
    protected function addFieldHandlers()
    {

        try {

            //Add price fieldhandler
            $this->addFieldHandler('price', function ($item) {
                try {
                    if ($item->getTypeId() === Bundle::TYPE_CODE || $item->getTypeId() == "grouped") {
                        //Get minimum price for bundle products
                        $price = $item
                            ->getPriceInfo()
                            ->getPrice('final_price')
                            ->getMinimalPrice()
                            ->getValue();
                    } else {
                        $price = $item->getFinalPrice();
                    }

                    return (float)$price;
                } catch (\Exception $e) {
                    return 0;
                }
            });

            //Add list_price fieldhandler
            $this->addFieldHandler('list_price', function ($item) {
                try {
                    $price = $item->getPrice();

                    //Fix for configurable products
                    if ($item->getTypeId() === Configurable::TYPE_CODE) {
                        $price = $item->getPriceInfo()->getPrice('regular_price')->getValue();
                    }

                    if ($item->getTypeId() === Bundle::TYPE_CODE) {
                        $price = $item
                            ->getPriceInfo()
                            ->getPrice('regular_price')
                            ->getMinimalPrice()
                            ->getValue();
                    }

                    return (float)$price;
                } catch (\Exception $e) {
                    return 0;
                }
            });

            //Add image fieldhandler
            $this->addFieldHandler('image', function ($item) {
                $imageUrl = $this->imageHelper->getUrl($item);

                return $imageUrl;
            });

	//Add url fieldhandler
            $this->addFieldHandler('url', function ($item) {
                return $item->getUrlModel()->getUrl($item);
            });

            //Add categories fieldhandler
            $this->addFieldHandler('categories', function ($item) {
                return $item->getCategoryIds();
            });

            //Add age fieldhandler
            $this->addFieldHandler('age', function ($item) {
                $createdAt = strtotime($item->getCreatedAt());
                $now = time();
                $diff = $now - $createdAt;
                return floor($diff / (60 * 60 * 24));
            });

            //Add on_sale fieldhandler
            $this->addFieldHandler('on_sale', function ($item) {
                try {
                    $finalPrice = $item->getFinalPrice();
                    $price = $item->getPrice();

                    return $finalPrice < $price;
                } catch (\Exception $e) {
                    return false;
                }
            });

        } catch (\Exception $e) {

            $this->clerk_logger->error('Getting Field Handlers Error', ['error' => $e->getMessage()]);

        }
    }

	
	
	
	
	
}