<?php

namespace Hudson\FinalPriceBoxBlockExtend\Block;

use Magento\Catalog\Pricing\Price;
use Magento\Framework\Pricing\Render\PriceBox as BasePriceBox;
use Magento\Msrp\Pricing\Price\MsrpPrice;
use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;


class FinalPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox
{
	public function __construct(
		Context $context,
		SaleableInterface $saleableItem,
		PriceInterface $price,
		RendererPool $rendererPool,
		array $data = [],
		SalableResolverInterface $salableResolver = null,
		MinimalPriceCalculatorInterface $minimalPriceCalculator = null,
		\Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
		\Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
		\Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		ProductRepositoryInterface $productRepository,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Framework\Registry $registry,
		\Magento\Customer\Model\Session $customerSession
	) {
		parent::__construct($context, $saleableItem, $price, $rendererPool, $data, $salableResolver);

		$this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
		$this->pricingHelper = $pricingHelper;
		$this->multiplicationHelper = $multiplicationHelper;
		$this->hudsonLogger = $hudsonLogger;
		$this->customerSession = $customerSession;
		$this->_customerRepositoryInterface = $customerRepositoryInterface;
		$this->httpContext = $httpContext;
		$this->productRepository = $productRepository;
		$this->registry = $registry;

	}

	public function getCurrentProduct() {
		return $this->registry->registry('current_product');
	}

	public function getYourProductPrice($product)
	{
		$result = "";

		if (!$this->multiplicationHelper->isEnabledInFrontend()) return $result; //no action

		try{
			$result = $this->getDiscountedPrice($product,$result);

		} catch (\Exception $e){
			return $e->getMessage();
			return "";
		}

		return $result;
	}

	protected function getDiscountedPrice($product,$result){
		$customer = null;

		$isProductModel = false;
		$customerId = null;

		if ($this->customerSession->isLoggedIn()) {
			$customerId = $this->customerSession->getCustomerId();

		} else {
			$customerId = $this->httpContext->getValue(\Hudson\CustomerPricing\Model\Customer\Context::CONTEXT_CUSTOMER_ID);

		}

		if ($customerId) {

			$customer = $this->_customerRepositoryInterface->getById($customerId);

		}

		if ($customer) {

			$priceValue = null;

			if ($product instanceof \Magento\Framework\Pricing\SaleableInterface) {

				$productType = $product->getTypeId();


				try {
					if($productType == "grouped") {
						$product = $this->productRepository->getById($product->getId());
						$isProductModel = true;
						$priceValue = $product->getData('unitprice');
					}else{
						$priceValue = $product->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
					}
				} catch (\Exception $e) {
					//Logging exception and returning empty string to front (not showing "your price")
					$caughtAt = "Caught at: trying to get PriceInfo in getDiscountedPrice() in ListProduct.php \n";
					$logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
					$this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
					return $result;
				}
			} else {
				return $result;
			}


			//                $productModel = $this->productRepository->getById($product->getId());


			$price = $priceValue;

			//Quering the db for customer prices
			try{
				if(!$isProductModel){
					$product = $this->productRepository->getById($product->getId());
				}
				$price = $this->pricingHelper->getViewPriceForCustomer($customer, $product, $priceValue);
			}catch (\Exception $e){
				//Logging exception and returning empty string to front (not showing "your price")
				$caughtAt = "Caught at: trying to get ViewPriceForCustomer in getDiscountedPrice() in ListProduct.php \n";
				$logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
				$this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
				return $result;
			}


			if ($price && is_numeric($price)) {
				$price = round($price, 2);
			}

			$priceValue = round($priceValue,2);

			//no action if price is the same as the original (assumed priceValue is rounded with precision 2)
			if ($priceValue == $price) return $result;


			$result = $this->replacePriceInHtml($price);

		}

		return $result;
	}

	protected function replacePriceInHtml($price)
	{
		//        $priceLabel = $this->multiplicationHelper->getPriceLabel();
		//
		//        $newValue = "<div class='your-price'>". $priceLabel . "<span class='discounted-price'>" . $this->currencySymbol . $price . "</span></div>";
		//
		//
		//        return $newValue;

		return $this->currencySymbol . $price;
	}


}
