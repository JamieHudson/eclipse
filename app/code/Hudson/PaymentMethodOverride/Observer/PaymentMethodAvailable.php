<?php

namespace Hudson\PaymentMethodOverride\Observer;

use Magento\Framework\Event\ObserverInterface;

class PaymentMethodAvailable implements ObserverInterface
{
	public function __construct (\Magento\Customer\Model\Session $customerSession) {
		$this->customerSession = $customerSession;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$paymentGroup = $this->customerSession->getCustomer()->getData('payment_group');

		$method = $observer->getMethodInstance();

		$result = $observer->getResult();
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/rpl.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Customer payment group:');
        $logger->info($paymentGroup);
        $logger->info('-------');
        
		if ($paymentGroup !== 'Proforma') {
			if ($method->getCode() == 'realexpayments_hpp') {
				$result->setData('is_available', false);
			}
		}	
	}
}

