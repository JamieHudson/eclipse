<?php
namespace Hudson\CacheManager\Cron;
use Hudson\CacheManager\Model\ResourceModel\HudsonCache\Collection;
use \Psr\Log\LoggerInterface;
use Magento\Framework\App\Cache\Manager;


class CleanCache {


    const FLUSH_CACHE_TYPES = ['full_page'];

    protected $logger;
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;
    protected $cacheCollectionFactory;
    protected $cacheManager;

    public function __construct(LoggerInterface $logger,
                                \Hudson\CacheManager\Model\ResourceModel\HudsonCache\CollectionFactory $cacheCollectionFactory,
                                \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
                                \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
                                Manager $cacheManager) {
        $this->logger = $logger;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->cacheCollectionFactory = $cacheCollectionFactory;
        $this->cacheManager = $cacheManager;

    }



    /**
     * Write to system.log
     *
     * @return void
     */

    public function execute() {

//        $types = array(
//            'config',
//            'layout',
//            'block_html',
//            'collections',
//            'reflection',
//            'db_ddl',
//            'eav',
//            'config_integration',
//            'config_integration_api',
//            'full_page',
//            'translate',
//            'config_webservice'
//        );
//
//        //This cleans cache
//        foreach ($types as $type) {
//            $this->_cacheTypeList->cleanType($type);
//        }



        $invalidated = $this->cacheCollectionFactory->create()
            ->addFieldToFilter('invalidated',['eq' => 1]);

        if(sizeof($invalidated)) {

            //This flushes cache
            $this->cacheManager->flush(self::FLUSH_CACHE_TYPES);

            $this->logger->info('CACHE WAS FLUSHED BY HUDSON CLEAN CACHE CRON! INVALID ENTRY FOUND!');

            //Restoring valid state of cache in hudson table
            foreach ($invalidated as $cacheEntry){
                $cacheEntry->setInvalidated(false);
                $cacheEntry->save();
            }

        }


    }

}