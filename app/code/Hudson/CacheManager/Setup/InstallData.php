<?php

namespace Hudson\CacheManager\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    protected $hudsonCacheFactory;

    /**
     * Init
     *
     * @param PageFactory $pageFactory
     */
    public function __construct(
        \Hudson\CacheManager\Model\HudsonCacheFactory $hudsonCacheFactory
    )
    {
        $this->hudsonCacheFactory = $hudsonCacheFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $this->createCacheEntry();

        $setup->endSetup();
    }

    protected function createCacheEntry()
    {
        $data = ['cache_code' => 'all', 'invalidated' => true];

        $cache = $this->hudsonCacheFactory->create();
        $cache->setData($data);
        $cache->save();
    }
}
