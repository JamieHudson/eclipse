<?php

namespace Hudson\CacheManager\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Table containing The top level Customer - Product Contracts
         * Create table 'hud_contracts'
         */
        $cacheTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_cache'))
            ->addColumn(
                'item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Item Id'
            )
            ->addColumn(
                'cache_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false,'default' => "all"],
                'Cache Code'
            )
            ->addColumn(
                'invalidated',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                32,
                ['nullable' => false,'default' => true],
                'Invalidated'
            )
            ->setComment('Hudson Cache');
        $installer->getConnection()->createTable($cacheTable);

        $installer->endSetup();

    }
}
