<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 21/12/2017
 * Time: 09:26
 */

namespace Hudson\CacheManager\Model\ResourceModel;


class HudsonCache extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = "hud_cache";
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME,'item_id');
    }
}