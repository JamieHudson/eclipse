<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 17:06
 */

namespace Hudson\CacheManager\Model;


class HudsonCache extends \Magento\Framework\Model\AbstractModel
{

    const CACHE_TAG = 'hud_cache';

    protected function _construct()
    {
        $this->_init('Hudson\CacheManager\Model\ResourceModel\HudsonCache');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}