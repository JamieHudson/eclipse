<?php

namespace Hudson\PriceCalculator\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_scopeConfig;

    protected $_multiplier;

    protected $_enabled;

    const FIELD_ENABLED = 'pricecalculator/general/enabled';
    const FIELD_MULTIPLIER = 'pricecalculator/general/multiplier';

    
    public function __construct(\Magento\Framework\App\Helper\Context $context)
    {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);

        $this->_multiplier = $this->getMultiplier();
        $this->_enabled = $this->isEnabledInFrontend();
    }

    /**
     *
     * check the module is enabled, frontend
     *
     * @param mix $store
     * @return string
     */
    public function isEnabledInFrontend()
    {
        $isEnabled = true;
        $enabled = $this->_scopeConfig->getValue(self::FIELD_ENABLED, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }
        return $isEnabled;
    }

    protected function getMultiplier()
    {
        //length is the longest line
        $multiplier = $this->_scopeConfig->getValue(self::FIELD_MULTIPLIER, ScopeInterface::SCOPE_STORE);
        $multiplier = trim($multiplier);

        return $multiplier;
    }

    public function getMultipliedPrice($price,$postedOptions,$optionId){

        $value = $postedOptions[$optionId];

        if(!$value){
            return false;
        }

        //Making sure value is rounded and formatted for multiplication
        $value = $this->getFormattedOptionValue($value);


        if(!$value){
            return false;
        }

        return $price * $value;

    }

    protected function getFormattedOptionValue($option){

        //Checking if numeric
        if(!is_numeric($option)) return false;


        $value = (float) $option;

        $value = round($value,2);

        if($value <= 0.00) return false;

        //TODO: make custom limit dictated from backend
        if($value >= 10000) return false;

        return $value;
    }


    public function getMultiplierOptionId($customOptions){

        foreach ($customOptions as $option){
            if($option->getTitle() === $this->_multiplier) return $option->getId();
        }

        return false;
    }

}
