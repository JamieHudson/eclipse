<?php

namespace Hudson\PriceCalculator\Observer;

class UpdatePriceCart implements \Magento\Framework\Event\ObserverInterface
{

    protected $_request;
    protected $_pcHelper;
    protected $_objectManager;
    protected $redirect;
    protected $responseFactory;
    protected $messageManager;

    
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Hudson\PriceCalculator\Helper\Data $_pcHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {

        $this->_request = $request;
        $this->_pcHelper = $_pcHelper;
        $this->_objectManager = $objectManager;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->messageManager = $messageManager;
    }

    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        //check if enabled
        //proceed if not
        if(!$this->_pcHelper->isEnabledInFrontend()) return;


        $quoteItem = $observer->getEvent()->getQuoteItem();
        $_product = $observer->getProduct();
        $customOptions = $_product->getOptions();

        //Check if option is set for this product
        //proceed if not
        $optionId = $this->_pcHelper->getMultiplierOptionId($customOptions);
        if(!$optionId){
            return;
        }

        $params = $this->_request->getParams();
        $productId = $_product->getId();

        $postedOptions = [];
        if(isset($params['options'])) {
            $postedOptions = $params['options'];
        }elseif (isset($params['options_'.$productId])){
            $postedOptions = $params['options_'.$productId];
        }else{
            //CAnnot find options in request, just ignore.
            return;
        }


        $currentPrice = $quoteItem->getProduct()->getFinalPrice();


        if($customPrice = $quoteItem->getCustomPrice()){
            $currentPrice = $customPrice;

        }elseif ($originalCustomPrice = $quoteItem->getOriginalCustomPrice()){
            $currentPrice = $originalCustomPrice;
        }

        $newPrice = $this->_pcHelper->getMultipliedPrice($currentPrice,$postedOptions,$optionId);


        //Validation of the filed fails
        if(false === $newPrice){
            $redirectUrl = $this->redirect->getRedirectUrl();
            $this->messageManager->addError(_("Input in one of the fields is invalid. Please try again."));
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
            die;
        }
//        $item = ( $quoteItem->getParentItem() ? $quoteItem->getParentItem() : $quoteItem );
        $quoteItem->setCustomPrice($newPrice);
        $quoteItem->setOriginalCustomPrice($newPrice);
        $quoteItem->getProduct()->setIsSuperMode(true);
    }
}
