<?php

namespace Hudson\EmailExtension\Observer;

class Emailtemplatevars implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transport = $observer->getEvent()->getTransport();

        $customerId = $transport->getOrder()->getCustomerId();

        $customer = $this->_customerRepositoryInterface->getById($customerId);

        $transport->setData('customer_number', $customer->getCustomAttribute('customer_number'));
        $transport['customer_number'] = $customer->getCustomAttribute('customer_number')->getValue();
    }
}
