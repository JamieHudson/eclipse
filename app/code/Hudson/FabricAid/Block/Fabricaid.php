<?php   
namespace Hudson\FabricAid\Block;

use \Magento\Catalog\Block\Product\ListProduct;

class Fabricaid extends \Magento\Framework\View\Element\Template
{   
    private $_productRepository; 
    protected $_productCollectionFactory;
    
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,

        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->listProductBlock = $listProductBlock;
        $this->_productRepository = $productRepository;
    }

    public function getProductCollection($sku)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->addFieldToFilter('sku', array('in' => array($sku)))->addAttributeToSelect('*')->load();
        
        return $collection;
    }
    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }
    
    public function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}
	
    public function getProductBySku($sku)
    {
            return $this->_productRepository->get($sku);
    }
    
    
    public function getMetres($rangeVal, $drop, $type){
        
        $rangeArray = explode("//", $rangeVal);
        $range = $rangeArray[0];
        $sku = $rangeArray[1]."-001";
        
        $dropInMetres = $drop/1000;
       
        $fullType = $type."-".$range;
        
        $multiplier = [
            "Free Hanging-Pleated" => 1.4,
            "Free Hanging-Hive Blackout" => 1.3,
            "Free Hanging-Hive Standard" => 1.1,
            "Free Hanging-Hive Textured" => 1.3,
            "Free Hanging-Hive Micro" => 1.4,
            "Tensioned-Pleated" => 1.4,
            "Tensioned-Hive Blackout" => 1.1,
            "Tensioned-Hive Standard" => 1.1,
            "Tensioned-Hive Textured" => 1.1,
            "Tensioned-Hive Micro" => 1.4
        ];
        
        $finalMultiplier = $multiplier[$fullType];
        
        $result["metres"] = $dropInMetres*$finalMultiplier;
        $result["sku"] = $sku;
        
        if( isset($result) && !empty($result) ){
            return $result;
        } else {
            return false;
        }
        
    }
    
    
    public function getForm(){
        ?>
            <form id='fabric_aid' action="/fabricaid/Index/Index" method="post">
                <label for="type">Blind Type</label>
                <select id="type" name="type">
                    <option value="" disabled selected>- select -</option>
                    <option>Tensioned</option>
                    <option>Free Hanging</option>
                </select>
                
                <label for="range">Range</label>
                <select name="range">
                    <option value="" disabled selected>- select -</option>
                    <option value="Pleated//PX37513">Argan asc eco - Moon - PX37513</option>
                    <option value="Pleated//PX37511">Argan asc eco - Sky - PX37511</option>
                    <option value="Pleated//PX37512">Argan asc eco - Vapour - PX37512</option>
                    <option value="Pleated//PX37521">Cactus asc eco - Light Oak - PX37521</option>
                    <option value="Pleated//PX37522">Cactus asc eco - Olive Green - PX37522</option>
                    <option value="Pleated//PX37523">Cactus asc eco - Stone Grey - PX37523</option>
                    <option value="Pleated//PX36603">Calia asc FR - Beige - PX36603</option>
                    <option value="Pleated//PX36606">Calia asc FR - Charcoal - PX36606</option>
                    <option value="Pleated//PX36608">Calia asc FR - Chilli - PX36608</option>
                    <option value="Pleated//PX36602">Calia asc FR - Cream - PX36602</option>
                    <option value="Pleated//PX36607">Calia asc FR - Emerald - PX36607</option>
                    <option value="Pleated//PX36605">Calia asc FR - Iron - PX36605</option>
                    <option value="Pleated//PX36609">Calia asc FR - Mustard - PX36609</option>
                    <option value="Pleated//PX36610">Calia asc FR - Nutmeg - PX36610</option>
                    <option value="Pleated//PX36604">Calia asc FR - Taupe - PX36604</option>
                    <option value="Pleated//PX36601">Calia asc FR - White - PX36601</option>
                    <option value="Pleated//PX65101">Capella - Gold - PX65101</option>
                    <option value="Pleated//PX65102">Capella - Silver - PX65102</option>
                    <option value="Pleated//PX37403">Charlotte asc - Rose - PX37403</option>
                    <option value="Pleated//PX37401">Charlotte asc - Snowdrop - PX37401</option>
                    <option value="Pleated//PX37404">Charlotte asc - Spring - PX37404</option>
                    <option value="Pleated//PX37402">Charlotte asc - Vanilla - PX37402</option>
                    <option value="Pleated//PX36513">Chenille asc - Beige - PX36513</option>
                    <option value="Pleated//PX36512">Chenille asc - Cream - PX36512</option>
                    <option value="Pleated//PX36511">Chenille asc - White - PX36511</option>
                    <option value="Pleated//PX80511">Daze asc - Noir - PX80511</option>
                    <option value="Pleated//PX6011">Galaxy asc Blackout - Calico - PX6011</option>
                    <option value="Pleated//PX6012">Galaxy asc Blackout - Concrete - PX6012</option>
                    <option value="Pleated//PX6001">Galaxy asc Blackout - Ecru - PX6001</option>
                    <option value="Pleated//PX6002">Galaxy asc Blackout - Silver - PX6002</option>
                    <option value="Pleated//PX6010">Galaxy asc Blackout - White - PX6010</option>
                    <option value="Pleated//PX65022">Grid - Cream - PX65022</option>
                    <option value="Pleated//PX65025">Grid - Graphite - PX65025</option>
                    <option value="Pleated//PX65028">Grid - Moss - PX65028</option>
                    <option value="Pleated//PX65026">Grid - Mulberry - PX65026</option>
                    <option value="Pleated//PX65027">Grid - Sapphire - PX65027</option>
                    <option value="Pleated//PX65024">Grid - Silver - PX65024</option>
                    <option value="Pleated//PX65023">Grid - Taupe - PX65023</option>
                    <option value="Pleated//PX65021">Grid - White - PX65021</option>
                    <option value="Pleated//PX64104">Halo Blackout - Dolphin - PX64104</option>
                    <option value="Pleated//PX64105">Halo Blackout - Elephant - PX64105</option>
                    <option value="Pleated//PX64106">Halo Blackout - Heron - PX64106</option>
                    <option value="Pleated//PX64103">Halo Blackout - Oyster - PX64103</option>
                    <option value="Pleated//PX64107">Halo Blackout - Panther - PX64107</option>
                    <option value="Pleated//PX64102">Halo Blackout - Papyrus - PX64102</option>
                    <option value="Pleated//PX64101">Halo Blackout - Swan - PX64101</option>
                    <option value="Pleated//PX6352">Hampton - Cream - PX6352</option>
                    <option value="Pleated//PX6353">Hampton - Silver - PX6353</option>
                    <option value="Pleated//PX6351">Hampton - White - PX6351</option>
                    <option value="Pleated//PX37012">Harlow asc  - Cream - PX37012</option>
                    <option value="Pleated//PX37013">Harlow asc  - Magnolia - PX37013</option>
                    <option value="Pleated//PX37015">Harlow asc  - Pewter - PX37015</option>
                    <option value="Pleated//PX37014">Harlow asc  - Silver - PX37014</option>
                    <option value="Pleated//PX37011">Harlow asc  - White - PX37011</option>
                    <option value="Pleated//PX80103">Helix - Pink - PX80103</option>
                    <option value="Pleated//PX80102">Helix - Silver - PX80102</option>
                    <option value="Pleated//PX80101">Helix - White - PX80101</option>
                    <option value="Pleated//PX37531">Hemp asc eco - Biscotti - PX37531</option>
                    <option value="Pleated//PX37532">Hemp asc eco - Ebony - PX37532</option>
                    <option value="Pleated//PX37533">Hemp asc eco - Porcelain - PX37533</option>
                    <option value="Hive Blackout//PX72003">Hive Blackout - Barley - PX72003</option>
                    <option value="Hive Blackout//PX72017">Hive Blackout - Berry - PX72017</option>
                    <option value="Hive Blackout//PX72004">Hive Blackout - Black - PX72004</option>
                    <option value="Hive Blackout//PX72015">Hive Blackout - Concrete - PX72015</option>
                    <option value="Hive Blackout//PX72002">Hive Blackout - Cream - PX72002</option>
                    <option value="Hive Blackout//PX72009">Hive Blackout - Denim - PX72009</option>
                    <option value="Hive Blackout//PX72016">Hive Blackout - Emerald - PX72016</option>
                    <option value="Hive Blackout//PX72007">Hive Blackout - Fudge - PX72007</option>
                    <option value="Hive Blackout//PX72013">Hive Blackout - Green - PX72013</option>
                    <option value="Hive Blackout//PX72005">Hive Blackout - Hessian - PX72005</option>
                    <option value="Hive Blackout//PX72006">Hive Blackout - Iron - PX72006</option>
                    <option value="Hive Blackout//PX72011">Hive Blackout - Teal - PX72011</option>
                    <option value="Hive Blackout//PX72001">Hive Blackout - White - PX72001</option>
                    <option value="Hive Blackout//PX73005">Hive Blackout FR - Cream - PX73005</option>
                    <option value="Hive Blackout//PX73004">Hive Blackout FR - Concrete - PX73004</option>
                    <option value="Hive Blackout//PX73003">Hive Blackout FR - Iron - PX73003</option>
                    <option value="Hive Blackout//PX73002">Hive Blackout FR - Sand - PX73002</option>
                    <option value="Hive Blackout//PX73001">Hive Blackout FR - White - PX73001</option>
                    <option value="Hive Standard//PX80411">Hive Decadence - Black - PX80411</option>
                    <option value="Hive Standard//PX74007">Hive Deluxe - Celeste - PX74007</option>
                    <option value="Hive Standard//PX74003">Hive Deluxe - Dove - PX74003</option>
                    <option value="Hive Standard//PX74010">Hive Deluxe - Lemon - PX74010</option>
                    <option value="Hive Standard//PX74009">Hive Deluxe - Mauve - PX74009</option>
                    <option value="Hive Standard//PX74005">Hive Deluxe - Midnight - PX74005</option>
                    <option value="Hive Standard//PX74004">Hive Deluxe - Nutshell - PX74004</option>
                    <option value="Hive Standard//PX74006">Hive Deluxe - Onyx - PX74006</option>
                    <option value="Hive Standard//PX74002">Hive Deluxe - Oyster - PX74002</option>
                    <option value="Hive Standard//PX74008">Hive Deluxe - Sage - PX74008</option>
                    <option value="Hive Standard//PX74011">Hive Deluxe - Steel - PX74011</option>
                    <option value="Hive Standard//PX74001">Hive Deluxe - Swan - PX74001</option>
                    <option value="Hive Blackout//PX75001">Hive Deluxe Blackout - Dove - PX75001</option>
                    <option value="Hive Blackout//PX75006">Hive Deluxe Blackout - Mauve - PX75006</option>
                    <option value="Hive Blackout//PX75007">Hive Deluxe Blackout - Nutshell - PX75007</option>
                    <option value="Hive Blackout//PX75008">Hive Deluxe Blackout - Onyx - PX75008</option>
                    <option value="Hive Blackout//PX75009">Hive Deluxe Blackout - Oyster - PX75009</option>
                    <option value="Hive Blackout//PX75004">Hive Deluxe Blackout - Rose - PX75004</option>
                    <option value="Hive Blackout//PX75005">Hive Deluxe Blackout - Sage - PX75005</option>
                    <option value="Hive Blackout//PX75003">Hive Deluxe Blackout - Sky - PX75003</option>
                    <option value="Hive Blackout//PX75002">Hive Deluxe Blackout - Steel - PX75002</option>
                    <option value="Hive Blackout//PX75010">Hive Deluxe Blackout - Swan - PX75010</option>
                    <option value="Hive Standard//PX80421">Hive Dolce - Green - PX80421</option>
                    <option value="Hive Standard//PX80422">Hive Dolce - Spice - PX80422</option>
                    <option value="Hive Standard//PX80431">Hive Envy - Moonrock - PX80431</option>
                    <option value="Hive Blackout//PX78703">Hive Geo Blackout - Oyster - PX78703</option>
                    <option value="Hive Blackout//PX78701">Hive Geo Blackout - Primrose - PX78701</option>
                    <option value="Hive Blackout//PX78702">Hive Geo Blackout - Titanium - PX78702</option>
                    <option value="Hive Textured//PX76003">Hive Gratia - Granite - PX76003</option>
                    <option value="Hive Textured//PX76001">Hive Gratia - Pearl - PX76001</option>
                    <option value="Hive Textured//PX76002">Hive Gratia - Sky - PX76002</option>
                    <option value="Hive Textured//PX76004">Hive Gratia - Swan - PX76004</option>
                    <option value="Hive Standard//PX80451">Hive Idole - Grey - PX80451</option>
                    <option value="Hive Standard//PX80452">Hive Idole - Forest Green - PX80452</option>
                    <option value="Hive Textured//PX80201">Hive Lusso - Ice - PX80201</option>
                    <option value="Hive Textured//PX80204">Hive Lusso - Silver  - PX80204</option>
                    <option value="Hive Textured//PX80205">Hive Lusso - Steel - PX80205</option>
                    <option value="Hive Textured//PX80202">Hive Lusso - Stone  - PX80202</option>
                    <option value="Hive Textured//PX80203">Hive Lusso - Walnut - PX80203</option>
                    <option value="Hive Standard//PX78101">Hive Matrix - Cream - PX78101</option>
                    <option value="Hive Standard//PX78102">Hive Matrix - Silver - PX78102</option>
                    <option value="Hive Blackout//PX78601">Hive Matrix Blackout - Cream - PX78601</option>
                    <option value="Hive Blackout//PX78602">Hive Matrix Blackout - Silver - PX78602</option>
                    <option value="Hive Micro//PX90502">Hive Micro - 18mm - Cream - PX90502</option>
                    <option value="Hive Micro//PX90503">Hive Micro - 18mm - Grey - PX90503</option>
                    <option value="Hive Micro//PX90501">Hive Micro - 18mm - White - PX90501</option>
                    <option value="Hive Standard//PX80461">Hive Muse - Blue - PX80461</option>
                    <option value="Hive Standard//PX80462">Hive Muse - Grey - PX80462</option>
                    <option value="Hive Standard//PX80463">Hive Muse - Neutral - PX80463</option>
                    <option value="Hive Standard//PX80441">Hive Nirvana - Black - PX80441</option>
                    <option value="Hive Standard//PX71003">Hive Plain - Barley - PX71003</option>
                    <option value="Hive Standard//PX71004">Hive Plain - Black - PX71004</option>
                    <option value="Hive Standard//PX71009">Hive Plain - Concrete - PX71009</option>
                    <option value="Hive Standard//PX71002">Hive Plain - Cream - PX71002</option>
                    <option value="Hive Standard//PX71005">Hive Plain - Hessian - PX71005</option>
                    <option value="Hive Standard//PX71006">Hive Plain - Iron - PX71006</option>
                    <option value="Hive Standard//PX71001">Hive Plain - White - PX71001</option>
                    <option value="Hive Standard//PX73502">Hive Plain FR - Concrete - PX73502</option>
                    <option value="Hive Standard//PX73501">Hive Plain FR - Sand - PX73501</option>
                    <option value="Hive Standard//PX73503">Hive Plain FR - White - PX73503</option>
                    <option value="Hive Standard//PX78004">Hive Silkweave - Ash - PX78004</option>
                    <option value="Hive Standard//PX78001">Hive Silkweave - Elephant - PX78001</option>
                    <option value="Hive Standard//PX78002">Hive Silkweave - Hills - PX78002</option>
                    <option value="Hive Standard//PX78003">Hive Silkweave - Raven - PX78003</option>
                    <option value="Hive Blackout//PX78504">Hive Silkweave Blackout - Ash - PX78504</option>
                    <option value="Hive Blackout//PX78501">Hive Silkweave Blackout - Elephant - PX78501</option>
                    <option value="Hive Blackout//PX78502">Hive Silkweave Blackout - Hills - PX78502</option>
                    <option value="Hive Blackout//PX78503">Hive Silkweave Blackout - Raven - PX78503</option>
                    <option value="Hive Textured//PX77002">Hive Telia - Dove - PX77002</option>
                    <option value="Hive Textured//PX77003">Hive Telia - Nutshell - PX77003</option>
                    <option value="Hive Textured//PX77005">Hive Telia - Onyx - PX77005</option>
                    <option value="Hive Textured//PX77004">Hive Telia - Oyster - PX77004</option>
                    <option value="Hive Textured//PX77001">Hive Telia - Sapphire - PX77001</option>
                    <option value="Hive Textured//PX77006">Hive Telia - Swan - PX77006</option>
                    <option value="Pleated//PX80531">Iconic asc - Beige - PX80531</option>
                    <option value="Pleated//PX80532">Iconic asc - Grey - PX80532</option>
                    <option value="Pleated//PX4003">Infusion - Beige - PX4003</option>
                    <option value="Pleated//PX4002">Infusion - Cream - PX4002</option>
                    <option value="Pleated//PX4001">Infusion - White - PX4001</option>
                    <option value="Pleated//PX4148">Infusion asc - Aqua - PX4148</option>
                    <option value="Pleated//PX4118">Infusion asc - Azure - PX4118</option>
                    <option value="Pleated//PX4104">Infusion asc - Beige - PX4104</option>
                    <option value="Pleated//PX4124">Infusion asc - Black - PX4124</option>
                    <option value="Pleated//PX4125">Infusion asc - Calico - PX4125</option>
                    <option value="Pleated//PX4138">Infusion asc - Charcoal - PX4138</option>
                    <option value="Pleated//PX4106">Infusion asc - Coffee - PX4106</option>
                    <option value="Pleated//PX4128">Infusion asc - Concrete - PX4128</option>
                    <option value="Pleated//PX4149">Infusion asc - Cool Mint - PX4149</option>
                    <option value="Pleated//PX4142">Infusion asc - Coral - PX4142</option>
                    <option value="Pleated//PX4102">Infusion asc - Cream - PX4102</option>
                    <option value="Pleated//PX4143">Infusion asc - Crimson - PX4143</option>
                    <option value="Pleated//PX4146">Infusion asc - Ecru - PX4146</option>
                    <option value="Pleated//PX4150">Infusion asc - Fawn - PX4150</option>
                    <option value="Pleated//PX4144">Infusion asc - Forest Green - PX4144</option>
                    <option value="Pleated//PX4136">Infusion asc - Glacier Blue - PX4136</option>
                    <option value="Pleated//PX4116">Infusion asc - Grape - PX4116</option>
                    <option value="Pleated//PX4145">Infusion asc - Indigo - PX4145</option>
                    <option value="Pleated//PX4127">Infusion asc - Iron - PX4127</option>
                    <option value="Pleated//PX4103">Infusion asc - Ivory - PX4103</option>
                    <option value="Pleated//PX4120">Infusion asc - Jade - PX4120</option>
                    <option value="Pleated//PX4108">Infusion asc - Lemon - PX4108</option>
                    <option value="Pleated//PX4131">Infusion asc - Lime Green - PX4131</option>
                    <option value="Pleated//PX4126">Infusion asc - Magnolia - PX4126</option>
                    <option value="Pleated//PX4129">Infusion asc - Muted Gold - PX4129</option>
                    <option value="Pleated//PX4140">Infusion asc - Ochre - PX4140</option>
                    <option value="Pleated//PX4130">Infusion asc - Olive Haze - PX4130</option>
                    <option value="Pleated//PX4117">Infusion asc - Pale Blue - PX4117</option>
                    <option value="Pleated//PX4147">Infusion asc - Pewter - PX4147</option>
                    <option value="Pleated//PX4139">Infusion asc - Primrose - PX4139</option>
                    <option value="Pleated//PX4115">Infusion asc - Purple - PX4115</option>
                    <option value="Pleated//PX4112">Infusion asc - Raspberry - PX4112</option>
                    <option value="Pleated//PX4141">Infusion asc - Saffron - PX4141</option>
                    <option value="Pleated//PX4151">Infusion asc - Sand - PX4151</option>
                    <option value="Pleated//PX4152">Infusion asc - Soft Lilac - PX4152</option>
                    <option value="Pleated//PX4137">Infusion asc - Stone Grey - PX4137</option>
                    <option value="Pleated//PX4105">Infusion asc - Taupe - PX4105</option>
                    <option value="Pleated//PX4119">Infusion asc - Teal - PX4119</option>
                    <option value="Pleated//PX4110">Infusion asc - Terracotta - PX4110</option>
                    <option value="Pleated//PX4153">Infusion asc - Tuscan - PX4153</option>
                    <option value="Pleated//PX4101">Infusion asc - White - PX4101</option>
                    <option value="Pleated//PX5004">Infusion asc FR - Beige - PX5004</option>
                    <option value="Pleated//PX5005">Infusion asc FR - Black - PX5005</option>
                    <option value="Pleated//PX5002">Infusion asc FR - Cream - PX5002</option>
                    <option value="Pleated//PX5003">Infusion asc FR - Ivory - PX5003</option>
                    <option value="Pleated//PX5001">Infusion asc FR - White - PX5001</option>
                    <option value="Hive Micro//PXM4104">Infusion asc Micro - 16mm - Beige - PXM4104</option>
                    <option value="Hive Micro//PXM4124">Infusion asc Micro - 16mm - Black - PXM4124</option>
                    <option value="Hive Micro//PXM4128">Infusion asc Micro - 16mm - Concrete - PXM4128</option>
                    <option value="Hive Micro//PXM4102">Infusion asc Micro - 16mm - Cream - PXM4102</option>
                    <option value="Hive Micro//PXM4127">Infusion asc Micro - 16mm - Iron - PXM4127</option>
                    <option value="Hive Micro//PXM4103">Infusion asc Micro - 16mm - Ivory - PXM4103</option>
                    <option value="Hive Micro//PXM4105">Infusion asc Micro - 16mm - Taupe - PXM4105</option>
                    <option value="Hive Micro//PXM4101">Infusion asc Micro - 16mm - White - PXM4101</option>
                    <option value="Pleated//PX51002">Infusion FR asc eco - Calico - PX51002</option>
                    <option value="Pleated//PX51005">Infusion FR asc eco - Concrete - PX51005</option>
                    <option value="Pleated//PX51004">Infusion FR asc eco - Pewter - PX51004</option>
                    <option value="Pleated//PX51003">Infusion FR asc eco - Stone Grey - PX51003</option>
                    <option value="Pleated//PX51001">Infusion FR asc eco - White - PX51001</option>
                    <option value="Pleated//PX36501">Jasmine asc - Bamboo - PX36501</option>
                    <option value="Pleated//PX36502">Jasmine asc - Ice - PX36502</option>
                    <option value="Pleated//PX36503">Jasmine asc - Iris - PX36503</option>
                    <option value="Pleated//PX36504">Jasmine asc - Mulberry - PX36504</option>
                    <option value="Pleated//PX37203">Luxe asc - Iron - PX37203</option>
                    <option value="Pleated//PX37201">Luxe asc - Snowdrop - PX37201</option>
                    <option value="Pleated//PX37202">Luxe asc - Vanilla - PX37202</option>
                    <option value="Pleated//PX36056">Mariella - Charcoal - PX36056</option>
                    <option value="Pleated//PX36054">Mariella - Havana - PX36054</option>
                    <option value="Pleated//PX36051">Mariella - Snowdrop - PX36051</option>
                    <option value="Pleated//PX36052">Mariella - Vanilla - PX36052</option>
                    <option value="Pleated//PX80013">Meadow Flower asc - Grape - PX80013</option>
                    <option value="Pleated//PX80011">Meadow Flower asc - Redcurrant - PX80011</option>
                    <option value="Pleated//PX80012">Meadow Flower asc - Spring - PX80012</option>
                    <option value="Pleated//PX36102">Mineral asc - Cream - PX36102</option>
                    <option value="Pleated//PX36104">Mineral asc - Fawn - PX36104</option>
                    <option value="Pleated//PX36105">Mineral asc - Iron - PX36105</option>
                    <option value="Pleated//PX36101">Mineral asc - Ivory - PX36101</option>
                    <option value="Pleated//PX36106">Mineral asc - Onyx - PX36106</option>
                    <option value="Pleated//PX36103">Mineral asc - Papyrus - PX36103</option>
                    <option value="Pleated//PX3133">Nordic asc - Cookie - PX3133</option>
                    <option value="Pleated//PX3131">Nordic asc - Ice - PX3131</option>
                    <option value="Pleated//PX64202">Perla FR asc - Bronze - PX64202</option>
                    <option value="Pleated//PX64203">Perla FR asc - Iron - PX64203</option>
                    <option value="Pleated//PX64201">Perla FR asc - Satin - PX64201</option>
                    <option value="Pleated//PX64204">Perla FR asc - Steel - PX64204</option>
                    <option value="Pleated//PX37503">Radiance asc - Atlantic Blue - PX37503</option>
                    <option value="Pleated//PX37501">Radiance asc - Bright White - PX37501</option>
                    <option value="Pleated//PX37502">Radiance asc - Metallic Bronze - PX37502</option>
                    <option value="Pleated//PX37504">Radiance asc - Pearl Grey - PX37504</option>
                    <option value="Pleated//PXM37503">Radiance asc Micro - Atlantic Blue - PXM37503</option>
                    <option value="Pleated//PXM37501">Radiance asc Micro - Bright White - PXM37501</option>
                    <option value="Pleated//PXM37502">Radiance asc Micro - Metallic Bronze - PXM37502</option>
                    <option value="Pleated//PXM37504">Radiance asc Micro - Pearl Grey - PXM37504</option>
                    <option value="Pleated//PX65005">Reed  - Midnight - PX65005</option>
                    <option value="Pleated//PX65004">Reed  - Mocha - PX65004</option>
                    <option value="Pleated//PX65002">Reed  - Oyster - PX65002</option>
                    <option value="Pleated//PX65003">Reed  - Papyrus - PX65003</option>
                    <option value="Pleated//PX65001">Reed  - White - PX65001</option>
                    <option value="Pleated//PX37108">Ribbons asc - Blue - PX37108</option>
                    <option value="Pleated//PX37102">Ribbons asc - Cream - PX37102</option>
                    <option value="Pleated//PX37104">Ribbons asc - Fawn - PX37104</option>
                    <option value="Pleated//PX37109">Ribbons asc - Ink Blue - PX37109</option>
                    <option value="Pleated//PX37103">Ribbons asc - Magnolia - PX37103</option>
                    <option value="Pleated//PX37106">Ribbons asc - Pewter - PX37106</option>
                    <option value="Pleated//PX37105">Ribbons asc - Silver - PX37105</option>
                    <option value="Pleated//PX37107">Ribbons asc - Teal - PX37107</option>
                    <option value="Pleated//PX37101">Ribbons asc - White - PX37101</option>
                    <option value="Hive Micro//PXM37102">Ribbons asc Micro - Cream - PXM37102</option>
                    <option value="Hive Micro//PXM37106">Ribbons asc Micro - Pewter - PXM37106</option>
                    <option value="Hive Micro//PXM37105">Ribbons asc Micro - Silver - PXM37105</option>
                    <option value="Hive Micro//PXM37101">Ribbons asc Micro - White - PXM37101</option>
                    <option value="Pleated//PX80111">Sakura - Grey - PX80111</option>
                    <option value="Pleated//PX80113">Sakura - Pink - PX80113</option>
                    <option value="Pleated//PX80112">Sakura - Primrose - PX80112</option>
                    <option value="Pleated//PX61003">Shadow Blackout - Amber - PX61003</option>
                    <option value="Pleated//PX61001">Shadow Blackout - Graphite - PX61001</option>
                    <option value="Pleated//PX61005">Shadow Blackout - Navy - PX61005</option>
                    <option value="Pleated//PX61004">Shadow Blackout - Rubin - PX61004</option>
                    <option value="Pleated//PX61006">Shadow Blackout - Wheat - PX61006</option>
                    <option value="Pleated//PX61002">Shadow Blackout - White - PX61002</option>
                    <option value="Pleated//PX37541">Shea asc eco - Ash - PX37541</option>
                    <option value="Pleated//PX37542">Shea asc eco - Clay - PX37542</option>
                    <option value="Pleated//PX37543">Shea asc eco - Teak - PX37543</option>
                    <option value="Pleated//PX37302">Sicily asc - Cream - PX37302</option>
                    <option value="Pleated//PX37301">Sicily asc - White - PX37301</option>
                    <option value="Pleated//PX80502">Sprinkle asc - Fuschia - PX80502</option>
                    <option value="Pleated//PX80501">Sprinkle asc - Hemp - PX80501</option>
                    <option value="Pleated//PX80503">Sprinkle asc - Lime - PX80503</option>
                    <option value="Pleated//PX80521">Vita asc - Azure - PX80521</option>
                    <option value="Pleated//PX3272">Woodbark asc - Beech - PX3272</option>
                    <option value="Pleated//PX3273">Woodbark asc - Chestnut - PX3273</option>
                    <option value="Pleated//PX3276">Woodbark asc - Forest Pine - PX3276</option>
                    <option value="Pleated//PX3271">Woodbark asc - White Oak - PX3271</option>
                </select>
                <?php echo $this->getDrop(); ?>
                <input type="submit" value="Calculate Length">
            </form>
        <?php
    }
    
    public function getDrop(){ 
        ?>
            <label for="drop">Drop</label>
            <select name="drop" id="drop">
                <option selected disabled>- Select -</option>
                
                <?php 
                    for ($x = 140; $x <= 3600; $x+=20) {
                        echo "<option>".$x."</option>";
                    }
                ?>
            </select>
        <?php
    }
    
      
} 
