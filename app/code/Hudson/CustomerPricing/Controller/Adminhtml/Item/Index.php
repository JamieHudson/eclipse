<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 16/02/2018
 * Time: 12:59
 */

namespace Hudson\CustomerPricing\Controller\Adminhtml\Item;


use Hudson\CustomerPricing\Controller\Adminhtml\Discounts;
use Magento\Framework\App\ResponseInterface;

class Index extends Discounts
{

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Item Discounts'));
        $this->_view->renderLayout();
    }
}