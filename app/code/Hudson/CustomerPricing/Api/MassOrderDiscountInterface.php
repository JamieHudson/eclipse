<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 14:29
 */

namespace Hudson\CustomerPricing\Api;


interface MassOrderDiscountInterface
{
    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function createRecords($records);

    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function deleteRecords($records);
}