<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:04
 */

namespace Hudson\CustomerPricing\Api;


interface PromotionInterface
{
    /**
     * @param string $customerNumber
     * @param string $productCode
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return mixed
     */
    public function createRecord($customerNumber, $productCode,$discountPercentage, $validFrom, $validTo, $discountCode);


    /**
     * @param string $customerNumber
     * @param string $productCode
     * @return bool
     */
    public function deleteRecord($customerNumber,$productCode);


}