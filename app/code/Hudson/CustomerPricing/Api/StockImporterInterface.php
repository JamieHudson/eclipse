<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 11:26
 */

namespace Hudson\CustomerPricing\Api;


interface StockImporterInterface
{

    /**
     * @param string $productCode
     * @param int $qty
     * @return bool
     */
    public function updateStock($productCode,$qty);

}