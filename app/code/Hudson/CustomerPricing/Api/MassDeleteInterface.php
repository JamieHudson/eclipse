<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 14:29
 */

namespace Hudson\CustomerPricing\Api;


interface MassDeleteInterface
{
    /**
     * Deletes all discounts.
     * Typically used overnight before reimporting all discounts again.
     * @return mixed
     */
    public function deleteAllDiscounts();

}