<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 14:29
 */

namespace Hudson\CustomerPricing\Api;


interface MassCreateInterface
{
    /**
     * @param array $entries
     * @return mixed
     */
    public function createContracts($entries);

    /**
     * @param array $entries
     * @return mixed
     */
    public function createItemDiscounts($entries);

    /**
     * @param array $entries
     * @return mixed
     */
    public function createGroupDiscounts($entries);

    /**
     * @param array $entries
     * @return mixed
     */
    public function createFamilyDiscounts($entries);

    /**
     * @param array $entries
     * @return mixed
     */
    public function createPromotions($entries);

    /**
     * @param array $entries
     * @return mixed
     */
    public function createOrderDiscounts($entries);
}