<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:05
 */

namespace Hudson\CustomerPricing\Api;


interface OrderDiscountInterface
{
    /**
     * @param string $customerNumber
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return mixed
     */
    public function createRecord($customerNumber,$discountPercentage, $validFrom, $validTo, $discountCode);

    /**
     * @param string $customerNumber
     * @return bool
     */
    public function deleteRecord($customerNumber);
}