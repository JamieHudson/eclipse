<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:03
 */

namespace Hudson\CustomerPricing\Api;


interface ContractInterface
{

    /**
     * @api
     * @param string $customerNumber
     * @param string $productCode
     * @param float $specialPrice
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return mixed
     */
    public function createRecord($customerNumber, $productCode,$specialPrice,$discountPercentage, $validFrom, $validTo, $discountCode);

    /**
     * @api
     * @param string $customerNumber
     * @param string $productCode
     * @return bool
     */
    public function deleteRecord($customerNumber,$productCode);

}