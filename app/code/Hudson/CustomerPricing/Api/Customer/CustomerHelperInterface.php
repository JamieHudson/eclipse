<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:03
 */

namespace Hudson\CustomerPricing\Api\Customer;


interface CustomerHelperInterface
{

    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function setAccountNumbers($records);

    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function setRepDetails($records);

}