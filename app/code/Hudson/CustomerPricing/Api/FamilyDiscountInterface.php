<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:04
 */

namespace Hudson\CustomerPricing\Api;


interface FamilyDiscountInterface
{
    /**
     * @param string $customerNumber
     * @param string $familyCode
     * @param float $specialPrice
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return mixed
     */
    public function createRecord($customerNumber, $familyCode,$specialPrice,$discountPercentage, $validFrom, $validTo, $discountCode);

    /**
     * @param string $customerNumber
     * @param string $familyCode
     * @return bool
     */
    public function deleteRecord($customerNumber,$familyCode);
}