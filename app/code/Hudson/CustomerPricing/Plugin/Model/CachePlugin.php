<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 09:59
 */

namespace Hudson\CustomerPricing\Plugin\Model;


use Psr\Log\LoggerInterface;

class CachePlugin
{
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;
    protected $cacheCollectionFactory;


    public function __construct(LoggerInterface $logger,
                               \Hudson\CacheManager\Model\ResourceModel\HudsonCache\CollectionFactory $cacheCollectionFactory) {
        $this->logger = $logger;
        $this->cacheCollectionFactory = $cacheCollectionFactory;
     }



    public function afterInvalidateCache(\Hudson\CustomerPricing\Model\CacheInvalidatingModel $subject,$result){


        //Invalidating full page cache
        $this->invalidateCache();

        return $result;


    }

    /**
     * Invalidates cache
     */
    protected function invalidateCache(){

        $entries = $this->cacheCollectionFactory->create()
            ->addFieldToFilter('cache_code',['eq' => "all"]);

        if(sizeof($entries)) {
            $entryToInvalidate = $entries->getFirstItem();

            if($entryToInvalidate){
                if(!$entryToInvalidate->getInvalidated()){
                    $entryToInvalidate->setInvalidated(true);
                    $entryToInvalidate->save();
                    $this->logger->info('CACHE WAS INVALIDATED BY PLUGIN');
                }
            }
        }

    }
}