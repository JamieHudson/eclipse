<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 18/12/2017
 * Time: 12:15
 */

namespace Hudson\CustomerPricing\Plugin;


use Hudson\CustomerPricing\Helper\Logger\Logger;

class ViewPrice
{

    //Calculates price for current customer
    protected $pricingHelper;

    protected $objectManager;
    //Getting customer data
    protected $customerSession;

    //Just for checking if module is enabled
    protected $multiplicationHelper;

    protected $_storeManager;

    protected $currencySymbol;

    protected $productRepository;

    protected $hudsonLogger;



    public function __construct(
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
//        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger
    ) {
        $this->pricingHelper = $pricingHelper;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->_storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->objectManager = $objectManager;
        $this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
        $this->customerSession = $this->objectManager->create('Magento\Customer\Model\SessionFactory')->create();
        $this->hudsonLogger = $hudsonLogger;



    }

    public function aroundGetProductPrice(\Magento\Catalog\Block\Product\AbstractProduct $subject,callable $proceed,$product){

        $result = $proceed($product);


        if(!$this->multiplicationHelper->isEnabledInFrontend()) return $result;             //no action

        if ($this->customerSession->isLoggedIn()) {


            $customer = $this->customerSession->getCustomer();
            if ($customer) {


                $priceValue = null;

                try {
                    $priceValue = $product->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
                }catch (\Exception $e){
                    $location = "ViewPrice.php get final price of product \n";
                    $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
                    $fullMessage = $location . $errorMessage;
                    $this->hudsonLogger->log($fullMessage,Logger::HUDSON_EXCEPTION);
                    return $result;
                }


                $productModel = $this->productRepository->getById($product->getId());


                //Quering the db for customer prices
                $price = $this->pricingHelper->getViewPriceForCustomer($customer, $productModel,$priceValue);

                if($price && is_numeric($price)) {
                    $price = round($price, 2);
                }

                //Returning original price with not other text (original html)
                if($priceValue == $price) return $result;


                $result = $this->replacePriceInHtml($result,$price);


            }


        }

        return $result;
    }

    protected function replacePriceInHtml($html,$price){

        $priceLabel = $this->multiplicationHelper->getPriceLabel();

        $textValue = trim(strip_tags($html));

        $newValue = $textValue. "<div class='your-price'>$priceLabel<span class='discounted-price'>" .$this->currencySymbol.$price. "</span></div>";

        $newHtml = str_replace($textValue,$newValue,$html);

        return $newHtml;
    }

}
