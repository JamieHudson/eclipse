<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 18/12/2017
 * Time: 12:15
 */

namespace Hudson\CustomerPricing\Plugin\Pricing;


use Hudson\CustomerPricing\Helper\Logger\Logger;

class GroupedViewPrice
{

    //Calculates price for current customer
    protected $pricingHelper;

    protected $objectManager;
    //Getting customer data
    protected $customerSession;

    //Just for checking if module is enabled
    protected $multiplicationHelper;

    protected $_storeManager;

    protected $currencySymbol;

    protected $productRepository;

    protected $hudsonLogger;


    public function __construct(
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
//        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger
    )
    {
        $this->pricingHelper = $pricingHelper;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->_storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->objectManager = $objectManager;
        $this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
        $this->customerSession = $this->objectManager->create('Magento\Customer\Model\SessionFactory')->create();
        $this->hudsonLogger = $hudsonLogger;


    }

    /**
     * Takes care of showing the price in the view of grouped product
     * @param \Magento\Catalog\Block\Product\AbstractProduct $subject
     * @param callable $proceed
     * @param $product
     * @return mixed
     */
    public function aroundGetProductPrice(\Magento\Catalog\Block\Product\AbstractProduct $subject, callable $proceed, $product)
    {

        $result = $proceed($product);


        if (!$this->multiplicationHelper->isEnabledInFrontend()) return $result;             //no action

        if ($this->customerSession->isLoggedIn()) {


            $customer = $this->customerSession->getCustomer();
            if ($customer) {


                $priceValue = null;

                try {
                    $priceValue = $product->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
                } catch (\Exception $e) {
                    //logging this , do not throw again as original result returned below
                    //getting final price fails
                    $location = "GroupedViewPrice.php get final price \n";
                    $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
                    $fullMessage = $location . $errorMessage;
                    $this->hudsonLogger->log($fullMessage,Logger::HUDSON_EXCEPTION);
                    return $result;
                }


                $productModel = $this->productRepository->getById($product->getId());


                //Quering the db for customer prices
                try {
                    $price = $this->pricingHelper->getViewPriceForCustomer($customer, $productModel, $priceValue);
                } catch (\Exception $e) {
                    //logging this , do not throw again as original result returned below
                    //getting view price per customer
                    $location = "GroupedViewPrice.php get price for customer \n";
                    $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
                    $fullMessage = $location . $errorMessage;
                    $this->hudsonLogger->log($fullMessage,Logger::HUDSON_EXCEPTION);
                    return $result;
                }


                if ($price && is_numeric($price)) {
                    $price = round($price, 2);
                }

                //Returning original price with not other text (original html)
                if ($priceValue == $price) return $result;


                $result = $this->replacePriceInHtml($result, $price);


            }


        }

        return $result;
    }

    protected function replacePriceInHtml($html, $price)
    {
        $textValue = trim(strip_tags($html));

        $priceLabel = $this->multiplicationHelper->getPriceLabel();

        $newValue = $textValue . "<div class='your-price'>". $priceLabel . "<span class='discounted-price'>" . $this->currencySymbol . $price . "</span></div>";

        $newHtml = str_replace($textValue, $newValue, $html);

        return $newHtml;
    }

}
