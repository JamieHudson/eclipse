<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Hudson\CustomerPricing\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Product list
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{

    protected $pricingHelper;

    protected $multiplicationHelper;

    protected $hudsonLogger;

    protected $customerSession;

    protected $currencySymbol;

    protected $_customerRepositoryInterface;

    protected $httpContext;

    protected $productRepository;
    /**
     * @param Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
        $this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
        $this->pricingHelper = $pricingHelper;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->hudsonLogger = $hudsonLogger;
        $this->customerSession = $customerSession;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->httpContext = $httpContext;
        $this->productRepository = $productRepository;

    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getYourProductPrice(\Magento\Catalog\Model\Product $product)
    {
        $result = "";

        if (!$this->multiplicationHelper->isEnabledInFrontend()) return $result; //no action

        try{
            $result = $this->getDiscountedPrice($product,$result);
        } catch (\Exception $e){
            //Logging exception and returning empty string to front (not showing "your price")
            $caughtAt = "Caught at: getCustomerPriceForProduct() in ListProduct.php \n";
            $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
            $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
            return "";
        }


        return $result;
    }

    protected function replacePriceInHtml($price)
    {
//        $priceLabel = $this->multiplicationHelper->getPriceLabel();
//
//        $newValue = "<div class='your-price'>". $priceLabel . "<span class='discounted-price'>" . $this->currencySymbol . $price . "</span></div>";
//
//
//        return $newValue;

        return $this->currencySymbol . $price;
    }


    protected function getDiscountedPrice($product,$result){


        $customer = null;

        $isProductModel = false;
        $customerId = null;

        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();

        } else {
            $customerId = $this->httpContext->getValue(\Hudson\CustomerPricing\Model\Customer\Context::CONTEXT_CUSTOMER_ID);

        }

        if ($customerId) {

            $customer = $this->_customerRepositoryInterface->getById($customerId);

        }

        if ($customer) {

            $priceValue = null;

            if ($product instanceof \Magento\Framework\Pricing\SaleableInterface) {

                $productType = $product->getTypeId();


                try {
                    if($productType == "grouped") {
                        $product = $this->productRepository->getById($product->getId());
                        $isProductModel = true;
                        $priceValue = $product->getData('unitprice');
                    }else{
                        $priceValue = $product->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
                    }
                } catch (\Exception $e) {
                    //Logging exception and returning empty string to front (not showing "your price")
                    $caughtAt = "Caught at: trying to get PriceInfo in getDiscountedPrice() in ListProduct.php \n";
                    $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
                    $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
                    return $result;
                }
            } else {
                return $result;
            }


//                $productModel = $this->productRepository->getById($product->getId());


            $price = $priceValue;

            //Quering the db for customer prices
            try{
                if(!$isProductModel){
                    $product = $this->productRepository->getById($product->getId());
                }
                $price = $this->pricingHelper->getViewPriceForCustomer($customer, $product, $priceValue);
            }catch (\Exception $e){
                //Logging exception and returning empty string to front (not showing "your price")
                $caughtAt = "Caught at: trying to get ViewPriceForCustomer in getDiscountedPrice() in ListProduct.php \n";
                $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
                $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
                return $result;
            }


            if ($price && is_numeric($price)) {
                $price = round($price, 2);
            }

	    $priceValue = round($priceValue,2);

            //no action if price is the same as the original (assumed priceValue is rounded with precision 2)
            if ($priceValue == $price) return $result;


            $result = $this->replacePriceInHtml($price);


        }

        return $result;

    }


}
