<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 19/12/2017
 * Time: 12:27
 */

namespace Hudson\CustomerPricing\Block\Product\View;

use Exception;
use Magento\Framework\Pricing\SaleableInterface;
use \Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use \Magento\Framework\App\Http\Context;


class CustomerPriceRenderer extends Template
{

    //Calculates price for current customer
    protected $pricingHelper;

    //Getting customer data
    protected $customerSession;

    //Just for checking if module is enabled
    protected $multiplicationHelper;

    protected $_storeManager;

    protected $currencySymbol;

    protected $productRepository;

    protected $httpContext;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * For logging exceptions related to this module
     * @var \Hudson\CustomerPricing\Helper\Logger\Logger
     */
    protected $hudsonLogger;

    /**
     * Construct
     *
     * @param Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Context $httpContext,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
        \Magento\Framework\ObjectManagerInterface $objectManager, // SMCN UNCOMMENT THIS
        array $data = []
    )
    {
        $this->registry = $registry;
        parent::__construct($context, $data);
        $this->pricingHelper = $pricingHelper;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->_storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
        $this->customerSession = $customerSession;
	$this->objectManager = $objectManager;
        $this->customerSession = $this->objectManager->create('Magento\Customer\Model\SessionFactory')->create(); // SMCN UNCOMMENT THIS
        $this->httpContext = $httpContext;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->hudsonLogger = $hudsonLogger;



    }


    public function getCustomerPriceForProduct()
    {
        $product = $this->getProduct();

        $result = "";

        if (!$this->multiplicationHelper->isEnabledInFrontend()) return $result; //no action

        try{
            $result = $this->getDiscountedPrice($product,$result);
        } catch (\Exception $e){
            //Logging exception and returning empty string to front (not showing "your price")
            $caughtAt = "Caught at: getCustomerPriceForProduct() in CustomerPriceRendered.php \n";
            $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
            $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
            return "";
        }

        return $result;
    }


    /**
     * Returns saleable item instance
     *
     * @return Product
     */
    protected function getProduct()
    {
        $parentBlock = $this->getParentBlock();

        $product = $parentBlock && $parentBlock->getProductItem()
            ? $parentBlock->getProductItem()
            : $this->registry->registry('product');
        return $product;
    }


    protected function replacePriceInHtml($price)
    {
        $priceLabel = $this->multiplicationHelper->getPriceLabel();

        $newValue = "<div class='your-price'>". $priceLabel . "<span class='discounted-price'>" . $this->currencySymbol . $price . "</span></div>";


        return $newValue;
    }


    protected function getDiscountedPrice($product,$result){
        $customer = null;

        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomer();

        } else {
            $customerId = $this->httpContext->getValue(\Hudson\CustomerPricing\Model\Customer\Context::CONTEXT_CUSTOMER_ID);

            if ($customerId) {
                $customer = $this->_customerRepositoryInterface->getById($customerId);

            }
        }

        if ($customer) {
            $priceValue = null;

            if ($product instanceof SaleableInterface) {

                $productType = $product->getTypeId();

                if($productType == "grouped") return $result; //Do not add any special price to the parent grouped product.

                try {
                    $priceValue = $product->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
                } catch (\Exception $e) {
                    //Logging exception and returning empty string to front (not showing "your price")
                    $caughtAt = "Caught at: trying to get PriceInfo in getDiscountedPrice() in CustomerPriceRendered.php \n";
                    $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
                    $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
                    return $result;
                }
            } else {
                return $result;
            }

//                $productModel = $this->productRepository->getById($product->getId());


            $price = $priceValue;
            //Quering the db for customer prices
            try{
                $price = $this->pricingHelper->getViewPriceForCustomer($customer, $product, $priceValue);

            }catch (\Exception $e){
                //Logging exception and returning empty string to front (not showing "your price")
                $caughtAt = "Caught at: trying to get ViewPriceForCustomer in getDiscountedPrice() in CustomerPriceRendered.php \n";
                $logMessage = $caughtAt . $e->getCode() . " - " . $e->getMessage(). "\n" . $e->getTraceAsString();
                $this->hudsonLogger->log($logMessage,\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_EXCEPTION);
                return $result;
            }

            if ($price && is_numeric($price)) {
                $price = round($price, 2);
            }

            //no action if price is the same as the original (assumed priceValue is rounded with precision 2)
            if ($priceValue == $price) return $result;

            $result = $this->replacePriceInHtml($price);

        }

        return $result;

    }

}
