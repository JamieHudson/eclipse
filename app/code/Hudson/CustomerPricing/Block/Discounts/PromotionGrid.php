<?php

namespace Hudson\CustomerPricing\Block\Discounts;

use Magento\Backend\Block\Widget\Grid\Column;

class PromotionGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{


    protected $modelCollectionFactory;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Hudson\CustomerPricing\Model\ResourceModel\Promotion\CollectionFactory $modelCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {

        parent::__construct($context, $backendHelper, $data);

        $this->modelCollectionFactory = $modelCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('promotions');
        $this->setDefaultSort('updated_at');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Promotions'));
        $this->setSaveParametersInSession(true);
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->modelCollectionFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('promotion_code', ['header' => __('Promotion Code'), 'index' => 'promotion_code']);
        $this->addColumn('product_code', ['header' => __('Product Code'), 'index' => 'product_code']);
        $this->addColumn('customer_number', ['header' => __('Customer Number (Acc No)'), 'index' => 'customer_number']);
        $this->addColumn('discount_percentage', ['header' => __('Discount Percentage'), 'index' => 'discount_percentage','align' => 'center', 'type' => 'number']);
        $this->addColumn('valid_from', ['header' => __('Valid From'), 'index' => 'valid_from','align' => 'center', 'type' => 'date']);
        $this->addColumn('valid_to', ['header' => __('Valid To'), 'index' => 'valid_to','align' => 'center', 'type' => 'date']);
        $this->addColumn('created_at', ['header' => __('Created At'), 'index' => 'created_at','align' => 'center', 'type' => 'datetime']);
        $this->addColumn('updated_at', ['header' => __('Updated At'), 'index' => 'updated_at','align' => 'center', 'type' => 'datetime']);

        return parent::_prepareColumns();
    }


}
