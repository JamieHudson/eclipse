<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 16/01/2018
 * Time: 09:29
 */

namespace Hudson\CustomerPricing\Block;


use Hudson\CustomerPricing\Helper\CartViewHelper;

class AdditionalCharge extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CartViewHelper
     */
    protected $cartViewHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Hudson\CustomerPricing\Helper\CartViewHelper $cartViewHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->cartViewHelper = $cartViewHelper;
    }

    /**
     * @return string additional charge
     */
    public function getAdditionalChargeText($product)
    {
        return $this->cartViewHelper->getCuttingChargeText($product);
    }
}