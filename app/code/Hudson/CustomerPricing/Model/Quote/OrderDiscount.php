<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Hudson\CustomerPricing\Model\Quote;

class OrderDiscount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * Discount calculation object
     *
     * @var \Magento\SalesRule\Model\Validator
     */
    protected $calculator;

    /**
     * Core event manager proxy
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Hudson\CustomerPricing\Helper\Calculator
     */
    protected $pricingHelper;

    /**
     * @var \Hudson\CustomerPricing\Helper\MathHelper
     */
    protected $mathHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\SalesRule\Model\Validator $validator
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\SalesRule\Model\Validator $validator,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Hudson\CustomerPricing\Helper\MathHelper $mathHelper,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->setCode('customer_order_discount');
        $this->eventManager = $eventManager;
        $this->calculator = $validator;
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
        $this->pricingHelper = $pricingHelper;
        $this->mathHelper = $mathHelper;
        $this->customerSession = $customerSession;
    }

    /**
     * Collect address discount amount
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);

        $address = $shippingAssignment->getShipping()->getAddress();
        $this->calculator->reset($address);

        $items = $quote->getItems();

        //Check if the user is logged in
        if ($this->customerSession->isLoggedIn()) {

            $customer = $this->customerSession->getCustomer();
            //Check if valid user is retrieved
            if ($customer) {
                $discountPercentage = $this->pricingHelper->getOrderDiscount($customer);
                //Check if user has discount percentage assigned
                if ($discountPercentage) {
                    $subtotalOfQuote = (float)$total->getSubtotal();
                    $feeOfQuote = (float)$quote->getFee();
                    $quoteTotal = $subtotalOfQuote + $feeOfQuote;

                    $discountAmount = $this->mathHelper->calculateOrderDiscountForAmount($discountPercentage, $quoteTotal);
                    //Needs to be negative as it is discount
                    $discountAmount = -1 * $discountAmount;

                    $label = "Discount of $discountPercentage%";
                    $appliedCartDiscount = 0;
                    if ($total->getDiscountDescription()) {
                        // If a discount exists in cart and another discount is applied, the add both discounts.
                        $appliedCartDiscount = $total->getDiscountAmount();
                        $discountAmount = $total->getDiscountAmount() + $discountAmount;
                        $label = $total->getDiscountDescription() . ', ' . $label;
                    }

                    $discountMultiplier = $discountPercentage / 100;

                    if ($items) {
                        foreach ($items as $item) {
                            $rowTotal = $item->getRowTotal();
                            $baseRowTotal = $item->getBaseRowTotal();

                            $currentDiscountPercent = $item->getDiscountPecent();
                            $currentDiscountAmount = $item->getDiscountAmount();
                            $currentBaseDiscountAmount = $item->getBaseDiscountAmount();

                            $item->setDiscountPercent($currentDiscountPercent + (float)$discountPercentage);
                            $item->setDiscountAmount($currentDiscountAmount + ($discountMultiplier * $rowTotal));
                            $item->setBaseDiscountAmount($currentBaseDiscountAmount + ($discountMultiplier * $baseRowTotal));

                            $item->save();
                        }
                    }

                    $total->setDiscountDescription($label);
                    $total->setDiscountAmount($discountAmount);
                    $total->setBaseDiscountAmount($discountAmount);
                    $total->setSubtotalWithDiscount($total->getSubtotal() + $discountAmount);
                    $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() + $discountAmount);

                    if (isset($appliedCartDiscount)) {
                        $total->addTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
                        $total->addBaseTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
                    } else {
                        $total->addTotalAmount($this->getCode(), $discountAmount);
                        $total->addBaseTotalAmount($this->getCode(), $discountAmount);
                    }

                }
            }


    }


return $this;


}


/**
 * Add discount total information to address
 *
 * @param \Magento\Quote\Model\Quote $quote
 * @param \Magento\Quote\Model\Quote\Address\Total $total
 * @return array|null
 */
public
function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
{
    $result = null;
    $amount = $total->getDiscountAmount();

    // ONLY return 1 discount. Need to append existing
    //see app/code/Magento/Quote/Model/Quote/Address.php

    if ($amount != 0) {
        $description = $total->getDiscountDescription();
        $result = [
            'code' => $this->getCode(),
            'title' => strlen($description) ? __('Discount (%1)', $description) : __('Discount'),
            'value' => $amount
        ];
    }
    return $result;


    /* in magento 1.x
       $address->addTotal(array(
            'code' => $this->getCode(),
            'title' => $title,
            'value' => $amount
        ));
     */
}
}
