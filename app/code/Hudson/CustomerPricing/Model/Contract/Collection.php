<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:32
 */

namespace Hudson\CustomerPricing\Model\Contract;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Hudson\CustomerPricing\Model\Contract','Hudson\CustomerPricing\Model\ResourceModel\Contract');
    }
}