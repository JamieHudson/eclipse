<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:00
 */

namespace Hudson\CustomerPricing\Model;

use \Hudson\CustomerPricing\Api\GroupDiscountInterface;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use \Magento\Framework\Stdlib\DateTime\DateTime as Date;


class GroupDiscount extends \Hudson\CustomerPricing\Model\CacheInvalidatingModel implements GroupDiscountInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_group_discount';

    protected $mathHelper;

    protected $dateFormatTool;

    protected $date;

    protected $hudsonLogger;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Hudson\CustomerPricing\Helper\MathHelper $mathHelper,
        DateFormatTool $dateFormatTool,
        Date $date,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
        array $data = []
    )
    {
        parent::__construct($context, $registry, null, null, $data);

        $this->mathHelper = $mathHelper;
        $this->dateFormatTool = $dateFormatTool;
        $this->date = $date;
        $this->hudsonLogger = $hudsonLogger;
    }

    protected function _construct()
    {
        $this->_init('Hudson\CustomerPricing\Model\ResourceModel\GroupDiscount');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function loadByCustomerAndGroup($customerNumber,$groupCode){

        $this->_getResource()->loadByCustomerAndGroup($this, $customerNumber, $groupCode);
        return $this;
    }

    /**
     *
     * Returns false if not found
     *
     * @param $customerNumber
     * @param $productCode
     * @param $originalPrice
     * @return bool|float
     */
    public function getPriceForCustomer($originalPrice)
    {

        if (!$this->isValid()) return false;

        $specialPrice = $this->getSpecialPrice();

        if ($specialPrice && $specialPrice > 0) {
            return $specialPrice;
        }

        $discountPercentage = $this->getDiscountPercentage();

        if ($discountPercentage) {
            return $this->mathHelper->calculateDiscount($discountPercentage, $originalPrice);
        }


        return false;
    }


    protected function isValid()
    {

        $currentData = $this->date->gmtDate();

        $validFrom = null;
        if ($validFrom = $this->getValidFrom()) {
            if ($currentData < $validFrom) return false;
        }

        $validTo = null;
        if ($validTo = $this->getValidTo()) {
            if ($currentData > $validTo) return false;
        }

        return true;
    }

    /**
     * @param string $customerNumber
     * @param string $groupCode
     * @param float $specialPrice
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return bool
     */
    public function createRecord($customerNumber, $groupCode, $specialPrice, $discountPercentage, $validFrom, $validTo, $discountCode)
    {
        $result = $this->createSingleRecord($customerNumber, $groupCode, $specialPrice, $discountPercentage, $validFrom, $validTo, $discountCode);

        if ($result === true) $this->invalidateCache();

        return $result;
    }


    /**
     * @param string $customerNumber
     * @param string $groupCode
     * @param float $specialPrice
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return bool|string
     */
    public function createSingleRecord($customerNumber, $groupCode, $specialPrice, $discountPercentage, $validFrom, $validTo, $discountCode)
    {
        $response = true;
        $new = false;

        $this->_getResource()->loadByCustomerAndGroup($this, $customerNumber, $groupCode);
        $this->setOrigData();

        $oldData = $this->getOrigData();
        //Check if it exists
        if(!$this->getId()){
            $new = true;
            $this->setCustomerNumber($customerNumber);
            $this->setGroupCode($groupCode);
        }

        $this->setSpecialPrice($specialPrice);
        $this->setDiscountPercentage($discountPercentage);

        try{
            $validFrom = $this->dateFormatTool->formatDate($validFrom,false);
            $validTo = $this->dateFormatTool->formatDate($validTo,false);
        } catch (\Exception $e){
            $error = "Date format not accepted.";
            return $error;
        }

        //check date period
        $from = date_create($validFrom);
        $to = date_create($validTo);

        if(!($from < $to)){
            //return "Date period is invalid";
        }

        $this->setValidFrom($validFrom);
        $this->setValidTo($validTo);
        $this->setDiscountCode($discountCode);

        $this->_getResource()->save($this);

        $newData = $this->getData();

        //Logging changes
        if($new){
            $this->hudsonLogger->log(
                "New ".self::CACHE_TAG." created with data: \"".implode(",",$newData)."\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }else{
            $this->hudsonLogger->log(
                "Existing ".self::CACHE_TAG." (ID:".$this->getId().") was changed from: \"".implode(",",$oldData)."\" to: \"".implode(",",$newData)."\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }

        return $response;
    }

    /**
     * @param string $customerNumber
     * @param string $groupCode
     * @return bool
     */
    public function deleteRecord($customerNumber, $groupCode)
    {
        $result = $this->deleteSingleRecord($customerNumber, $groupCode);

        if ($result === true) $this->invalidateCache();

        return $result;
    }


    /**
     * @param string $customerNumber
     * @param string $groupCode
     * @return bool
     */
    public function deleteSingleRecord($customerNumber, $groupCode)
    {
        $this->_getResource()->loadByCustomerAndGroup($this, $customerNumber, $groupCode);

        $exists = false;
        $data = [];
        //Check if it exists
        if ($this->getId()) {
            $exists = true;
            $data = $this->getData();
        }

        $this->_getResource()->delete($this);

        if ($exists) {
            $this->hudsonLogger->log(
                "Existing " . self::CACHE_TAG . " (ID:" . $this->getId() . ") deleted with data: \"" . implode(",", $data) . "\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }


        return true;
    }

    /**
     * Checks whether a valid special price should be applied,
     * since special price is per unit in ERP it needs multiplied by the units in the product
     * to be brought to the correct sale price
     */
    public function priceNeedsMultiplication()
    {

        $specialPrice = $this->getSpecialPrice();

        if ($specialPrice && $specialPrice > 0) {
            return true;
        }

        return false;

    }
}