<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 09:07
 *
 * A class that will allow to easily create a plugin for save methods of all cache invalidating models
 *
 *
 * CACHE INVALIDATING MODELS
 * -CONTRACT
 * -FAMILYDISCOUNT
 * -GROUPDISCOUNT
 * -ITEMDISCOUNT
 * -PROMOTION
 *
 *
 *
 * NON CACHE INVALIDATING MODELS
 * -ORDERDISCOUNT (not used for rendering on frontend)
 *
 */

namespace Hudson\CustomerPricing\Model;


class CacheInvalidatingModel  extends \Magento\Framework\Model\AbstractModel
{

    /*
     * A method to be picked up by the plugin
     */
    public function invalidateCache(){
        return true;
    }


}