<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:01
 */

namespace Hudson\CustomerPricing\Model\ResourceModel;



use Exception;

class OrderDiscount extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = "hud_order_discount";
    const CUSTOMER_NUMBER_COLUMN = "customer_number";

    protected function _construct()
    {
        $this->_init('hud_order_discount','discount_id');
    }



    public function loadByCustomer($object,$customerNumber){

        $connection = $this->getConnection();


        //find item id
        $select = $connection->select()->from($this->getTable(self::TABLE_NAME))->where("`".self::CUSTOMER_NUMBER_COLUMN. '` = "'.$customerNumber.'"');

            $itemId = $connection->fetchOne($select);

        return $this->load($object, $itemId);
    }
}