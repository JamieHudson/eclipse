<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:00
 */

namespace Hudson\CustomerPricing\Model\ResourceModel;


class GroupDiscount extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = "hud_group_discount";
    const CUSTOMER_NUMBER_COLUMN = "customer_number";
    const GROUP_CODE_COLUMN = "group_code";

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME,'discount_id');
    }

    public function loadByCustomerAndGroup($object,$customerNumber,$groupCode){

        $connection = $this->getConnection();

        //find item id
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable(self::TABLE_NAME))
            ->where('`'.self::CUSTOMER_NUMBER_COLUMN. '` = "'.$customerNumber.'" AND `'.self::GROUP_CODE_COLUMN.'` = "'.$groupCode.'"');
        $itemId = $connection->fetchOne($select);

        return $this->load($object, $itemId);
    }
}