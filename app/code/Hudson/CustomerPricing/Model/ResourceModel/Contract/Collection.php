<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 21/12/2017
 * Time: 09:29
 */

namespace Hudson\CustomerPricing\Model\ResourceModel\Contract;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init('Hudson\CustomerPricing\Model\Contract', 'Hudson\CustomerPricing\Model\ResourceModel\Contract');
    }


}