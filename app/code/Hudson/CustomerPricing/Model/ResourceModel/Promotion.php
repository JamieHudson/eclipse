<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:01
 */

namespace Hudson\CustomerPricing\Model\ResourceModel;


class Promotion extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const TABLE_NAME = "hud_promotion";
    const CUSTOMER_NUMBER_COLUMN = "customer_number";
    const PRODUCT_CODE_COLUMN = "product_code";


    protected function _construct()
    {
        $this->_init(self::TABLE_NAME,'promotion_id');
    }


    public function loadByCustomerAndProduct($object,$customerNumber,$productCode){

        $connection = $this->getConnection();

        //find item id
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable(self::TABLE_NAME))
            ->where('`'.self::CUSTOMER_NUMBER_COLUMN. '` = "'.$customerNumber.'" AND `'.self::PRODUCT_CODE_COLUMN.'` = "'.$productCode.'"');
        $itemId = $connection->fetchOne($select);


        return $this->load($object, $itemId);
    }
}