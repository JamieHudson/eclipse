<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:01
 */

namespace Hudson\CustomerPricing\Model;

use \Hudson\CustomerPricing\Api\OrderDiscountInterface;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use \Magento\Framework\Stdlib\DateTime\DateTime as Date;


class OrderDiscount extends \Magento\Framework\Model\AbstractModel implements OrderDiscountInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'hud_order_discount';

    protected $mathHelper;

    protected $dateFormatTool;

    protected $date;

    protected $hudsonLogger;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Hudson\CustomerPricing\Helper\MathHelper $mathHelper,
        DateFormatTool $dateFormatTool,
        Date $date,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
        array $data = []
    )
    {
        parent::__construct($context, $registry, null, null, $data);

        $this->mathHelper = $mathHelper;
        $this->dateFormatTool = $dateFormatTool;
        $this->date = $date;
        $this->hudsonLogger = $hudsonLogger;
    }

    protected function _construct()
    {
        $this->_init('Hudson\CustomerPricing\Model\ResourceModel\OrderDiscount');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


    public function loadByCustomer($customerNumber)
    {

        $this->_getResource()->loadByCustomer($this, $customerNumber);
        return $this;
    }

    /**
     *
     * Returns false if not found
     *
     * @param $customerNumber
     * @param $productCode
     * @param $originalPrice
     * @return bool|float
     */
    public function getPriceForCustomer($originalPrice)
    {

        if (!$this->isValid()) return false;

        $discountPercentage = $this->getDiscountPercentage();

        if ($discountPercentage) {
            return $this->mathHelper->calculateDiscount($discountPercentage, $originalPrice);
        }

        return false;
    }

    public function isValid()
    {

        $currentData = $this->date->gmtDate();

        $validFrom = null;
        if ($validFrom = $this->getValidFrom()) {
            if ($currentData < $validFrom) return false;
        }

        $validTo = null;
        if ($validTo = $this->getValidTo()) {
            if ($currentData > $validTo) return false;
        }

        return true;
    }

    /**
     * @param string $customerNumber
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return bool
     */
    public function createRecord($customerNumber, $discountPercentage, $validFrom, $validTo, $discountCode)
    {
        return $this->createSingleRecord($customerNumber, $discountPercentage, $validFrom, $validTo, $discountCode);
    }

    /**
     * @param string $customerNumber
     * @param float $discountPercentage
     * @param string $validFrom
     * @param string $validTo
     * @param string $discountCode
     * @return bool|string
     */
    public function createSingleRecord($customerNumber, $discountPercentage, $validFrom, $validTo, $discountCode)
    {
        $response = true;
        $new = false;

        $this->_getResource()->loadByCustomer($this, $customerNumber);
        $this->setOrigData();

        $oldData = $this->getOrigData();
        //Check if it exists
        if(!$this->getId()){
            $new = true;
            $this->setCustomerNumber($customerNumber);
        }

        $this->setDiscountPercentage($discountPercentage);

        try {
            $validFrom = $this->dateFormatTool->formatDate($validFrom, false);
            $validTo = $this->dateFormatTool->formatDate($validTo, false);
        } catch (\Exception $e) {
            $error = "Date format not accepted.";
            return $error;
        }

        //check date period
        $from = date_create($validFrom);
        $to = date_create($validTo);

        if(!($from < $to)){
            //return "Date period is invalid";
        }

        $this->setValidFrom($validFrom);
        $this->setValidTo($validTo);
        $this->setDiscountCode($discountCode);

        $this->_getResource()->save($this);

        $newData = $this->getData();

        //Logging changes
        if($new){
            $this->hudsonLogger->log(
                "New ".self::CACHE_TAG." created with data: \"".implode(",",$newData)."\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }else{
            $this->hudsonLogger->log(
                "Existing ".self::CACHE_TAG." (ID:".$this->getId().") was changed from: \"".implode(",",$oldData)."\" to: \"".implode(",",$newData)."\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }

        return $response;
    }

    /**
     * @param string $customerNumber
     * @return bool
     */
    public function deleteRecord($customerNumber)
    {
        return $this->deleteSingleRecord($customerNumber);

    }

    /**
     * @param string $customerNumber
     * @return bool
     */
    public function deleteSingleRecord($customerNumber)
    {
        $this->_getResource()->loadByCustomer($this, $customerNumber);

        $exists = false;
        $data = [];
        //Check if it exists
        if ($this->getId()) {
            $exists = true;
            $data = $this->getData();
        }

        $this->_getResource()->delete($this);

        if ($exists) {
            $this->hudsonLogger->log(
                "Existing " . self::CACHE_TAG . " (ID:" . $this->getId() . ") deleted with data: \"" . implode(",", $data) . "\"",
                \Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_INFO
            );
        }

        return true;
    }
}