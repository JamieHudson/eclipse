<?php

namespace Hudson\CustomerPricing\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_scopeConfig;

    protected $_multiplier;

    protected $_enabled;

    protected $_onlyCheaper;

    protected $_priceLabel;


    const FIELD_ENABLED = 'customerpricing/general/enabled';
    const FIELD_ONLYCHEAPER = 'customerpricing/general/onlycheaper';
    const FIELD_MULTIPLIER = 'customerpricing/general/multiplier';
    const FIELD_MAXVALUE = 'customerpricing/general/maxvalue';
    const FIELD_PRICELABEL = 'customerpricing/storefront/pricelabel';
    const FIELD_NFSATTRIBUTE = 'customerpricing/notforsale/nfsattribute';


    public function __construct(\Magento\Framework\App\Helper\Context $context)
    {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);

        $this->_multiplier = $this->getMultiplier();
        $this->_enabled = $this->isEnabledInFrontend();
        $this->_onlyCheaper = $this->isOnlyCheaperEnabled();
        $this->_priceLabel = $this->getPriceLabelFromConfig();
    }

    /**
     *
     * check the module is enabled, frontend
     *
     * @return bool
     */
    public function isEnabledInFrontend()
    {
        $isEnabled = true;
        $enabled = $this->_scopeConfig->getValue(self::FIELD_ENABLED, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }
        return $isEnabled;
    }

    /**
     * Get custom option string
     * @return mixed|string
     */
    protected function getMultiplier()
    {

        $multiplier = $this->_scopeConfig->getValue(self::FIELD_MULTIPLIER, ScopeInterface::SCOPE_STORE);
        $multiplier = trim($multiplier);

        return $multiplier;
    }

    /**
     * Gets the priceLabel from the store config
     * @return mixed|string
     */
    protected function getPriceLabelFromConfig()
    {
        $priceLabel = $this->_scopeConfig->getValue(self::FIELD_PRICELABEL, ScopeInterface::SCOPE_STORE);

        return $priceLabel;
    }


    /**
     * This is multiplication for the custom option of the product e.g Length
     * and validation
     * @param $price
     * @param $postedOptions
     * @param $optionId
     * @return bool|float
     */
    public function getMultipliedPrice($price, $postedOptions, $optionId)
    {
//	if(!isset($postedOptions[$optionId])){
//		return $price;
//	}

        $value = $postedOptions[$optionId];

        if (!$value) {
            return false;
        }

        //Making sure value is rounded and formatted for multiplication
        $value = $this->getFormattedOptionValue($value);


        if (!$value) {
            return false;
        }

        return $price * $value;
    }

    /**
     * Making sure formatted option can be used in calculation
     * Does some validation too.
     * @param $option
     * @return bool|float
     */
    protected function getFormattedOptionValue($option)
    {

        //Checking if numeric
        if (!is_numeric($option)) {
            return false;
        }


        $value = (float) $option;

        $value = round($value, 2);

        //Do not allow 0 or negative values
        if ($value <= 0.00) {
            return false;
        }


        $maxvalue = $this->_scopeConfig->getValue(self::FIELD_MAXVALUE, ScopeInterface::SCOPE_STORE);
        if (is_numeric($maxvalue)) {
            $maxvalue = (float) $maxvalue;

            if ($maxvalue >0) { //do not allow maxvalue of 0 or less
                if ($value > $maxvalue) {
                    return false;
                }
            }
        }

        return $value;
    }


    /**
     * @param $customOptions
     * @return bool|int
     */
    public function getMultiplierOptionId($customOptions)
    {
        $multiplierOptions = explode(",", $this->_multiplier);

        foreach ($customOptions as $option) {
            if (in_array($option->getTitle(), $multiplierOptions)) {
                return $option->getId();
            }
        }

        return false;
    }

    /**
     * Returns the set price label
     * @return mixed|string
     */
    public function getPriceLabel()
    {
        return $this->_priceLabel;
    }

    protected function isOnlyCheaperEnabled()
    {
        $isEnabled = true;
        $enabled = $this->_scopeConfig->getValue(self::FIELD_ONLYCHEAPER, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }

        return $isEnabled;
    }

    public function getOnlyCheaperIsEnabled()
    {
        return $this->_onlyCheaper;
    }

    public function getNotFofSaleAttributeCode()
    {
        $code = trim($this->_scopeConfig->getValue(self::FIELD_NFSATTRIBUTE, ScopeInterface::SCOPE_STORE));
        if ($code == null || $code == '') {
            return null;
        }
        return $code;
    }
}
