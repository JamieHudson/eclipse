<?php

namespace Hudson\CustomerPricing\Helper\Logger;

class Logger
{

    const HUDSON_EXCEPTION = 'exception';
    const HUDSON_INFO = 'info';
    const HUDSON_API_INFO = 'api_info';
    const HUDSON_API_EXCEPTION = 'api_exception';

    public function log($msg, $type = self::HUDSON_INFO)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hudson_'.$type.'.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }

}