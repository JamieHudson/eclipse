<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 20/12/2017
 * Time: 11:25
 */

namespace Hudson\CustomerPricing\Helper;


use Hudson\CustomerPricing\Api\StockImporterInterface;

class StockImporter implements StockImporterInterface
{

    /**
     * @param string $productCode
     * @param int $qty
     * @return mixed
     */
    public function updateStock($productCode, $qty)
    {
        // TODO: Implement updateStock() method.
    }
}