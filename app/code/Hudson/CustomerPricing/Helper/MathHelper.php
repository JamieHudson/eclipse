<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 14/12/2017
 * Time: 16:10
 */

namespace Hudson\CustomerPricing\Helper;

class MathHelper extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     *
     * Calculates discounted price for an item
     *
     *
     */
    public function calculateDiscount($discountPercentage,$price){

        //Prevent £0 prices
        if($discountPercentage >= 100) return $price;

        $fraction = (float) (100 - (float) $discountPercentage);

        $fraction = $fraction/100;

        $newPrice = $fraction * ( (float) $price);

        $newPrice = round($newPrice,2,PHP_ROUND_HALF_UP);

        return $newPrice;
    }

    /**
     * Calculates amount discount for the whole order
     * @param $discountPercentage
     * @param $total
     * @return bool|float
     */
    public function calculateOrderDiscountForAmount($discountPercentage,$total){
        //Prevent <=£0 prices and negative discounts
        if($discountPercentage >= 100 || $discountPercentage < 0) return false;

        $fraction = (float) $discountPercentage;

        $fraction = $fraction/100;

        $discountAmount = $fraction * ( (float) $total);

        //Round half down as it is discount amount
        $discountAmount = round($discountAmount,2,PHP_ROUND_HALF_DOWN);

        return $discountAmount;
    }




}