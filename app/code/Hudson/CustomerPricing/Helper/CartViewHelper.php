<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 16/01/2018
 * Time: 09:39
 */

namespace Hudson\CustomerPricing\Helper;


use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Helper\Context;

class CartViewHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CUTTINGCHARGE = "cuttingcharge";
    const CUTCHARGECODE = "cutchargecode";
    const CUTTINGPRICE = "cuttingprice";

    protected $_storeManager;

    protected $currencySymbol;

    protected $_productRepo;

    public function __construct(Context $context,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                ProductRepository $productRepository)
    {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->currencySymbol = $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
        $this->_productRepo = $productRepository;
    }

    /**
     * @param \Magento\Catalog\Model\Product\Interceptor $product
     * @return string
     */
    public function getCuttingChargeText($product)
    {

        $id = $product->getId();

        if (!$id) {
            //NO ID
            return "";
        }

        $product = $this->_productRepo->getById($id);



        if(!$product->getData(self::CUTTINGCHARGE)){
            //no charge applied
            return "";
        }


        $cuttingPrice = (float) $product->getData(self::CUTTINGPRICE);
        $cuttingPrice = round($cuttingPrice,2);

        $cuttingPriceWithCurrency = $this->currencySymbol.$cuttingPrice;

        $cutChargeCode = $product->getData(self::CUTCHARGECODE);

        $additionalChargeText = "Cutting charge: ";
        $additionalChargeText .= "<span class='cut-charge-code'>$cutChargeCode</span> : ";
        $additionalChargeText .= "<span class='cutting-price'>$cuttingPriceWithCurrency</span>";
        $additionalChargeText .= " <span class='charge-note'>(per item)</span>";

        return $additionalChargeText;
    }


}