<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 14/12/2017
 * Time: 16:10
 */

namespace Hudson\CustomerPricing\Helper;

use Magento\Framework\App\Helper\Context;

class Calculator extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * To get the final price of the product per customer we do the following in sequence:
     *
     * STEP 1 - FIND ITEM PRICE (with fallbacks shown - stop if found)
     * - check CONTRACT PRICES
     * - check ITEM DISCOUNT
     * - check FAMILY DISCOUNT
     * - check GROUP DISCOUNT
     * - get standard item price
     *
     * STEP 2 - CHECK FOR PROMOTIONS
     * - if promotion found, the found price from STEP 1 is additionally discounted
     *
     * STEP 3 - CHECK ORDER DISCOUNTS
     * - if discount on all orders found, the calculated price from STEP 2 is additionally discounted
     *
     * STEP 4 - SHOW FINAL PRICE
     *
     */


    protected $contractFactory;

    protected $familyDiscountFactory;

    protected $groupDiscountFactory;

    protected $itemDiscountFactory;

    protected $orderDiscountFactory;

    protected $promotionFactory;

    protected $dataHelper;


    public function __construct(Context $context,
                                \Hudson\CustomerPricing\Helper\Data $dataHelper,
                                \Hudson\CustomerPricing\Model\ContractFactory $contractFactory,
                                \Hudson\CustomerPricing\Model\FamilyDiscountFactory $familyDiscountFactory,
                                \Hudson\CustomerPricing\Model\GroupDiscountFactory $groupDiscountFactory,
                                \Hudson\CustomerPricing\Model\ItemDiscountFactory $itemDiscountFactory,
                                \Hudson\CustomerPricing\Model\OrderDiscountFactory $orderDiscountFactory,
                                \Hudson\CustomerPricing\Model\PromotionFactory $promotionFactory)
    {
        parent::__construct($context);
        $this->contractFactory = $contractFactory;
        $this->familyDiscountFactory = $familyDiscountFactory;
        $this->groupDiscountFactory = $groupDiscountFactory;
        $this->itemDiscountFactory = $itemDiscountFactory;
        $this->orderDiscountFactory = $orderDiscountFactory;
        $this->promotionFactory = $promotionFactory;
        $this->dataHelper = $dataHelper;
    }


    /**
     *
     * Calculates the price of the item to be added to the cart
     *
     * @param $customer
     * @param $product
     */
    public function getCartPriceForCustomer($customer, $product, $price)
    {


        $newPrice = $this->getViewPriceForCustomer($customer, $product, $price);

        //Applying Order discount
        //$newPrice = $this->getOrderDiscount($customer, $newPrice);


        return $newPrice;
    }

    /**
     * Calculates the price per item to be shown on product page
     * excludes order percentage discount
     *
     * @param $customer
     * @param $product
     */
    public function getViewPriceForCustomer($customer, $product, $price)
    {
	$customerNumber = $this->getCustomerNumber($customer);

        if (!$customerNumber) {
	    $logger->info("No customer number");
            //do not calculate if customer number is missing
            return $price;
        }
	
        //Change code to SKU
//        $productCode = $product->getProductCode();
        $productCode = $product->getSku();
        $productFamily = $product->getItemFamily();
        $productGroup = $product->getItemGroup();
        $logger->info("SKU: ".$productCode);
	
	$newPrice = $price;
	
        //Check for discounts that need productCode
        if ($productCode) {
		//Applying STEP 1 discount
		$newPrice = $this->getDiscountedPriceForCustomer($newPrice, $customerNumber, $productCode, $productFamily, $productGroup,$product);
		
		//Applying Promotion
                $newPrice = $this->getPromotionPrice($customerNumber, $productCode, $newPrice);
        }

        if($this->dataHelper->getOnlyCheaperIsEnabled()) {
            //Return the old price if the new price is more expensive
            if ($newPrice > $price) {
                return $price;
            }
        }

        return $newPrice;
    }

    /**
     *
     * Main logic for STEP 1 of discounts
     *
     * IN SEQUENCE:
     * 1.CONTRACTS
     * 2.ITEM DISCOUNT
     * 3.FAMILY DISCOUNT
     * 4.GROUP DISCOUNT
     *
     *
     * @param $customerNumber
     * @param $productCode
     * @param $price
     */
    protected function getDiscountedPriceForCustomer($price, $customerNumber, $productCode, $productFamily, $productGroup,$product)
    {

        //CONTRACT
        $contract = $this->contractFactory->create()->loadByCustomerAndProduct($customerNumber, $productCode);
        if ($contract) {
	    $newPrice = $contract->getPriceForCustomer($price);

            if($contract->priceNeedsMultiplication()){
                $newPrice = $this->multiplyPrice($newPrice,$product);
            }

            if ($newPrice) return $newPrice;
        }


        //ITEM DISCOUNT
        $itemdiscount = $this->itemDiscountFactory->create()->loadByCustomerAndProduct($customerNumber, $productCode);
	if ($itemdiscount) {
            if($itemdiscount->priceNeedsMultiplication()){
                $newPrice = $this->multiplyPrice($newPrice,$product);
            }

            if ($newPrice) return $newPrice;
        }



        //FAMILY DISCOUNT
        $familydiscount = $this->familyDiscountFactory->create()->loadByCustomerAndFamily($customerNumber, $productFamily);
	if ($familydiscount) {
            $newPrice = $familydiscount->getPriceForCustomer($price);

            if($familydiscount->priceNeedsMultiplication()){
                $newPrice = $this->multiplyPrice($newPrice,$product);
            }

            if ($newPrice) return $newPrice;
        }



        //GROUP DISCOUNT
        $groupdiscount = $this->groupDiscountFactory->create()->loadByCustomerAndGroup($customerNumber, $productGroup);
	if ($groupdiscount) {
            $newPrice = $groupdiscount->getPriceForCustomer($price);

            if($groupdiscount->priceNeedsMultiplication()){
                $newPrice = $this->multiplyPrice($newPrice,$product);
            }

            if ($newPrice) return $newPrice;
        }


        return $price;
    }

    /**
     *
     * STEP 2 OF THE SEQUENCE
     *
     * @param $customerNumber
     * @param $productCode
     * @param $price
     * @return mixed
     */
    protected function getPromotionPrice($customerNumber, $productCode, $price)
    {
        $promotion = $this->promotionFactory->create()->loadByCustomerAndProduct($customerNumber, $productCode);
        if ($promotion) {
            $newPrice = $promotion->getPriceForCustomer($price);

            if ($newPrice) return $newPrice;
        }

        return $price;
    }

    /**
     * STEP 3 OF THE SEQUENCE
     *
     * @param $customer
     * @return bool|float
     */
    public function getOrderDiscount($customer)
    {

        $customerNumber = $this->getCustomerNumber($customer);

        $orderdiscount = $this->orderDiscountFactory->create()->loadByCustomer($customerNumber);
        if ($orderdiscount) {

            if($orderdiscount->isValid()){
                return (float) $orderdiscount->getDiscountPercentage();
            }
        }

        return false;

    }

    /**
     * Getting the customer number from different instances.
     * This is needed to handle the weird customer object distribution in M2
     * @param $customer
     * @return mixed|null
     */
    protected function getCustomerNumber($customer){
        if($customer instanceof \Magento\Customer\Model\Data\Customer){
            $attr = $customer->getCustomAttribute('customer_number');
            if($attr) {
                return $attr->getValue();
            }
        }elseif($customer instanceof \Magento\Customer\Model\Customer\Interceptor){
            return $customer->getCustomerNumber();
        }

        return null;

    }


    /**
     * Logic behind multiplying the discounted price by the packsize to get the adequate price for customer
     * @param $price
     * @param $product
     * @return float
     */
    protected function multiplyPrice($price,$product){

        $multiplicator = 1;

            //hard coded multiplier attribute
            //TODO: Choose from configuration ?!
            if($packsize = $product->getData('packsize')) {
                if ($packsize && is_numeric($packsize) && (float)$packsize > 1) {
                    $multiplicator = (float)$packsize;
                }
            }

        return (float) $price*$multiplicator;
    }


}
