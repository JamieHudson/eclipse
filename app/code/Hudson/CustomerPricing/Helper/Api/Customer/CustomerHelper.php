<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 04/01/2018
 * Time: 13:06
 */

namespace Hudson\CustomerPricing\Helper\Api\Customer;


use Hudson\CustomerPricing\Api\Customer\CustomerHelperInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\DateTime as DateFormatTool;
use Magento\Framework\Webapi\Exception;
use Psr\Log\LoggerInterface;

class CustomerHelper extends \Hudson\CustomerPricing\Helper\MassApiHelper implements CustomerHelperInterface
{


    //Model Specific
    const CREATE_REQUIRED_FIELDS = ['email', 'customerNumber'];

    //Model factory for creating records
    protected $modelRepository;


    public function __construct(CustomerRepositoryInterface $modelRepository,
                                DateFormatTool $dateFormatTool,
                                LoggerInterface $logger,
                                \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
                                \Hudson\CacheManager\Model\ResourceModel\HudsonCache\CollectionFactory $cacheCollectionFactory)
    {
        parent::__construct($logger, $cacheCollectionFactory, $hudsonLogger, $dateFormatTool);
        $this->modelRepository = $modelRepository;
    }


    /**
     * @param mixed $records
     * @return mixed
     */
    public function setAccountNumbers($records)
    {
            $errors = $this->validateRecords($records, self::CREATE_REQUIRED_FIELDS,[],[]);

            if (true !== $errors) {
                //Logging errors for records
                $this->logValidationErrors($errors, get_class($this), __FUNCTION__);

                throw new Exception(__("Errors detected on validation. No records are processed."), 400, Exception::HTTP_BAD_REQUEST, $errors);
            }


            $result = $this->setMultipleAccountNumbers($records);

            //Invalidate cache
            if ($result === true) {
                $this->invalidateCache();
                return $result;
            }

            //Logging errors for records
            $this->logProcessingErrors($result, get_class($this), __FUNCTION__);

            throw new Exception(__("Errors detected when processing. Previous records are processed."), 400, Exception::HTTP_BAD_REQUEST, [$result]);

    }

    protected function setMultipleAccountNumbers($records)
    {

        foreach ($records as $index => $record) {

            try {

                $customer = $this->modelRepository->get($record['email']);
            }catch (\Exception $e){
                $this->hudsonLogger->log("API: EMAIL NOT FOUND: ".$record['email'],\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_API_INFO);
                continue;
            }


            if ($customer->getId()) {
                $customer->setCustomAttribute('customer_number',$record['customerNumber']);
                $this->modelRepository->save($customer);
                $this->hudsonLogger->log("API: CUSTOMER NUMBER OF ".$record['email']." set to ".$record['customerNumber'],\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_API_INFO);
            }
        }

        return true;
    }

    protected function setMultipleRepDetails($records){

        foreach ($records as $index => $record) {

            try {

                $customer = $this->modelRepository->get($record['email']);
            }catch (\Exception $e){
                $this->hudsonLogger->log("API: EMAIL NOT FOUND: ".$record['email'],\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_API_INFO);
                continue;
            }


            if ($customer->getId()) {
                if(isset($record['repName'])) {
                    $customer->setCustomAttribute('rep_name', $record['repName']);
                }
                if(isset($record['repEmail'])) {
                    $customer->setCustomAttribute('rep_email', $record['repEmail']);
                }
                if(isset($record['repContact'])) {
                    $customer->setCustomAttribute('rep_contact', $record['repContact']);
                }

                $this->modelRepository->save($customer);
                $this->hudsonLogger->log("API: Rep details OF ".$record['email']." are now set",\Hudson\CustomerPricing\Helper\Logger\Logger::HUDSON_API_INFO);
            }
        }

        return true;
    }

    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function setRepDetails($records)
    {

        $requiredFields = ['email'];
        $errors = $this->validateRecords($records,$requiredFields ,[],[]);

        if (true !== $errors) {
            //Logging errors for records
            $this->logValidationErrors($errors, get_class($this), __FUNCTION__);

            throw new Exception(__("Errors detected on validation. No records are processed."), 400, Exception::HTTP_BAD_REQUEST, $errors);
        }


        $result = $this->setMultipleRepDetails($records);

        //Invalidate cache
        if ($result === true) {
            $this->invalidateCache();
            return $result;
        }

        //Logging errors for records
        $this->logProcessingErrors($result, get_class($this), __FUNCTION__);

        throw new Exception(__("Errors detected when processing. Previous records are processed."), 400, Exception::HTTP_BAD_REQUEST, [$result]);
    }
}