<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 15/02/2018
 * Time: 16:20
 */

namespace Hudson\CustomerPricing\Helper\Api;

use Hudson\CustomerPricing\Helper\Logger\Logger;
use Hudson\CustomerPricing\Model\Contract\CollectionFactory as ContractCollectionFactory;
use Hudson\CustomerPricing\Model\FamilyDiscount\CollectionFactory as FamilyDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\GroupDiscount\CollectionFactory as GroupDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\ItemDiscount\CollectionFactory as ItemDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\OrderDiscount\CollectionFactory as OrderDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\Promotion\CollectionFactory as PromotionCollectionFactory;
use Hudson\CustomerPricing\Api\MassDeleteInterface;

class DeleteAllDiscounts implements MassDeleteInterface
{

//ID FLAGS
    const CONTRACT_FLAG = "contract";
    const FAMILYDISCOUNT_FLAG = "familydiscount";
    const GROUPDISCOUNT_FLAG = "groupdiscount";
    const ITEMDISCOUNT_FLAG = "itemdiscount";
    const ORDERDISCOUNT_FLAG = "orderdiscount";
    const PROMOTION_FLAG = "promotion";

    protected $logger;

    protected $contractCollectionFactory;
    protected $familyDiscountCollectionFactory;
    protected $groupDiscountCollectionFactory;
    protected $itemDiscountCollectionFactory;
    protected $orderDiscountCollectionFactory;
    protected $promotionCollectionFactory;

    public function __construct(Logger $logger,
                                ContractCollectionFactory $contractCollectionFactory,
                                FamilyDiscountCollectionFactory $familyDiscountCollectionFactory,
                                GroupDiscountCollectionFactory $groupDiscountCollectionFactory,
                                ItemDiscountCollectionFactory $itemDiscountCollectionFactory,
                                OrderDiscountCollectionFactory $orderDiscountCollectionFactory,
                                PromotionCollectionFactory $promotionCollectionFactory
    )
    {
        $this->logger = $logger;
        $this->contractCollectionFactory = $contractCollectionFactory;
        $this->familyDiscountCollectionFactory = $familyDiscountCollectionFactory;
        $this->groupDiscountCollectionFactory = $groupDiscountCollectionFactory;
        $this->itemDiscountCollectionFactory = $itemDiscountCollectionFactory;
        $this->orderDiscountCollectionFactory = $orderDiscountCollectionFactory;
        $this->promotionCollectionFactory = $promotionCollectionFactory;
    }


    /**
     * Deletes all discounts.
     * Typically used overnight before reimporting all discounts again.
     * @return mixed
     */
    public function deleteAllDiscounts()
    {
        $this->deleteExpiredRecords(self::CONTRACT_FLAG);
        $this->deleteExpiredRecords(self::FAMILYDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::GROUPDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::ITEMDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::ORDERDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::PROMOTION_FLAG);

        return true;
    }

    protected function deleteExpiredRecords($flag)
    {

        $deleteCollection = null;

        switch ($flag) {
            case self::CONTRACT_FLAG:
                $deleteCollection = $this->contractCollectionFactory->create();
                break;
            case self::FAMILYDISCOUNT_FLAG:
                $deleteCollection = $this->familyDiscountCollectionFactory->create();
                break;
            case self::GROUPDISCOUNT_FLAG:
                $deleteCollection = $this->groupDiscountCollectionFactory->create();
                break;
            case self::ITEMDISCOUNT_FLAG:
                $deleteCollection = $this->itemDiscountCollectionFactory->create();
                break;
            case self::ORDERDISCOUNT_FLAG:
                $deleteCollection = $this->orderDiscountCollectionFactory->create();
                break;
            case self::PROMOTION_FLAG:
                $deleteCollection = $this->promotionCollectionFactory->create();
                break;
            default:
                $this->logger->log("COLLECTION FLAG NOT RECOGNIZED : $flag", Logger::HUDSON_API_INFO);
                return;
        }

        if ($deleteCollection->getSize()) {
            $this->logActivity("ALL RECORDS ARE BEING DELETED FOR: $flag from API DELETE ALL CALL \n");

            $deleteCollection->walk('delete');

            $this->logActivity("ALL RECORDS DELETED: $flag records were deleted from API DELETE ALL CALL \n");
        }
        return;
    }


    protected function logActivity($message)
    {
        $this->logger->log($message, Logger::HUDSON_API_INFO);
    }
}