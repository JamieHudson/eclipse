<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 22/12/2017
 * Time: 12:50
 */

namespace Hudson\CustomerPricing\Helper\Api;

use \Hudson\CustomerPricing\Api\MassContractInterface;
use Hudson\CustomerPricing\Api\MassFamilyDiscountInterface;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use Magento\Framework\Webapi\Exception;
use Psr\Log\LoggerInterface;



class MassFamilyDiscount extends \Hudson\CustomerPricing\Helper\MassApiHelper implements MassFamilyDiscountInterface
{
    //Model Specific
    const CREATE_REQUIRED_FIELDS = ['customerNumber','familyCode','specialPrice','discountPercentage','validFrom','validTo','discountCode'];
    const DELETE_REQUIRED_FIELDS = ['customerNumber','familyCode'];
    const NUMERIC_FIELDS = ['specialPrice','discountPercentage'];
    const DATE_FIELDS = ['validFrom','validTo'];

    //Model factory for creating records
    protected $modelFactory;

    //For mass delete of all matching records
    protected $modelCollectionFactory;


    public function __construct(\Hudson\CustomerPricing\Model\FamilyDiscountFactory $modelFactory,
                                \Hudson\CustomerPricing\Model\FamilyDiscount\CollectionFactory $modelCollectionFactory,
                                DateFormatTool $dateFormatTool,
                                LoggerInterface $logger,
                                \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
                                \Hudson\CacheManager\Model\ResourceModel\HudsonCache\CollectionFactory $cacheCollectionFactory)
    {
        parent::__construct($logger,$cacheCollectionFactory,$hudsonLogger,$dateFormatTool);
        $this->modelFactory = $modelFactory;
        $this->modelCollectionFactory = $modelCollectionFactory;
    }


    //This method needs to be in this class as this class implements the interface used by the API
    /**
     *
     * @inheritdoc
     */
    public function createRecords($records){


        $errors = $this->validateRecords($records,self::CREATE_REQUIRED_FIELDS,self::NUMERIC_FIELDS,self::DATE_FIELDS);

        if(true !== $errors) {
            $this->logValidationErrors($errors,get_class($this),__FUNCTION__);

            throw new Exception(__("Errors detected on validation."),400,Exception::HTTP_BAD_REQUEST,$errors);
        }

        $result = $this->createMultipleRecords($records);


        //Invalidate cache
        if($result === true) {
            $this->invalidateCache();
            return $result;
        }

        $this->logProcessingErrors($result,get_class($this),__FUNCTION__);

        throw new Exception(__("Errors detected when processing. Previous records are processed."),400,Exception::HTTP_BAD_REQUEST,[$result]);
    }

    protected function createMultipleRecords($array){

        foreach ($array as $index => $record){

            $model = $this->modelFactory->create();

            $result = $model->createSingleRecord(
                $record['customerNumber'],
                $record['familyCode'],
                $record['specialPrice'],
                $record['discountPercentage'],
                $record['validFrom'],
                $record['validTo'],
                $record['discountCode']
            );


            if(true !==  $result){
                return "Execution stopped: $result - At record $index (index) : ".implode(" , ",$record);
            }
        }

        return true;
    }


    /**
     * @inheritdoc
     */
    public function deleteRecords($records)
    {
        $errors = $this->validateRecords($records,self::DELETE_REQUIRED_FIELDS,self::NUMERIC_FIELDS,self::DATE_FIELDS);

        if(true !== $errors) {
            $this->logValidationErrors($errors,get_class($this),__FUNCTION__);

            throw new Exception(__("Errors detected on validation."),400,Exception::HTTP_BAD_REQUEST,$errors);
        }


        $result = $this->deleteMultipleRecords($records);

        //Invalidate cache
        if($result === true) {
            $this->invalidateCache();
            return $result;
        }

        $this->logProcessingErrors($result,get_class($this),__FUNCTION__);

        throw new Exception(__("Errors detected when processing. Previous records are processed."),400,Exception::HTTP_BAD_REQUEST,[$result]);
    }

    protected function deleteMultipleRecords($array){

        foreach ($array as $record){

            $collection = $this->modelCollectionFactory->create();
            $collection->addFieldToFilter('customer_number', ["eq" => $record['customerNumber']]);
            $collection->addFieldToFilter('family_code', ["eq" => $record['familyCode']]);

            $result = true;

            try {
                $collection->walk("delete");
            }catch (\Exception $e){
                $result = $e->getMessage();
            }

            if(true !==  $result){
                return $result;
            }
        }

        return true;
    }
}