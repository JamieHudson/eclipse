<?php
namespace Hudson\CustomerPricing\Cron;

use Hudson\CustomerPricing\Helper\Logger\Logger;
use Hudson\CustomerPricing\Model\Contract\CollectionFactory as ContractCollectionFactory;
use Hudson\CustomerPricing\Model\FamilyDiscount\CollectionFactory as FamilyDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\GroupDiscount\CollectionFactory as GroupDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\ItemDiscount\CollectionFactory as ItemDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\OrderDiscount\CollectionFactory as OrderDiscountCollectionFactory;
use Hudson\CustomerPricing\Model\Promotion\CollectionFactory as PromotionCollectionFactory;


class CleanExpired {

    //ID FLAGS
    const CONTRACT_FLAG = "contract";
    const FAMILYDISCOUNT_FLAG = "familydiscount";
    const GROUPDISCOUNT_FLAG = "groupdiscount";
    const ITEMDISCOUNT_FLAG = "itemdiscount";
    const ORDERDISCOUNT_FLAG = "orderdiscount";
    const PROMOTION_FLAG = "promotion";

    protected $logger;

    protected $contractCollectionFactory;
    protected $familyDiscountCollectionFactory;
    protected $groupDiscountCollectionFactory;
    protected $itemDiscountCollectionFactory;
    protected $orderDiscountCollectionFactory;
    protected $promotionCollectionFactory;

    public function __construct(Logger $logger,
                                ContractCollectionFactory $contractCollectionFactory,
                                FamilyDiscountCollectionFactory $familyDiscountCollectionFactory,
                                GroupDiscountCollectionFactory $groupDiscountCollectionFactory,
                                ItemDiscountCollectionFactory $itemDiscountCollectionFactory,
                                OrderDiscountCollectionFactory $orderDiscountCollectionFactory,
                                PromotionCollectionFactory $promotionCollectionFactory
                                ) {
        $this->logger = $logger;
        $this->contractCollectionFactory = $contractCollectionFactory;
        $this->familyDiscountCollectionFactory = $familyDiscountCollectionFactory;
        $this->groupDiscountCollectionFactory = $groupDiscountCollectionFactory;
        $this->itemDiscountCollectionFactory = $itemDiscountCollectionFactory;
        $this->orderDiscountCollectionFactory = $orderDiscountCollectionFactory;
        $this->promotionCollectionFactory = $promotionCollectionFactory;
    }



    /**
     * Deletes the expired discounts to keep the db updated and tidy
     * @return void
     */

    public function execute() {

        $this->deleteExpiredRecords(self::CONTRACT_FLAG);
        $this->deleteExpiredRecords(self::FAMILYDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::GROUPDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::ITEMDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::ORDERDISCOUNT_FLAG);
        $this->deleteExpiredRecords(self::PROMOTION_FLAG);
    }

    protected function deleteExpiredRecords($flag){

        $todaysDate = date("Y-m-d");

        $expiredCollection = null;

        switch ($flag) {
            case self::CONTRACT_FLAG:
                $expiredCollection = $this->contractCollectionFactory->create();
                break;
            case self::FAMILYDISCOUNT_FLAG:
                $expiredCollection = $this->familyDiscountCollectionFactory->create();
                break;
            case self::GROUPDISCOUNT_FLAG:
                $expiredCollection = $this->groupDiscountCollectionFactory->create();
                break;
            case self::ITEMDISCOUNT_FLAG:
                $expiredCollection = $this->itemDiscountCollectionFactory->create();
                break;
            case self::ORDERDISCOUNT_FLAG:
                $expiredCollection = $this->orderDiscountCollectionFactory->create();
                break;
            case self::PROMOTION_FLAG:
                $expiredCollection = $this->promotionCollectionFactory->create();
                break;
            default:
                $this->logger->log("COLLECTION FLAG NOT RECOGNIZED : $flag",Logger::HUDSON_INFO);
                return;
        }

        //Adding expired filter
        $expiredCollection->addFieldToFilter('valid_to', ["lt" => $todaysDate]);

        if($expiredCollection->getSize()) {

            $this->logActivity($flag, $expiredCollection);

            $expiredCollection->walk('delete');
        }

        return;
    }


    protected function logActivity($flag,$collection){

        $collection->load();

        $recordsString = "EXPIRED RECORDS: $flag records were deleted from cron job \n";
        foreach ($collection as $record){

            $data = $record->getData();

            $recordLine = implode(",",$data);
            $recordsString .= "DELETED EXPIRED RECORD: ".$recordLine. "\n";

        }

        $this->logger->log($recordsString,Logger::HUDSON_INFO);
    }





}
