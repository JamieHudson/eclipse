<?php

namespace Hudson\CustomerPricing\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Table containing The top level Customer - Product Contracts
         * Create table 'hud_contracts'
         */
        $contractsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_contract'))
            ->addColumn(
                'contract_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Contract Id'
            )
            ->addColumn(
                'discount_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Discount Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            )
            ->addColumn(
                'product_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Product Code'
            )->addColumn(
                'special_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Special Price'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_contract',
                    ['customer_number', 'product_code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_number', 'product_code'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Customer Contracts');
        $installer->getConnection()->createTable($contractsTable);


        /**
         * Table containing Item discounts per customer
         * Create table 'hud_item_discount'
         */
        $itemDiscountsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_item_discount'))
            ->addColumn(
                'discount_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Discount Id'
            )
            ->addColumn(
                'discount_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Discount Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            )
            ->addColumn(
                'product_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Product Code'
            )->addColumn(
                'special_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Special Price'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_item_discount',
                    ['customer_number', 'product_code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_number', 'product_code'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Item Discounts');
        $installer->getConnection()->createTable($itemDiscountsTable);


        /**
         * Table containing Family discounts per customer
         * Create table 'hud_family_discount'
         */
        $familyDiscountsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_family_discount'))
            ->addColumn(
                'discount_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Discount Id'
            )
            ->addColumn(
                'discount_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Discount Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            )
            ->addColumn(
                'family_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Family Code'
            )->addColumn(
                'special_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Special Price'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_family_discount',
                    ['customer_number', 'family_code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_number', 'family_code'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Family Discounts');
        $installer->getConnection()->createTable($familyDiscountsTable);


        /**
         * Table containing Group discounts per customer
         * Create table 'hud_group_discount'
         */
        $groupDiscountsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_group_discount'))
            ->addColumn(
                'discount_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Discount Id'
            )
            ->addColumn(
                'discount_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Discount Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            )
            ->addColumn(
                'group_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Group Code'
            )->addColumn(
                'special_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Special Price'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'hud_group_discount',
                    ['customer_number', 'group_code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_number', 'group_code'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Group Discounts');
        $installer->getConnection()->createTable($groupDiscountsTable);


        /**
         * Table containing Order discounts per customer
         * Create table 'hud_order_discount'
         */
        $orderDiscountsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_order_discount'))
            ->addColumn(
                'discount_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Discount Id'
            )
            ->addColumn(
                'discount_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Discount Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )->addIndex(
                $installer->getIdxName('hud_order_discount', ['customer_number'],\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE),
                ['customer_number'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Order Discounts');
        $installer->getConnection()->createTable($orderDiscountsTable);


        /**
         * Table containing Promotions per customer
         * Create table 'hud_promotion'
         */
        $promotionsTable = $installer->getConnection()
            ->newTable($installer->getTable('hud_promotion'))
            ->addColumn(
                'promotion_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Promotion Id'
            )
            ->addColumn(
                'promotion_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true,'default' => null],
                'Promotion Code'
            )
            ->addColumn(
                'customer_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Customer Number'
            ) ->addColumn(
                'product_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Product Code'
            )->addColumn(
                'discount_percentage',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true,'default' => null],
                'Discount Percentage'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true, 'default' => null],
                'Valid To'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )->addIndex(
                $installer->getIdxName(
                    'hud_promotion',
                    ['customer_number', 'product_code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['customer_number', 'product_code'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Hudson Promotions');
        $installer->getConnection()->createTable($promotionsTable);


        $installer->endSetup();

    }
}
