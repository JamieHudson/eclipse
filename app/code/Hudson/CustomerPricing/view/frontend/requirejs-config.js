var config = {
    map: {
         '*': {
                priceBox:'Hudson_CustomerPricing/js/turnoffcustompricebox',
                AjaxCrossOrigin:"Hudson_CustomerPricing/js/ajaxcrossorigin"
         }
    },
    shim: {
        'Hudson_CustomerPricing/js/ajaxcrossorigin': ['jquery']
    }
};