<?php

namespace Hudson\CustomerPricing\Observer;

use Exception;
use Hudson\CustomerPricing\Helper\Calculator;
use Hudson\CustomerPricing\Helper\Logger\Logger;
use Magento\Framework\Exception\LocalizedException;

class UpdatePriceCart implements \Magento\Framework\Event\ObserverInterface
{
    //has the parameters
    protected $_request;

    //Calculates the price times the custom option
    protected $multiplicationHelper;

    //Calculates price for current customer
    protected $pricingHelper;

    //redirector
    protected $redirect;

    //for server-side validation of custom options
    protected $responseFactory;
    protected $messageManager;

    //Getting customer data
    protected $customerSession;

    protected $hudsonLogger;


    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger
    ) {

        $this->_request = $request;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->pricingHelper = $pricingHelper;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->hudsonLogger = $hudsonLogger;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        //check if enabled
        //proceed if not
        if (!$this->multiplicationHelper->isEnabledInFrontend()) {
            return;
        }

        /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $_product = $observer->getProduct();

        $notForSaleAttributeCode = $this->multiplicationHelper->getNotFofSaleAttributeCode();
        if ($notForSaleAttributeCode) {
            if ($_product->getData($notForSaleAttributeCode)) {
                throw new LocalizedException(__("This product cannot be added to the cart. Please contact us for details."));
            }
        }

        $changedFlag = false;

        $finalPrice = round($_product->getFinalPrice(), 2);

        $price = round($_product->getTierPrice($quoteItem->getQty()), 2);

        if ($finalPrice < $price) {
            $price = $finalPrice;
        }

        //Finds if the customer has a special discount on this product
        try {
            if ($this->customerSession->isLoggedIn()) {
                $customer = $this->customerSession->getCustomer();
                if ($customer) {
                    //Quering the db for customer prices
                    $price = $this->pricingHelper->getCartPriceForCustomer($customer, $_product, $price);
                    $changedFlag = true;
                }
            }
        } catch (Exception $e) {
            //Customer check for special price fails
           //Logging and throwing again
            $location = "UpdatePriceCart.php check for special price for customer \n";
            $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
            $fullMessage = $location . $errorMessage;
            $this->hudsonLogger->log($fullMessage, Logger::HUDSON_EXCEPTION);
            throw $e;
        }

        //Check if option is set for this product
        //proceed if not
        $customOptions = $_product->getOptions();
        $optionId = $this->multiplicationHelper->getMultiplierOptionId($customOptions); //Checks if the option set in the backend is set for this product
        
        if (!$optionId) {
            //Proceed by setting the discounted price calculated if any
            if ($changedFlag) {
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }

            return;
        }

        $params = $this->_request->getParams();
        $productId = $_product->getId();
        
        $postedOptions = [];
        if (isset($params['options'])) {
            $postedOptions = $params['options'];
        } elseif (isset($params['options_'.$productId])) {
            $postedOptions = $params['options_'.$productId];
        } else {
            //Proceed by setting the discounted price calculated if any
            if ($changedFlag) {
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }
            return;
        }
        
        $newPrice = false;
        try {
            $newPrice = $this->multiplicationHelper->getMultipliedPrice($price, $postedOptions, $optionId);
        } catch (\Exception $e) {
            //logging this , do not throw again as error is returned below
            //Multiplying price fails
            $location = "UpdatePriceCart.php multiplying option and validation \n";
            $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
            $fullMessage = $location . $errorMessage;
            $this->hudsonLogger->log($fullMessage, Logger::HUDSON_EXCEPTION);
        }

        //Validation of the field fails
        if (false === $newPrice) {
            $redirectUrl = $this->redirect->getRedirectUrl();
            $this->messageManager->addError(_("Input in one of the fields is invalid or value is not in range. Please try again."));
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
            //This 'die' is not left behind and should stay
            die;
        }

        //Setting the new price
        $quoteItem->setCustomPrice($newPrice);
        $quoteItem->setOriginalCustomPrice($newPrice);
        $quoteItem->getProduct()->setIsSuperMode(true);
    }
}
