<?php

namespace Hudson\CustomerPricing\Observer;

use Exception;
use Hudson\CustomerPricing\Helper\Calculator;
use Hudson\CustomerPricing\Helper\Logger\Logger;
use Magento\Framework\Exception\LocalizedException;

class UpdateCart implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Hudson\CustomerPricing\Helper\Data $multiplicationHelper,
        \Hudson\CustomerPricing\Helper\Calculator $pricingHelper,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Hudson\CustomerPricing\Helper\Logger\Logger $hudsonLogger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {

        $this->_request = $request;
        $this->multiplicationHelper = $multiplicationHelper;
        $this->pricingHelper = $pricingHelper;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->hudsonLogger = $hudsonLogger;
        $this->productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //check if enabled
        //proceed if not
        if (!$this->multiplicationHelper->isEnabledInFrontend()) {
            return;
        }

        $quote = $observer->getEvent()->getCart()->getQuote();
        $quoteItems = $quote->getItemsCollection();

        foreach ($quoteItems as $quoteItem) {
            $product = $quoteItem->getProduct();
            $this->updateProduct($product, $quoteItem);
        }
    }

    protected function updateProduct($_product, $quoteItem)
    {
        //check if enabled
        //proceed if not
        if (!$this->multiplicationHelper->isEnabledInFrontend()) {
            return;
        }

        $notForSaleAttributeCode = $this->multiplicationHelper->getNotFofSaleAttributeCode();
        if ($notForSaleAttributeCode) {
            if ($_product->getData($notForSaleAttributeCode)) {
                throw new LocalizedException(__("This product cannot be added to the cart. Please contact us for details."));
            }
        }

        $changedFlag = false;

        $finalPrice = round($_product->getFinalPrice(), 2);

        $price = round($_product->getTierPrice($quoteItem->getQty()), 2);

        if ($finalPrice < $price) {
            $price = $finalPrice;
        }

        //Finds if the customer has a special discount on this product
        try {
            if ($this->customerSession->isLoggedIn()) {
                $customer = $this->customerSession->getCustomer();
                if ($customer) {
                    //Quering the db for customer prices
                    $price = $this->pricingHelper->getCartPriceForCustomer($customer, $_product, $price);
                    if ($_product->getPrice() != $price) {
                        $changedFlag = true;
                    }
                }
            }
        } catch (Exception $e) {
            //Customer check for special price fails
           //Logging and throwing again
            $location = "UpdatePriceCart.php check for special price for customer \n";
            $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
            $fullMessage = $location . $errorMessage;
            $this->hudsonLogger->log($fullMessage, Logger::HUDSON_EXCEPTION);
            throw $e;
        }

        //Check if option is set for this product
        //proceed if not
        $customOptions = $_product->getOptions();

        if ($customOptions) {
            $optionId = $this->multiplicationHelper->getMultiplierOptionId($customOptions); //Checks if the option set in the backend is set for this product
        } else {
            $optionId = false;
        }

        if (!$optionId) {
            //Proceed by setting the discounted price calculated if any
            if ($changedFlag) {
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }

            return;
        }

        $params = $this->_request->getParams();
        $productId = $_product->getId();
        $postedOptions = [];
        
        if (isset($params['options'])) {
            $postedOptions = $params['options'];
        } elseif (isset($params['options_'.$productId])) {
            $postedOptions = $params['options_'.$productId];
        } else {
            //Proceed by setting the discounted price calculated if any
            if ($changedFlag) {
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
                $quoteItem->getProduct()->setIsSuperMode(true);
            }
            return;
        }
        
        if(!in_array($optionId,$postedOptions)){
            return;
        }
        
        
        $newPrice = false;
        try {
            $newPrice = $this->multiplicationHelper->getMultipliedPrice($price, $postedOptions, $optionId);
        } catch (\Exception $e) {
            //logging this , do not throw again as error is returned below
            //Multiplying price fails
            $location = "UpdatePriceCart.php multiplying option and validation \n";
            $errorMessage = $e->getCode() . " - " . $e->getMessage() . "\n" . $e->getTraceAsString();
            $fullMessage = $location . $errorMessage;
            $this->hudsonLogger->log($fullMessage, Logger::HUDSON_EXCEPTION);
        }

        //Validation of the field fails
        if (false === $newPrice) {
            $redirectUrl = $this->redirect->getRedirectUrl();
            $this->messageManager->addError(_("Input in one of the fields is invalid or value is not in range. Please try again."));
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
            //This 'die' is not left behind and should stay
            die;
        }

        //Setting the new price
        $quoteItem->setCustomPrice($newPrice);
        $quoteItem->setOriginalCustomPrice($newPrice);
        $quoteItem->getProduct()->setIsSuperMode(true);
    }
}
