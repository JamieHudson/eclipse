<?php

namespace Hudson\FixGroupPricing\Observer;

use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Framework\Event\ObserverInterface;

class UpdatePricing implements ObserverInterface
{
	public function __construct
		(
		 \Magento\Framework\ObjectManagerInterface $objectmanager,
		 \Magento\Catalog\Api\ProductTierPriceManagementInterface $tier,
		 \Magento\Customer\Model\Session $customerSession
		){
			$this->_objectManager = $objectmanager;
			$this->tier = $tier;
			$this->_customerSession = $customerSession;
		}

	public function execute(EventObserver $observer)
	{
		$cart = $observer->getData('cart');
		$quote = $cart->getData('quote');
		$items = $quote->getAllVisibleItems();
		$customerGroup = $this->_customerSession->getCustomer()->getGroupId();

		foreach ($items as $item) {
			$list = $this->tier->getList($item->getSku(), 'all');

			foreach ($list as $tier) {
				$tierCustomerGroup = $tier->getCustomerGroupId();
				$tierQty = $tier->getQty();
				$tierPrice = $tier->getValue();
				$itemQty = $item->getQty();

				if (!$tierCustomerGroup && !$tierQty && !$tierPrice && !$itemqty) {
					continue;
				}	

				if (($tierCustomerGroup == $customerGroup || $tierCustomerGroup == 32000) &&
				    $itemQty >= $tierQty) {
					$item->setCustomPrice($tierPrice);
					$item->setOriginalCustomPrice($tierPrice);
					$item->getProduct()->setIsSuperMode(true);
		                } else {
					$item->setCustomPrice($item->getProduct()->getPrice());
					$item->setOriginalCustomPrice($item->getProduct()->getPrice());
					$item->getProduct()->setIsSuperMode(true);
				}

				$tierCustomerGroup = false;
				$tierQty = false;
				$tierPrice = false;
				$itemQty = false;
			}

		}
	}
}
