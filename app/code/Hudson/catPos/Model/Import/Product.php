<?php

namespace Hudson\catPos\Model\Import;


use Magento\Catalog\Model\Product\Visibility;

use Magento\CatalogImportExport\Model\Import\Product\RowValidatorInterface as ValidatorInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Model\ResourceModel\Db\ObjectRelationProcessor;
use Magento\Framework\Model\ResourceModel\Db\TransactionManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Store\Model\Store;


class Product extends \Magento\CatalogImportExport\Model\Import\Product
{


  protected $_resourceFactory;


  protected $skuProcessor;

  protected $transactionManager;

 // protected $_optionEntity;


  public function __construct(

    \Magento\Framework\Json\Helper\Data $jsonHelper,
    \Magento\ImportExport\Helper\Data $importExportData,
    \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
    \Magento\Eav\Model\Config $config,
    \Magento\Framework\App\ResourceConnection $resource,
    \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
    \Magento\Framework\Stdlib\StringUtils $string,
    ProcessingErrorAggregatorInterface $errorAggregator,
    \Magento\Framework\Event\ManagerInterface $eventManager,
    \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
    \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
    \Magento\CatalogInventory\Model\Spi\StockStateProviderInterface $stockStateProvider,
    \Magento\Catalog\Helper\Data $catalogData,
    \Magento\ImportExport\Model\Import\Config $importConfig,
    \Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory $resourceFactory,
    \Magento\CatalogImportExport\Model\Import\Product\OptionFactory $optionFactory,
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setColFactory,
    \Magento\CatalogImportExport\Model\Import\Product\Type\Factory $productTypeFactory,
    \Magento\Catalog\Model\ResourceModel\Product\LinkFactory $linkFactory,
    \Magento\CatalogImportExport\Model\Import\Proxy\ProductFactory $proxyProdFactory,
    \Magento\CatalogImportExport\Model\Import\UploaderFactory $uploaderFactory,
    \Magento\Framework\Filesystem $filesystem,
    \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory $stockResItemFac,
    \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
    DateTime $dateTime,
    \Psr\Log\LoggerInterface $logger,
    \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
    \Magento\CatalogImportExport\Model\Import\Product\StoreResolver $storeResolver,
    \Magento\CatalogImportExport\Model\Import\Product\SkuProcessor $skuProcessor,
    \Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor $categoryProcessor,
    \Magento\CatalogImportExport\Model\Import\Product\Validator $validator,
    ObjectRelationProcessor $objectRelationProcessor,
    TransactionManagerInterface $transactionManager,
    \Magento\CatalogImportExport\Model\Import\Product\TaxClassProcessor $taxClassProcessor,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Catalog\Model\Product\Url $productUrl,
    array $data = [],
    array $dateAttrCodes = []



  ) {
    parent::__construct(
      $jsonHelper,
      $importExportData,
      $importData,
      $config,
      $resource,
      $resourceHelper,
      $string,
      $errorAggregator,
        $eventManager,
        $stockRegistry,
        $stockConfiguration,
        $stockStateProvider,
        $catalogData,
        $importConfig,
        $resourceFactory,
        $optionFactory,
        $setColFactory,
        $productTypeFactory,
        $linkFactory,
        $proxyProdFactory,
        $uploaderFactory,
        $filesystem,
        $stockResItemFac,
        $localeDate,
        $dateTime,
        $logger,
        $indexerRegistry,
        $storeResolver,
        $skuProcessor,
        $categoryProcessor,
        $validator,
        $objectRelationProcessor,
        $transactionManager,
        $taxClassProcessor,
        $scopeConfig,
        $productUrl
        // $optionEntity = isset(
        //     $data['option_entity']
        // ) ? $data['option_entity'] : $optionFactory->create(
        //     ['data' => ['product_entity' => $this]]
        // )

          );


  }



  protected function _saveProductCategories(array $categoriesData)
  {
    static $tableName = null;

    if (!$tableName) {
      $tableName = $this->_resourceFactory->create()->getProductCategoryTable();
    }
    if ($categoriesData) {
      $categoriesIn = [];
      $delProductId = [];

      foreach ($categoriesData as $delSku => $categories) {
        $productId = $this->skuProcessor->getNewSku($delSku)['entity_id'];
        $delProductId[] = $productId;

        foreach (array_keys($categories) as $categoryId) {
          $categoriesIn[] = ['product_id' => $productId, 'category_id' => $categoryId, 'position' => 1];
        }
      }
      if (Import::BEHAVIOR_APPEND != $this->getBehavior()) {
        $this->_connection->delete(
          $tableName,
          $this->_connection->quoteInto('product_id IN (?)', $delProductId)
        );
      }
      if ($categoriesIn) {
        $this->_connection->insertOnDuplicate($tableName, $categoriesIn, ['product_id', 'category_id']);
      }
    }
    return $this;
  }
}
