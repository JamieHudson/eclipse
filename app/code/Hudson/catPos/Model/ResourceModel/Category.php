<?php
/**
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/

/**
* Catalog category model
*
* @author      Magento Core Team <core@magentocommerce.com>
*/
namespace Hudson\catPos\Model\ResourceModel;

use Magento\Framework\EntityManager\EntityManager;


class Category extends \Magento\Catalog\Model\ResourceModel\Category
{

      protected $_eventManager = null;

  public function __construct(
    \Magento\Eav\Model\Entity\Context $context,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Catalog\Model\Factory $modelFactory,
    \Magento\Framework\Event\ManagerInterface $eventManager,
    \Magento\Catalog\Model\ResourceModel\Category\TreeFactory $categoryTreeFactory,
    \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
    $data = [],
    \Magento\Framework\Serialize\Serializer\Json $serializer = null
  ) {

    parent::__construct(
      $context,
      $storeManager,
      $modelFactory,
      $eventManager,
      $categoryTreeFactory,
      $categoryCollectionFactory,
      $data,
      $serializer
    );


  }

  protected function _saveCategoryProducts($category)
  {
      $category->setIsChangedProductList(false);
      $id = $category->getId();
      /**
       * new category-product relationships
       */
      $products = $category->getPostedProducts();

      /**
       * Example re-save category
       */
      if ($products === null) {
          return $this;
      }

      /**
       * old category-product relationships
       */
      $oldProducts = $category->getProductsPosition();

      $insert = array_diff_key($products, $oldProducts);
      $delete = array_diff_key($oldProducts, $products);

      /**
       * Find product ids which are presented in both arrays
       * and saved before (check $oldProducts array)
       */
      $update = array_intersect_key($products, $oldProducts);
      $update = array_diff_assoc($update, $oldProducts);

      $connection = $this->getConnection();

      /**
       * Delete products from category
       */
      if (!empty($delete)) {
          $cond = ['product_id IN(?)' => array_keys($delete), 'category_id=?' => $id];
          $connection->delete($this->getCategoryProductTable(), $cond);
      }

      /**
       * Add products to category
       */
      if (!empty($insert)) {
          $data = [];
          foreach ($insert as $productId => $position) {
              $data[] = [
                  'category_id' => (int)$id,
                  'product_id' => (int)$productId,
                  'position' => (int)$position ? (int)$position : 1
              ];
          }
          $connection->insertMultiple($this->getCategoryProductTable(), $data);
      }

      /**
       * Update product positions in category
       */
      if (!empty($update)) {
          $newPositions = [];
          foreach ($update as $productId => $position) {
              $delta = $position - $oldProducts[$productId];
              if (!isset($newPositions[$delta])) {
                  $newPositions[$delta] = [];
              }
              $newPositions[$delta][] = $productId;
          }

          foreach ($newPositions as $delta => $productIds) {
              $bind = ['position' => new \Zend_Db_Expr("position + ({$delta})")];
              $where = ['category_id = ?' => (int)$id, 'product_id IN (?)' => $productIds];
              $connection->update($this->getCategoryProductTable(), $bind, $where);
          }
      }

      if (!empty($insert) || !empty($delete)) {
          $productIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));
          $this->_eventManager->dispatch(
              'catalog_category_change_products',
              ['category' => $category, 'product_ids' => $productIds]
          );

          $category->setChangedProductIds($productIds);
      }

      if (!empty($insert) || !empty($update) || !empty($delete)) {
          $category->setIsChangedProductList(true);

          /**
           * Setting affected products to category for third party engine index refresh
           */
          $productIds = array_keys($insert + $delete + $update);
          $category->setAffectedProductIds($productIds);
      }
      return $this;
  }


  }
