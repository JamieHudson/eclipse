<?php

namespace Hudson\HideProducts\Plugin\Model;

class Layer
{
    public function __construct(
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->httpContext = $httpContext;
        $this->customerRepository = $customerRepositoryInterface;
    }
    public function afterGetProductCollection($subject, $result)
    {
        $collection = $result;

        $customerId = $this->httpContext->getValue('customer_id');
        $customer = $this->customerRepository->getById($customerId);

        $manufacturer = (bool) $customer->getCustomAttribute('manufacturer')->getValue();

        if ($manufacturer) {
            $collection->addAttributeToFilter('hide_from_manufacturer', ['eq' => '0']);
        }

        return $collection;
    }
}
