<?php

namespace Hudson\HideProducts\Plugin\Controller\Product;

class View
{
    public function __construct(
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
    ) {
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
        $this->httpContext = $httpContext;
        $this->customerRepository = $customerRepositoryInterface;
        $this->productRepository = $productRepositoryInterface;
    }

    public function aroundExecute($subject, $proceed)
    {
        $productId = $subject->getRequest()->getParam('id', false);
        $product = $this->productRepository->getById($productId);

        $productShouldBeHidden = (bool) $product->getData('hide_from_manufacturer');

        $customerId = $this->httpContext->getValue('customer_id');
        $customer = $this->customerRepository->getById($customerId);

        $customerIsManufacturer = (bool) $customer->getCustomAttribute('manufacturer')->getValue();

        if ($customerIsManufacturer && $productShouldBeHidden) {
            $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $redirect->setUrl('/');
            $this->messageManager->addErrorMessage('You do not have permissions to view this page, if you belief this was an error please contact us!');

            return $redirect;
        } else {
            return $proceed();
        }
    }
}
