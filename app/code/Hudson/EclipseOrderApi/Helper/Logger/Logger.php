<?php

namespace Hudson\EclipseOrderApi\Helper\Logger;

class Logger
{

    const HUDSON_API_INFO = 'orders_api_info';
    const HUDSON_API_EXCEPTION = 'orders_api_exception';

    public function log($msg, $type = self::HUDSON_API_INFO)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hudson_'.$type.'.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }

}