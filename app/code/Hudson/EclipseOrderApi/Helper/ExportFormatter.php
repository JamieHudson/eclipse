<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 10/01/2018
 * Time: 14:02
 */

namespace Hudson\EclipseOrderApi\Helper;

use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\App\Helper\Context;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Webapi\Exception;
use Hudson\EclipseOrderApi\Helper\Logger\Logger;


class ExportFormatter extends \Magento\Framework\App\Helper\AbstractHelper
{

    const CUT_CHARGE_CODE = "cutchargecode";
    const CUTTING_CHARGE = "cuttingcharge";
    const CUTTING_PRICE = "cuttingprice";
    const VAT_PERCENTAGE = 20.00;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var Logger
     */

    protected $logger;

    public function __construct(Context $context,
                                CustomerRepository $customerRepository,
                                Logger $logger,
                                ProductRepository $productRepository)
    {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
        $this->logger= $logger;
    }

    /**
     * Processes the orders into the expected format
     * @param \Magento\Sales\Model\ResourceModel\Order\Collection $collection
     * @return mixed
     */
    public function formatOrdersForExport($collection)
    {


        $collection->load();

	$counter = 0;

        $ordersFormatted = [];
        foreach ($collection as $order) {
            if ($order->getData('status') == 'gp_pending') {
                continue;
            }
            $this->logger->log("ORDER ".$order->getIncrementId(). " formatting for export!");
            try {
                $ordersFormatted[] = $this->_getOrderFormatted($order);
            }catch (\Exception $e){
                $this->logger->log("ORDER ".$order->getIncrementId(). " failed formatting with message : ".$e->getMessage(),Logger::HUDSON_API_EXCEPTION);
            }
            $this->logger->log("ORDER ".$order->getIncrementId(). " successfully formatted for export!");
	    $counter++;
        }


        $this->logger->log($counter . " ORDERS SENT OUT!");

        return ["ORDERDOWNLOAD" => $ordersFormatted];
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function _getOrderFormatted($order)
    {

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/smcn.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($order->debug(), true));

        $orderArray = [];

        $billingAddress = $order->getBillingAddress();
        $billing_name = $billingAddress->getFirstname() . " " . $billingAddress->getLastname();
        $billing_street = implode(" ", $billingAddress->getStreet());
        $billing_city = $billingAddress->getCity();
        $billing_postcode = $billingAddress->getPostcode();
        $billing_company = $billingAddress->getCompany();
        $billing_country = $billingAddress->getCountryId();
        $billing_tel = $billingAddress->getTelephone();

        $shippingAddress = $order->getShippingAddress();
        $shipping_name = $shippingAddress->getFirstname() . " " . $shippingAddress->getLastname();
        $shipping_street = implode(" ", $shippingAddress->getStreet());
        $shipping_city = $shippingAddress->getCity();
        $shipping_postcode = $shippingAddress->getPostcode();
        $shipping_company = $shippingAddress->getCompany();
        $shipping_country = $shippingAddress->getCountryId();
        $shipping_tel = $shippingAddress->getTelephone();

        $shipping_method = $order->getShippingMethod();
        $shipping_desc = $order->getShippingDescription();

        $customerNumber = "GUEST-OR-NOTFOUND";

        if (!$order->getCustomerIsGuest() && $order->getCustomerId()) {

            $customer = $this->customerRepository->getById($order->getCustomerId());
            if ($customer) {
                $customerNumberAttribute = $customer->getCustomAttribute('customer_number');
                if($customerNumberAttribute) {
                    $customerNumber = $customerNumberAttribute->getValue();
                }
            }
        }

        $orderHeader = ["ORDERHEADERDATA" => [
            "ORDERNO" => $order->getIncrementId(),
            "CUSTNO" => $customerNumber,
            "CUSTNM" => $billing_company,
            "DOCCUSTREF" => $order->getPurchaseOrderReference(),
            "SHIPPINGMETHOD" => $shipping_method,
            "SHIPPINGDESC" => $shipping_desc,
            "ORDERT" => $order->getCreatedAt(),
//            "ORDERDT" => $order->getCreatedAt(),
//            "PAYMENTMETHOD" => $order->getPayment()->getMethod(),
            "ORDERVL" => $order->getGrandTotal(),
//            "CUTCHARGETOTAL" => $order->getFee(),
//            "DISCOUNT" => $order->getDiscountAmount(),
//            "SUBTOTAL" => $order->getSubtotal(),
//            "SHIPPINGTOTAL" => $order->getShippingAmount(),
            "TAXVL" => $order->getTaxAmount(),
            "VATRATE" => self::VAT_PERCENTAGE,
            "CURR" => "Pound",
            "VATREGNO" => "N/A",
//            "BILLINGADDRESS" => [
//                "ACCNO" => $customerNumber,
//                "NAME" => $billing_name,
//                "COMPANY" => $billing_company,
//                "ADD1" => $billing_street,
                "INVADD1" => $shipping_street,
//                "ADD2" => $billing_city,
                "INVADD2" => $shipping_city,
//                "ADD3" => "",
                "INVADD3" => $shipping_country,
                "INVADD4" => "",
//                "POSTCODE" => $billing_postcode,
                "INVPOSTCODE" => $shipping_postcode,
                "COUNTRY" => $billing_country
//                "TEL" => $billing_tel
//            ],
//            "SHIPPINGADDRESS" => [
//                "NAME" => $shipping_name,
//                "COMPANY" => $shipping_company,
//                "ADD1" => $shipping_street,
//                "ADD2" => $shipping_city,
//                "ADD3" => "",
//                "POSTCODE" => $shipping_postcode,
//                "COUNTRY" => $shipping_country,
//                "TEL" => $shipping_tel
//            ]
        ]
        ];


	if ($order->getFeeAmount() != '0') {
		array_push($orderHeader, ["PROFORMAFEE" => $order->getFeeAmount()]);
	}

        //Adding all items to order
        $orderLine = [];
        $items = $order->getAllItems();

        foreach ($items as $index => $item) {
            $orderLine[] = $this->_getOrderLineFormatted($item,$order,$index);
        }

        //Adding cut charge as products
        $cutCharges = $this->_getCutChargeItems($items,$order);

        array_push($orderLine,$cutCharges);



        $orderArray['ORDER'] = [
            "ORDERHEADER" => $orderHeader,
            "ORDERLINE" => $orderLine
        ];


        return $orderArray;

    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     * @return array
     */
    protected function _getOrderLineFormatted($item,$order,$index)
    {
        //start from 1
        $index++;

        $product = $item->getProduct();

        $optionsString = "";

        $packsize = null;

        $multiplierAttributesString = $this->scopeConfig->getValue("customerpricing/general/multiplier");
        $multipliersArray = [];
        if($multiplierAttributesString){
            $multipliersArray = explode(",",$multiplierAttributesString);
        }

        $this->_logger->info($multiplierAttributesString);

        $options = $item->getProductOptions();
        if (isset($options['options']) && !empty($options['options'])) {
            foreach ($options['options'] as $option) {
                $optionsString .= $option['label'] . ":". $option['option_value'] . ",";

                if(in_array($option['label'],$multipliersArray)){
                    $packsize = $option['option_value'];
                }

            }
        }

        $optionsString = rtrim($optionsString,",");

        if(!$product){
            try {
                $product = $this->productRepository->get($item->getSku());
            }catch (NoSuchEntityException $e){
                $product = null;
            }
        }

//        if($product) {
//
//            $orderLineData = [
//                "ORDERLINEDATA" => [
//                    "PRODUCTCODE" => $product->getData('product_code'),
//                    "NAME" => $product->getName(),
//                    "QTY" => $item->getQtyOrdered(),
//                    "PACKSIZE" => $product->getData("pack_size"), //TODO: PUT NEW ATTRIBUTE HERE
//                    "ROWTOTAL" => $item->getRowTotal(), //TODO: Find out if per item or total and check if this is sell-price(including discounts)
//                    "VAT" => $item->getTaxAmount(), //TODO: Check if correct
//                    "VATRATE" => self::VAT_PERCENTAGE, //TODO: Check if correct
//                    "OPTIONS" => $optionsString, //TODO: Find out
//                ]
//            ];
//        }else{
//
//            $productCode = $item->getSku();
//            $productCode = str_replace("MAG","",$productCode);
//
//            $orderLineData = [
//                "ORDERLINEDATA" => [
//                    "PRODUCTCODE" => $productCode,
//                    "NAME" => $item->getName(),
//                    "QTY" => $item->getQtyOrdered(),
//                    "PACKSIZE" => "PRODUCT_DELETED_UNKNOWN", //TODO: PUT NEW ATTRIBUTE HERE
//                    "ROWTOTAL" => $item->getRowTotal(), //TODO: Find out if per item or total and check if this is sell-price(including discounts)
//                    "VAT" => $item->getTaxAmount(), //TODO: Check if correct
//                    "VATRATE" => self::VAT_PERCENTAGE, //TODO: Check if correct
//                    "OPTIONS" => $optionsString, //TODO: Find out
//                ]
//            ];
//        }




        if(!$packsize){
            if($product)
                $packsize = $product->getData('packsize');
            else
                $packsize = "N/A";
        }



        $orderLineData = [
                "ORDERLINEDATA" => [
                    "ORDLNNO" => $index,
                    "ORDNO" => $order->getIncrementId(),
                    "DOCCUSTREF" => "WEB: ".$order->getIncrementId(),
                    "CUSTREF" => "N/A",
                    "BLINDTYPE" => "N/A",
                    "PACKSIZE" => $packsize,
                    "ARTNO" => $item->getSku(),
                    "ARTNM" => $item->getName(),
//                    "QTY" => $item->getQtyOrdered(),
                    "ORDQTY" => $item->getQtyOrdered(),
//                    "PACKSIZE" => $product->getData("pack_size"), //TODO: PUT NEW ATTRIBUTE HERE
//                    "ROWTOTAL" => $item->getRowTotal(), //TODO: Find out if per item or total and check if this is sell-price(including discounts)
                    "ORDLVVL" => $item->getRowTotal(), //TODO: Find out if per item or total and check if this is sell-price(including discounts)
//                    "VAT" => $item->getTaxAmount(), //TODO: Check if correct
                    "TAXLNVL" => $item->getTaxAmount(), //TODO: Check if correct
//                    "VATRATE" => self::VAT_PERCENTAGE, //TODO: Check if correct
                    "OPTIONS" => $optionsString, //TODO: Find out
                    "WIDTH" => "N/A", //TODO: Find out
                    "DROP" => "N/A", //TODO: Find out
                    "OPTIONLINE" => "", //TODO: Find out
                    "BILLOFMATERIALS" => "", //TODO: Find out
                ]
            ];



        return $orderLineData;
    }

    /**
     * Converting cutting charge to products for order line
     * @param $items
     * @return array
     */
    private function _getCutChargeItems($items,$order)
    {
        $cutChargeOrderLine = [];

        $chargesQty = [];
        $chargesTotal = [];
        $chargeIndex = 1;

        foreach ($items as $item){

            $chargeIndex++;


            $product = $item->getProduct();


            if(!$product){
                try {
                    $product = $this->productRepository->get($item->getSku());
                }catch (NoSuchEntityException $e){
                    $product = null;
                }
            }

            if($product) {

                if (!$product->getData(self::CUTTING_CHARGE)) continue;

                $cutChargeCode = $product->getData(self::CUT_CHARGE_CODE);
                $cuttingPrice = (float)$product->getData(self::CUTTING_PRICE);
                //$cutQty = (int)$item->getQtyOrdered();
		$cutQty = 1;
		$cutTotal = $cuttingPrice;

                if (isset($chargesQty[$cutChargeCode])) {
                    $chargesQty[$cutChargeCode] += $cutQty;
                } else {
                    $chargesQty[$cutChargeCode] = $cutQty;
                }

	        if (isset($chargesTotal[$cutChargeCode])) {
                    $chargesTotal[$cutChargeCode] += $cutTotal;
                } else {
                    $chargesTotal[$cutChargeCode] = $cutTotal;
		}

            }else{
                continue;
            }

        }


        foreach ($chargesQty as $code => $qty){

            $total = $chargesTotal[$code];

            $vat = round((self::VAT_PERCENTAGE/100) * $total,2);

//            $cutChargeOrderLine = [
//                "ORDERLINEDATA" => [
//                    "PRODUCTCODE" => $code,
//                    "NAME" => $code . " CUT CHARGE",
//                    "QTY" => $qty,
//                    "PACKSIZE" => 1, //TODO: PUT NEW ATTRIBUTE HERE
//                    "ROWTOTAL" => $chargesTotal[$code], //TODO: Find out if per item or total and check if this is sell-price(including discounts)
//                    "VAT" => $vat, //TODO: Check if correct
//                    "VATRATE" => self::VAT_PERCENTAGE, //TODO: Check if correct
//                    "OPTIONS" => "", //TODO: Find out
//                ]
//            ];

            $cutChargeOrderLine = [
                "ORDERLINEDATA" => [
                    "ORDLNNO" => $chargeIndex,
                    "ORDNO" => $order->getIncrementId(),
                    "DOCCUSTREF" => "WEB: ".$order->getIncrementId(),
                    "CUSTREF" => "N/A",
                    "BLINDTYPE" => "N/A",
//                    "PRODUCTCODE" => $product->getData('product_code'),
                    "ARTNO" => $code,
                    "ARTNM" => $code . " CUT CHARGE",
//                    "QTY" => $item->getQtyOrdered(),
                    "ORDQTY" => $qty,
//                    "PACKSIZE" => $product->getData("pack_size"), //TODO: PUT NEW ATTRIBUTE HERE
//                    "ROWTOTAL" => $item->getRowTotal(), //TODO: Find out if per item or total and check if this is sell-price(including discounts)
                    "ORDLVVL" => $chargesTotal[$code], //TODO: Find out if per item or total and check if this is sell-price(including discounts)
//                    "VAT" => $item->getTaxAmount(), //TODO: Check if correct
                    "TAXLNVL" => $chargesTotal[$code]*0.2, //TODO: Check if correct
//                    "VATRATE" => self::VAT_PERCENTAGE, //TODO: Check if correct
                    "OPTIONS" => "", //TODO: Find out
                    "WIDTH" => "N/A", //TODO: Find out
                    "DROP" => "N/A", //TODO: Find out
                    "OPTIONLINE" => "", //TODO: Find out
                    "BILLOFMATERIALS" => "", //TODO: Find out
                ]
            ];

            $chargeIndex++;

        }



        return $cutChargeOrderLine;
    }


}
