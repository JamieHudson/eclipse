<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:03
 */

namespace Hudson\EclipseOrderApi\Api;


interface OrderInterface
{


    /**
     * Returns latest NOT EXPORTED orders.
     * Marks them as exported (can be changed)
     * @api
     * @return mixed
     */
    public function getLatestOrders();

    /**
     * Returns all orders after specific datetime
     * Does not mark them as exported (can be changed)
     * @param string $datetime
     * @return mixed
     */
    public function getOrdersAfter($datetime);

    /**
     * Marks the orders with these INCREMENT_ID values as exported
     * @param mixed $orderIds
     * @return boolean
     */
    public function confirmOrders($orderIds);

}