<?php
namespace Hudson\EclipseOrderApi\Setup;
 
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetup;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Sales\Model\Order;
  
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    const CONFIRMED_FIELD = "export_confirmed";
    
    /** @var SalesSetupFactory */
    private $salesSetupFactory;

    /**
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        SalesSetupFactory $salesSetupFactory
    ) {
        $this->salesSetupFactory = $salesSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        /** @var SalesSetup $salesSetup */
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);

        // Order attribute to store exported flag
        $salesSetup->addAttribute(Order::ENTITY, self::CONFIRMED_FIELD, [
            'type' => 'boolean',
            'label' => 'Export Confirmed',
            'input' => 'text',
            'required' => true,
            'default' => false,
            'visible' => false,
            'user_defined' => true,
            'position' => 999,
            'system' => 0,
        ]);
    }
}   