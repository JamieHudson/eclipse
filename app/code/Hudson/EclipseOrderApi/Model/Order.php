<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 08/01/2018
 * Time: 10:23
 */

namespace Hudson\EclipseOrderApi\Model;


use Hudson\EclipseOrderApi\Api\OrderInterface;
use Hudson\EclipseOrderApi\Helper\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use Magento\Framework\Webapi\Exception;


class Order implements OrderInterface
{

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Hudson\EclipseOrderApi\Helper\ExportFormatter
     */
    protected $exportFormatter;

    /**
     * @var DateFormatTool
     */
    protected $datetimeFormatTool;

    /**
     * @var Logger
     */

    protected $logger;

    public function __construct(DateFormatTool $dateFormatTool,
                                OrderCollectionFactory $orderCollectionFactory,
                                Logger $logger,
                                \Hudson\EclipseOrderApi\Helper\ExportFormatter $exportFormatter)
    {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->exportFormatter = $exportFormatter;
        $this->datetimeFormatTool = $dateFormatTool;
        $this->logger= $logger;
    }


    /**
     * @api
     * @return mixed
     */
    public function getLatestOrders()
    {
        $orders = $this->orderCollectionFactory->create();

        $orders->addFieldToFilter(\Hudson\EclipseOrderApi\Setup\InstallData::CONFIRMED_FIELD, ["eq" => false]);
        $orders->addFieldToFilter('status', ["neq" => 'gp_pending']); // smcn

        $formattedOrders = $this->exportFormatter->formatOrdersForExport($orders);

        //TODO: Discuss this with Eclipse
//        $ids = $orders->getAllIds();
//        $this->confirmOrders($ids);

        return $formattedOrders;
    }

    /**
     * @api
     * @param string $datetime
     * @return mixed
     * @throws Exception
     */
    public function getOrdersAfter($datetime)
    {

        $formattedDateTime = $datetime;

        try {
            $formattedDateTime = $this->datetimeFormatTool->formatDate($datetime, true);
        } catch (\Exception $e) {
            throw new Exception(__("Couldn't fetch orders. Invalid datetime provided."), 400, Exception::HTTP_BAD_REQUEST);
        }

        $orders = $this->orderCollectionFactory->create();
        $orders->addFieldToFilter("created_at", ["gt" => $formattedDateTime]);

        $formattedOrders = $this->exportFormatter->formatOrdersForExport($orders);

        //TODO: Discuss this with Eclipse
        //$ids = $orders->getAllIds();
        //$this->confirmOrders($ids);

        return $formattedOrders;
    }

    /**
     * @api
     * @param array $orderIds
     * @return bool
     * @throws Exception
     */
    public function confirmOrders($orderIds)
    {
        if (!$this->_checkIfIntegers($orderIds)) {
            throw new Exception(__("Could not confirm orders as exported. Array contains non-integer values."), 400, Exception::HTTP_BAD_REQUEST);
        }

        $orders = $this->orderCollectionFactory->create();
        $orders->addFieldToFilter("increment_id", ["in" => $orderIds]);
        $orders->load();

        foreach ($orders as $order){
            $order->setExportConfirmed(true);
            $order->save();

            $orderId = $order->getIncrementId();
            $this->logger->log("Order $orderId - successful export confirmed!");
        }

        return true;
    }

    /**
     * Checks if all array elements are integers
     * @param $ids
     * @return boolean
     */
    protected function _checkIfIntegers($ids)
    {
        foreach ($ids as $id) {
            //Check if is integer
            if(!is_numeric($id)){
                return false;
            }
        }
        return true;
    }

}
