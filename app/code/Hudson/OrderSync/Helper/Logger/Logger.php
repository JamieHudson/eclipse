<?php

namespace Hudson\OrderSync\Helper\Logger;

class Logger
{

    const HUDSON_API_INFO = 'stock_api_info';
    const HUDSON_API_EXCEPTION = 'stock_api_exception';

    public function log($msg, $type = self::HUDSON_API_INFO)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hudson_'.$type.'.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }

}