<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 08/01/2018
 * Time: 10:23
 */

namespace Hudson\OrderSync\Model;

use Hudson\OrderSync\Api\UpdateOrderInterface;
use Hudson\OrderSync\Model\MassApiHelper;
use \Magento\Framework\Stdlib\DateTime as DateFormatTool;
use Magento\Framework\Webapi\Exception;
use \Magento\Sales\Api\OrderRepositoryInterface;

class UpdateOrder extends MassApiHelper implements UpdateOrderInterface
{

    //Model Specific
    const REQUIRED_FIELDS = ['MagentoOrder', 'OrderStatus', 'StatusDesc'];
    const NUMERIC_FIELDS = [];
    const DATE_FIELDS = [];


    const WAREHOUSE_ID = 1; //Default warehouse

    protected $productCollectionFactory;

    private $warehouseItemFactory;

    private $backendAuthSession;
    private $stockMovementFactory;
    protected $stockState;
    protected $stockRegistry;
    protected $orderRepository;
    protected $searchCriteriaBuilder;

    public function __construct(DateFormatTool $dateFormatTool, \Hudson\OrderSync\Helper\Logger\Logger $hudsonLogger, \Magento\Backend\Model\Auth\Session $backendAuthSession, OrderRepositoryInterface $orderRepository, \Magento\Sales\Api\Data\OrderInterface $orderInterface)
    {
        parent::__construct($hudsonLogger, $dateFormatTool);
        $this->backendAuthSession = $backendAuthSession;
        $this->_orderRepository = $orderRepository;
        $this->_orderInterface = $orderInterface;
    }

    /**
     * @inheritdoc
     */
    public function updateOrder($records)
    {

        $errors = $this->validateRecords($records, self::REQUIRED_FIELDS, self::NUMERIC_FIELDS, self::DATE_FIELDS);

        if (true !== $errors) {
            //Logging errors for records
            $this->logValidationErrors($errors, get_class($this), __FUNCTION__);

            throw new Exception(__("Errors detected on validation. No records are processed."), 400, Exception::HTTP_BAD_REQUEST, $errors);
        }

        //ACtually set the stocks
        $result = $this->updateOrders($records);

        //Return the success
        if ($result === true) {
            return $result;
        }

         //Logging errors for records
         $this->logProcessingErrors($result, get_class($this), __FUNCTION__);

         throw new Exception(__("Errors detected when processing. Previous records are processed."), 400, Exception::HTTP_BAD_REQUEST, [$result]);
    }


    protected function updateOrders($records)
    {

        foreach ($records as $index => $record) {
            try {
            // SMCN HUDSON CONSIGNMENT NUMBER WILL GO IN HERE

                $orderCode = preg_replace('/\bWEB:/', '', $record['MagentoOrder']);
                $statusCode = 'code_' . $record['OrderStatus'];
                // obtain the order with the order ID

                $order = $this->_orderRepository->get($orderCode);
                $order = $this->_orderInterface->loadByIncrementId($orderCode);

                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/smcn.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $logger->info(print_r($record, true));

                if (array_key_exists('consignment_number', $record)) {
			$order->setConsignmentNumber($record['consignment_number']);

                    $order->save();
                }


                if ($order->getStatus()) {
                    $order->setStatus($statusCode);

                    $order->addStatusToHistory($order->getStatus(), 'Order status changed to: ' . $record['StatusDesc']);
                    $this->_orderRepository->save($order);
                    $this->hudsonLogger->log("Order: " . $orderCode . " status changed to " . $record['StatusDesc'] . "(" . $statusCode . ")");
                } else {
                    $this->hudsonLogger->log("Order: " . $orderCode . " does not exist, skipping...");
                }
            } catch (\Exception $e) {
                return "Execution stopped: ". $e->getMessage(). " - At record $index (index) OBJECT: ".implode(" , ", $record);
            }
        }

        return true;
    }
}
