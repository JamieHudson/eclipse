<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 12/12/2017
 * Time: 15:03
 */

namespace Hudson\OrderSync\Api;


interface UpdateOrderInterface
{


    /**
     * @api
     * @param mixed $records
     * @return mixed
     */
    public function updateOrder($records);

}