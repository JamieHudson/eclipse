<?php

namespace Hudson\ExtendFees\Helper;

class Data extends \Anowave\Fee\Helper\Data
{
    
    	/**
	 * Create a calculation closure 
	 * 
	 * @param \Magento\Quote\Model\Quote $quote
	 */
	public function calculator(\Magento\Quote\Model\Quote $quote = null)
	{
		$type = $this->getConfig('fee/fee/type');

		
		$this->validateConditions($quote);
		
		/**
		 * Get fee
		 *
		 * @var (float)
		 */
		$fee = (float) $this->getFeeAmount();
		
		/**
		 * Calculate fee per product
		 */

	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/jm.log');
	$logger = new \Zend\Log\Logger();
	$logger->addWriter($writer);
	$logger->info("message");	
                
		if (1 === (int) $this->getConfig('fee/fee/calculate_per_product'))
		{
			$attribute = $this->getConfig('fee/fee/product_fee_attribute');
			
                        $skus = []; // Set skus array
			foreach ($quote->getAllVisibleItems() as $item)
			{
                            
                            // get product sku
                            $sku = $item->getProduct()->getSku();
                            // If sku is already there skip this product else add it and proceed. 
                            if(in_array($sku, $skus)){
                                continue;
                            } else {
                                $skus[] = $sku;
                            }
                            
                            
				if (\Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE == $item->getProduct()->getTypeId())
				{
					if ($option = $item->getOptionByCode('simple_product')) 
					{
						if ($option->getProduct())
						{
							$product = $this->productFactory->create()->load
							(
								$option->getProduct()->getId()
							);
						}
						else 
						{
							$product = $this->productFactory->create()->load
							(
								$item->getProduct()->getId()
							);
						}
					}
					else 
					{
						$product = $this->productFactory->create()->load
						(
							$item->getProduct()->getId()
						);
					}
				}
				else 
				{
					$product = $this->productFactory->create()->load
					(
						$item->getProduct()->getId()
					);
				}

				if (true)
				{
					/**
					 * Read product fee
					 * 
					 * @var float
					 */
					$product_fee = (float) $product->getData($attribute);
					
					if (!$this->validateConditionsEntity($quote, $product))
					{
						$product_fee = 0;
					}

					/**
					 * Check if product falls into particular condition
					 */
					$use_quantity = (int) $this->getConfig('fee/fee/calculate_quantity');
					
					if ($product_fee)
					{
						switch($type)
						{
							case self::FEE_TYPE_FIXED_PRODUCT:
								$fee += ($product_fee * ($use_quantity ? $item->getQty() : 1));
								break;
							case self::FEE_TYPE_PERCENTAGE_PRODUCT: 
								
								$fee_percent = ($product_fee * $item->getPrice())/100;
								
								$fee += ($fee_percent * ($use_quantity ? $item->getQty() : 1));
								break;
							default: 
								$fee += ($product_fee * ($use_quantity ? $item->getQty() : 1));
								break;
						}
					}
					else 
					{
						/**
						 * Collect category fees
						 * 
						 * @var []
						 */
						$categories = [];
						
						foreach ($product->getCategoryIds() as $id)
						{
							$category = $this->categoryFactory->create()->load($id);
							
							$categories[(int) $category->getId()] = (float) $category->getData(self::FEE_ATTRIBUTE);
						}
						
						
						if (\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE == $product->getTypeId() || \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL)
						{
							$parents = $this->configurableType->getParentIdsByChild
							(
								$product->getId()
							);
							
							if ($parents)
							{
								foreach ($parents as $identifier)
								{
									$parent = $this->productFactory->create()->load($identifier);
									
									foreach ($parent->getCategoryIds() as $parentCategoryId)
									{
										$category = $this->categoryFactory->create()->load($parentCategoryId);
										
										$categories[(int) $category->getId()] = (float) $category->getData(self::FEE_ATTRIBUTE);
									}
								}
							}
						}
						
						foreach ($categories as $category_fee)
						{
							switch ($type)
							{
								case self::FEE_TYPE_FIXED_CATEGORY:
							
									$fee += $category_fee;
							
									break;
								case self::FEE_TYPE_PERCENTAGE_CATEGORY:
							
									$fee_percent = ($category_fee * $item->getPrice())/100;
									
									$fee += ($fee_percent * ($use_quantity ? $item->getQty() : 1));
							
									break;
							}
						}
					}
				}
			}
			
			$fee += $this->getCombinedFees($quote);
			
			return function(\Magento\Quote\Model\Quote $quote = null) use ($fee)
			{
				return $fee;
			};
		}
		else 
		{
			if (!$this->validateConditions($quote))
			{
				return function(\Magento\Quote\Model\Quote $quote = null) use ($fee)
				{
					return 0;
				};
			}
			
			switch($type)
			{
				case self::FEE_TYPE_PERCENTAGE: 
				{
					$fee += $this->getCombinedFees($quote);
					
					return function(\Magento\Quote\Model\Quote $quote = null) use ($fee)
					{
						$total = (float) $quote->getBaseSubtotal();
					
						if (false)
						{
							if ($quote->getShippingAddress())
							{
								$total += $quote->getShippingAddress()->getBaseShippingAmount();
							}
						}
	
						return ($total * $fee)/100;
					};
				}
				case self::FEE_TYPE_ONCE_PER_CATEGORY:
					
					$once = [];
					
					foreach ($quote->getAllVisibleItems() as $item)
					{
						$product = $this->productFactory->create()->load($item->getProduct()->getId());
						
						/**
						 * Collect category fees
						 *
						 * @var []
						 */
						$categories = [];
						
						foreach ($product->getCategoryIds() as $id)
						{
							$category = $this->categoryFactory->create()->load($id);
								
							$categories[(int) $category->getId()] = (float) $category->getData(self::FEE_ATTRIBUTE);
						}

						foreach ($categories as $category_id => $category_fee)
						{
							$once[$category_id] = $category_fee;
						}
					}
					
					$fee += $this->getCombinedFees($quote);

					return function(\Magento\Quote\Model\Quote $quote = null) use ($fee, $once)
					{
						return $fee + array_sum($once);
					};
					
					break;
				case self::FEE_TYPE_ONCE_PER_CATEGORY_P:
					
					$once = [];
						
					foreach ($quote->getAllVisibleItems() as $item)
					{
						$product = $this->productFactory->create()->load($item->getProduct()->getId());
					
						/**
						 * Collect category fees
						 *
						 * @var []
						*/
						$categories = [];
					
						foreach ($product->getCategoryIds() as $id)
						{
							$category = $this->categoryFactory->create()->load($id);
					
							$categories[(int) $category->getId()] = (float) $category->getData(self::FEE_ATTRIBUTE);
						}
					
						foreach ($categories as $category_id => $category_fee)
						{
							$fee_percent = ($category_fee * $item->getPrice())/100;
							
							$once[$category_id] = $fee_percent;
						}
						
						$fee += $this->getCombinedFees($quote);
					
						return function(\Magento\Quote\Model\Quote $quote = null) use ($fee, $once)
						{
							return $fee + array_sum($once);
						};
					}
					
					break;
				case self::FEE_TYPE_ONCE_PER_CATEGORY_PQ:
							
						$once = [];
					
						foreach ($quote->getAllVisibleItems() as $item)
						{
							$product = $this->productFactory->create()->load($item->getProduct()->getId());
								
							/**
							 * Collect category fees
							 *
							 * @var []
							*/
							$categories = [];
								
							foreach ($product->getCategoryIds() as $id)
							{
								$category = $this->categoryFactory->create()->load($id);
									
								$categories[(int) $category->getId()] = (float) $category->getData(self::FEE_ATTRIBUTE);
							}
								
							foreach ($categories as $category_id => $category_fee)
							{
								$fee_percent = ($category_fee * $item->getPrice())/100;
									
								$once[$category_id] = $fee_percent * $item->getQty();
							}
							
							$fee += $this->getCombinedFees($quote);
								
							return function(\Magento\Quote\Model\Quote $quote = null) use ($fee, $once)
							{
								return $fee + array_sum($once);
							};
						}
							
					break;

				default: 
				{
					$fee += $this->getCombinedFees($quote);
					
					return function(\Magento\Quote\Model\Quote $quote = null) use ($fee)
					{
						return $fee;
					};
				}
			}
		}
	}
}
