<?php
namespace Hudson\PaymentGroupCustomerAttribute\Model\Attribute\Source;

class PaymentGroup extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{

    const NO_GROUP  = '-';
    const PROFORMA_GROUP  = 'Proforma';
    const CREDIT_ACCOUNT_GROUP  = 'Credit Account';

    /**
     * @var \Magento\Framework\Convert\DataObject
     */
    protected $_converter;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \Magento\Framework\Convert\DataObject $converter
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \Magento\Framework\Convert\DataObject $converter
    ) {
        $this->_converter = $converter;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options[] = ['value' => self::NO_GROUP, 'label' => self::NO_GROUP]; 
        $this->_options[] = ['value' => self::PROFORMA_GROUP, 'label' => self::PROFORMA_GROUP]; 
        $this->_options[] = ['value' => self::CREDIT_ACCOUNT_GROUP, 'label' => self::CREDIT_ACCOUNT_GROUP]; 

        return $this->_options;
    }
}
