<?php

namespace Hudson\UpdateOutOfStock\Cron;


use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\LocalizedException;
use Hudson\UpdateOutOfStock\Logger;



class UpdateOutOfStock
{
    /**
     * @var ProductCollectionFactory
     */
    private $productCollection;
    protected $stockState;
    protected $logger;
    protected $stockRegistry;

    /**
     * UpdateOutOfStock constructor.
     * @param ProductCollectionFactory $productCollection
     */
    public function __construct(\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection, \Magento\CatalogInventory\Api\StockStateInterface $stockState, \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, \Hudson\UpdateOutOfStock\Logger\Logger $logger) {
        $this->productCollection = $productCollection;
        $this->stockState = $stockState;
        $this->stockRegistry = $stockRegistry;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->info('####################');
        $this->logger->info('Update Out of Stock');
        $this->logger->info('------running-------');
        $this->logger->info('####################');
        $collection = $this->productCollection->create();
        
        $collection->addAttributeToSelect('*')->setPageSize(100);
        
        $lastPage = $collection->getLastPageNumber();
        for($i = 1; $i <= $lastPage;$i++){
            $collection->setCurPage($i);
            $collection->load();


            foreach($collection as $product){
                try{
                    $packsize = $product->getCustomAttribute('packsize');
                    if($packsize){
                        if($packsize->getValue()){
                            //packsize value found, now we are checking if packsize is more than quantity
                            $productStock = $this->stockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
                            if(intVal($packsize->getValue())){
                                if(intVal($packsize->getValue()) > 1){
                                    if($productStock < $packsize->getValue()){
                                        $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                                        if($stockItem->getIsInStock()){
                                            $stockItem->setIsInStock(false);
                                            $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                                            $this->logger->info('changed product ID: ' . $product->getId() . ' To out of stock.');
                                        }
                                    }elseif($productStock >= $packsize->getValue()){
                                        
                                    }
                                }
                            }
                        }
                    }



                } catch (AuthenticationException $e) {
                    throw new \Exception($e->getMessage());
                } catch (LocalizedException $e) {
                    $this->logger->critical($e->getMessage());
                }
                
            }

            $collection->clear();

        }
        $this->logger->info('####################');
        $this->logger->info('Update Out of Stock');
        $this->logger->info('------stopped-------');
        $this->logger->info('####################');
        return $this;
    }
}