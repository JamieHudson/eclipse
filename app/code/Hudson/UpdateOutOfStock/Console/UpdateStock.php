<?php 

namespace Hudson\UpdateOutOfStock\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

Class UpdateStock extends Command
{

    public function __construct(
        \Magento\Framework\App\State $appState,
        \Hudson\UpdateOutOfStock\Cron\UpdateOutOfStock $cronService
    ){
        $this->appState = $appState;
        $this->cronService = $cronService;
        parent::__construct();
    }

    Protected function configure()
    {
        $this->setName('hudson:updatestock');
        $this->setDescription('Manual method to preform pack size vs quantity checks.');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('executing command...');
        $this->appState->emulateAreaCode(
            \Magento\Framework\App\Area::AREA_CRONTAB, function(){
                $this->cronService->execute();
            }
        );
        //$this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_CRONTAB);

        


        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}