<?php

namespace Hudson\AddSummaryAttribute\Plugin;

class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{

    protected $scopeConfig;

    public function __construct(\Magento\Framework\Model\Context $context,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Registry $registry,
                                \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
                                \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
                                array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->scopeConfig = $scopeConfig;
    }

    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {

        $items = $result['totalsData']['items'];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        for($i=0;$i<count($items);$i++){

            $quoteId = $items[$i]['item_id'];
            $quote = $objectManager->create('\Magento\Quote\Model\Quote\Item')->load($quoteId);
            $productId = $quote->getProductId();
            $product = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);

            $attributesToShow = $this->getAttributesToShow();

            foreach ($attributesToShow as $attributeCode) {

                $attributeEntity = $product->getResource()->getAttribute($attributeCode);

                $attributeValue = $product->getData($attributeCode);

                if ($attributeEntity->usesSource()) {
                    $attributeValue = $attributeEntity->getFrontend()->getValue($product);
                }

                $items[$i][$attributeCode] = $attributeValue;

            }
        }
        $result['totalsData']['items'] = $items;
        return $result;
    }

    /**
     * @return array
     */
    private function getAttributesToShow(){

        $codes = $this->scopeConfig->getValue('hudsonsummary/general/attributes');

        if($codes == "") return [];

        return explode(",",$codes);
    }

}