<?php
/**
 * Created by PhpStorm.
 * User: Vassil
 * Date: 19/12/2017
 * Time: 12:27
 */

namespace Hudson\GroupedProduct\Block\Product\View;

use Exception;
use Magento\Framework\Pricing\SaleableInterface;
use \Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use \Magento\Framework\App\Http\Context;


class Backorder extends Template
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;


    protected $stockStateInterface;


    /**
     * Construct
     *
     * @param Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        array $data = []
    )
    {
        $this->registry = $registry;
        parent::__construct($context, $data);
        $this->stockStateInterface = $stockStateInterface;

    }


    public function getBackorderHtml()
    {



        $product = $this->getProduct();

        $result = "";

        if(!$product) return $result;

        $result = $this->getBackorderMessage($product);

        return $result;
    }


    /**
     * Returns saleable item instance
     *
     * @return Product
     */
    protected function getProduct()
    {
        $parentBlock = $this->getParentBlock();

        $product = $parentBlock && $parentBlock->getProductItem()
            ? $parentBlock->getProductItem()
            : $this->registry->registry('product');
        return $product;
    }


    protected function replacePriceInHtml($isInStock,$product)
    {


        if($isInStock){
            return "";
        }

        if($product->isSalable()){
            $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/stockmessage');
        }else{
            $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/discontinuedmessage');
        }


        $newValue = "<div class='out-of-stock-message'>$oosMessage</div>";

	// SMCN
// 	$restockDate = $product->getData('restock_date');
// 	if ($restockDate && strtotime($restockDate) > strtotime('yesterday')) {
// 		$newValue .= "<div>Restock Date: ".date('d M Y', strtotime($restockDate))."</div>";
// 	}
	// SMCN

        return $newValue;
    }


    public function getBackorderMessage($product){

        $productId = $product->getId();

        $type = $product->getTypeId();

        if($type == "grouped") return "";


        try {
            $stockQty = $this->stockStateInterface->getStockQty($productId);
        }catch (\Exception $e){
            return $e->getMessage();
        }


        $inStock = true;


        if ($stockQty <= 0) $inStock = false;


        return $this->replacePriceInHtml($inStock,$product);

    }

}
