<?php
/**
 * Catalog grouped product info block
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Hudson\GroupedProduct\Block\Product\View\Type;

class Grouped extends \Magento\Catalog\Block\Product\View\AbstractView
{
    protected $attributeRepository;

    protected $_productRepo;

    protected $stockStateInterface;



    public function __construct(\Magento\Catalog\Block\Product\Context $context,
                                \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
                                \Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository,
                                \Magento\Catalog\Model\ProductRepository $productRepo,
                                \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
                                array $data = [])
    {
        $this->_productRepo = $productRepo;
        $this->attributeRepository = $attributeRepository;
        parent::__construct($context, $arrayUtils, $data);
        $this->stockStateInterface = $stockStateInterface;
    }


    public function getPriceForProductWithDiscount($item){

        return $this->getProductPrice($item);

    }
    /**
     * @return array
     */
    public function getAssociatedProducts()
    {
        return $this->getProduct()->getTypeInstance()->getAssociatedProducts($this->getProduct());
    }

    /**
     * Set preconfigured values to grouped associated products
     *
     * @return $this
     */
    public function setPreconfiguredValue()
    {
        $configValues = $this->getProduct()->getPreconfiguredValues()->getSuperGroup();
        if (is_array($configValues)) {
            $associatedProducts = $this->getAssociatedProducts();
            foreach ($associatedProducts as $item) {
                if (isset($configValues[$item->getId()])) {
                    $item->setQty($configValues[$item->getId()]);
                }
            }
        }
        return $this;
    }

    public function getAttributesToShow(){
        $attributeString = $this->_scopeConfig->getValue('hudsongroupedproduct/general/attributeset');

        if(!$attributeString) return false;

        $attributeCodes = explode(',', $attributeString);

        $attributes = array_fill_keys($attributeCodes,"");

            foreach ($attributeCodes as $attributeCode) {
                $attribute = $this->attributeRepository->get($attributeCode);
                $attributeName = $attribute->getDefaultFrontendLabel();
                $attributes[$attributeCode] = $attributeName;
            }

        return $attributes;
    }

    public function getIdAttribute(){
        $attributeCode = $this->_scopeConfig->getValue('hudsongroupedproduct/general/idattribute');

        if(!$attributeCode) return false;


        $attribute = $this->attributeRepository->get($attributeCode);
        $attributeName = $attribute->getDefaultFrontendLabel();
        $attributeArray["code"] = $attributeCode;
        $attributeArray["name"] = $attributeName;


        return $attributeArray;
    }

    public function isGroupedModuleEnabled(){
        return $this->_scopeConfig->getValue('hudsongroupedproduct/general/enabled');
    }

    public function getProductById($id){
        return $this->_productRepo->getById($id);
    }

    public function getBackorderMessage($product){

        $productId = $product->getId();

        try {
            $stockQty = $this->stockStateInterface->getStockQty($productId);
        }catch (\Exception $e){
            return $e->getMessage();
        }


        $inStock = true;


        if ($stockQty <= 0) $inStock = false;


        return $this->replacePriceInHtml($inStock,$product);

    }

    protected function replacePriceInHtml($isInStock,$product)
    {

        $SoonToBeDiscontinued = 'testing!!';

        if($isInStock){
            return "";
        }

        if($product->isSalable()){
            $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/stockmessage');
        }else{
            $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/discontinuedmessage');
        }


        //$newValue = "<div class='out-of-stock-message'>$oosMessage</div>";
        $newValue = "<div class='out-of-stock-message'>$SoonToBeDiscontinued</div>";


        return $newValue;
    }
}
