<?php

namespace Hudson\GroupedProduct\Block\Product\View\Type;

use Magento\Catalog\Api\ProductRepositoryInterface;

class BssGrouped extends \Magento\GroupedProduct\Block\Product\View\Type\Grouped
{
    /**
     * Bss grouped product option helper.
     *
     * @var \Bss\GroupedProductOption\Helper\Data
     */
    private $helperBss;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;


    protected $attributeRepository;

    protected $stockStateInterface;



    /**
     * Initialize dependencies.
     *
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Bss\GroupedProductOption\Helper\Data $helperBss
     * @param ProductRepositoryInterface $productRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Bss\GroupedProductOption\Helper\Data $helperBss,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $arrayUtils,
            $data
        );

        $this->helperBss = $helperBss;
        $this->productRepository = $productRepository;
        $this->attributeRepository = $attributeRepository;
        $this->stockStateInterface = $stockStateInterface;


    }

    public function getPriceForProductWithDiscount($item)
    {

        return $this->getProductPrice($item);

    }


    /**
     * Render product html.
     *
     * @param \Magento\Catalog\Model\Product $item
     * @return bool|string
     */
    public function renderBlockProduct($item)
    {
        $html = '';
        $product = $this->getProductInfo($item);
        $typeProduct = $product->getTypeId();
        if ($typeProduct == \Bss\GroupedProductOption\Helper\Data::PRODUCT_TYPE_CONFIGURABLE) {
            $block = $this->_addConfigurableBlock($product);
        }

        if (isset($block)) {
            $html .= '<div class="bss-gpo-configurable-info">' .
                $block->toHtml() .
                '</div>';
        }

        $customOption = $this->_addCustomOption($product);
        if (isset($customOption)) {
            $html .= '<div class="bss-gpo-custom-option" data-product-id="' . $product->getId() . '">'
                . '<div class="fieldset">'
                . $customOption->toHtml()
                . '</div></div>';
        }

        if (isset($html) && $html != '') {
            return $html;
        }

        return false;
    }

    /**
     * Get JSON encoded configuration array which can be used for JS dynamic
     * price calculation depending on product options
     *
     * @return string
     */
    public function getJsonConfig()
    {
        return $this->_layout->createBlock('\Magento\Catalog\Block\Product\View')->getJsonConfig();
    }

    /**
     * Add configurable block to layout.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    protected function _addConfigurableBlock($product)
    {
        $layout = $this->_layout->createBlock('Magento\Swatches\Block\Product\Renderer\Configurable')
            ->setProduct($product);

        if (isset($layout)) {
            return $layout;
        }

        return false;
    }

    /**
     * Add custom option block to layout.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    protected function _addCustomOption($product)
    {
        $layout = $this->_layout->getBlock('bss.gpo.product.info.options')->setProduct($product);

        if (isset($layout)) {
            return $layout;
        }

        return false;
    }

    /**
     * Get loaded product.
     *
     * @param \Magento\Catalog\Model\Product $item
     * @return \Magento\Catalog\Api\ProductRepositoryInterface
     */
    public function getProductInfo($item)
    {
        return $this->productRepository->getById($item->getId());
    }

    /**
     * Get grouped info html.
     *
     * @param \Magento\Catalog\Model\Product $item
     * @return string
     */
    public function getProductInfoGpo($item)
    {
        $html = '';
        $product = $this->getProductInfo($item);
        if ($this->helperBss->getConfig('show_link') && $product->getVisibility() != 1) {
            $html .= '<a href = "' . $product->getProductUrl() . '" class="bss-gpo-img">';
        }

        $html .= '<img src="' . $this->getProductImage($product) . '"  alt=' . $product->getName() . ' />';
        if ($this->helperBss->getConfig('show_link') && $product->getVisibility() != 1) {
            $html .= '</a>';
        }

        return $html;
    }

    /**
     * Get product image.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool|string
     */
    protected function getProductImage($product)
    {
        $imageSize = 135;
        $productImage = $this->_imageHelper->init(
            $product,
            'category_page_list',
            ['height' => $imageSize, 'width' => $imageSize]
        )->getUrl();

        if (!$productImage) {
            return false;
        }

        return $productImage;
    }

    /**
     * Get Bss grouped product option helper.
     *
     * @return \Bss\GroupedProductOption\Helper\Data
     */
    public function getBssHelper()
    {
        return $this->helperBss;
    }

    public function getAttributesToShow()
    {
        $attributeString = $this->_scopeConfig->getValue('hudsongroupedproduct/general/attributeset');

        if (!$attributeString) return false;

        $attributeCodes = explode(',', $attributeString);

        $attributes = array_fill_keys($attributeCodes, "");

        foreach ($attributeCodes as $attributeCode) {
            $attribute = $this->attributeRepository->get($attributeCode);
            $attributeName = $attribute->getDefaultFrontendLabel();
            $attributes[$attributeCode] = $attributeName;
        }

        return $attributes;
    }

    public function getIdAttribute()
    {
        $attributeCode = $this->_scopeConfig->getValue('hudsongroupedproduct/general/idattribute');

        if (!$attributeCode) return false;


        $attribute = $this->attributeRepository->get($attributeCode);
        $attributeName = $attribute->getDefaultFrontendLabel();
        $attributeArray["code"] = $attributeCode;
        $attributeArray["name"] = $attributeName;

        return $attributeArray;
    }

    public function isGroupedModuleEnabled()
    {
        return $this->_scopeConfig->getValue('hudsongroupedproduct/general/enabled');
    }

    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    /**
     * Override and reorder products showing ones with custom options at the end
     * @return array
     */
    public function getAssociatedProducts()
    {
        $collection = $this->getProduct()->getTypeInstance()->getAssociatedProducts($this->getProduct());

//        /* @var $collection \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection */


        $withOptions = [];

        foreach ($collection as $key => $item) {

            $product = $this->getProductById($item->getId());
            $options = $product->getOptions();

            if (sizeof($options) != 0) {
                unset($collection[$key]);
                array_push($withOptions, $item);
            }

        }

//        $collection->setOrder("name",\Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection::SORT_ORDER_DESC);

        $collection = array_merge($collection, $withOptions);


        return $collection;

    }

    public function getBackorderMessage($product){

        $productId = $product->getId();

        try {
            $stockQty = $this->stockStateInterface->getStockQty($productId);
        }catch (\Exception $e){
            return $e->getMessage();
        }


        $inStock = true;


        if ($stockQty <= 0) $inStock = false;


        return $this->replacePriceInHtml($inStock,$product);

    }

    public function canBeSold($product) {
        $product = $this->productRepository->get($product->getSku());

        $poa = $product->getPoa();
        $soontobediscontinued = $product->getSoontobediscontinued();
        $stockQty = $this->stockStateInterface->getStockQty($product->getId());
        $packsize = $product->getCustomAttribute('packsize');
        if (isset($packsize)) {
            $packsize = $packsize->getValue();
        } else {
            $packsize = 0;
        }

        if ($poa || ($soontobediscontinued && $packsize > $stockQty)) {
            return false;
        } else {
            return true;
        }
    }

    protected function replacePriceInHtml($isInStock,$product)
    {
	$product = $this->productRepository->get($product->getSku());

        $SoonToBeDiscontinued = $product->getData('soontobediscontinued');

	// SMCN
	$packsize = $product->getCustomAttribute('packsize');
	if (isset($packsize)) {
		$packsize = $packsize->getValue();
	} else {
		$packsize = 1;
	}
	$stockQty = $this->stockStateInterface->getStockQty($product->getId());

        //if($isInStock){
        if($isInStock && $stockQty >= $packsize && $SoonToBeDiscontinued == '0'){
	// SMCN
            return "";
        }

        if($product->isSalable()){
            if($SoonToBeDiscontinued == '1'){
                $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/discontinuedmessage');
            }else{
                $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/stockmessage');
            }
            
        }else{
            $oosMessage = $this->_scopeConfig->getValue('hudsongroupedproduct/general/discontinuedmessage');
        }

        $newValue = "<div class='out-of-stock-message'>$oosMessage</div>";
        //$newValue = "<div class='out-of-stock-message'>$SoonToBeDiscontinued</div>";

	// SMCN
	$restockDate = $product->getData('restock_date');
	if ($restockDate && strtotime($restockDate) > strtotime('yesterday')) {
		$newValue .= "<div>Restock Date: ".date('d M Y', strtotime($restockDate))."</div>";
	}
	// SMCN

        return $newValue;
    }
}
