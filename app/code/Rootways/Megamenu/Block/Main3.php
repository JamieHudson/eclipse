<?php

/**

 * Mega Menu HTML Block.

 *

 * @category  Site Search & Navigation

 * @package   Root_Mega_Menu

 * @author    Developer RootwaysInc <developer@rootways.com>

 * @copyright 2017 Rootways Inc. (https://www.rootways.com)

 * @license   Rootways Custom License

 * @link      https://www.rootways.com/shop/media/extension_doc/license_agreement.pdf

*/

namespace Rootways\Megamenu\Block;

class Main3 extends \Magento\Framework\View\Element\Template

{

     protected $_categoryHelper;

     protected $categoryFlatConfig;

     protected $topMenu;

     protected $_filterProvider;

     protected $_helper;

     protected $resourceConfig;

    /**

     * Inherits other blocks

     */

    public function __construct(

        \Magento\Framework\View\Element\Template\Context $context,

        \Magento\Catalog\Helper\Category $categoryHelper,

        \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState,

        \Magento\Theme\Block\Html\Topmenu $topMenu,

        \Magento\Cms\Model\Template\FilterProvider $filterProvider,

        \Rootways\Megamenu\Helper\Data $helper,

        \Magento\Config\Model\ResourceModel\Config $resourceConfig

    ) {

        $this->_categoryHelper = $categoryHelper;

        $this->categoryFlatConfig = $categoryFlatState;

        $this->topMenu = $topMenu;

        $this->_filterProvider = $filterProvider;

        $this->_customhelper = $helper;

        $this->_customresourceConfig = $resourceConfig;

        parent::__construct($context);

    }

    

    /**

     * Support for get attribute value with HTML 

    */

    public function getBlockContent($content = '') {

        if(!$this->_filterProvider)

            return $content;

        return $this->_filterProvider->getBlockFilter()->filter(trim($content));

    }

   

    /**

     * Check if current page is home

    */  

    public function getIsHomePage()

    {

        $currentUrl = $this->getUrl('', ['_current' => true]);

        $urlRewrite = $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);

        return $currentUrl == $urlRewrite;

    }

    

    /**

     * Return categories helper

    */   

    public function getCategoryHelper()

    {

        return $this->_categoryHelper;

    }

    

    /**

     * Return top menu html

    */   

    public function getHtml()

    {

        return $this->topMenu->getHtml();

    }

    

    /**

     * Retrieve current store categories

    */    

    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)

    {

        return $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);

    }

    

    /**

    * Retrieve child store categories

    */ 

    public function getChildCategories($category)

    {

        $children = [];

        if ($this->categoryFlatConfig->isFlatEnabled() && $category->getUseFlatResource()) {

            $subcategories = (array)$category->getChildrenNodes();

        } else {

            $subcategories = $category->getChildren();

        }

        foreach($subcategories as $category) {

            if (!$category->getIsActive()) {

                continue;

            }

            $children[] = $category;

        }

        return $children; 

    }

    

    /**

     * Simple Mega Menu HTML Block.

    */

    public function simpletMenu($category)

	{

        $categoryHelper = $this->getCategoryHelper();

		$catHtml = '';

	 // 2nd Level Category

        if ($childrenCategories = $this->getChildCategories($category)) {

			$catHtml .= '<ul class="rootmenu-submenu">';

            foreach ($childrenCategories as $childCategory) {

			    $collection_sub = $this->getChildCategories($childCategory);

                if (count($collection_sub)) {

                    $arrow = '<span class="cat-arrow"></span>';

                } else { 

                    $arrow = '';

                }

				$catHtml .= '<li><a href="'.$categoryHelper->getCategoryUrl($childCategory).'">'.$childCategory->getName().$arrow.'</a>';

                    


				$catHtml .= '</li>';

			}

			$catHtml .= '</ul>';

		}

		return $catHtml;

	}   

    

    /**

     * Half-Width Mega Menu HTML Block.

    */

    public function halfMenu($category)

	{

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $catHtml = '';

        // 2nd Level Category

		if ($childrenCategories = $this->getChildCategories($category)) {

			$catHtml .= '<div class="halfmenu clearfix">';

				$catHtml .= '<div class="root-col-1 clearfix">';

						$catHtml .= '<ul class="root-col-2 clearfix level2-popup">';

						$cnt = 1;

                        // 3rd Level Category

						foreach ($childrenCategories as $childCategory) {

							$load_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory->getId());

							

							if ( $main_cat->getMegamenuShowCatimage() == 1 ) {

								if ( $load_cat->getImageUrl() != '' ) {

									$imageurl = $load_cat->getImageUrl(); 

								} else {

									$imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');

								}	

								$image_html = '<img style="width:'.$main_cat->getMegamenuShowCatimageWidth().'px; height:'.$main_cat->getMegamenuShowCatimageHeight().'px;" src='.$imageurl.' alt="'.$load_cat->getName().'"/>';

							} else { 

								$image_html = '<span class="cat-arrow"></span>';

								//$image_html  = '<i aria-hidden="true" class="verticalmenu-arrow fa fa-angle-right"></i>';

							}

							

							$catHtml .= '<li><a href='.$load_cat->getURL().'>';

							$catHtml .= $image_html;

							$catHtml .= '<span class="sub-cat-name" style="height:'.$main_cat->getMegamenuShowCatimageWidth().'px;">'.$load_cat->getName().'</span>';

							
							$catHtml .=  '</li>';

							

							//if($cnt%$brk == 0 ){ $catHtml .= '</ul></div><div class="root-col-'.$colnum.' clearfix"><ul class="sub_cat_listing">';}

							//$cnt ++;

							if ( $cnt%6 == 0 ) { $catHtml .= '</ul><ul  class="root-col-2 clearfix level2-popup">'; }

							$cnt ++;

						}

			

			

			

						

						$catHtml .= '</ul>';

				$catHtml .= '</div>';

			$catHtml .= '</div>';

		}

		return $catHtml;

	}

	

    /**

     * Full-Width Mega Menu HTML Block.

    */

	public function fullWidthLinks($category)

	{

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $catHtml = '';

        

        $colnum = $main_cat->getMegamenuTypeNumofcolumns();

		if ( $colnum == 0 ) {

            $colnum = 4;

        }

        

        // 2nd Level Category

		if ( $childrenCategories = $this->getChildCategories($category) ) {

			$catHtml .= '<div class="megamenu clearfix linksmenu">';

				$catHtml .= '<ul class="root-col-'.$colnum.' clearfix level2-popup">';

				$cnt = 1;

				$cat_tot = count($childrenCategories);

				$brk = ceil($cat_tot/$colnum);

				// 3rd Level Category

				foreach ($childrenCategories as $childCategory) {

					$load_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory->getId());

					

                    if ( $main_cat->getMegamenuShowCatimage() == 1 ) {

                        if ($load_cat->getImage() != '') {

                            $imageurl = $load_cat->getImageUrl();

                        } else {  

                            $imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');

                        }

                        $image_html = '<img style="width:'.$main_cat->getMegamenuShowCatimageWidth().'px; height:'.$main_cat->getMegamenuShowCatimageHeight().'px;" src='.$imageurl.' alt="'.$load_cat->getName().'"/>';

                    } else { 

						$image_html = '<span class="cat-arrow"></span>';

                        //$image_html = '<i aria-hidden="true" class="verticalmenu-arrow fa fa-angle-right"></i>';

                    }

                    

                    $catHtml .= '<li><a href='.$load_cat->getURL().'>';

                    $catHtml .= $image_html;

                    $catHtml .= '<span class="sub-cat-name" style="height:'.$main_cat->getMegamenuShowCatimageHeight().'px;">'.$load_cat->getName();

                    $catHtml .= '</span>';

                   

					 $catHtml .= '</a>';

					


                    $catHtml .=  '</li>';

                    if ($cnt%$brk == 0 ) { $catHtml .= '</ul><ul class="root-col-'.$colnum.' clearfix level2-popup">';}

                    $cnt ++;

                }

				

				$catHtml .= '</ul>';

			$catHtml .= '</div>';

		}

		return $catHtml;

	}

    

    /**

     * Full-Width With Right Side Content Mega Menu HTML Block.

    */

	public function fullWidthSubRight($category)

	{

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $catHtml = '';

        

		$rightcol_type_num_of_col = $main_cat->getMegamenuTypeNumofcolumns();

		if ( $rightcol_type_num_of_col == 0) {

            $rightcol_type_num_of_col =3; 

        }

        // 2nd Level Category

        if ($childrenCategories = $this->getChildCategories($category)) {

			$catHtml .= '<div class="megamenu clearfix categoriesmenu">';

				$catHtml .= '<div class="root-col-75 clearfix">';

					$cat_cnt = 1;

					// 3rd Level Category

                    foreach ($childrenCategories as $childCategory) {

						$catHtml .= '<div class="root-col-'.$rightcol_type_num_of_col.' clearfix">';

                            $load_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory->getId());

							if ( $main_cat->getMegamenuShowCatimage() == 1 ) {

								$imageurl = $load_cat->getImageUrl(); 

								$catHtml .= '<a href='.$load_cat->getURL().' class="catproductimg"><img width='.$main_cat->getMegamenuShowCatimageWidth().' height='.$main_cat->getMegamenuShowCatimageHeight().' src='.$imageurl.' alt="'.$main_cat->getName().'"/></a>'; 

                            }

							

							$catHtml .= '<div class="title"><a href='.$load_cat->getURL().'>'.$load_cat->getName().'</a></div>';

						
						$catHtml .= '</div>';

						if ( $cat_cnt%$rightcol_type_num_of_col==0 ) {

                            $catHtml .= '<div class="clearfix"></div>';

                        }

						$cat_cnt++;

				    }

					$catHtml .= '</div>';

					$catHtml .= '<div class="root-col-4 clearfix rootmegamenu_block">';

                        $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_staticblock'));

					$catHtml .= '</div>';

			$catHtml .= '</div>';

		}

		return $catHtml;

	}

	

    /**

     * Full-Width With Left Side Content Mega Menu HTML Block.

    */

	public function fullWidthSubLeft($category)

	{

		$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $catHtml = '';

		

		$leftcol_type_num_of_col = $main_cat->getMegamenuTypeNumofcolumns();

		if ($leftcol_type_num_of_col == 0) {

            $leftcol_type_num_of_col =3; 

        }

        $catHtml = '';

        // 2nd Level Category

        if ($childrenCategories = $this->getChildCategories($category)) {

			$catHtml .= '<div class="megamenu clearfix contentmenu">';

				$catHtml .= '<div class="root-col-4 clearfix rootmegamenu_block">';

                    //$catHtml .= $main_cat->getData('megamenu_type_staticblock'); 

                    $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_staticblock'));

				$catHtml .= '</div>';

				

				$catHtml .= '<div class="root-col-75 clearfix">';	

					$cat_cnt = 1;

                    

                    // 3rd Level Category

					foreach ( $childrenCategories as $childCategory ) {

						$catHtml .= '<div class="root-col-'.$leftcol_type_num_of_col.' clearfix ">';

                            $load_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory->getId());

							if ( $main_cat->getMegamenuShowCatimage() == 1 ) {

								$imageurl = $load_cat->getImageUrl(); 

								$catHtml .= '<a href='.$load_cat->getURL().' class="catproductimg"><img width='.$main_cat->getMegamenuShowCatimageWidth().' height='.$main_cat->getMegamenuShowCatimageHeight().' src='.$imageurl.' alt="'.$main_cat->getName().'"/></a>';

                            }

							$catHtml .= '<div class="title"><a href='.$load_cat->getURL().'>'.$load_cat->getName().'</a></div>';

							

						$catHtml .= '</div>';

						if ( $cat_cnt%$leftcol_type_num_of_col==0 ) {

                            $catHtml .= '<div class="clearfix"></div>';

                        }

						$cat_cnt++;

					}

				$catHtml .= '</div>';

			$catHtml .= '</div>';

		}

		return $catHtml;

	}

	

    /**

     * Tabbing Mega Menu HTML Block.

    */  	

	public function tabMenu($category)

	{

		$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $catHtml = '';

        

		$tab_num_of_col = $main_cat->getMegamenuTypeNumofcolumns();

		if ($tab_num_of_col == 0) {

            $tab_num_of_col =5;

        }

		$catHtml = '';

        // 2nd Level Category

		if ($childrenCategories = $this->getChildCategories($category)) {

			$catHtml .= '<div class="megamenu clearfix tabmenu">';

			$catHtml .= '<div class="mainmenuwrap clearfix">';

			$catHtml .= '<ul class="root-col-1 clearfix vertical-menu">';

				$cnt = 0;

                // 3rd Level Category

				foreach ($childrenCategories as $childCategory) {

					$load_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory->getId());

					

					$imageurl = $load_cat->getImageUrl();

					$catHtml .= '<li class="clearfix"><a href='.$load_cat->getUrl().' class="root-col-4">'.$load_cat->getName().'<span class="cat-arrow"></span></a>';

                    if ($cnt == 0) {

                        $open = "openactive";

                    } else {

                        $open = "";

                    } $cnt++;

                    if ($childrenCategories_2 = $this->getChildCategories($childCategory)) {

						$catHtml .= '<div class="root-col-75 verticalopen '.$open.'">';

							$sub_cnt = 1;

                            // 4th Level Category

							foreach ($childrenCategories_2 as $childCategory2) {

								

                                if ($main_cat->getMegamenuShowCatimage() == 1) {

									$brake_point = $tab_num_of_col * 2;

								} else {

									$brake_point = $tab_num_of_col * 6;	

								}

								if ($sub_cnt > $brake_point) { continue; }

								$sub_cnt++;

                                $load_cat_sub = $_objectManager->create('Magento\Catalog\Model\Category')->load($childCategory2->getId());

								

								$catHtml .= '<div class="tabimgwpr root-col-'.$tab_num_of_col.'"><a href='.$load_cat_sub->getURL().' class="tabimtag">';

								if ($main_cat->getMegamenuShowCatimage() == 1) {

									if ($load_cat_sub->getImageUrl() != '') {

                                        $imageurl = $load_cat_sub->getImageUrl(); 

                                    } else { 

                                        $imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');

                                    }

									$catHtml .= '<img width='.$main_cat->getMegamenuShowCatimageWidth().' height='.$main_cat->getMegamenuShowCatimageHeight().' src='.$imageurl.' alt="'.$main_cat->getName().'"/>';

								}

								$catHtml .= '<div class="tabimgtext">'.$load_cat_sub->getName().'</div></a> </div>';

							}

                            if ( $sub_cnt > $brake_point ) {

                                $catHtml .= '<a href='.$load_cat->getURL().' class="view_all">View All &raquo;</a>';

                            }

						$catHtml .= '</div>';

					 } else {

						$catHtml .= '<div class="root-col-75 verticalopen empty_category '.$open.'">';

						    $catHtml .= '<span>Sub-category not found for '.$load_cat->getName().' Category</span>';

						$catHtml .= '</div>';

                    }

					$catHtml .= '</li>';

				}

			$catHtml .= '</ul>';

			$catHtml .= '</div>';

			$catHtml .= '</div>';

		}

		return $catHtml;	

	}

	

    /**

     * Product Listing Mega Menu HTML Block.

    */   

	public function productMenu($category)

	{

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$categoryHelper = $this->getCategoryHelper();

        $main_cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());

        $products = $main_cat->getProductCollection();

        $products->addAttributeToSelect('*');

        $catHtml = '';

        

        $media_url = $_objectManager->get('Magento\Store\Model\StoreManagerInterface')

                    ->getStore()

                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);        

        $currencysymbol = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');

        $currency = $currencysymbol->getStore()->getCurrentCurrencyCode();

        

        /* 

        foreach ($products as $product) {

            $productRepository = $_objectManager->get('\Magento\Catalog\Model\ProductRepository');

            $product = $productRepository->getById($product->getId()); 

            //echo '<pre>Load Vish';print_r($product->getData());

            //echo $product->getName() . ' - ' . $product->getProductUrl() . '<br />';

        }

       exit;

       */

        $catHtml .= '<div class="megamenu clearfix product-thumbnail">';

			$pro_cnt = 0;

       		foreach ( $products as $product ) {

                //echo '<pre>'; print_r($product->getData());exit;

				if ( $pro_cnt > 10 ) { 

                    continue;

                }

                $pro_cnt++;

                $productRepository = $_objectManager->get('\Magento\Catalog\Model\ProductRepository');

                $_product = $productRepository->getById($product->getId()); 

                

                $catHtml .= '<div class="root-col-5 clearfix">';

					$catHtml .= '<div class="probox01imgwp">';

						$catHtml .= '<div class="proimg"><a href='.$_product->getProductUrl().'><img src='.$media_url.'catalog/product'.$_product->getImage().' alt="'.$main_cat->getName().'"></a></div>';

					$catHtml .= '</div>';

				  	$catHtml .= '<div class="proinfo clearfix">';

						$catHtml .= '<div class="proname clearfix"><a href="#">'.$_product->getName().'</a></div>';

						$catHtml .= '<div class="pricebox"> <span>'.$currency.number_format($_product['price'],2).'</span> <a href="'.$_product->getProductUrl().'" class="addtocart-but">Add to Cart</a> </div>';

					  $catHtml .= '</div>';

				$catHtml .= '</div> ';

			}

        $catHtml .= '</div>';

		return $catHtml;

    }

    

    /**

     * Act HTML Block.

    */ 

    public function act()

	{

		$dt_nw = strtotime(date("Y-m-d"));

		$dt_db = $this->_scopeConfig->getValue('rootmegamenu_option/general/lcstatus');

		if ( $dt_nw != $dt_db ) {

			

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

			//$u = $objectManager->create('Magento\Backend\Helper\Data')->getHomePageUrl();

			$u = $this->_storeManager->getStore()->getBaseUrl();

			$l = $this->_scopeConfig->getValue('rootmegamenu_option/general/licencekey');

			$surl = base64_decode($this->_customhelper->surl());

            

            $url= $surl."?u=".$u."&l=".$l."&extname=m2_megamenu";

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_URL,$url);

			$result=curl_exec($ch);

			curl_close($ch);

			$act_data = json_decode($result, true);

           

			if ( $act_data['status'] == '0' ) {

					return "PGRpdiBzdHlsZT0iYmFja2dyb3VuZDogYmxhY2sgbm9uZSByZXBlYXQgc2Nyb2xsIDAlIDAlOyBmbG9hdDogbGVmdDsgcGFkZGluZzogMTBweDsgd2lkdGg6IDEwMCU7IGNvbG9yOiByZWQ7IiBpZD0ibm90X2FjdGl2YXRlZCI+SXNzdWUgd2l0aCB5b3VyIFJvb3R3YXlzIG1lZ2EgbWVudSBleHRlbnNpb24gbGljZW5zZSBrZXksIHBsZWFzZSBjb250YWN0IDxhIGhyZWY9Im1haWx0bzpoZWxwQHJvb3R3YXlzLmNvbSI+aGVscEByb290d2F5cy5jb208L2E+PC9kaXY+";

            } else {

				$dt_ad = date("Y-m-d",$dt_nw);

				$this->_customresourceConfig->saveConfig('rootmegamenu_option/general/lcstatus', $dt_ad, 'default', 0);

            }

		}

	}

	

    /**

     * Contact Us Mega Menu HTML Block.

    */   

	public function contactus()

	{

		$catHtml = '';

		$catHtml .= '<li><a href="javascript:void(0);">'.__('Contact Us').'</a>';

			$catHtml .= '<div class="megamenu contacthalfmenu clearfix">';

				$catHtml .= '<div class="root-col-2 clearfix">';

					$contact_content = $this->_scopeConfig->getValue('rootmegamenu_option/general/contactus_content');

                    $catHtml .= $this->getBlockContent($contact_content);

				$catHtml .= '</div>';

                $base_url = $this->_storeManager->getStore()->getBaseUrl();

					

				$catHtml .= '<div class="root-col-2 clearfix">';

					$catHtml .= '<div class="title">'.__('Contact Us').'</div>';

					$catHtml .=	'<form id="megamenu_contact_form" name="megamenu_contact_form" class="menu_form">';

						$catHtml .= '<input id="name" name="name" type="text" autocomplete="off" placeholder="'.__('Name').'">';

						$catHtml .= '<input id="email" name="email" type="text" autocomplete="off" placeholder="'.__('Email').'">';

						$catHtml .= '<input type="text" title="Telephone" id="telephone" name="telephone" autocomplete="off" placeholder="'.__('Telephone').'">';

						$catHtml .= '<textarea id="comment" name="comment" placeholder="'.__('Your message...').'"></textarea>';

						$catHtml .= '<input type="text" style="display:none !important;" value="" id="hideit" name="hideit">';

						$catHtml .= '<input type="text" style="display:none !important;" value="'.$base_url.'" name="base_url" id="base_url" >';

						$catHtml .= '<input onclick="rootFunction()" type="button" value="'.__('Reset').'">';

						$catHtml .= '<input id="megamenu_submit" type="submit" value="'.__('Send').'">';

					$catHtml .= '</form>';

				$catHtml .= '</div>';

			$catHtml .= '</div>';

		$catHtml .= '</li>';

			

		return $catHtml;

	}

    

    /*

    public function getValuesConfig()

    {

            return $this->_scopeConfig->getValue('rootmegamenu_option/general/licencekey');

    }

    */

    

    public function _getMenuItemAttributes($item)

    {

        $menuItemClasses = $this->_getMenuItemClasses($item);

        return implode(' ', $menuItemClasses);

    }

    

    /**

     * Get Class of categories.

    */  

    protected function _getMenuItemClasses($item)

    {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        

        $classes = [];

        /*

        $classes[] = 'rootlevel' . $item->getLevel();

        $classes[] = $item->getPositionClass();





        if ($item->hasChildren()) {

            $classes[] = 'has-parent';

        }

        */

        

        $cur_cat = $_objectManager->get('Magento\Framework\Registry')->registry('current_category');

        $categoryPathIds = explode(',', $cur_cat->getPathInStore());

        if (in_array($item->getId(), $categoryPathIds) == '1') {

            $classes[] = 'active';   

        }

        return $classes;

    }

    

    /**

     * Get Class of categories.

    */  

    public function getCustomLinks($category_id)

    {

        $base_url = $this->_storeManager->getStore()->getBaseUrl();   

        $customMenus = $this->_scopeConfig->getValue('rootmegamenu_option/general/custom_link');

        $customLinkHtml = '';

        if ( $customMenus ) {

            $customMenus = json_decode($customMenus, true);

            if ( is_array($customMenus) ) {

                foreach ( $customMenus as $customMenusRow ) {

                    if ($customMenusRow['custommenulink'] != '') {

                       $no_custom_link = $base_url.$customMenusRow['custommenulink'];

                    } else {

                        $no_custom_link = 'javascript:void(0);';

                    }

                    if ( $customMenusRow['custom_menu_position'] == $category_id  && $customMenusRow['custom_menu_position'] != '' ) {

                        $customLinkHtml .= '<li class="custom-menus"><a href="'.$no_custom_link.'">'.$customMenusRow['custommenuname'].'</a>';

                        if ($customMenusRow['custom_menu_block'] != '') {

                            $customLinkHtml .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($customMenusRow['custom_menu_block'])->toHtml();

                        }

                        echo '</li>';   

                    }

                    

                    if ( $category_id == false && $customMenusRow['custom_menu_position'] == '') {

                        $customLinkHtml .= '<li class="custom-menus"><a href="'.$no_custom_link.'">'.$customMenusRow['custommenuname'].'</a>';

                        if ($customMenusRow['custom_menu_block'] != '') {

                            $customLinkHtml .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($customMenusRow['custom_menu_block'])->toHtml();

                        }

                        echo '</li>';   

                    }

                    

                }

            }

        }

        return $customLinkHtml;

    }

}

