function rootFunction() {
	document.getElementById("megamenu_contact_form").reset();
}
require(['jquery'],function($){
    jQuery(document).ready(function(){
        jQuery('.rootmenu-list li').has('.rootmenu-submenu, .rootmenu-submenu-sub, .rootmenu-submenu-sub-sub').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
        jQuery('.rootmenu-list li').has('.megamenu').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
        jQuery('.rootmenu-list li').has('.halfmenu').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
       
        jQuery('.rootmenu-click').click(function(){
            jQuery(this).siblings('.rootmenu-submenu').slideToggle('slow');
            jQuery(this).children('.rootmenu-arrow').toggleClass('rootmenu-rotate');
            jQuery(this).siblings('.rootmenu-submenu-sub').slideToggle('slow');
            jQuery(this).siblings('.rootmenu-submenu-sub-sub').slideToggle('slow');
            jQuery(this).siblings('.megamenu').slideToggle('slow');		
            jQuery(this).siblings('.halfmenu').slideToggle('slow');		
        });
		jQuery('.level2-popup .rootmenu-click').click(function(){
			jQuery(this).closest('li').find('.level3-popup').slideToggle('slow');
		});
		jQuery('.level3-popup .rootmenu-click').click(function(){
			jQuery(this).closest('li').find('.level4-popup').slideToggle('slow');
		});
     
        jQuery( ".nav-toggle" ).click(function() {
            if (jQuery("html").hasClass("nav-open")) { 
                jQuery( "html" ).removeClass('nav-before-open nav-open');
            } else {
                jQuery( "html" ).addClass('nav-before-open nav-open');  
            } 
        });
		
		
		setmenuheight();
		jQuery(window).bind("load resize", function() {
			var w_height = jQuery( window ).width();
			if(w_height <= 900){
				jQuery(".tabmenu").css('height','100%');
				jQuery(".verticalopen").css('height','100%');
			} else {
				setmenuheight();
			}
		});
		
		jQuery( ".rootmenu-list > li" ).hover(
			function() {
				jQuery( this ).addClass( "hover" );
				setmenuheight();
			}, function() {
				jQuery( this ).removeClass( "hover" );
			}
		);
     
        jQuery("#megamenu_submit").click(function(){
            var name = jQuery("#name").val();
            var email = document.getElementById('email');
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus;
                return false;
            }
             
            var email = jQuery("#email").val();
            var comment = jQuery("#comment").val();
            var telephone = jQuery("#telephone").val();
            var hideit = jQuery("#hideit").val();
            var base_url = jQuery("#base_url").val();
            
            var dataString = 'name='+ name + '&email='+ email + '&comment='+ comment + '&telephone='+ telephone + '&hideit='+ hideit;
            if(name==''||email==''||comment==''){
                alert("Please Fill All Fields");
            } else {
                jQuery('#megamenu_submit').attr('id','menu_submit_loader');
                jQuery.ajax({
                    type: "POST",
                    url: base_url+"contact/index/post/",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        alert('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                        jQuery('#menu_submit_loader').attr('id','megamenu_submit');
                    }
                });
            }
            return false;
        });
    });
});

function setmenuheight() {
    var w_inner_width = window.innerWidth;
    if(w_inner_width <= 900){
        jQuery(".tabmenu").css('height','100%');
        jQuery(".verticalopen").css('height','100%');
    } else {
        var tabMaxHeight = jQuery(".hover .tabmenu .vertical-menu").innerHeight() + 10;
        jQuery(".hover .tabmenu .vertical-menu > li").each(function() {
            var h = jQuery(this).find(".verticalopen").innerHeight();
            tabMaxHeight = h > tabMaxHeight ? h : tabMaxHeight;
            jQuery(this).find(".verticalopen").css('height',tabMaxHeight);
        });
        jQuery(".hover .tabmenu").css('height',tabMaxHeight+10);
    }
}