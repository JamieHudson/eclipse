<?php
namespace Rootways\Megamenu\Model\Attribute;

class Menutype extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['value' => '0', 'label' => __('Default')],
                ['value' => '1', 'label' => __('Simple dropdown')],
                ['value' => '2', 'label' => __('Half width')],
                ['value' => '3', 'label' => __('Full width - only links')],
                ['value' => '4', 'label' => __('Full width - sub category with right block')],
                ['value' => '5', 'label' => __('Full width - sub category with left block')],
                ['value' => '6', 'label' => __('Tabbing menu')],
                ['value' => '7', 'label' => __('Products only')]
            ];
        }
        
        return $this->_options;
    }
}
