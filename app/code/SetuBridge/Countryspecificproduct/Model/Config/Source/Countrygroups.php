<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

class Countrygroups extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
    * @var \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    */
    protected $countrygroups;
    /**
    * @var \Magento\Framework\App\ProductMetadataInterface $productMetadata
    */
    protected $productMetadata;

    public function __construct(
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    )
    {
        $this->countrygroups = $countrygroups;
    }
    public function getAllOptions()
    {
        $groups=$this->countrygroups->getCollection();
        $grouparray=[];
        if(count($groups)){
            $i=1;
            foreach($groups as $group)
            { 
                $grouparray[$i]['value']=$group->getData('countryspecificproduct_id');
                $grouparray[$i]['label']=$group->getData('grouptitle');
                $i++;
            }
        }
        $result= array_unshift($grouparray,['value'=>'','label'=>__('-- Please Select --')]);
        return $grouparray;
    }   
}
