<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model\Config\Source;

class Dropdown
{
    /**
    * @var \Magento\Cms\Model\Page $cmsPage,
    */
    protected $cmsPage;

    public function __construct(
        \Magento\Cms\Model\Page $cmsPage
    )
    {
        $this->cmsPage = $cmsPage;
    }
    public function toOptionArray()
    {
        $cmsPages = $this->cmsPage->getCollection()->addFieldToFilter('is_active',1)->getData();
        foreach($cmsPages as $cms)
        {
            $cmsidntidfier[]=$cms['identifier'];
            $cmstitle[]=$cms['title'];
        }
        $cmsarraycombine=array_combine($cmsidntidfier,$cmstitle);
        return $cmsarraycombine;
    }   
}
