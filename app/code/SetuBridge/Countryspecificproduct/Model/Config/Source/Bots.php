<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model\Config\Source;

class Bots
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'Googlebot',
                'label' => __('Google'),
            ),
            array(
                'value' => 'bingbot',
                'label' => __('Bing'),
            ),
             array(
                'value' => 'Slurp',
                'label' => __('Slurp Bot'),
            ),
             array(
                'value' => 'DuckDuckBot',
                'label' => __('DuckDuckBot'),
            ),
             array(
                'value' => 'Baiduspider',
                'label' => __('Baiduspider'),
            ),
             array(
                'value' => 'YandexBot',
                'label' => __('Yandex Bot'),
            ),
             array(
                'value' => 'Sogou',
                'label' => __('Sogou Spider'),
            ),
              array(
                'value' => 'Exabot',
                'label' => __('Exabot'),
            ),
             array(
                'value' => 'facebot',
                'label' => __('Facebook External Hit'),
            ),
              array(
                'value' => 'ia_archiver',
                'label' => __('Alexa Crawler'),
            ),
             
                 
        );
    }   
}
