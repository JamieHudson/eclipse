<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

namespace SetuBridge\Countryspecificproduct\Model\Config\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    /**
    * Value determine country database
    *
    * @var int
    */
    const GEOIP_COUNTRY_DATABASE = 2;

    /**
    * Value determine city database
    *
    * @var int
    */
    const GEOIP_REST_API    = 1;

    /**
    * Options getter
    *
    * @return array
    */
    public function toOptionArray()
    {
        return
        [
            ['value' => self::GEOIP_REST_API, 'label' => __('Geoplugin Api Service')],
            ['value' => self::GEOIP_COUNTRY_DATABASE, 'label' => __('Maxmind GeoIP Database')]
        ];
    }
}
