<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

namespace SetuBridge\Countryspecificproduct\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Symfony\Component\Config\Definition\Exception\Exception;
use GeoIp2\Database\Reader as GeoIP2Reader;
use SetuBridge\Countryspecificproduct\Helper\Database as GeoIpHelper;

/**
* Geoip model.
*
* Work with customer location
*/
class Geoip extends \Magento\Framework\Model\AbstractModel
{

    /**
    * @var \Magento\Framework\Filesystem\Directory\Write
    */
    protected $directory;

    /**
    * @var \Magento\Framework\Filesystem\Driver\File
    */
    protected $driver;

    /**
    * @var \Magento\Framework\Archive\Gz
    */
    protected $gz;

    /**
    * @var \Magento\Framework\HTTP\ZendClientFactory
    */
    protected $zendClientFactory;

    /**
    * @var \Magento\Directory\Model\ResourceModel\Country\Collection
    */
    protected $countryCollection;

    /**
    * @param \Magento\Framework\Filesystem $filesystem
    * @param \Magento\Framework\Filesystem\Driver\File $driver
    * @param \Magento\Framework\Archive\Gz $gz
    * @param \Magento\Framework\HTTP\ZendClientFactory $clientFactory
    * @param GeoIpHelper $geoIpHelper
    */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Driver\File $driver,
        \Magento\Framework\Archive\Gz $gz,
        \Magento\Framework\HTTP\ZendClientFactory $clientFactory,
        \Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection,
        GeoIpHelper $geoIpHelper
    ) {
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->driver = $driver;
        $this->gz = $gz;
        $this->zendClientFactory = $clientFactory;
        $this->countryCollection = $countryCollection;
        $this->geoIpHelper = $geoIpHelper;
    }

    /**
    * Get customer location data by IP
    *
    * @param string $ip
    * @return array
    */
    protected function getGeoIpLocation($ip)
    {
        $dbPath = $this->geoIpHelper->getDatabasePath();

        if (!$this->driver->isExists($dbPath)) {
            return array();
        }

        $data = ['ip' => $ip];
        $reader = new GeoIP2Reader($dbPath, ['en']);

        try {
            $record = $reader->country($ip);
            $data['code'] = $record->country->isoCode;
            $data['country'] = $record->country->name;
        } catch(\Exception $e) {
            $data['code']    = null;
            $data['country'] = null;
        }

        return $data;
    }

    /**
    * Loads location data by ip and puts it in object
    *
    * @param string $ip
    * @return  \Magento\Framework\DataObject
    */
    public function getCurrentLocation($ip)
    {
        $data = $this->getGeoIpLocation($ip);
        $obj = new \Magento\Framework\DataObject($data);
        return $obj;
    }

    /**
    * Downloads file from remote server
    *
    * @param string $source
    * @param string $destination
    * @return Geoip
    * @throws \Magento\Framework\Exception\LocalizedException
    * @throws \Zend_Http_Client_Exception
    */
    public function downloadFile($source, $destination, $createBackupFlag = null)
    {
        if (!$this->directory->isExist(GeoIpHelper::DB_PATH)) {
            $this->directory->create(GeoIpHelper::DB_PATH);
        }

        $newFile = $this->directory->getDriver()->fileOpen($destination, "wb");

        if (!$newFile) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Can't create new file. Check that folder %s has write permissions", $dir));
        }

        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->zendClientFactory->create();
        $client->setUri($source);
        $client->setConfig(['maxredirects' => 0, 'timeout' => 120]);

        try {
            $result = $client->request(\Zend_Http_Client::GET)->getBody();
            $this->directory->getDriver()->fileWrite($newFile, $result);
        } catch (Exception $e) {
            throw $e;
        }

        if (!$this->directory->getDriver()->isExists($destination)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('DataBase source is temporary unavailable'));
        }

        $this->directory->getDriver()->fileClose($newFile);

        if ($createBackupFlag) {
            $backupDestination = $destination . GeoIpHelper::ARCHIVE_SUFFIX . time();
            $this->directory->getDriver()->copy($destination, $backupDestination);
        }

        $this->unCompressFile($this->geoIpHelper->getTempUpdateFile(), $this->geoIpHelper->getDatabasePath());
        $this->directory->getDriver()->deleteFile($this->geoIpHelper->getTempUpdateFile());

        return $this;
    }
    /**
    * Unpack .gz archive
    *
    * @param string $source
    * @param string $destination
    * @return bool
    * @throws \Exception
    * @throws \Magento\Framework\Exception\LocalizedException
    */
    protected function unCompressFile($source, $destination)
    {
        try {
            $this->gz->unpack($source, $destination);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw $e;
        }

        return $this;
    }
}
