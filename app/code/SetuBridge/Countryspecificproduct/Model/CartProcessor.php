<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model;
use Magento\Checkout\Model\SessionFactory;
use Magento\Quote\Model\QuoteRepository;
use Magento\Catalog\Model\ProductRepository;

/**
* ModelNametab modelname model
*/
class CartProcessor extends \Magento\Framework\DataObject
{

    protected $cspValidator;
    protected $checkoutSession;
    protected $quoteRepository;
    public function __construct(
        Validator $cspValidator,
        SessionFactory $checkoutSession,
        QuoteRepository $quoteRepository
    ) {
        $this->cspValidator = $cspValidator;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
    }
    public function process(){
        $quote=$this->checkoutSession->create()->getQuote();
        if($quote && $quote->getId() && $quote->hasItems()){
            $quoteSave=false;
            foreach($quote->getAllItems() as $item){
                if(!$this->cspValidator->isValidProductForCsp($item->getProductId())){
                    $quote->deleteItem($item);
                    $quoteSave=true;
                }
            }
            try{
                if($quoteSave){
                    $quote->setTotalsCollectedFlag(false)->collectTotals();
                    $this->quoteRepository->save($quote);  
                }
            }catch(\Exception $e){
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            }

        }
    }
}