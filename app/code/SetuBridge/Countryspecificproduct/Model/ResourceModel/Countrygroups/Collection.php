<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Model\ResourceModel\Countrygroups;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
    * @var string
    */
    protected $_idFieldName = 'countryspecificproduct_id';
    /**
    * Define resource model.
    */
    protected function _construct()
    {
        $this->_init('SetuBridge\Countryspecificproduct\Model\Countrygroups', 'SetuBridge\Countryspecificproduct\Model\ResourceModel\Countrygroups');
    }
}