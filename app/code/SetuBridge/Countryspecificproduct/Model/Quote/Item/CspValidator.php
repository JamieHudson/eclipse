<?php
/**
* Product inventory data validator
*
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/

namespace SetuBridge\Countryspecificproduct\Model\Quote\Item;

use SetuBridge\Countryspecificproduct\Helper\Data as CspHelper;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Item; 

/**
* Quote item quantity validator.
*
* @api
* @since 100.0.2
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*
* @deprecated 2.3.0 Replaced with Multi Source Inventory
* @link https://devdocs.magento.com/guides/v2.3/inventory/index.html
* @link https://devdocs.magento.com/guides/v2.3/inventory/catalog-inventory-replacements.html
*/
class CspValidator
{
    const CSP_ERROR_CODE='csp_error';
    /**
    * @var CspHelper
    */
    protected $cspHelper;

    /**
    * @var SetuBridge\Countryspecificproduct\Model\Validator
    */
    protected $cspValidator;

    /**
    * @param CspHelper $cspHelper
    * @param \SetuBridge\Countryspecificproduct\Model\Validator $cspValidator
    * @return void
    */
    public function __construct(
        CspHelper $cspHelper,
        \SetuBridge\Countryspecificproduct\Model\Validator $cspValidator

    ) {
        $this->cspHelper = $cspHelper;
        $this->cspValidator = $cspValidator;
    }
    /**
    *
    * @param \Magento\Framework\Event\Observer $observer
    *
    * @return void
    * @throws \Magento\Framework\Exception\LocalizedException
    */
    public function validate(Observer $observer)
    {
        /* @var $quoteItem Item */
        $quoteItem = $observer->getEvent()->getItem();
        if (!$quoteItem ||
        !$quoteItem->getProductId() ||
        !$quoteItem->getQuote()  ||
        !$this->cspHelper->getConfigValue('enable_select') ||
        !$this->cspHelper->getShippingValidationConfigValue('valiadte_with_shipping')
        ) {
            return;
        }
        $product = $quoteItem->getProduct();
        $shippingAddress = $quoteItem->getQuote()->getShippingAddress();
        if($shippingAddress && $shippingAddress->getCountryId()){  
            if(!$this->cspValidator->isValidProductForCsp($quoteItem->getProductId(),$shippingAddress->getCountryId())){
                $quoteItem->addErrorInfo(
                    'countryspecificproduct',
                    self::CSP_ERROR_CODE,
                    __($this->cspHelper->getShippingValidationConfigValue('item_validation_message'))
                );
                $quoteItem->getQuote()->addErrorInfo(
                    'csp',
                    'countryspecificproduct',
                    self::CSP_ERROR_CODE,
                    __($this->cspHelper->getShippingValidationConfigValue('validation_message'))
                );  
            }else{
                // Delete error from item and its quote
                $this->_removeErrorsFromQuoteAndItem($quoteItem, self::CSP_ERROR_CODE);
            } 
        }else{
            // Delete error from item and its quote
            $this->_removeErrorsFromQuoteAndItem($quoteItem, self::CSP_ERROR_CODE);
        }

        return;

    }


    /**
    * Removes error statuses from quote and item, set by this observer
    *
    * @param Item $item
    * @param int $code
    * @return void
    */
    protected function _removeErrorsFromQuoteAndItem($item, $code)
    {
        if ($item->getHasError()) {
            $params = ['origin' => 'countryspecificproduct', 'code' => $code];
            $item->removeErrorInfosByParams($params);
        }

        $quote = $item->getQuote();
        if ($quote->getHasError()) {
            $quoteItems = $quote->getItemsCollection();
            $canRemoveErrorFromQuote = true;
            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getItemId() == $item->getItemId()) {
                    continue;
                }

                $errorInfos = $quoteItem->getErrorInfos();
                foreach ($errorInfos as $errorInfo) {
                    if ($errorInfo['code'] == $code) {
                        $canRemoveErrorFromQuote = false;
                        break;
                    }
                }

                if (!$canRemoveErrorFromQuote) {
                    break;
                }
            }

            if ($canRemoveErrorFromQuote) {
                $params = ['origin' => 'countryspecificproduct', 'code' => $code];
                $quote->removeErrorInfosByParams(null, $params);
            }
        }
    }
}
