<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model;

use Magento\Framework\Exception\ModelNameException;

/**
* ModelNametab modelname model
*/
class Countrygroups extends \Magento\Framework\Model\AbstractModel
{
    /**
    * @var \SetuBridge\Countryspecificproduct\Helper\Data $this->cspHelper
    */ 
    protected $cspHelper;

    /**
    * @var \Magento\Store\Model\Store $store
    */
    protected $store;
    /**
    * @var \Magento\Catalog\Model\ProductFactory $productFactory,
    */
    protected $productFactory;
    /**
    * @var \Magento\Catalog\Model\Product\Action $productAction,
    */
    protected $productActionFactory;

    /**
    * @param \Magento\Framework\Model\Context $context
    * @param \SetuBridge\Countryspecificproduct\Helper\Data $cspHelper
    * @param \Magento\Store\Model\Store $store,
    * @param \Magento\Catalog\Model\ProductFactory $productFactory,
    * @param \Magento\Catalog\Model\Product\Action $productAction
    * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
    * @param \Magento\Framework\Data\Collection\Db $resourceCollection
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \SetuBridge\Countryspecificproduct\Helper\Data $cspHelper,
        \Magento\Store\Model\Store $store,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Product\ActionFactory $productAction,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->cspHelper = $cspHelper;
        $this->store = $store;
        $this->productFactory = $productFactory;
        $this->productActionFactory = $productAction;

    }

    /**
    * @return void
    */
    public function _construct()
    {
        $this->_init('SetuBridge\Countryspecificproduct\Model\ResourceModel\Countrygroups');
    }
    public function loadBycode($code){
        $this->load('groupcode',$code);
        return $this;
    }
    public function validateGroupCode($code){
        $collection =$this->getCollection()->addFieldToFilter('groupcode',$code);
        if($collection->count()) return false;
        return true;
    }
    public function updateCspProductAttributes($countryGroupId,$attributesData){
        $storeIds = $this->store->getCollection()->getAllIds();
        $countryattribid=$this->cspHelper->getcountryattribid();
        $currentcountry=$this->cspHelper->getCurrentCountry();
        $connection=$this->_getResource()->getConnection();
        foreach($storeIds as $storeId){
            try{
                $_products=$this->getLinkedProducts($countryGroupId,$storeId);
                if(!empty($_products)){
                    $this->_updateAttributes($_products, $attributesData,$storeId);
                }
            }catch(\Exception $e){
            }
        }
    }
    public function _updateAttributes($_products,$attributesData,$storeId=\Magento\Store\Model\Store::DEFAULT_STORE_ID){
        $this->productActionFactory->create()->updateAttributes($_products, $attributesData,$storeId);
    }
    public function getLinkedProducts($countryGroupId,$storeId=\Magento\Store\Model\Store::DEFAULT_STORE_ID){
        $collection = $this->productFactory->create()->getCollection()->addStoreFilter($storeId);
        $collection->addAttributeToFilter('countrygroupid',$countryGroupId,'inner');
        return $collection->getAllIds();
    }
    public function getCacheTags()
    {
        $tags = parent::getCacheTags();
        $tags[]=\Magento\PageCache\Model\Cache\Type::CACHE_TAG;
        return $tags;
    }

}