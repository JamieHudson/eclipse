<?php
/**
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace SetuBridge\Countryspecificproduct\Model\Indexer\Product;


class Csp implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{

    const CSP_INDEXER_ID='setubridge_countryspecificproduct';
    /**
    * @var \SetuBridge\Countryspecificproduct\Model\Countrygroups
    */
    protected $countrygroups;
    /**
    * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
    */ 
    protected $productCollectionFactory; 
    /**
    * @var \SetuBridge\Countryspecificproduct\Helper\Data
    */ 
    protected $helper;

    public function __construct(
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \SetuBridge\Countryspecificproduct\Helper\Data $helper
    ) {
        $this->countrygroups = $countrygroups;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->helper = $helper;
    }
    private function updateRequiredProducts(){
        if( $this->helper->getConfigValue('enable_select')){
            $cspProducts=$this->productCollectionFactory->create()->addAttributeToFilter('countrygroupid',['notnull'=>true])->getAllIds();
            $withoutCspProducts=$this->productCollectionFactory->create();
            if(!empty($cspProducts)){
                $withoutCspProducts->addAttributeToFilter('entity_id', ['nin' => $cspProducts]);
            }
            try{
                if($withoutCspProducts->getSize()){
                    $this->countrygroups->_updateAttributes($withoutCspProducts->getAllIds(),['countrygroupid'=>null,'countrygroupstring'=>null]);   
                }
            }catch(\Exception $e){
            }  
        }
    }
    /**
    * Execute materialization on ids entities
    *
    * @param int[] $ids
    * @return void
    */
    public function execute($ids)
    {
        $this->updateRequiredProducts();
    }

    /**
    * Execute full indexation
    *
    * @return void
    */
    public function executeFull()
    {
        $this->updateRequiredProducts();
    }

    /**
    * Execute partial indexation by ID list
    *
    * @param int[] $ids
    * @return void
    */
    public function executeList(array $ids)
    {
        $this->updateRequiredProducts();
    }

    /**
    * Execute partial indexation by ID
    *
    * @param int $id
    * @return void
    */
    public function executeRow($id)
    {
        $this->updateRequiredProducts();
    }

}
