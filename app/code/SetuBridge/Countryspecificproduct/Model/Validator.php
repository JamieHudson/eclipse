<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Model;

use Magento\Catalog\Model\ProductRepository;

class Validator extends \Magento\Framework\DataObject
{

    protected $cspHelper;
    protected $productRepository;
    public function __construct(
        \SetuBridge\Countryspecificproduct\Helper\Data $cspHelper,
        ProductRepository $productRepository
    ) {
        $this->cspHelper = $cspHelper;
        $this->productRepository = $productRepository;
    }
    public function isValidProductForCsp($productId,$country=null){
        $product= $this->productRepository->getById($productId);
        $country =empty($country)?$this->cspHelper->getCurrentCountry():strtolower($country);
        $isValid=true;
        if($country && !empty($product->getCountrygroupstring())){
            if(strpos($product->getCountrygroupstring(),$country)===false){
                $isValid=false;
            }  
        }
        return $isValid;
    }
}