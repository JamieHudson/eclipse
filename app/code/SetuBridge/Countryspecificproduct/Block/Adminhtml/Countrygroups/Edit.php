<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
    * Core registry.
    *
    * @var \Magento\Framework\Registry
    */
    protected $_coreRegistry = null;

    /**
    * @param \Magento\Backend\Block\Widget\Context $context
    * @param \Magento\Framework\Registry           $registry
    * @param array                                 $data
    */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) 
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
    * Initialize Imagegallery Images Edit Block.
    */
    protected function _construct()
    {        
        $this->_objectId = 'id';
        $this->_blockGroup = 'SetuBridge_Countryspecificproduct';
        $this->_controller = 'adminhtml_countrygroups';
        parent::_construct();
        if ($this->_isAllowedAction('SetuBridge_Countryspecificproduct::countrygroups_index')) {
            $this->buttonList->update('save', 'label', __('Save'));
        } else {
            $this->buttonList->remove('save');
        }
        $this->buttonList->remove('reset');
        $this->buttonList->update('delete', 'label', __('Delete'));
        $this->setId('country_group_view');
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ]
                ]
            ],
            -100
        );
    }

    /**
    * Retrieve text for header element depending on loaded image.
    *
    * @return \Magento\Framework\Phrase
    */
    public function getHeaderText()
    {
        return __('Create New Group');
    }
    protected function _getSaveAndContinueUrl()
    {   
        return $this->getUrl('*/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '','store'=>$this->_getStoreId()]);
    }
    /**
    * Check permission for passed action.
    *
    * @param string $resourceId
    *
    * @return bool
    */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    public function getHeaderCssClass()
    {
        return 'icon-head head-customer-groups';
    }
    /**
    * Get form action URL.
    *
    * @return string
    */
    public function getFormActionUrl()
    {   
        return $this->getUrl('countryspecificproduct/*/save',['_current' => true, 'back' => 'edit', 'active_tab' => '','store'=>$this->_getStoreId()]);
    }
    public function getValidationUrl()
    {
        return $this->getUrl('countryspecificproduct/*/validate', ['_current' => true]);
    }
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
        function toggleEditor() {
        if (tinyMCE.getInstanceById('page_content') == null) {
        tinyMCE.execCommand('mceAddControl', false, 'content');
        } else {
        tinyMCE.execCommand('mceRemoveControl', false, 'content');
        }
        };
        ";
        return parent::_prepareLayout();
    }
    protected function _getStoreId(){
       return (int)$this->getRequest()->getParam('store', 0);
    }
}

