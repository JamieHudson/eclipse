<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Renderer;

use \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class ProductLink extends AbstractRenderer
{
    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        if ($row->getEntityId()) {
            $url = $this->getUrl('catalog/product/edit', ['id' => $row->getEntityId()]);
            $text = __('Edit');
            $link = "<a href='$url' target='_blank'>$text</a>";
        }
        return $link;
    }
}


