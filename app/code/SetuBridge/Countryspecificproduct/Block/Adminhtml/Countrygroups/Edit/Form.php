<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
    * @var \Magento\Store\Model\System\Store
    */
    protected $_systemStore;

    /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\Registry             $registry
    * @param \Magento\Framework\Data\FormFactory     $formFactory
    * @param array                                   $data
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) 
    {
        parent::__construct($context, $registry, $formFactory, $data);
    }
    public function _construct()
    {
        parent::_construct();
        $this->setId('countryspecificproduct_form');
        $this->setTitle(__('Country Group Information'));
    }
    protected function _prepareForm()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('countryspecificproduct_data'); 
        $action=$this->getUrl('countryspecificproduct/*/save',['store'=>$this->_getStoreId()]);
        if($model){
            $action=$this->getUrl('countryspecificproduct/*/save',['id'=>$model->getCountryspecificproductId(),'store'=>$this->_getStoreId()]);
        }       

        $form = $this->_formFactory->create(
            ['data' => [
                'id' => 'edit_form', 
                'enctype' => 'multipart/form-data', 
                'action' => $action, 
                'method' => 'post'
                ]
            ]
        );
        $form->setHtmlIdPrefix('countryspecificproduct_');
        if ($model && $model->getCountryspecificproductId()) {
            $form->addField('id', 'hidden', ['name' => 'countryspecificproduct_id']);
            $form->addValues(
                ['countryspecificproduct_id' => $model->getCountryspecificproductId()]
            );
        }

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    protected function _getStoreId(){
       return (int)$this->getRequest()->getParam('store', 0);
    }

}


