<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Tab;

class RelatedProducts extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
    * @var \Magento\Catalog\Model\ProductFactory
    */

    protected $_productFactory;
    /**
    * @var \SetuBridge\Countryspecificproduct\Helper\Data
    */
    protected $cspHelper;

    /**
    * @var \Magento\Framework\App\ResourceConnection
    */
    protected $resource;
    /**
    * @var \Magento\Framework\Data\Collection
    */
    protected $emptyCollection;

    /**
    * @var \Magento\Framework\App\ResourceConnection
    */
    protected $_store;
    /**
    * @var \SetuBridge\Countryspecificproduct\Model\Countrygroups
    */
    protected $_countryGroup;

    /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Backend\Helper\Data $backendHelper
    * @param \Magento\Catalog\Model\ProductFactory $productFactory
    * @param \Magento\Catalog\Model\ProductFactory $productFactory,
    * @param \SetuBridge\Countryspecificproduct\Helper\Data $productFactory,
    * @param array $data
    *
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \SetuBridge\Countryspecificproduct\Helper\Data $cspHelper,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countryGroup,
        \Magento\Framework\Data\Collection $emptyCollection,
        array $data = []
    ) {
        $this->emptyCollection = $emptyCollection;
        $this->_productFactory = $productFactory;
        $this->resource = $resource;
        $this->cspHelper = $cspHelper;
        $this->_countryGroup = $countryGroup;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
    * @return void
    */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('countrygroupRelatedProductGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setDefaultFilter(['products_related' => 1]);
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    /**
    * @return Store
    */
    protected function _getStore()
    {
        if(!$this->_store){
            $storeId = (int)$this->getRequest()->getParam('store', 0);
            $this->_store= $this->_storeManager->getStore($storeId);
        }
        return $this->_store;
    }

    /**
    * @return $this
    */
    protected function _prepareCollection()
    {
        try{
            $currentgroupid=$this->getRequest()->getParam('id');
            $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect('name');
            if($this->_getStore()->getId()){
                $collection->addStoreFilter($this->_getStore()); 
            }
            $groupCollection=$this->_countryGroup->getCollection();
            if($currentgroupid){
                $groupCollection ->addFieldToFilter('countryspecificproduct_id',['nin'=>$currentgroupid]);  
            }                            
            $groupIds=$groupCollection->getAllIds();
            if(!empty($groupIds)){
                $collection->addAttributeToFilter(
                    [
                        ['attribute'=> 'countrygroupid','null' => true],
                        ['attribute'=> 'countrygroupid','nin' => $groupIds]
                    ]
                    ,'left');  
            }
            $this->setCollection($collection);
            parent::_prepareCollection();

            return $this;
        }
        catch(\Exception $e)
        {
            return $this;
        }
    }

    /**
    * @param \Magento\Backend\Block\Widget\Grid\Column $column
    * @return $this
    */

    protected function _prepareColumns()
    {   
        $this->addColumn(
            'products_related',
            [
                'type' => 'checkbox',
                'html_name' => 'products_related',
                'required'  => true,
                'values' => $this->_getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );
        $this->addColumn('entity_id',
            [
                'header'    => __('ID'),
                'sortable'  => true,
                'width'     => 40,
                'index'     => 'entity_id'
            ]
        );

        $this->addColumn('name',
            [
                'header'    => __('Name'),
                'index'     => 'name'
            ]
        );

        $this->addColumn('sku', [
            'header'    => __('SKU'),
            'width'     => 80,
            'index'     => 'sku'
            ]
        );
        $this->addColumn('action', [
            'header'    => __('Edit'),
            'type'     => 'action',
            'width'     => 80,
            'renderer' => 'SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Renderer\ProductLink',
            'getter' => 'getId',
            'filter' => false,
            'sortable' => false
            ]
        );

        $this->addColumn('position', [
            'header'            => __('Position'),
            'name'              => 'position',
            'type'              => 'number',
            'validate_class'    => 'validate-number',
            'index'             => 'position',
            'width'             => 0,
            'editable'          => true,
            'edit_only'         => true,
            'column_css_class'=>'no-display',//this sets a css class to the column row item
            'header_css_class'=>'no-display',//this sets a css class to the column header
            ]
        );

        return parent::_prepareColumns();
    }


    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'products_related') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    protected function _getSelectedProducts()
    {
        $relatedProduct=$this->getSelectedRelatedProducts();
        $products = !empty($relatedProduct)?array_keys($relatedProduct):[];
        return $products;
    }
    public function getSelectedRelatedProducts()
    {
        $currentgroupid=$this->getRequest()->getParam('id');
        $proIds = array();
        if($currentgroupid){
            $store = $this->_getStore();
            $countryattribid=$this->cspHelper->getcountryattribid();
            $collection = $this->_productFactory->create()->getCollection()
            ->addAttributeToSelect('name')
            ->addStoreFilter($store);
            $collection->addAttributeToFilter('countrygroupid',$currentgroupid,'inner'); 
            if($collection->getSize()){
                foreach($collection as $product) { 
                    $proIds[$product->getId()] = array('id'=>$product->getId());
                }

            }  
        }
        return $proIds;
    }
    /**
    * @return string
    */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/relatedproductgrid', ['_current' => true]);
    }

    /**
    * @param \Magento\Catalog\Model\Product|\Magento\Framework\Object $row
    * @return string
    */
    public function getRowUrl($row)
    {
        return false;
    }
}
