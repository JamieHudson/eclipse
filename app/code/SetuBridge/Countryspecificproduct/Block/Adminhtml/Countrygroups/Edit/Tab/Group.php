<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;

class Group extends Generic implements TabInterface
{
    /**
    * @var \Magento\Store\Model\System\Store
    */
    protected $_systemStore;

    /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\Registry             $registry
    * @param \Magento\Framework\Data\FormFactory     $formFactory
    * @param array                                   $data
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    ) 
    {
        $this->setTemplate('countryspecific/group.phtml');
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
    * Prepare form.
    *
    * @return $this
    */

    /**
    * Prepare label for tab
    *
    * @return string
    */
    public function getTabLabel()
    {
        return __('General Info');
    }

    /**
    * Prepare title for tab
    *
    * @return string
    */
    public function getTabTitle()
    {
        return __('General Info');
    }

    /**
    * {@inheritdoc}
    */
    public function canShowTab()
    {
        return true;
    }

    /**
    * {@inheritdoc}
    */
    public function isHidden()
    {
        return false;
    }
    public function getCurrentGroup(){
        return $this->_coreRegistry->registry('countryspecificproduct_data');
    }
}


