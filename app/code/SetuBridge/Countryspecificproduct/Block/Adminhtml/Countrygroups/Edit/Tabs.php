<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('countrygroups_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Country Group Information'));
    }

    /**
    * @return $this
    */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'country_group_info',
            [
                'label' => __('General'),
                'title' => __('General'),
                'content' => $this->getLayout()->createBlock(
                    'SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Tab\Group'
                )->toHtml(),
                'active' => true
            ]
        );
        $this->addTab('selectecountry', array(
            'label'     => __('Group Country Selection'),
            'url'       => $this->getUrl('*/*/selectecountry', array('_current' => true)),
            'class'     => 'ajax',
        ));
        $this->addTab('related', array(
            'label'     => __('Related Products'),
            'url'       => $this->getUrl('*/*/relatedProduct', array('_current' => true)),
            'class'     => 'ajax',
            
        ));

        return parent::_beforeToHtml();
    }

}


