<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Tooltip extends \Magento\Config\Block\System\Config\Form\Field
{
    protected function _renderValue(AbstractElement $element)
    {   
        if ($element->getTooltip()) {
            $html = '<td class="value with-tooltip">';
            $html .= $this->_getElementHtml($element);
            $html .= '<div class="tooltip"><span class="help"><span></span></span>';
            $html .= '<div class="tooltip-content"><img src="'.$this->getViewFileUrl("SetuBridge_Countryspecificproduct::images/{$element->getTooltip()}").'"></div></div>';
        } else {
            $html = '<td class="value">';
            $html .= $this->_getElementHtml($element);
        }
        if ($element->getComment()) {
            $html .= '<p class="note"><span>' . $element->getComment() . '</span></p>';
        }
        $html .= '<style>#countryspecificproduct_general_section_shipping_validation tr td .tooltip .tooltip-content{max-width:400px;width:400px;}</style>';
        $html .= '</td>';
        return $html;
    }  
}

