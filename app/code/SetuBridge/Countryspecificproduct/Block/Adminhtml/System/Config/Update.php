<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

namespace SetuBridge\Countryspecificproduct\Block\Adminhtml\System\Config;

/**
 * GeoIP Update system block to display custom field in module settings
 */
class Update extends \Magento\Config\Block\System\Config\Form\Field
{

    protected $helperDatabase;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \SetuBridge\Countryspecificproduct\Helper\Database $helperDatabase,
        array $data = []
    ) {
        $this->helperDatabase = $helperDatabase;
        parent::__construct($context, $data);
    }
    
    /**
     * Set template
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('SetuBridge_Countryspecificproduct::update-database.phtml');
    }
    
    /**
     * Adds update button to config field
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }
        
    /**
     * Return update button html
     *
     * @param string $sku
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'database_update',
                'label' => __('Update Database'),
                'onclick' => 'javascript:startUpdate(); return false;',
            ]
        );
        
        return $button->toHtml();
    }
    
    public function getFormUrl()
    {
        return $this->_urlBuilder->getUrl('countryspecificproduct/database/update/') . '?isAjax=1';
    }

    public function getLastUpdateTime()
    {
        return $this->helperDatabase->getLastUpdateTime();
    }
}
