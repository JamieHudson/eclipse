<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Database;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;

/**
* GeoIP UPDATE controller
*/
class Update extends \Magento\Backend\App\Action
{
    /**
    * @var \SetuBridge\Countryspecificproduct\Helper\Database
    */
    protected $databaseHelper; 

    /**
    * @var \Magento\Config\Model\ResourceModel\ConfigFactory
    */   
    protected $configFactory; 

    /**
    * @var \SetuBridge\Countryspecificproduct\Model\GeoipFactory
    */
    protected $geoipFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \SetuBridge\Countryspecificproduct\Helper\Database $databaseHelper,
        \Magento\Config\Model\ResourceModel\ConfigFactory $configFactory,
        \SetuBridge\Countryspecificproduct\Model\GeoipFactory $geoipFactory
    ) {
        parent::__construct($context);
        $this->databaseHelper = $databaseHelper;
        $this->configFactory = $configFactory;
        $this->geoipFactory = $geoipFactory;
    }
    /**
    * Update country/city database
    *
    * @return string
    */
    public function execute()
    {
        $createBackupFlag = $this->getRequest()->getParam('backup');
        try {
            $this->geoipFactory->create()->downloadFile(
                $this->databaseHelper->getDbUpdateSource(),
                $this->databaseHelper->getTempUpdateFile(),
                $createBackupFlag
            );
            $time = time();
            $this->configFactory->create()->saveConfig(
                \SetuBridge\Countryspecificproduct\Helper\Database::XML_GEOIP_UPDATE_DB,
                $time,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                0
            );
            $returnData['last_update'] = date('F d, Y / h:i', $time);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $returnData['last_update'] = $e->getRawMessage();
        }
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($returnData);
        return $result;
    }
}
