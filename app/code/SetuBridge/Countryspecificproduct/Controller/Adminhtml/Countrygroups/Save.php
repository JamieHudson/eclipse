<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

class Save extends \Magento\Backend\App\Action
{
    protected $countrygroups;
    protected $cspHelper;
    protected $jsHelper;
    protected $dateTime;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups,
        \SetuBridge\Countryspecificproduct\Helper\Data $cspHelper,
        \Magento\Backend\Helper\Js $jsHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) 
    {
        parent::__construct($context);
        $this->countrygroups = $countrygroups;
        $this->cspHelper = $cspHelper;
        $this->jsHelper = $jsHelper;
        $this->dateTime = $dateTime;
    }
    public function execute()
    {
        $postData = $this->getRequest()->getPost();
        $data = $this->getRequest()->getPost('country_group',[]);
        $id = $this->getRequest()->getParam('id');
        $storeId=(int)$this->getRequest()->getParam('store', 0);
        if (!$data) {
            $this->_redirect('countryspecificproduct/*/index');
            return;
        }
        try {
            $rowData = $this->countrygroups;
            if($id) $rowData->load($id);
            $getcountry=isset($data['countryselect']) ? $data['countryselect'] :[];
            if(isset($data['countryselect']) && !empty($data['countryselect']) && is_array($data['countryselect'])){
                $data['countryselect']=strtolower(implode(',',$data['countryselect']));
            }
            $magentoDateObject = $this->dateTime;
            if(!$id) $data['created_time'] = $magentoDateObject->gmtDate();
            $data['update_time'] = $magentoDateObject->gmtDate();
            $rowData->addData($data);
            if (isset($data['id'])) {
                $rowData->setCountryspecificproductId($data['id']);
            }
            $rowData->save();
            $linkedProducts=$rowData->getLinkedProducts($rowData->getId(),$storeId);
            if(array_key_exists('products_related',$postData)){
                if ($relatedProductIds = $this->getRequest()->getParam('products_related', null) ) {
                    $relatedProducts=$this->jsHelper->decodeGridSerializedInput($relatedProductIds);
                    if(!empty($relatedProducts)){
                        $selectedProduct=array_keys($relatedProducts);
                        $diffrences=array_diff($linkedProducts,$selectedProduct);
                        if(!empty($diffrences) && !empty($rowData)){
                            $rowData->_updateAttributes($diffrences,$this->_getAttributeData($rowData,true),$storeId);  
                        }
                        if(!empty($selectedProduct) && !empty($rowData)){
                            $rowData->_updateAttributes($selectedProduct,$this->_getAttributeData($rowData),$storeId);  
                        }
                    }else{
                       $rowData->_updateAttributes($linkedProducts,$this->_getAttributeData($rowData,true),$storeId);  
                    }
                }
            } 
            /*if(isset($id) && $id!='' && !empty($getcountry)){
                $countryModel=$rowData->load($id);
                $attributesData=['countrygroupstring' => $countryModel->getData('countryselect'),'countrygroupid'=>$countryModel->getId()]; 
                $countryModel->updateCspProductAttributes($id,$attributesData);
            }*/ 
            $this->messageManager->addSuccess(__('Group data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect(
                '*/*/edit',
                [
                    'id' => $rowData->getId(),
                    'store'=>$storeId
                ]
            );
            return;
        }

        $this->_redirect('countryspecificproduct/*/index');
    }
    protected function _getAttributeData($countryModel,$empty=false){
        return  [
            'countrygroupstring' => !$empty?$countryModel->getData('countryselect'):'',
            'countrygroupid'=>!$empty?$countryModel->getId():''
        ];
    }
    /**
    *
    * @return bool
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
}