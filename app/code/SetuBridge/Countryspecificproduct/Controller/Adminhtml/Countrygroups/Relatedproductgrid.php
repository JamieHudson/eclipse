<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Relatedproductgrid extends Action
{
    public function execute()
    {  
        $this->_view->loadLayout();
        $grid = $this->_view->getLayout()->createBlock('SetuBridge\Countryspecificproduct\Block\Adminhtml\Countrygroups\Edit\Tab\RelatedProducts')
        ->setProductsRelated($this->getRequest()->getPost('products_related', null))
        ->toHtml();
        $this->getResponse()->setBody($grid);
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }  
}
