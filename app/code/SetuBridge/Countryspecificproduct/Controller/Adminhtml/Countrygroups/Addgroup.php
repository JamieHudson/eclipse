<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Framework\Controller\ResultFactory;

class Addgroup extends \Magento\Backend\App\Action
{
    public function execute()
    {
       $resultForward=$this->resultFactory->create(ResultFactory::TYPE_FORWARD)->forward('edit');
       return $resultForward;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
} 

