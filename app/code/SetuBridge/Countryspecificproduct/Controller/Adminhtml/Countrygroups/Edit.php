<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Framework\Controller\ResultFactory;

class Edit extends \Magento\Backend\App\Action
{
    protected $countrygroups;
    protected $cspHelper;
    protected $coreRegistry;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups,
        \Magento\Framework\Registry $coreRegistry
    ) 
    {
        parent::__construct($context);
        $this->countrygroups = $countrygroups;
        $this->coreRegistry = $coreRegistry;
    }
    /**
    * Add New Row Form page.
    *
    * @return \Magento\Backend\Model\View\Result\Page
    */
    public function execute()
    {

        $groupId = (int) $this->getRequest()->getParam('id');
        $rowData = $this->countrygroups;
        if ($groupId) {
            $rowData = $rowData->load($groupId);
            $rowTitle = $rowData->getGrouptitle();
            if (!$rowData->getCountryspecificproductId()) {
                $this->messageManager->addError(__('group data no longer exist.'));
                $this->_redirect('countryspecificproduct/countrygroups/index');
                return;
            }
        }
        $this->coreRegistry->register('countryspecificproduct_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $groupId ? __("Edit Country Specific Product Group '%1'",$rowTitle) : __('Add New Group');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
} 

