<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class RelatedProduct extends \Magento\Backend\App\Action
{
    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;
    protected $resultLayoutFactory;


    /**
    * @var \Magento\Backend\Model\View\Result\Page
    */
    protected $resultPage;

    /**
    * @param Context $context
    * @param PageFactory $resultPageFactory
    */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    )
    {
        parent::__construct($context);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    public function execute()
    {   
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('setubridge.countryspecificproduct.countrygroups.edit.tab.related')
        ->setSelectedRelatedProducts($this->getRequest()->getPost('products_related', null));
        return $resultLayout;

    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
}
