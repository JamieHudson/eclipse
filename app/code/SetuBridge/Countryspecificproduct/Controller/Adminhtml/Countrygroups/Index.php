<?php
/**
 * Setubridge Technolabs
 * http://www.setubridge.com/
 * @author SetuBridge
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

class Index extends \Magento\Backend\App\Action
{
    protected $jsonResultFactory;

    protected $resultPageFactory;
    protected $resultPage;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) 
    {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        //Call page factory to render layout and page content
        $this->_setPageData();
        return $this->getResultPage();
    }

    /*
    * Check permission via ACL resource
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }

    public function getResultPage()
    {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->_resultPageFactory->create();
        }
        return $this->resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('SetuBridge_Countryspecificproduct::countrygroups_index');
        $resultPage->getConfig()->getTitle()->prepend((__('Country Groups')));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('SetuBridge'), __('Country Groups'));
        $resultPage->addBreadcrumb(__('SetuBridge'), __('Country Groups'));
        return $this;
    }
} 

