<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Framework\Controller\ResultFactory;
/**
* Product validate
*
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
class Validate extends \Magento\Backend\App\Action
{

    protected $jsonFactory;
    protected $countrygroups;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    ) 
    {
        parent::__construct($context);
        $this->countrygroups = $countrygroups;
    }
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $response->setError(false);
        $errorMessage='';
        $layout=$this->_view->getLayout();
        try {
            $groupData = $this->getRequest()->getPost('country_group', []);
            $id = $this->getRequest()->getParam('id');
            if(!isset($groupData['groupcode']) && ( !$id && empty($groupData['groupcode']))){
                $response->setError(true);
                $errorMessage=__('Groupcode required');
            }
            if(!isset($groupData['grouptitle']) && empty($groupData['grouptitle'])){
                $response->setError(true);
                $errorMessage=__('Group Title required');
            }
            if(isset($groupData['groupcode']) && !$id && !$this->countrygroups->validateGroupCode($groupData['groupcode'])){
                $response->setError(true);
                $errorMessage=__('Country group with the same code already exists.'); 
            }

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $response->setError(true);
            $layout->initMessages();
            $response->setError(true);
            $response->setHtmlMessage($layout->getMessagesBlock()->getGroupedHtml());
        }
        if($response->getError() && $errorMessage){
            $this->messageManager->addError($errorMessage);
            $layout->initMessages();
            $response->setError(true);
            $response->setHtmlMessage($layout->getMessagesBlock()->getGroupedHtml());
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($response);
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
}
