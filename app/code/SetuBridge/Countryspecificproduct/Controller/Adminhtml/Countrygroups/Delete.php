<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

class Delete extends \Magento\Backend\App\Action
{
    protected $countrygroups;
    protected $cspHelper;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    ) 
    {
        parent::__construct($context);
        $this->countrygroups = $countrygroups;
    }
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $rowData = $this->countrygroups->load($id);
        try{
            $attributesData=['countrygroupstring' => null,'countrygroupid'=>null];
            $rowData->updateCspProductAttributes($id,$attributesData);
            $rowData->delete();
            $this->messageManager->addSuccess(
                __('Group have been deleted')
            );
        }catch(\Exception $e){
            $this->messageManager->addError(
                __($e->getMessage())
            );  
        }
        return $this->_redirect('*/*/index');
    }
    /**
    * Check delete Permission.
    *
    * @return bool
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
}