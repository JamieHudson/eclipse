<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Controller\Adminhtml\Countrygroups;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use SetuBridge\Countryspecificproduct\Model\ResourceModel\Countrygroups\CollectionFactory;

class MassDelete extends \Magento\Backend\App\Action
{

    /**
    * Massactions filter.
    *
    * @var Filter
    */
    protected $_filter;

    /**
    * @var CollectionFactory
    */
    protected $_collectionFactory; 
    /**
    * @var \SetuBridge\Countryspecificproduct\Model\Countrygroups
    */
    protected $countrygroups;

    /**
    * @param Context           $context
    * @param Filter            $filter
    * @param CollectionFactory $collectionFactory
    * @param \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \SetuBridge\Countryspecificproduct\Model\Countrygroups $countrygroups
    ) 
    {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->countrygroups = $countrygroups;
        parent::__construct($context);
    }
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());

        $recordDeleted = 0;
        try{
            $ids=$this->getRequest()->getParam('selected');
            if($collection->getSize()){
                foreach ($collection as $item) {
                    $model=$this->countrygroups->load($item->getId());
                    $attributesData=['countrygroupstring' => null,'countrygroupid'=>null];
                    $model->updateCspProductAttributes($item->getId(),$attributesData);
                    $model->delete();
                    $recordDeleted++;
                }
            }
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) have been deleted.', $recordDeleted)
            ); 
        }catch(\Exception $e){
            $this->messageManager->addError(
                __($e->getMessage())
            ); 
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    /**
    * Check delete Permission.
    *
    * @return bool
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SetuBridge_Countryspecificproduct::countrygroups_index');
    }
}