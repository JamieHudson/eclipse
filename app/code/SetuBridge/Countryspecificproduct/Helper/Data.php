<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Helper;
use Magento\Store\Model\Store;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_GENERAL_CONFIG_PATH='countryspecificproduct_general_section/general_group/%s';
    const XML_SHIPPING_SETTINGS_CONFIG_PATH='countryspecificproduct_general_section/shipping_validation/%s';
    const XML_PATH_GEOIP_TYPE='countryspecificproduct_general_section/geoip_database/type';
    protected $eavConfig;
    protected $remoteAddress;
    protected $store;
    protected $storeId;
    protected $currentCountry=null;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Model\Config $eavconfig,
        \Magento\Store\Model\StoreManagerInterface $storeManage,
        \SetuBridge\Countryspecificproduct\Model\GeoipFactory $geoipFactory
    ) {
        parent::__construct($context);
        $this->eavConfig = $eavconfig;
        $this->store = $storeManage->getStore();
        $this->storeId = $this->store->getId();
        $this->remoteAddress=$context->getRemoteAddress();
        $this->geoipFactory=$geoipFactory;
    }

    public function isActive(){
        return (bool) ($this->getConfigValue('enable_select') && !$this->botDetect() && !$this->isAllowedIp());
    }
    public function getAllowedIps(){
        $allowIps = explode(',',$this->getConfigValue('allow_ips'));
        $trimmedIps=array_map('trim',$allowIps);
        return $trimmedIps;
    }
    public function botDetect(){
        $bots=$this->getConfigValue('allow_bots');
        if(!$bots){
            return false;
        }
        $botsList = explode(",",$bots);
        if(!empty($botsList)){
            $regexp='/'.  implode("|", $botsList).'/';
            $ua=$_SERVER['HTTP_USER_AGENT'];
            if(preg_match($regexp, $ua))
            {
                return true;
            }
            return false;  
        }
        return false; 
    }
    protected function isAllowedIp(){
        $ip=$this->remoteAddress->getRemoteAddress();
        if(in_array($ip,$this->getAllowedIps())){
            return true;
        }
        return false;
    }
    private function getGeoType(){
        return $this->scopeConfig->getValue(self::XML_PATH_GEOIP_TYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->store);;
    }
    public function getConfigValue($path,$store = null)
    {   
        if(is_null($store)){
            $store=$this->store;
        }
        return $this->scopeConfig->getValue(sprintf(self::XML_GENERAL_CONFIG_PATH,$path), \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    public function getShippingValidationConfigValue($path,$store = null)
    {   
        if(is_null($store)){
            $store=$this->store;
        }
        return $this->scopeConfig->getValue(sprintf(self::XML_SHIPPING_SETTINGS_CONFIG_PATH,$path), \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    public function getStoreId(){
        return $this->storeId;
    }
    public function getCurrentCountry()
    {
        if(!$this->currentCountry){
            $ip=$this->remoteAddress->getRemoteAddress();
            try{
                if($this->getGeoType() == \SetuBridge\Countryspecificproduct\Model\Config\Source\Type::GEOIP_COUNTRY_DATABASE){
                    $location=$this->geoipFactory->create()->getCurrentLocation($ip);
                    $this->currentCountry=$location && $location->getCode() ? strtolower($location->getCode()):null;
                }else{
                    $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=$ip");
                    $countryname = (string)$xml->geoplugin_countryCode; 
                    $this->currentCountry=strtolower($countryname);  
                }
            }catch(\Exception $e){
                return null;
            }
        }
        return $this->currentCountry;
    }
    public function getcountryattribid()
    {
        $attribute= $this->eavConfig->getAttribute('catalog_product', "countrygroupid"); 
        return $attribute->getAttributeId();
    }

    public function getCountryStringAttribute()
    {
        return $this->eavConfig ->getAttribute('catalog_product', "countrygroupstring"); 
    }

}
