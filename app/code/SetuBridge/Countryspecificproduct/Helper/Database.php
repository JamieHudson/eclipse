<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

namespace SetuBridge\Countryspecificproduct\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
* GeoIP DATABASE helper
*/
class Database extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
    * Backend area code.
    *
    * @var string
    */
    const AREA_BACKEND = 'adminhtml';

    /**
    * XML config path database type
    */
    const XML_GEOIP_DATABASE_TYPE   = 'countryspecificproduct_general_section/geoip_database/database_type';

    /**
    * XML config path to database update info
    */
    const XML_GEOIP_UPDATE_DB       = 'countryspecificproduct_general_section/geoip_database/database_update';

    /**
    * URL database country update
    */
    const DB_COUNTRY_UPDATE_SOURCE  = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz';

    /**
    * URL database city update
    */
    const DB_CITY_UPDATE_SOURCE     = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz';

    /**
    * Path to the geoip lib
    */
    const DB_PATH = 'countryspecificproduct/geoip';

    /**
    * Suffix for archive backup file
    */
    const ARCHIVE_SUFFIX = '_backup_';

    /**
    * @var \Magento\Framework\App\State
    */
    protected $appState;

    /**
    * @param \Magento\Framework\App\Helper\Context $context
    * @param \Magento\Framework\App\State $appState
    */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\State $appState
    ) {
        $this->appState = $appState;
        parent::__construct($context);
    }
    /**
    * Get time of last DB update
    *
    * @return string
    */
    public function getLastUpdateTime()
    {
        $time = $this->scopeConfig->getValue(self::XML_GEOIP_UPDATE_DB);
        if (!$time) {
            return false;
        }
        return date('F d, Y / h:i', $time);
    }

    /**
    * Return source for database update
    *
    * @return string
    */
    public function getDbUpdateSource()
    {
        return self::DB_COUNTRY_UPDATE_SOURCE;
    }

    /**
    * Return temp path of updating file
    *
    * @return string
    */
    public function getTempUpdateFile()
    {
        $dbPath = $this->getDatabasePath();
        return $dbPath . '_temp.gz';
    }

    /**
    * Return full path to GeoIP database
    *
    * @return string
    */
    public function getDatabasePath()
    {
        $geoipLibPath = BP . DIRECTORY_SEPARATOR . 'pub' . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . self::DB_PATH;
        $cityPath = $geoipLibPath . DIRECTORY_SEPARATOR . 'GeoLite2-City.mmdb';
        $countryPath = $geoipLibPath . DIRECTORY_SEPARATOR . 'GeoLite2-Country.mmdb' ;
        return $countryPath ;
    }
}
