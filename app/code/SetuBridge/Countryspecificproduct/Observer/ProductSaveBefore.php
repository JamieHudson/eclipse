<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductSaveBefore implements ObserverInterface
{
    protected $request; 
    protected $countrygroups; 
    public function __construct(
        \SetuBridge\Countryspecificproduct\Model\CountrygroupsFactory $countrygroupsFactory
    ) {
        $this->countrygroupsFactory = $countrygroupsFactory;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $product = $event->getProduct();
        if(!empty($product->getCountrygroupid()))
        {
            $countrymodel=$this->countrygroupsFactory->create()->load($product->getCountrygroupid());
            $countrydata=$countrymodel->getData('countryselect')?$countrymodel->getData('countryselect'):'';
            $product->setCountrygroupstring($countrydata);
        }else{ 
            $product->setCountrygroupstring('');
            $product->setCountrygroupid('');
        }
        return $this; 
    }

}
