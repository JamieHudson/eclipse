<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use SetuBridge\Countryspecificproduct\Model\CartProcessor;

class ProcessCart implements ObserverInterface
{   
    protected $url; 
    protected $helper; 
    public function __construct(
        CartProcessor $cartProcessor,
        \SetuBridge\Countryspecificproduct\Helper\Data $helper
    ) {
        $this->cspHelper = $helper;
        $this->cartProcessor = $cartProcessor;;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($this->cspHelper->isActive()){
            $moduleName = $observer->getEvent()->getRequest()->getModuleName();
            $fullActionName = $observer->getEvent()->getRequest()->getFullActionName();
            $controller = $observer->getControllerAction();
            if($moduleName=='checkout_cart_index' || $fullActionName='checkout_index_index' || $fullActionName='onestepcheckout_index_index'){
                $this->cartProcessor->process();
            } 
        }
        return $this; 
    }

}
