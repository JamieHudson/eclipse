<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductControllerInit implements ObserverInterface
{   
    protected $url; 
    protected $helper; 
    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \SetuBridge\Countryspecificproduct\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->url = $url;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();        
        $cspGroupId=$product->getData('countrygroupid');
        $checkcountrystring=$product->getData('countrygroupstring');
        if($this->helper->isActive() && !empty($cspGroupId))
        {
            $checkcountryary=explode(",",$checkcountrystring);  
            if(!in_array($this->helper->getCurrentCountry(),$checkcountryary))
            {
                $backendselectcms=$this->helper->getConfigValue('cms_select');

                if($backendselectcms=="home")
                {
                    $cmsselectionurl=$this->url->getUrl();
                }
                else
                {
                    $cmsselectionurl=$this->url->getUrl($backendselectcms);
                }
                $observer->getControllerAction()
                ->getResponse()
                ->setRedirect($cmsselectionurl);
                return false;
            }        
        }
        return $this; 
    }

}
