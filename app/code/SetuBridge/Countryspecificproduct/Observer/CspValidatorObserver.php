<?php
/**
 * Product inventory data validator
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;

class CspValidatorObserver implements ObserverInterface
{
    /**
     * @var \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $quantityValidator
     */
    protected $quantityValidator;

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $quantityValidator
     */
    public function __construct(
        \SetuBridge\Countryspecificproduct\Model\Quote\Item\CspValidator $quantityValidator
    ) {
        $this->cspValidator = $quantityValidator;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {   
        $this->cspValidator->validate($observer);
    }
}
