<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use \SetuBridge\Countryspecificproduct\Model\Countrygroups;

class MassUpdateGroup implements ObserverInterface
{   
    protected $countryGroups; 
    public function __construct(
        Countrygroups $countryGroups
    ) {
        $this->countryGroups = $countryGroups;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $attributeData = $event->getAttributesData(); 
        if(array_key_exists('countrygroupid',$attributeData)){
            $this->countryGroups->load($attributeData['countrygroupid']);
            if($this->countryGroups->getId() && !empty($attributeData['countrygroupid'])){
                $attributeData['countrygroupstring']=$this->countryGroups->getCountryselect(); 
                $event->setAttributesData($attributeData);
            }else{
                $attributeData['countrygroupstring']=''; 
                $event->setAttributesData($attributeData); 
            }
        }
    }

}
