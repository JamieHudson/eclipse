<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductCollectionApplyLimitAfter implements ObserverInterface
{   
    protected $url; 
    protected $helper; 
    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \SetuBridge\Countryspecificproduct\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->url = $url;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $collection = $event->getCollection();
        $queryPart=$collection->getSelect()->getPart(\Magento\Framework\DB\Select::FROM);
        if($this->helper->isActive() && !isset($queryPart["at_countrygroupstring_default"]))
        {    
            $currentcountry= $this->helper->getCurrentCountry(); 
            $collection->addAttributeToFilter(
                [
                    ['attribute'=> 'countrygroupstring','finset' => $currentcountry],
                    ['attribute'=> 'countrygroupstring','eq' => ''],
                    ['attribute'=> 'countrygroupstring','null' => true]
                ]
            );
        }
        return $this; 
    }


}
