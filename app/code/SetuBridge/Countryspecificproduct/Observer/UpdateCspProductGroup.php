<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use SetuBridge\Countryspecificproduct\Model\Indexer\Product\Csp;

class UpdateCspProductGroup implements ObserverInterface
{
    /**
    * @var \Magento\Framework\Indexer\IndexerInterfaceFactory
    */  
    protected $indexerFactory; 

    public function __construct(
        \Magento\Framework\Indexer\IndexerInterfaceFactory $indexerFactory
    ) {
        $this->indexerFactory = $indexerFactory;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try{
            $this->indexerFactory->create()->load(Csp::CSP_INDEXER_ID)->invalidate();
        }catch(\Exception $e){
        }
    }

}
