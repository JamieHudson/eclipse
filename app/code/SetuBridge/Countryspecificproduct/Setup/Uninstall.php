<?php
/**
* Copyright © 2013 SetuBridge. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace SetuBridge\Countryspecificproduct\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Setup\CategorySetupFactory;

class Uninstall implements UninstallInterface
{

    protected $categorySetupFactory;

    public function __construct(CategorySetupFactory $categorySetupFactory)
    {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
    * Module uninstall code
    *
    * @param SchemaSetupInterface $setup
    * @param ModuleContextInterface $context
    * @return void
    */
    public function uninstall(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $connection = $setup->getConnection();
        $connection->dropTable($connection->getTableName('countryspecificproduct'));
        $categorySetupManager = $this->categorySetupFactory->create();
        $categorySetupManager->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'countrygroupid'
        );
        $categorySetupManager->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'countrygroupstring'
        );
        $setup->endSetup();
    }
}
