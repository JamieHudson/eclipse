<?php 
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface{
    /**
    * EAV setup factory
    *
    * @var EavSetupFactory
    */
    private $eavSetupFactory;
    private $productModel;
    private $attributeSets;
    private $productAction;
    /**
    * Init
    *
    * @param EavSetupFactory $eavSetupFactory
    */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets,
        \Magento\Catalog\Model\Product\Action $productAction,
        \Magento\Framework\App\State $state
    )
    {
        $state->setAreaCode(\Magento\Backend\App\Area\FrontNameResolver::AREA_CODE); 
        $this->eavSetupFactory = $eavSetupFactory;
        $this->productModel = $productModel;
        $this->attributeSets = $attributeSets;
        $this->productAction = $productAction;
    }
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $countrygroupcode="countrygroupid";
        $countrylabel="Country Group";
        $attribute_type="textarea";
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $countrygroupcode, [
            'type'       => 'text',
            'input'      => 'select',
            'label'      => $countrylabel,
            'visible' => 1,  
            'sort_order' => 10,
            'visible' => 1,
            'required' => 0,
            'user_defined' => 1,
            'searchable' => 1,
            'filterable' => 0,
            'comparable' => 0,
            'visible_on_front' => 0,
            'visible_in_advanced_search' => 1,
            'is_html_allowed_on_front' => 0,
            'is_configurable' => 0,
            'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            //'backend'    => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            'source' => 'SetuBridge\Countryspecificproduct\Model\Config\Source\Countrygroups',
            'is_user_defined'=>1,
            'is_html_allowed_on_front' => '1',
            'is_visible_on_front' => '0',
            'used_in_product_listing' => '1',
            'default_value_textarea'=>'',
            'is_wysiwyg_enabled' => '0',
        ]);

        $countrygroupstring="countrygroupstring";
        $countrystringlabel="Country Group String";
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $countrygroupstring, [
            'type'       => 'text',
            'input'      => $attribute_type,
            'label'      => $countrystringlabel,
            'visible' => 1,  
            'sort_order' => 10,
            'visible' => 1,
            'required' => 0,
            'user_defined' => 1,
            'searchable' => 0,
            'filterable' => 0,
            'comparable' => 0,
            'visible_on_front' => 0,
            'visible_in_advanced_search' => 0,
            'is_html_allowed_on_front' => 1,
            'is_configurable' => 0,
            'global'     => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            //'backend'    => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            'is_user_defined'=>1,
            'is_visible_on_front' => '0',
            'used_in_product_listing' => '1',
            'default_value_textarea'=>'',
            'is_wysiwyg_enabled' => '0',
        ]);
        /* create attribute end */
        /* assign attribute to attribute set start */
        foreach (["countrygroupid","countrygroupstring"] as $attCode )
        {
            $entityType = $this->productModel->getResource()->getEntityType();
            $collection = $this->attributeSets->setEntityTypeFilter($entityType->getId());
            foreach ($collection as $attributeSet) {
                // $attributeGroupId = $eavSetup->getDefaultAttributeGroupId('catalog_product',$attributeSet->getId());
                $eavSetup->addAttributeGroup(\Magento\Catalog\Model\Product::ENTITY, $attributeSet->getAttributeSetName(), $countrylabel, 1000); 
                $attributeGroupId = $eavSetup->getAttributeGroupId(\Magento\Catalog\Model\Product::ENTITY, $attributeSet->getId(), $countrylabel);
                $eavSetup->addAttributeToSet(\Magento\Catalog\Model\Product::ENTITY, $attributeSet->getId(), $attributeGroupId, $attCode);
            }
        }

        /* set null value for two custom attribue start */
        $collectionproduct = $this->productModel->getCollection()->getAllIds();
        if(!empty($collectionproduct)){
            $this->productAction->updateAttributes($collectionproduct, array('countrygroupid' => null), \Magento\Store\Model\Store::DEFAULT_STORE_ID);
            $this->productAction->updateAttributes($collectionproduct, array('countrygroupstring' => null), \Magento\Store\Model\Store::DEFAULT_STORE_ID);
            // set null value for two custom attribue end */   
        }
        $setup->endSetup();
    }
}
