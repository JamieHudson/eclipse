<?php
/**
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace SetuBridge\Countryspecificproduct\Plugin\Checkout\Model;

use SetuBridge\Countryspecificproduct\Model\Validator;
use SetuBridge\Countryspecificproduct\Helper\Data as CspHelper;
use Magento\Quote\Api\CartRepositoryInterface;

class GuestCspValidation
{
    private $validator;
    private $cspHelper;
    private $cartRepository;
    public function __construct(
        Validator $validator,
        CspHelper $cspHelper,
        CartRepositoryInterface $cartRepository,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory
    ) {
        $this->validator = $validator;
        $this->cspHelper = $cspHelper;
        $this->cartRepository = $cartRepository;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
    }

    /**
    * @param \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject
    * @param string $cartId
    * @param string $email
    * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
    * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
    * @return void
    * @SuppressWarnings(PHPMD.UnusedFormalParameter)
    */
    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $this->validateCsp($cartId);
    }

    /**
    * @param \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject
    * @param string $cartId
    * @param string $email
    * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
    * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
    * @return void
    * @SuppressWarnings(PHPMD.UnusedFormalParameter)
    */
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $this->validateCsp($cartId);
    }
    private function validateCsp($cartId){
        try{
            $quote=$this->_getQuote($cartId);
            if($this->cspHelper->getShippingValidationConfigValue('enable_select') && $this->cspHelper->getShippingValidationConfigValue('valiadte_with_shipping') && $quote->getShippingAddress() && $quote->getShippingAddress()->getCountryId()){ 
                foreach($quote->getAllItems() as $item){
                    if (!$this->validator->isValidProductForCsp($item->getProductId(),$quote->getShippingAddress()->getCountryId())) {
                        throw new \Magento\Framework\Exception\CouldNotSaveException(
                            __($this->cspHelper->getShippingValidationConfigValue('validation_message'))
                        );
                        break;  
                    }
                }  
            }  
        }catch(\Exception $e){
        }
    }
    private function _getQuote($cartId){
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->cartRepository->getActive($quoteIdMask->getQuoteId());
    }
}
