<?php
/**
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace SetuBridge\Countryspecificproduct\Plugin\Checkout\Model;

use SetuBridge\Countryspecificproduct\Model\Validator;
use SetuBridge\Countryspecificproduct\Helper\Data as CspHelper;
use Magento\Quote\Api\CartRepositoryInterface;
/**
* Class Validation
*/
class CspValidation
{
    private $validator;
    private $cspHelper;
    private $cartRepository;
    public function __construct(
        Validator $validator,
        CspHelper $cspHelper,
        CartRepositoryInterface $cartRepository

    ) {
        $this->validator = $validator;
        $this->cspHelper = $cspHelper;
        $this->cartRepository = $cartRepository;
    }
    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $this->validateCsp($cartId);
    }
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $this->validateCsp($cartId);
    }
    private function validateCsp($cartId){
        $quote=$this->cartRepository->getActive($cartId);
        if($this->cspHelper->getConfigValue('enable_select') && $this->cspHelper->getShippingValidationConfigValue('valiadte_with_shipping') && $quote->getShippingAddress() && $quote->getShippingAddress()->getCountryId()){ 
            foreach($quote->getAllItems() as $item){
                if (!$this->validator->isValidProductForCsp($item->getProductId(),$quote->getShippingAddress()->getCountryId())) {
                    throw new \Magento\Framework\Exception\CouldNotSaveException(
                        __($this->cspHelper->getShippingValidationConfigValue('validation_message'))
                    );
                    break;  
                }
            }  
        } 


    }
}
