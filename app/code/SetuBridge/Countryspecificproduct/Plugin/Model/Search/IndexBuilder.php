<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Plugin\Model\Search;

use Magento\Framework\App\ResourceConnection;
/**
* IndexBuilder
*/
class IndexBuilder
{
    protected $helper;
    /**
    * @var Resource
    */
    private $resource;
    public function __construct(
        \SetuBridge\Countryspecificproduct\Helper\Data $helper,
        ResourceConnection $resource
    ) {
        $this->helper = $helper;
        $this->resource = $resource;
    }
    public function afterBuild($subject, $select) {
        $queryPart=$select->getPart(\Magento\Framework\DB\Select::FROM);
        if($this->helper->isActive() && !isset($queryPart["cpetcon"]))
        {   
            $attribute=$this->helper->getCountryStringAttribute();
            $fkEntityId=$attribute->getEntityIdField();
            $countryattribid=$attribute->getAttributeId();
            $currentcountry=$this->helper->getCurrentCountry();
            $storeId=$this->helper->getStoreId(); 
            $defaultStoreId=\Magento\Store\Model\Store::DEFAULT_STORE_ID; 
            $select->joinLeft( array('cpetcon'=> $this->resource->getTableName('catalog_product_entity_text')), "(`cpetcon`.`$fkEntityId` = `search_index`.`entity_id`) AND (`cpetcon`.`attribute_id`=$countryattribid)  AND (cpetcon.store_id=$storeId) ",[])
            ->joinInner( array('cpetcon_default'=> $this->resource->getTableName('catalog_product_entity_text')), "(`cpetcon_default`.`$fkEntityId` = `search_index`.`entity_id`) AND (`cpetcon_default`.`attribute_id`=$countryattribid) AND (cpetcon_default.store_id=$defaultStoreId) ", []);
            $select->where("(FIND_IN_SET('$currentcountry', IF(cpetcon.value_id > 0, cpetcon.value, cpetcon_default.value))) OR (IF(cpetcon.value_id > 0, cpetcon.value, cpetcon_default.value) = '') OR (IF(cpetcon.value_id > 0, cpetcon.value, cpetcon_default.value) IS NULL)");
        }
        return $select;
    }
}