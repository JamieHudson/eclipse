<?php
/**
* Setubridge Technolabs
* http://www.setubridge.com/
* @author SetuBridge
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/
namespace SetuBridge\Countryspecificproduct\Plugin\Http;

use SetuBridge\Countryspecificproduct\Helper\Data;
/**
* Context Plugin
*/
class Context
{
    const CONTEXT_CSP_STR='SETUBRIDGE_CSP';
    protected $dataHelper;
    public function __construct(
        Data $dataHelper
    ) 
    {
        $this->dataHelper = $dataHelper;
    }
    public function beforeGetVaryString(\Magento\Framework\App\Http\Context $subject){
        if($country=$this->dataHelper->getCurrentCountry()){
            $subject->setValue(self::CONTEXT_CSP_STR,$country,'none');
        }
    }

}
