define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate',
    'priceBox'
], function ($) {
    'use strict';

    $.widget('bss.grouped', {
        options: {
            show_option: false,
            json: {}
        },

        _create: function () {
            this._initGrouped()
            this._initPriceBox();
        },

        _initGrouped: function () {
            var options = this.options;
            var optionCache = {},
                showOption = options.show_option;

            $.validator.addMethod(
                'required-option-field',
                function (value) {
                    return (value !== '');
                },
                $.mage.__('This is a required field.')
            );

            $('#super-product-table input.qty').change(function () {
                var childId,
                    element,
                    qty = $(this).val();
                childId = $(this).attr('data-product-id');
                element = $('#super-product-table .bss-gpo-child-product-id-'+childId);
                if (!showOption) {
                    if (qty != 0) {
                        element.show();
                    } else {
                        $('#super-product-table .bss-gpo-child-product-id-'+childId).hide();
                    }
                } else {
                    element.find('.product-custom-option').each(function () {
                        var $this = $(this);
                        if (qty == 0) {
                            if ($this.hasClass('required')) {
                                $this.addClass('bss-required');
                                $this.removeClass('required');
                            }
                            if (typeof $this.attr('data-validate') !== typeof undefined && $this.attr('data-validate') !== false) {
                                var attrValue = $this.attr('data-validate');
                                $this.attr('data-validate-bss', attrValue);
                                $this.removeClass('required-option-field');
                                $this.removeAttr('data-validate');
                            }
                            $this.removeClass('mage-error');
                        } else {
                            if ($this.hasClass('bss-required')) {
                                $this.addClass('required');
                                $this.removeClass('bss-required');
                            }
                            if (typeof $this.attr('data-validate-bss') !== typeof undefined && $this.attr('data-validate-bss') !== false) {
                                var attrValue2 = $this.attr('data-validate-bss');
                                $this.attr('data-validate', attrValue2);
                                $this.addClass('required-option-field');
                                $this.removeAttr('data-validate-bss');
                            }
                        }
                    });
                }
            });

            $('#super-product-table input.qty').change();
        },

        _initPriceBox: function () {
            var options = this.options;
            var priceBoxes = $('[data-role=priceBox]');

            priceBoxes = priceBoxes.filter(function (index, elem) {
                return !$(elem).find('.price-from').length;
            });

            priceBoxes.priceBox({'priceConfig': options.json});
        }
    });

    return $.bss.grouped;
});