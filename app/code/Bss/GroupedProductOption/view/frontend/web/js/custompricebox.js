define(
    [
        'jquery',
        'Magento_Catalog/js/price-utils',
        'underscore',
        'mage/template',
        'Magento_Catalog/js/price-box',
        'jquery/ui'
    ],
    function ($, utils, _, mageTemplate) {
        'use strict';
        var bssItemId = '';
        $.widget('bss_groupedproductoption.priceBox', $.mage.priceBox, {
            groupItemPrices: [],
            originGroupItemPrices: [],
            groupElement: [],

            _create: function createPriceBox()
            {
                var box = this.element,
                    self = this;

                self.cache = {};
                self._setDefaultsFromPriceConfig();
                self._setDefaultsFromDataSet();
                $('.bss-gpo-child-product-info .product-custom-option').change(function () {
                    var id = this.closest('.bss-gpo-custom-option');
                    if ($(id).length) {
                        bssItemId = $(id).attr('data-product-id');
                    } else {
                        bssItemId = '';
                    }
                });
                box.on('reloadPrice', self.reloadPrice.bind(self));
                box.on('updatePrice', self.onUpdatePrice.bind(self));

                this.groupItemPrices[this.options.productId] = utils.deepClone(this.options.prices);
                this.originGroupItemPrices[this.options.productId] = utils.deepClone(this.options.prices);
                this.groupElement[this.options.productId] = this.element;
            },

            updatePrice: function updatePrice(newPrices)
            {
                var prices = this.cache.displayPrices,
                    additionalPrice = {},
                    pricesCode = [];

                this.cache.additionalPriceObject = this.cache.additionalPriceObject || {};

                if (newPrices) {
                    $.extend(this.cache.additionalPriceObject, newPrices);
                }

                if (!_.isEmpty(additionalPrice)) {
                    pricesCode = _.keys(additionalPrice);
                } else if (!_.isEmpty(prices)) {
                    pricesCode = _.keys(prices);
                }

                _.each(this.cache.additionalPriceObject, function (additional, key) {

                    var a = key.split('[');
                    var b = Object.keys(newPrices)[0];
                    if (b) {
                        var c = b.split('[');
                        if (a[0] != c[0]) {
                            return;
                        }
                    }

                    if (additional && !_.isEmpty(additional)) {
                        pricesCode = _.keys(additional);
                    }
                    _.each(pricesCode, function (priceCode) {
                        var priceValue = additional[priceCode] || {};
                        priceValue.amount = +priceValue.amount || 0;
                        priceValue.adjustments = priceValue.adjustments || {};

                        additionalPrice[priceCode] = additionalPrice[priceCode] || {
                                'amount': 0,
                                'adjustments': {}
                            };
                        additionalPrice[priceCode].amount =  0 + (additionalPrice[priceCode].amount || 0)
                            + priceValue.amount;
                        _.each(priceValue.adjustments, function (adValue, adCode) {
                            additionalPrice[priceCode].adjustments[adCode] = 0
                                + (additionalPrice[priceCode].adjustments[adCode] || 0) + adValue;
                        });
                    });
                });

                if (_.isEmpty(additionalPrice)) {
                    this.cache.displayPrices = utils.deepClone(this.options.prices);
                } else {
                    _.each(additionalPrice, function (option, priceCode) {
                        if (bssItemId != '') {
                            var origin = this.originGroupItemPrices[bssItemId][priceCode] || {},
                                final = this.groupItemPrices[bssItemId][priceCode] || {};
                        } else {
                            var origin = this.options.prices[priceCode] || {},
                                final = prices[priceCode] || {};
                        }
                        option.amount = option.amount || 0;
                        origin.amount = origin.amount || 0;
                        origin.adjustments = origin.adjustments || {};
                        final.adjustments = final.adjustments || {};

                        final.amount = 0 + origin.amount + option.amount;
                        _.each(option.adjustments, function (pa, paCode) {
                            final.adjustments[paCode] = 0 + (origin.adjustments[paCode] || 0) + pa;
                        });
                    }, this);
                }

                this.element.trigger('reloadPrice');
            },

            reloadPrice: function reDrawPrices()
            {
                var priceFormat = (this.options.priceConfig && this.options.priceConfig.priceFormat) || {},
                    priceTemplate = mageTemplate(this.options.priceTemplate);

                if (bssItemId != '') {
                    var prices = this.groupItemPrices[bssItemId];
                } else {
                    var prices = this.cache.displayPrices;
                }

                _.each(prices, function (price, priceCode) {
                    price.final = _.reduce(price.adjustments, function (memo, amount) {
                        return memo + amount;
                    }, price.amount);

                    price.formatted = utils.formatPrice(price.final, priceFormat);
                    if (this.cache && bssItemId) {
                        if (this.groupElement[bssItemId].attr('data-product-id') != bssItemId) {
                            return;
                        }
                    }

                    if (bssItemId != '') {
                        var element = this.groupElement[bssItemId];
                    } else {
                        var element = this.element;
                    }
                    $('[data-price-type="' + priceCode + '"]', element).html(priceTemplate({data: price}));
                }, this);
            },

        });

        return $.bss_groupedproductoption.priceBox;
    }
);
