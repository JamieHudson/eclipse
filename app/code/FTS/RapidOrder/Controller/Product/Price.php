<?php

namespace FTS\RapidOrder\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;

class Price extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\LayoutFactory
    */
    protected $layoutFactory;

    /**
    * @var \Magento\Framework\Controller\Result\JsonFactory
    */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->layoutFactory = $layoutFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultJsonFactory->create();

        // Validate product was found
        if (!$this->getProduct()) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_NOT_FOUND);
            return $result;
        }

        // Validate all required paramaters are present
        if (!$this->validateRequest()) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            return $result;
        }

        // Return rendered product
        $layout = $this->layoutFactory->create();
        $listProductBlock = $layout->createBlock("\Hudson\CustomerPricing\Block\Product\ListProduct");
        $price = $customPrice = $listProductBlock->getYourProductPrice($this->getProduct());

        //if (empty($customPrice)) {
        //    $price = $this->getProduct()->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
        //}
        if (!empty($this->_request->getParam('length'))) {
	    $length = $this->_request->getParam('length');
            $price = str_replace('£', '', $price) * $length;
        }

        return $result->setData(['price' => "&pound;" . number_format($price, 2)]);
    }

    public function getProduct()
    {
        if ($this->product) return $this->product;
        $request = $this->getRequest()->getParams();
        try {
            return $this->product = $this->productRepository->get($request['sku']);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function validateRequest()
    {
        return !empty($this->_request->getParam('sku'))
            && $this->getProduct()->getIsSalable()
            && $this->getProduct()->getTypeId() == 'simple'
            && $this->getProduct()->getQuantityAndStockStatus()['qty'] != 0
            && (empty($this->getProduct()->getOptions()) || (!empty($length = $this->_request->getParam('length')) && is_numeric($length)));
    }
}

