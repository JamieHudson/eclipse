<?php

namespace FTS\RapidOrder\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;

class Render extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\LayoutFactory
    */
    protected $layoutFactory;

    /**
    * @var \Magento\Framework\Controller\Result\RawFactory
    */
    protected $resultRawFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultRawFactory->create();

        // Validate product was found
        if (!$this->getProduct()) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_NOT_FOUND);
            return $result;
        }

        // Validate all required paramaters are present
        if (!$this->validateRequest()) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            return $result;
        }

        // Return rendered product
        $layout = $this->layoutFactory->create();
        $listProductBlock = $layout->createBlock("\Hudson\CustomerPricing\Block\Product\ListProduct");
        $block = $layout->createBlock('\FTS\RapidOrder\Block\Product')
            ->setTemplate('FTS_RapidOrder::product.phtml')
            ->setProduct($this->getProduct())
            ->setListProductBlock($listProductBlock)
	    ->toHtml();

	return $result->setContents($block);

    }

    public function getProduct()
    {
        if ($this->product) return $this->product;
	$request = $this->getRequest()->getParams();

        try {
            return $this->product = $this->productRepository->get($request['sku']);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function validateRequest()
    {
        return !empty($this->_request->getParam('sku'))
            && $this->getProduct()->getIsSalable()
            && $this->getProduct()->getTypeId() == 'simple'
            && $this->getProduct()->getQuantityAndStockStatus()['qty'] != 0;
    }
}

