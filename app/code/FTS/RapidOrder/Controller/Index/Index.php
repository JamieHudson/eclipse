<?php

namespace FTS\RapidOrder\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;

class Index extends \Magento\Framework\App\Action\Action
{
    /** @var PageFactory */
    protected $pageFactory;

    /** @var Session */
    protected $session;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Session $session
    ) {
        $this->pageFactory = $pageFactory;
        $this->session = $session;
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->session->isLoggedIn()) {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login');
            return $resultRedirect;
        }

        $resultPage = $this->pageFactory->create();
        return $resultPage;
    }
}

