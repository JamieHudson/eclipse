<?php

namespace FTS\RapidOrder\Controller\Cart;

use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Quote\Model\Quote as Quote;
use Magento\Framework\App\Action\Context;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultRawFactory;

    protected $cart;
    
    protected $quote;

    protected $productRepository;

    protected $groupedProductFactory;

    protected $_messageManager;

    public function __construct(
        Context $context,
        CustomerCart $cart,
        Quote $quote,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\GroupedProduct\Model\Product\Type\GroupedFactory $groupedProductFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->cart = $cart;
        $this->quote = $quote;
        $this->resultRawFactory = $resultRawFactory;
        $this->productRepository = $productRepository;
        $this->groupedProductFactory = $groupedProductFactory;
        $this->productFactory = 
        $this->_messageManager = $messageManager;
    }

    public function execute()
    {
        $params = $this->_request->getParams();
            
       	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cam.log');
       	$logger = new \Zend\Log\Logger();
       	$logger->addWriter($writer);
        $logger->info(print_r($params,true));

        if (array_key_exists('sku', $params))
        {
            foreach($params['sku'] as $index => $sku) {
                
                $logger->info('Product: '.$sku);
                // Validate quantity is set and load product
                if (!isset($params['qty'][$index])){
                    $logger->info('bad qty request');
                    return $this->badRequest();
                }
                
                $product = $this->productRepository->get($sku);
                
                // Check for product option
                if ($options = $product->getOptions()) {
                    // Validate option is set

                    // SMCN uncommented the next three lines
                    if (!isset($params['length'][$index])) {
                        $logger->info('bad length request');
                        return $this->badRequest();
                    }
                }
                
                // Reload parent grouped product
                $groupedProductType = $this->groupedProductFactory->create();
                $parents = $groupedProductType->getParentIdsByChild($childProductId = $product->getId());
                if (!empty($parents)) {
                    $productParent = $this->productRepository->getById($parents[0]);
                }

                // Build fake params to match the traditional add to cart
		$mageParams = [
                    "product" => $product->getId(),
                    "qty" => $params['qty'][$index]
       	        ];
	
		if ($parents) {
                    // Set group product option related stuff (link to parent in cart)
                    $mageParams['super_group'][$childProductId] = $params['qty'][$index];
                    $mageParams['super_product_config'] = [
                        'product_type' => $productParent->getTypeId(),
                        'product_id' => $parents[0]
                    ];
		}
                
		// Checks for the "length(m)" custom option, and uses that as the user input.
                if (isset($params['length'][$index])) {
                    foreach($options as $i => $item) {
                        $checkType = $options[$i]->getTitle();
                        if ($checkType == "Length(m)"){
                            $mageParams["options"][$options[$i]->getOptionId()] = $params['length'][$index];
                        }
                    }
		}
                
                // Fake the request for anything (i.e. hudson_custompricing) that inspects it directly
                $this->_request->setParams($mageParams);
                
                $this->cart->addProduct($product, $mageParams);
                
                $this->_eventManager->dispatch(
                    'checkout_cart_add_product_complete',
                    ['product' => $product, 'request' => $this->_request, 'response' => $this->getResponse()]
                );
                $logger->info(print_r($mageParams, true));

                $this->cart->save();
            }
            
            
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart');
            return $resultRedirect;
        }
        else
        {
            $message = 'Please enter a valid Product Name/Sku...';
            $this->_messageManager->addError($message);
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rapidorder');
            
            $logger->info('GTE 2');
            return $resultRedirect;
	}

    }

    private function badRequest()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultRawFactory->create();
        $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
        return $result;
    }
}
