<?php

namespace FTS\RapidOrder\Controller\Ajax;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Helper\Stock as StockHelper;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Search\Model\QueryFactory;

class Search extends \Magento\Framework\App\Action\Action
{
    /** @var Session */
    protected $session;

    protected $searchQueryFactory;

    protected $stockHelper;

    public function __construct(
        Context $context,
        Session $session,
        QueryFactory $searchQueryFactory,
        StockHelper $stockHelper,
        ProductRepositoryInterface $productRepository
    ) {
        $this->session = $session;
        $this->searchQueryFactory = $searchQueryFactory;
        $this->stockHelper = $stockHelper;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->session->isLoggedIn()) {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login');
            return $resultRedirect;
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($this->getResults());
        return $resultJson;
    }

    public function getResults()
    {
        $results = [];
        $collection = $this->searchQueryFactory->get()
            ->getSearchCollection()
            ->addAttributeToSelect('name')
            //->addAttributeToSelect('is_in_stock')
            ->addSearchFilter($this->_request->getParam("q"))
            ->setCurPage(0)
            ->setPageSize(20);
        $this->stockHelper->addIsInStockFilterToCollection($collection);
        $collection->load();


        foreach ($collection as $product) {
            // List simple products only
            if ($product->getTypeId() != "simple") continue;

            // Reload via repository to get options and stock data
            $product = $this->productRepository->get($product->getSku());

            // Don't list any products not directly salable
            if (!$product->getIsSalable()) continue;

            // Don't list any products out of stock
            if ($product->getQuantityAndStockStatus()['qty'] == 0) continue;

            $name = $product->getName();

            // Append info about length to product name
            if ($options = $product->getOptions()) {
                $name .= " (Custom length";
                $name .= $product->getCuttingcharge() ? " + cutting charge" : ", no cutting charge";
                $name .= ")";
            } elseif ($product->getUnit() == "MTR") {
                $name .= " (Length {$product->getPacksize()} meters)";
            }

            $results[] = [
                'sku' => $product->getSku(),
                'bareName' => $product->getName(),
                'name' => $name,
                'customLength' => !empty($options),
                'fixedLength' => empty($options) && $product->getUnit() == "MTR" ? $product->getPacksize() : ''
            ];
        }
        return $results;
    }
}