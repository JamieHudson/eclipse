<?php

namespace FTS\RapidOrder\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Product extends Template
{
    public function getFormattedPrice()
    {
        $price = $customPrice = $this->getListProductBlock()->getYourProductPrice($this->getProduct());
        if (empty($customPrice)) {
            $price = $this->getProduct()->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
        }
	
	if (is_string($price)) {
		$price = (float) substr($price, 2);
	}
	
        return "&pound;" . number_format($price, 2);
    }
}
