/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            rapidOrderSearch: 'FTS_RapidOrder/js/fts-rapidorder-search',
            rapidOrderResult: 'FTS_RapidOrder/js/fts-rapidorder-result'
        }
    }
};
