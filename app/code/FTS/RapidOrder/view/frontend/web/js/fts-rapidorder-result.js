/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Modified for FTS_RapidOrder non-form usage
 */

/**
 * @api
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('fts.rapidOrderResult', {
        options: {},

        _create: function() {
            // Set up events
            // Delete button click
            this.deleteElem = this.element.find('.fro-delete-button');
            this.deleteElem.click($.proxy(function() {
                this.element.remove();
            }, this));

            // Set default price
            this.priceElem = this.element.find('span.price');
            this.setPrice(this.options.price);

            // Update price on length change
            this.lengthElem = this.element.find('.length-input');
            this.lengthElem.keyup($.proxy(this.updatePrice, this));
        },

        updatePrice: function() {
            $.get(this.options.priceUpdateUrl, {
                'sku': this.options.sku,
                'length': this.lengthElem.val()
            }, $.proxy(function(data) {
                this.setPrice(data.price);
            }, this));
        },

        setPrice: function(price) {
            this.priceElem.html(price);
        }
    });

    return $.fts.rapidOrderResult;
});
