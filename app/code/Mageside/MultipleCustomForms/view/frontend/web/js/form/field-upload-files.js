/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
define([
    'jquery',
    'Magento_Ui/js/core/app'
], function ($, app) {
    'use strict';

    $.widget('mageside.fieldUploadFiles', {

        options: {
            formSelector: '#custom-form',
            uploaderConfig: {}
        },

        _create: function() {
            var self = this;
            $(this.options.formSelector).on('customFormInitFields', function (event) {
                self.initFileUploader();
            }.bind(this));
        },

        initFileUploader: function () {
            app(this.options.uploaderConfig, false);
        }
    });

    return $.mageside.fieldUploadFiles;
});
