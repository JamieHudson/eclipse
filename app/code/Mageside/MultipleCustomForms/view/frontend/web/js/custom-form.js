/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
define([
    'jquery',
    'underscore',
    'mage/translate',
    'Magento_Customer/js/customer-data',
    'jquery/ui',
    'Magento_Ui/js/modal/modal',
    'mage/validation',
    'Magento_Ui/js/lib/view/utils/async',
    'Mageside_MultipleCustomForms/js/form/agreements',
    'Mageside_MultipleCustomForms/js/form/field-upload-files',
    'Mageside_MultipleCustomForms/js/form/field-recaptcha'
], function($, _, $t, customerData) {
    "use strict";

    $.widget('mageside.customForm', {

        options: {
            loaderEnabled: true,
            afterSubmit: 'redirect',
            successMessage: 'Thank you! Your request is sent.',
            formContainerSelector: '.block-custom-form',
            formContentSelector: '.block-content',
            messagesBlockSelector: '.block-messages',
            reCaptcha: {
                selector: '.field.recaptcha',
                show: 'enabled',
                inputSelector: 'g-recaptcha-response',
                disableSubmitButton: false
            },
            display: 'static',
            modal: {
                title: 'Custom form',
                prefixTriggerName: 'showCustomForm_',
                formCode: null,
                class: 'custom-form-modal',
                buttonSelector: '#custom-form-open-modal-button'
            }
        },
        modal: null,

        _create: function() {
            if (this.options.display !== 'static') {
                this.initModal();
            } else {
                this.element.trigger('customFormInitFields');
            }

            this._bindSubmit();
            this.prepareFields();
        },

        _bindSubmit: function() {
            var self = this;
            this.element.on('submit', function(e) {
                e.preventDefault();
                self.submitForm($(this));
            });
        },

        isReCaptchaEnabled: function () {
            var customer = customerData.get('customer');
            return this.options.reCaptcha.show === 'enabled'
                || (this.options.reCaptcha.show === 'only_for_guests' && !customer().firstname);
        },

        prepareFields: function () {
            var $form = $(this.element);
            var $saveButton = $form.find('button.save');
            if (this.isReCaptchaEnabled()) {
                $(this.options.reCaptcha.selector).show();
                if (this.options.reCaptcha.disableSubmitButton) {
                    $saveButton.addClass('disabled');
                    $form.on('msRecaptchaReady', function (event) {
                        $saveButton.removeClass('disabled');
                    });
                }
            }
        },

        validateForm: function () {
            var $form = this.element,
                validationConfig = this.isReCaptchaEnabled()
                    ? {ignore: ":hidden:not(.recaptcha-input, .file-input, input[name^='validate_datetime_'])"}
                    : {ignore: ":hidden:not(.file-input, input[name^='validate_datetime_'])"};
            var formValidator = $form ? $form.data('validator') : null;
            if (!formValidator) {
                $form.validation(
                    _.extend({
                        errorPlacement: function (error, element) {
                            var validationBlock = element.parents('.validation-block');
                            if (validationBlock.length) {
                                validationBlock.siblings(this.errorElement + '.' + this.errorClass).remove();
                                validationBlock.after(error);
                            } else {
                                element.after(error);
                            }
                        }
                    }, validationConfig)
                );
            }

            return $form.validation('isValid');
        },

        submitForm: function (form) {
            var self = this;

            if (!this.validateForm()) {
                return;
            }

            if (form.has('input[type="file"]').length && form.find('input[type="file"]').val() !== '') {
                self.element.off('submit');
                form.submit();
            } else {
                self.ajaxSubmit(form);
            }
        },

        ajaxSubmit: function(form) {
            var self = this;

            self.element.find(self.options.messagesBlockSelector).html('');

            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                type: 'post',
                dataType: 'json',
                beforeSend: function() {
                    if (self.options.loaderEnabled) {
                        $('body').trigger('processStart');
                    }
                },
                success: function(response) {
                    if (self.options.loaderEnabled) {
                        $('body').trigger('processStop');
                    }

                    if (self.options.afterSubmit === 'message' || response.error) {
                        var message = response.messages || self.options.successMessage;
                        self.element
                            .parents(self.options.formContainerSelector)
                            .find(self.options.messagesBlockSelector)
                            .html(message);
                    }

                    if (!response.error) {
                        if (self.options.afterSubmit === 'redirect') {
                            window.location = response.redirectUrl || self.options.redirectUrl || '/';
                        } else {
                            self.element
                                .parents(self.options.formContentSelector)
                                .remove();
                        }
                    }
                }
            });
        },

        initModal: function () {
            var self = this;
            var $customFormModal = self.element.parents(self.options.formContainerSelector);

            $customFormModal.modal({
                title: self.options.modal.title,
                modalClass: self.options.modal.class,
                buttons: []
            });

            this.modal = $customFormModal;

            $(this.options.modal.buttonSelector).on('click', function (event) {
                self.showModal();
            }.bind(this));

            var triggerName = this.options.modal.prefixTriggerName + this.options.modal.formCode;
            $('body').on(triggerName, function (event) {
                self.showModal();
            }.bind(this));
        },

        showModal: function () {
            this.element.trigger('customFormInitFields');
            this.modal.modal('openModal');
        }
    });

    return $.mageside.customForm;
});
