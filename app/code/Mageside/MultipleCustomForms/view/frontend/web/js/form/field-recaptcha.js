/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
define([
    'jquery',
    'recaptcha'
], function ($) {
    'use strict';

    $.widget('mageside.fieldRecaptcha', {

        options: {
            widgetId: '',
            widgetPrefix: 'g-recaptcha-',
            inputSelectorPrefix: '#g-recaptcha-response-',
            recaptchaPublicKey: '',
            theme: 'light',
            callbacks: {
                onLoad: 'msOnLoadCallback',
                onVerify: 'msOnVerifyCallback',
                onExpire: 'msOnExpireCallback',
                onReset: 'msOnReset'
            }
        },
        widget: null,
        initialized: false,

        _create: function() {
            this._bind();
            this.onRender();
        },

        _bind:function () {
            var self = this;

            $(this.element).parents('form').on('customFormInitFields', function (event) {
                self.initialized = true;
                self.onRender();
            }.bind(this));

            $('body')
                .on(
                    this.options.callbacks.onLoad,
                    function (event) {
                        self.onRender();
                    }.bind(this)
                )
                .on(
                    this.options.callbacks.onVerify + this.options.widgetId,
                    function (event, response) {
                        self.onVerify(response);
                    }.bind(this)
                )
                .on(
                    this.options.callbacks.onExpire + this.options.widgetId,
                    function (event) {
                        self.onExpire();
                    }.bind(this)
                )
                .on(
                    this.options.callbacks.onReset,
                    function (event, widgetId) {
                        self.onReset(widgetId);
                    }.bind(this)
                );

        },

        onRender: function () {
            if (window.recaptchaLoaded && this.widget === null && this.initialized) {
                this.widget = grecaptcha.render(this.options.widgetPrefix + this.options.widgetId, {
                    'sitekey': this.options.recaptchaPublicKey,
                    'callback': this.options.callbacks.onVerify + this.options.widgetId,
                    'expired-callback': this.options.callbacks.onExpire + this.options.widgetId,
                    'theme': this.options.theme
                });
                $(this.element).parents('form').trigger('msRecaptchaReady');
            }
        },

        onVerify: function (response) {
            $(this.options.inputSelectorPrefix + this.options.widgetId).val(response);
            $('body').trigger(this.options.callbacks.onReset, [this.widget]);
        },

        onExpire: function () {
            this.reset();
        },

        onReset: function (widgetId) {
            if (this.widget !== widgetId) {
                this.reset();
            }
        },

        reset: function () {
            if (this.widget !== null) {
                grecaptcha.reset(this.widget);
                $(this.options.inputSelectorPrefix + this.options.widgetId).val('');
            }
        }
    });

    return $.mageside.fieldRecaptcha;
});
