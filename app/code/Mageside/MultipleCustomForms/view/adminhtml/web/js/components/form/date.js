/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
define([
    'Magento_Ui/js/form/element/date',
    'Mageside_MultipleCustomForms/js/components/form/strategy'
], function (Element, strategy) {
    'use strict';

    return Element.extend(strategy);
});