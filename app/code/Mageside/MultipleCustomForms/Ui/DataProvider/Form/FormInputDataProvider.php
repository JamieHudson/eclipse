<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Ui\DataProvider\Form;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Mageside\MultipleCustomForms\Model\CustomForm\Field\Settings;

class FormInputDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var PoolInterface
     */
    private $_pool;

    /**
     * @var null|\Mageside\MultipleCustomForms\Model\CustomForm\Field
     */
    private $_field = null;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * @var \Mageside\MultipleCustomForms\Model\CustomForm\FieldFactory
     */
    private $_fieldFactory;

    /**
     * @var \Mageside\MultipleCustomForms\Helper\Config
     */
    protected $_configHelper;

    /**
     * FormInputDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magento\Framework\App\RequestInterface $request
     * @param PoolInterface $pool
     * @param \Mageside\MultipleCustomForms\Model\CustomForm\FieldFactory $fieldFactory
     * @param \Mageside\MultipleCustomForms\Helper\Config $configHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Framework\App\RequestInterface $request,
        PoolInterface $pool,
        \Mageside\MultipleCustomForms\Model\CustomForm\FieldFactory $fieldFactory,
        \Mageside\MultipleCustomForms\Helper\Config $configHelper,
        array $meta = [],
        array $data = []
    ) {
        $this->_request = $request;
        $this->_pool = $pool;
        $this->_fieldFactory = $fieldFactory;
        $this->_configHelper = $configHelper;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getMeta()
    {
        $meta = parent::getMeta();
        /** @var ModifierInterface $modifier */
        foreach ($this->_pool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        $options = [];
        $dateFormat = $this->_configHelper->getDateFormat();
        $options['inputDateFormat'] = $dateFormat;
        $options['outputDateFormat'] = $dateFormat;
        $options['showsDate'] = true;
        $options['showsTime'] = true;

        $field = $this->getField();
        if ($field->getId()) {
            if ($field->getData(Settings::OPTION_TIME_FORMAT) == '1') {
                $options['timeFormat'] = 'HH:mm';
            }
        }

        if (!empty($options)) {
            $meta['fields']['children']['default_value_date']['arguments']['data']['config']['options'] = $options;
        }

        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $formId = $this->_request->getParam('form_id');

        if ($formId) {
            $this->data['config']['data']['field']['form_id'] = (int)$formId;
        }

        $field = $this->getField();
        if ($field->getId()) {
            $this->data['config']['data']['field'] = $field->toArray();

            if (!empty($field->getValidation())) {
                $this->data['config']['data']['field']['validation'] = explode(',', $field->getValidation());
            }

            if ($field->getOptionsSource() == 'custom') {
                $options = $field->getOptions(false, false, false);
                if (!empty($options)) {
                    $this->data['config']['data']['field']['options'] = $options;
                }
            }

            $type = $field->getType();
            if ($defaultValue = $field->getData('default_value')) {
                if ($type == 'select' || $type == 'radio') {
                    $this->data['config']['data']['field']['default_value_select'] = $defaultValue;
                } elseif ($type == 'checkbox' || $type == 'multiselect') {
                    $this->data['config']['data']['field']['default_value_multiselect'] = explode(',', $defaultValue);
                } elseif ($type == 'date') {
                    $this->data['config']['data']['field']['default_value_date'] = $defaultValue;
                }
            }
        }

        /** @var ModifierInterface $modifier */
        foreach ($this->_pool->getModifiersInstances() as $modifier) {
            $this->data = $modifier->modifyData($this->data);
        }

        return $this->data;
    }

    protected function getField()
    {
        if ($this->_field === null) {
            $field = $this->_fieldFactory->create();
            if ($fieldId = $this->_request->getParam('field_id')) {
                $field->load((int)$fieldId, 'id');
            }
            $this->_field = $field;
        }

        return $this->_field;
    }

    /**
     * @inheritdoc
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
    }
}
