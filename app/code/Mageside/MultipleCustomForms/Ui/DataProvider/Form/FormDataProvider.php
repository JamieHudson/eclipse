<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Ui\DataProvider\Form;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Collection;
use Mageside\MultipleCustomForms\Helper\Config;
use Magento\Framework\ObjectManagerInterface;

class FormDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var PoolInterface
     */
    protected $_pool;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Mageside\MultipleCustomForms\Helper\Config
     */
    protected $_configHelper;

    /**
     * FormDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param PoolInterface $pool
     * @param Collection $collection
     * @param Config $configHelper
     * @param ObjectManagerInterface $objectManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        PoolInterface $pool,
        Collection $collection,
        Config $configHelper,
        ObjectManagerInterface $objectManager,
        array $meta = [],
        array $data = []
    ) {
        $this->_pool = $pool;
        $this->collection = $collection;
        $this->_configHelper = $configHelper;
        $this->_objectManager = $objectManager;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getMeta()
    {
        $meta = parent::getMeta();
        /** @var ModifierInterface $modifier */
        foreach ($this->_pool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        if (!$this->isReCaptchaInstalled()) {
            $meta['custom_form']['children']['reCaptcha']['arguments']['data']['config']['disabled'] = true;
            $meta['custom_form']['children']['reCaptcha']['arguments']['data']['config']['tooltip'] = [
                'link' => 'https://mageside.com/media/attachment/file/m/u/multipleforms_installation.pdf',
                'description' => __(
                    'If you want to use this feature, you will have to install and configure Google reCaptcha library.'
                )
            ];
        }

        return $meta;
    }

    /**
     * @return bool
     */
    private function isReCaptchaInstalled()
    {
        try {
            if (!$secret = $this->_configHelper->getConfigModule('recaptcha_secret_key')) {
                return false;
            }
            if (!$this->_configHelper->getConfigModule('recaptcha_public_key')) {
                return false;
            }

            $reCaptcha = $this->_objectManager->create(
                \ReCaptcha\ReCaptcha::class,
                ['secret' => $secret]
            );

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $items = $this->collection->getItems();
        /** @var $form \Mageside\MultipleCustomForms\Model\CustomForm */
        foreach ($items as $form) {
            $this->data[$form->getId()]['form'] = $form->getData();
        }

        /** @var ModifierInterface $modifier */
        foreach ($this->_pool->getModifiersInstances() as $modifier) {
            $this->data = $modifier->modifyData($this->data);
        }

        return $this->data;
    }

    /**
     * @inheritdoc
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        parent::addFilter($filter);
    }
}
