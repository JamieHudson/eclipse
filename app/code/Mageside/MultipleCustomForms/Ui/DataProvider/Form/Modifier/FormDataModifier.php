<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Ui\DataProvider\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;

class FormDataModifier implements ModifierInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * @var \Mageside\MultipleCustomForms\Model\CustomForm\FieldFactory
     */
    private $collection;

    /**
     * FormDataModifier constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Collection $fieldCollection
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Collection $fieldCollection
    ) {
        $this->_request = $request;
        $this->collection = $fieldCollection;
    }

    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $form_id = $this->_request->getParam('id');
        $inputsCollection = $this->collection
            ->addFieldToFilter('form_id', ['eq' => $form_id])
            ->addOrder('position', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);

        $inputsData = [];
        if (!empty($inputsCollection)) {
            foreach ($inputsCollection->getItems() as $input) {
                $inputsData[] = [
                    'id'            => $input->getData('id'),
                    'title'         => $input->getData('title'),
                    'type'          => $input->getData('type'),
                    'required'      => $input->getData('required') ? __('yes') : '',
                    'position'      => $input->getData('position'),
                ];
            }
        }

        $data[$this->_request->getParam('id')]['form']['inputs'] = $inputsData;

        return $data;
    }
}
