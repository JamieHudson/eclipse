<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model;

use Mageside\MultipleCustomForms\Model\CustomForm\Field;
use Magento\Framework\App\Filesystem\DirectoryList;

class EmailSender
{

    const XML_PATH_EMAIL_TEMPLATE = 'mageside_customform/general/email_template';

    const XML_PATH_EMAIL_SENDER = 'contact/email/sender_email_identity';

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * EmailSender constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param UploadTransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        UploadTransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->_request = $request;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_filesystem = $filesystem;
        $this->_escaper = $escaper;
    }

    /**
     * @param $form
     * @param $data
     * @return $this
     * @throws \Magento\Framework\Exception\MailException
     */
    public function send($form, $data)
    {
        $emails = array_map(
            function($value) {
                return trim($value);
            },
            explode(',', $form->getData('recipient_emails'))
        );

        if (empty($emails)) {
            return $this;
        }

        $this->_inlineTranslation->suspend();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $this->_transportBuilder
            ->setTemplateIdentifier(
                $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope)
            )
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars($this->prepareEmailVars($form, $data))
            ->setFrom(
                $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope)
            )
            ->addTo($emails);

        $this->_processAttachments($form, $data);

        $this->_transportBuilder->getTransport()->sendMessage();
        $this->_inlineTranslation->resume();

        return $this;
    }

    /**
     * @param $form
     * @param $data
     * @return array
     */
    protected function prepareEmailVars($form, $data)
    {
        $content = '';
        /** @var \Mageside\MultipleCustomForms\Model\CustomForm $form */
        $formFieldsData = $form->getFieldCollection();
        foreach ($formFieldsData as $field) {
            /** @var \Mageside\MultipleCustomForms\Model\CustomForm\Field $field */
            $value = $field->getSubmittedValue($data);
            $value = !empty($value) ? $field->getFieldOutput($value) : '&nbsp;';
            $content .= "<b>" .
                $this->_escaper->escapeHtml($field->getTitle()) .
                "</b>:" .
                "\t" .
                $this->_escaper->escapeHtml($value) .
                "<br>";
        }

        $vars = [
            'form_name' => $form->getName(),
            'content'   => $content,
            'subject'   => $form->getSubjectEmail()
        ];

        return $vars;
    }

    /**
     * @param $form
     * @param $data
     */
    protected function _processAttachments($form, $data)
    {
        $directory = $form->getSubmissionType() == 'both'
            ? FileUploader::SUBMISSION_DIRECTORY
            : FileUploader::TEMP_DIRECTORY;

        $mediaDirectory = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);

        foreach ($form->getFieldCollection() as $field) {
            /** @var \Mageside\MultipleCustomForms\Model\CustomForm\Field $field */
            if ($field->getType() == 'file') {
                $files = explode(
                    ",",
                    $data[Field::FIELD_PREFIX . $field->getId()]
                );
                foreach ($files as $file) {
                    if ($mediaDirectory->isFile($directory . '/' . $file)) {
                        $path = $mediaDirectory->getAbsolutePath($directory . '/' . $file);
                        $this->_transportBuilder->createAttachment($path, $file);
                    }
                }
            }
        }
    }
}
