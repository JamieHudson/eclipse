<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model\CustomForm\Field;

class Options extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Options');
    }
}
