<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model;

class UploadTransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    /**
     * @param $path
     * @param $name
     * @return $this
     */
    public function createAttachment($path, $name)
    {
        $this->message->createAttachment(
            file_get_contents($path),
            \Zend_Mime::TYPE_OCTETSTREAM,
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $name
        );

        return $this;
    }
}
