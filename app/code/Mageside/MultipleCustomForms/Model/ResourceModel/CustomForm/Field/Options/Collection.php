<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Options;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Mageside\MultipleCustomForms\Model\CustomForm\Field\Options',
            'Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Options'
        );
    }
}
