<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field;

class Options extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('ms_cf_field_options', 'id');
    }

    /**
     * Save options
     *
     * @param $data
     * @return $this
     */
    public function saveOptions($data)
    {
        $this->getConnection()->insertMultiple($this->getMainTable(), $data);

        return $this;
    }

    /**
     * Get options by ids
     *
     * @param array $ids
     * @return array
     */
    public function getOptionsByIds(array $ids)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from(['main_table' => $this->getMainTable()], ['label'])
            ->where("id in (?)", $ids);

        $result = $connection->fetchAll($select);

        return $result;
    }

    /**
     * Update options
     *
     * @param $options
     * @return $this
     */
    public function updateOptions($options)
    {
        $connection = $this->getConnection();

        foreach ($options as $option) {
            $connection->update(
                $this->getMainTable(),
                ['label'  => $option['label']],
                ['id = ?' => (int)$option['id']]
            );
        }

        return $this;
    }

    /**
     * Delete options by ids
     *
     * @param $ids
     * @return $this
     */
    public function deleteOptions($ids)
    {
        $connection = $this->getConnection();
        $connection->delete($this->getMainTable(), ['id IN (?)' => $ids]);

        return $this;
    }
}
