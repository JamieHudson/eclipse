<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Mageside\MultipleCustomForms\Model\CustomForm\Field',
            'Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field'
        );
    }

    protected function _afterLoad()
    {
        $savedSettings = $this->loadAdditionalSettings();
        if (!empty($savedSettings)) {
            foreach ($this->getItems() as $item) {
                if (array_key_exists($item->getId(), $savedSettings)) {
                    $item->addData($savedSettings[$item->getId()]);
                }
            }
        }

        parent::_afterLoad();
    }

    /**
     * @return array
     */
    private function loadAdditionalSettings()
    {
        $settings = [];
        $ids = $this->getAllIds();
        if (!empty($ids)) {
            $connection = $this->getConnection();
            $select = $connection->select()
                ->from($this->getTable('ms_cf_field_settings'))
                ->where('field_id in (?)', $ids);

            $savedSettings = $connection->fetchAll($select);

            if (!empty($savedSettings)) {
                foreach ($savedSettings as $setting) {
                    $settings[$setting['field_id']][$setting['key']] = $setting['value'];
                }
            }
        }

        return $settings;
    }

    /**
     * Add order collection by position
     *
     * @return $this
     */
    public function addOrderByPosition()
    {
        $this->addOrder('position', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);

        return $this;
    }

    /**
     * Add options data to fields collection
     *
     * @return $this
     */
    public function addOptionsData()
    {
        foreach ($this->getItems() as $field) {
            /** @var \Mageside\MultipleCustomForms\Model\CustomForm\Field $field */
            $field->setOptions($field->getOptions(false));
        }

        return $this;
    }

    public function addSubmissionDataToCollection($submissionId)
    {
        foreach ($this->getItems() as $field) {
            /** @var \Mageside\MultipleCustomForms\Model\CustomForm\Field $field */
            $field->setSubmissionValue($field->getSubmissionData($submissionId));
        }

        return $this;
    }
}
