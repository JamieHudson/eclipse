<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Model\ResourceModel;

class CustomForm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field
     */
    protected $_fieldResourceModel;

    /**
     * Class constructor
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field $fieldResourceModel,
        $connectionName = null
    ) {
        $this->_fieldResourceModel = $fieldResourceModel;
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('ms_custom_form', 'id');
    }

    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $formCode = preg_replace(
            '/[^a-z0-9]+/',
            '_',
            strtolower(str_replace(' ', '_', trim($object->getCode())))
        );
        $object->setCode($formCode);

        return parent::_beforeSave($object);
    }

    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->updateRelatedFieldsList($object);

        return parent::_afterSave($object);
    }

    /**
     * @param $object
     * @return $this
     */
    public function updateRelatedFieldsList($object)
    {
        if (!$object->getInputs()) {
            return $this;
        }

        $deleteIds = [];
        foreach ($object->getInputs() as $input) {
            if (isset($input['delete']) && $input['delete'] === 'true') {
                $deleteIds[] = $input['id'];
                continue;
            }
            $this->_fieldResourceModel->updateFieldPosition($input);
        }

        if (!empty($deleteIds)) {
            $this->_fieldResourceModel->deleteFieldsById($deleteIds);
        }

        return $this;
    }
}
