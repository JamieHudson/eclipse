<?php
/**
 * Copyright © 2018 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\MultipleCustomForms\Block\Widget\CustomForm;

class Fields extends \Mageside\MultipleCustomForms\Block\Widget\AbstractBlock
{
    /**
     * @var \Mageside\MultipleCustomForms\Model\CustomForm
     */
    private $_form;

    /**
     * @var null|array|\Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Collection
     */
    private $_fields = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @param $form
     * @return $this
     */
    public function setForm($form)
    {
        $this->_form = $form;

        return $this;
    }

    /**
     * Get fields collection
     *
     * @return array|\Mageside\MultipleCustomForms\Model\ResourceModel\CustomForm\Field\Collection|null
     */
    public function getFields()
    {
        if ($this->_fields === null) {
            $fieldCollection = $this->_form->getFieldCollection();
            $this->_fields = $fieldCollection;
        }

        return $this->_fields;
    }

    /**
     * Get fields html
     *
     * @param \Mageside\MultipleCustomForms\Model\CustomForm\Field $field
     * @return mixed
     */
    public function getFieldHtml(\Mageside\MultipleCustomForms\Model\CustomForm\Field $field)
    {
        $type = $field->getType();
        $renderer = $this->getChildBlock($type . '_' . $field->getOptionsSource());
        $type = $renderer ? $type . '_' . $field->getOptionsSource() : $type;

        if (!$renderer) {
            $renderer = $this->getChildBlock($type);
        }

        if ($renderer) {
            $renderer->setField($field);

            return $this->getChildHtml($type, false);
        }

        return '';
    }
}
