<?php 
namespace Amanweb\Cartcomment\Controller\Ajax;

class Ajax extends \Magento\Framework\App\Action\Action {

	protected $_request;
	protected $_resultJsonFactory;
	protected $_formKeyValidator;
	protected $_logger;
	protected $_pageFactory;
	protected $_cart;
	protected $_checkoutSession;
	
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
		\Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
		\Magento\Checkout\Model\Cart $cart,
		\Magento\Checkout\Model\Session $checkoutSession,
		\Psr\Log\LoggerInterface $logger
    ) {
		$this->_request = $request;
		$this->_formKeyValidator = $formKeyValidator;
        $this->_resultJsonFactory = $resultJsonFactory;
		$this->_cart = $cart;
		$this->_checkoutSession = $checkoutSession;
		$this->_logger = $logger;
		parent::__construct($context);
    }
	
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
		if (!$this->_formKeyValidator->validate($this->_request)) {
            $resultJson->setStatusHeader(
				\Zend\Http\Response::STATUS_CODE_400,
				\Zend\Http\AbstractMessage::VERSION_11,
				'Bad Request'
			);
			$response = [
				'error'	=> true,
				'message' => __('Form validation failed refresh page')
			];
			return $resultJson->setData($response);
        }
		$cart = $this->_request->getParam('cart');
		$quote = $this->_checkoutSession->getQuote();
		$this->_cart->updateItems($cart);
		$this->_cart->saveQuote();
		$this->_cart->save();
    }
}