<?php

namespace Amanweb\Cartcomment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		$installer = $setup;
        $installer->startSetup();

        $quoteitem = $installer->getTable('quote_item');
		$salesitem = $installer->getTable('sales_order_item');
		
        $columns = [
            'item_comment' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => '64k',
                'nullable' => false,
                'comment' => 'Item Comment',
            ],
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($quoteitem,$name,$definition);
			$connection->addColumn($salesitem,$name,$definition);
        }		
		
        $installer->endSetup();
    }
}
