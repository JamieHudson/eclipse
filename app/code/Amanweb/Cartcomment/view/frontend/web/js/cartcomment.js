require([
    'jquery',
	'mage/url',
	'domReady!'
], function($,url){
	$('.cartcomment').on('focus', function(event){
		$(this).data('current-val', $(this).val());
	});
	$('.cartcomment').on('focusout', function(event){
		if($(this).data('current-val') != $(this).val()){
			var formdata = $('#form-validate').serialize();
			var ajaxurl = url.build('cartcomment/ajax/ajax');
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				data: formdata,
				success: function (data) {
				},
				error: function(jqXHR, textStatus, errorThrown){
					if (errorThrown) {
						
					}
				},
				complete: function (data) {
					
				}
			});
		}
	})
});