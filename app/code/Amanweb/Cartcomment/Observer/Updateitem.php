<?php
namespace Amanweb\Cartcomment\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;

class Updateitem implements ObserverInterface
{
	protected $_logger;
	protected $_request;
	public function __construct(
		LoggerInterface $logger,
		RequestInterface $request
	){
		$this->_logger = $logger;
		$this->_request = $request;
	}
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$cart = $observer->getEvent()->getCart();
		$info = $observer->getEvent()->getInfo();
		$params = $this->_request->getParams();
		//$this->_logger->debug('satart log',['data'=>print_r($params['cart'],true)]);
		foreach($info->getData() as $itemId => $itemInfo){
			$item = $cart->getQuote()->getItemById($itemId);
			if(isset($params['cart'][$itemId]['item_comment'])){
				$item->setItemComment($params['cart'][$itemId]['item_comment']);
			}
		}
	}
}