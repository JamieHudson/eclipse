<?php
namespace Amanweb\Cartcomment\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	public function getConfig($config_path)
    {
		return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
	public function getModuleEnable()
    {
		return $this->getConfig('cartcomment/fields_masks/enable');
	}
	public function getEmailCommentEnable()
    {
		return $this->getConfig('cartcomment/fields_masks/enable_email_comment');
	}
}