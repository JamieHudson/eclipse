<?php
namespace Amanweb\Cartcomment\Plugin;

class QuoteItemToOrderItemPlugin
{
    public function aroundConvert(\Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = [])
    {
        $orderItem = $proceed($item,$additional);
		if($item->getItemComment()){
			$orderItem->setItemComment($item->getItemComment());
		}
        return $orderItem;
    }
}