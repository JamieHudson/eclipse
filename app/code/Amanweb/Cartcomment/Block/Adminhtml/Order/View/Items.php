<?php
namespace Amanweb\Cartcomment\Block\Adminhtml\Order\View;

class Items extends \Magento\Sales\Block\Adminhtml\Order\View\Items
{
	protected $_helper;
	
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
		\Amanweb\Cartcomment\Helper\Data $helper,
        array $data = []
    ) {
		$this->_helper = $helper;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $data);
    }
	
	
    public function getColumns()
    {
		$newcolumn = array();
		if(array_key_exists('columns', $this->_data)){
			foreach($this->_data['columns'] as $key=>$value){
				if($key=='product'){
					$newcolumn[$key] = $value;
					if($this->_helper->getModuleEnable()){
						$newcolumn['item-comment'] = 'Item Reference';
					}
				}else{
					$newcolumn[$key] = $value;
				}
			}
		}else{
			$newcolumn = [];
		}
        return $newcolumn;
    }
}
