<?php

namespace Amanweb\Cartcomment\Block\Adminhtml\Order\View\Items\Renderer;

class DefaultRenderer extends \Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer
{
	protected $_helper;
	
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\GiftMessage\Helper\Message $messageHelper,
        \Magento\Checkout\Helper\Data $checkoutHelper,
		\Amanweb\Cartcomment\Helper\Data $helper,
        array $data = []
    ) {
		$this->_helper = $helper;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $messageHelper, $checkoutHelper, $data);
    }
	
    public function getColumnHtml(\Magento\Framework\DataObject $item, $column, $field = null)
    {
        $html = '';
        switch ($column) {
            case 'product':
                if ($this->canDisplayContainer()) {
                    $html .= '<div id="' . $this->getHtmlId() . '">';
                }
                $html .= $this->getColumnHtml($item, 'name');
                if ($this->canDisplayContainer()) {
                    $html .= '</div>';
                }
                break;
			case 'item-comment':
                $html = $item->getItemComment();
                break;
            case 'status':
                $html = $item->getStatus();
                break;
            case 'price-original':
                $html = $this->displayPriceAttribute('original_price');
                break;
            case 'tax-amount':
                $html = $this->displayPriceAttribute('tax_amount');
                break;
            case 'tax-percent':
                $html = $this->displayTaxPercent($item);
                break;
            case 'discont':
                $html = $this->displayPriceAttribute('discount_amount');
                break;
            default:
                $html = parent::getColumnHtml($item, $column, $field);
        }
        return $html;
    }
	public function getColumns()
    {
        //$columns = array_key_exists('columns', $this->_data) ? $this->_data['columns'] : [];
		$newcolumn = array();
		if(array_key_exists('columns', $this->_data)){
			foreach($this->_data['columns'] as $key=>$value){
				if($key=='product'){
					$newcolumn[$key] = $value;
					if($this->_helper->getModuleEnable()){
						$newcolumn['item-comment'] = 'Item Reference';
					}
				}else{
					$newcolumn[$key] = $value;
				}
			}
		}else{
			$newcolumn = [];
		}
        return $newcolumn;
    }
}
