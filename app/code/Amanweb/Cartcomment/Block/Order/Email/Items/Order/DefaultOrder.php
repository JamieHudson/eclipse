<?php
namespace Amanweb\Cartcomment\Block\Order\Email\Items\Order;

class DefaultOrder extends \Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder
{
    public function setTemplate($template) {
        return parent::setTemplate('Amanweb_Cartcomment::email/items/order/default.phtml');
    }
}