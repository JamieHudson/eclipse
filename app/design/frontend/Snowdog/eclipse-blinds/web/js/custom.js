require([ 'jquery' ], function($){ 

    $(".link-blocks .shop-looks__category").click(function() {
        window.location = $(this).find("a").attr("href"); 
        return false;
      });

    $('.filter-options .items').has('li:eq(4)').addClass('boxed');

    function initialise(){
        $('.filter-options .items').has('li:eq(4)').addClass('boxed');
    };

    $(document).ajaxComplete(function () {
        initialise();
    });

	$('.utilities-menu__nav').css("display", "none");

	$('.utilities-menu__trigger').click(function(){
		$('.utilities-menu__nav').toggle();
	});

	$(document).click(function(e){
		if ( !$(event.target).hasClass('utilities-menu__trigger')){
			$('.utilities-menu__nav').hide();
		}
    });

    $(".sidebar-main").prepend("<button class='button button--filter'>Filter products</button>");
    $("#narrow-by-list").addClass("filter-mobile");

    $('.button--filter').click(function(){
    $('#narrow-by-list').toggle();
    });

    $('.form-sample').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
    });

    jQuery(".read-more").click(function(e){
		jQuery('html, body').animate({
			scrollTop: jQuery(".product.info.detailed").offset().top
		}, 500);
    });
    
    $(".header.links li").click(function() {
        window.location = $(this).find("a").attr("href"); 
        return false;
    });

});